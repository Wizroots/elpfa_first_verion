package wizroots.elpfa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EndTestActivity extends AppCompatActivity {
    TextView right_txt,percentage_txt;
    int right;

    int compl_completed,scheduled_completed,progress_completed,all_completed;
    int compl_startedd,scheduled_startedd,progress_startedd,all_startedd;
    String compl_duration,compl_pass_percent,compl_test_mandatory,compl_no_of_que;
    String scheduled_duration,scheduled_pass_percent,scheduled_test_mandatory,scheduled_no_of_que;
    String progress_duration,progress_pass_percent,progress_test_mandatory,progress_no_of_que;
    String all_duration,all_pass_percent,all_test_mandatory,all_no_of_que;
    int compl_enrollment_key,compl_status,scheduled_enrollment_key,scheduled_status,progress_enrollment_key,progress_status,all_enrollment_key,all_status;
    String compl_path,compl_name,compl_desc,compl_startdate,compl_course_id,compl_track_content,compl_users_to_courseid,compl_completion_date;
    String scheduled_path,scheduled_name,scheduled_desc,scheduled_startdate,scheduled_course_id,scheduled_track_content,scheduled_users_to_courseid,scheduled_completion_date;
    String progress_path,progress_name,progress_desc,progress_startdate,progress_course_id,progress_track_content,progress_users_to_courseid,progress_completion_date;
    String all_path,all_name,all_desc,all_startdate,all_course_id,all_track_content,all_users_to_courseid,all_completion_date;
    String number_assigned,number_started,number_not_started,number_completed;
    String username,user_id;
    JSONObject jObject;
    float percentage;
    Button btn;
    int pass_percentage;
    String course_id;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    int total_number_of_questions;
    String course_name;
    TextView selected_array;
    ArrayList<String> selected_answers= new ArrayList<>();
    private Toolbar mToolbar;
    CoordinatorLayout coordinatorLayout;
    TextView textt;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_test);
        username=getIntent().getStringExtra("username");
        user_id=getIntent().getStringExtra("user_id");
        right_txt=(TextView)findViewById(R.id.right_txt);
        percentage_txt=(TextView)findViewById(R.id.percentage_txt);
        btn=(Button)findViewById(R.id.btn);
        selected_array=(TextView)findViewById(R.id.selected_array);
        Bundle b;
        b=getIntent().getExtras();
        right=b.getInt("right");
        total_number_of_questions=b.getInt("total_number_of_questions");
        selected_answers=getIntent().getStringArrayListExtra("selected_answers");
        course_id=getIntent().getStringExtra("course_id");
        course_name=getIntent().getStringExtra("course_name");
        mToolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("" + course_name);
        mToolbar.setNavigationIcon(R.drawable.back_white);
        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinatorLayout);
        setSupportActionBar(mToolbar);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected == true) {
                    serverSync();

                }
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("username", sharedpreferences.getString("Username", null));
                i.putExtra("user_id", sharedpreferences.getString("User_id", null));
                EndTestActivity.this.finish();
                startActivity(i);
            }
        });

        selected_array.setText(""+selected_answers);
        textt=(TextView)findViewById(R.id.textt);
        right_txt.setText(Integer.toString(right));
        float perc=(float)right/total_number_of_questions;
        percentage=perc*100;

        int y = (int) percentage;
        percentage_txt.setText(y + "%");
        textt.setText(right + " out of " + total_number_of_questions + " are correct");



        try {

            String url = Config.LoadPassPercentage;
            ExecuteUrl task = new ExecuteUrl();
            task.execute(url,course_id);
            pass_percentage = Integer.parseInt(task.get());

        } catch (Exception ex) {
        }


        if (percentage>=pass_percentage)
        {
            try {
                Config url_login = new Config();
                String url = url_login.update_score;
                ExecuteUrl task = new ExecuteUrl();
                task.execute(url, user_id,course_id, String.valueOf(percentage));

            } catch (Exception ex) {
            }
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "You have successfully completed \nthe course", Snackbar.LENGTH_INDEFINITE);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.rgb(1,22,39));
            final TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextSize(18);
            tv.setTextColor(Color.rgb(255, 255, 255));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            else
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
            snackbar.show();
           btn.setVisibility(View.GONE);
        }
        else
        {
            try {

                String url = Config.CheckTestAlreadyDone;
                ExecuteUrl task = new ExecuteUrl();
                task.execute(url,user_id,course_id);

            } catch (Exception ex) {
            }
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "You need minimum of " + pass_percentage + "% \n to complete the course", Snackbar.LENGTH_INDEFINITE);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(Color.rgb(236, 42, 42));
            final TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextSize(18);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            else
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
                    snackbar.show();
            btn.setText("Try again");

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getBaseContext(),TestInstructions.class);
                    i.putExtra("username", username);
                    i.putExtra("course_id", course_id);
                    i.putExtra("user_id", user_id);
                    i.putExtra("course_name",course_name);
                    EndTestActivity.this.finish();
                    startActivity(i);
                }
            });

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK )return true;
        return super.onKeyDown(keyCode, event);
    }


    public void finish( View v ) {
        finish();
    }

    public void serverSync()
    {
        try {

            Config url_login = new Config();

                    /* using volley in login - combine everything */
            String tag_string_req = "req_state";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url_login.FetchAllDetailsRefresh, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {

                        DatabaseHandler db1 = new DatabaseHandler(EndTestActivity.this);
                        jObject = new JSONObject(response);
                        db1.deleteCourseReportsTable();
                        //fetch number of assigned courses
                        JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                        JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                        number_assigned = Jasonobject.getString("count(*)");
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("assigned", number_assigned);
                        editor.commit();


//

                        //fetch number of started courses
                        JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                        JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                        number_started = JasonobjectStarted.getString("count(*)");

                        editor.putString("started", number_started);
                        editor.commit();

                        //fetch number of not-started courses
                        JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");
                        JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                        number_not_started = JasonobjectNotStarted.getString("count(*)");

                        editor.putString("not_started", number_not_started);
                        editor.commit();



                        //fetch number of completed courses
                        JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                        JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                        number_completed = JasonobjectCompleted.getString("count(*)");

                        editor.putString("completed", number_completed);
                        editor.commit();


                                            /* add to course reports table */
                        db1.addCourseReports(new Elpfa("report",number_assigned, number_started, number_not_started,number_completed));


                        //fetch all  courses

                        editor.putString("json_String_scheduled", response);
                        editor.putString("json_String_progress", response);
                        editor.putString("json_String_completed", response);
                        editor.putString("json_String_all", response);
                        editor.commit();

                        //get all completed courses
                        db1.deleteCompletedCoursesTable();
                        JSONArray jsonArrayCompletedCourses = jObject.getJSONArray("completed_courses");

                        final int completed_length = jsonArrayCompletedCourses.length();

                        for (int i = 0; i < completed_length; i++) {
                            JSONObject JasonobjectCompl = jsonArrayCompletedCourses.getJSONObject(i);
                            compl_path = Config.BASE_CONTENT_URL + JasonobjectCompl.getString("path");

                            compl_name = JasonobjectCompl.getString("name");
                            compl_desc = JasonobjectCompl.getString("description");
                            compl_startdate = JasonobjectCompl.getString("assign_date");
                            compl_course_id=JasonobjectCompl.getString("course_id");
                            compl_enrollment_key=JasonobjectCompl.getInt("enrollment_key");
                            compl_status=JasonobjectCompl.getInt("status");
                            compl_track_content= String.valueOf(JasonobjectCompl.getInt("track_course"));
                            user_id=JasonobjectCompl.getString("user_id");
                            compl_users_to_courseid=JasonobjectCompl.getString("id");
                            compl_duration=JasonobjectCompl.getString("course_duration");
                            compl_no_of_que=JasonobjectCompl.getString("no_of_que");
                            compl_pass_percent=JasonobjectCompl.getString("pass_percent");
                            compl_test_mandatory=JasonobjectCompl.getString("test_mandatory");
                            compl_startedd=JasonobjectCompl.getInt("started");
                            compl_completed=JasonobjectCompl.getInt("completed");
                            compl_completion_date=JasonobjectCompl.getString("completion_date");
                            //insert completed courses into sqlite

                            db1.addCompletedCourses(new Elpfa("completed",compl_course_id, compl_users_to_courseid, compl_name, compl_desc, compl_duration, compl_enrollment_key, compl_status,compl_path, compl_startdate, compl_track_content, compl_no_of_que, compl_pass_percent, compl_test_mandatory,compl_startedd,compl_completed,compl_completion_date));





                        }




                        // end fetch all  completed courses



                        /******** fetch all scheduled courses ***********/


                        db1.deleteScheduledCoursesTable();
                        JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                        final int scheduled_length = jsonArrayScheduledCourses.length();

                        for (int i = 0; i < scheduled_length; i++) {
                            JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                            scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");


                            scheduled_name = JasonobjectScheduled.getString("name");
                            scheduled_desc = JasonobjectScheduled.getString("description");
                            scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                            scheduled_course_id=JasonobjectScheduled.getString("course_id");
                            scheduled_enrollment_key=JasonobjectScheduled.getInt("enrollment_key");
                            scheduled_status=JasonobjectScheduled.getInt("status");
                            scheduled_track_content= String.valueOf(JasonobjectScheduled.getInt("track_course"));
                            user_id=JasonobjectScheduled.getString("user_id");
                            scheduled_users_to_courseid=JasonobjectScheduled.getString("id");
                            scheduled_duration=JasonobjectScheduled.getString("course_duration");
                            scheduled_no_of_que=JasonobjectScheduled.getString("no_of_que");
                            scheduled_pass_percent=JasonobjectScheduled.getString("pass_percent");
                            scheduled_test_mandatory=JasonobjectScheduled.getString("test_mandatory");
                            scheduled_startedd=JasonobjectScheduled.getInt("started");
                            scheduled_completed=JasonobjectScheduled.getInt("completed");
                            scheduled_completion_date=JasonobjectScheduled.getString("completion_date");

                            //insert completed courses into sqlite

                            db1.addScheduledCourses(new Elpfa("scheduled",scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status,scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory,scheduled_startedd,scheduled_completed,scheduled_completion_date));





                        }



                        /**********  end fetch all  completed courses ***********/





                        // end fetch all in progress courses

                        /******** fetch all in progress courses ***********/


                        db1.deleteInProgressCoursesTable();
                        if (!jObject.isNull("progress_courses")) {
                            JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                            final int progress_length = jsonArrayInProgressCourses.length();

                            for (int i = 0; i < progress_length; i++) {
                                JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                                progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");
                                progress_name = JasonobjectProgress.getString("name");
                                progress_desc = JasonobjectProgress.getString("description");
                                progress_startdate = JasonobjectProgress.getString("assign_date");
                                progress_course_id=JasonobjectProgress.getString("course_id");
                                progress_enrollment_key=JasonobjectProgress.getInt("enrollment_key");
                                progress_status=JasonobjectProgress.getInt("status");
                                progress_track_content= String.valueOf(JasonobjectProgress.getInt("track_course"));
                                user_id=JasonobjectProgress.getString("user_id");
                                progress_users_to_courseid=JasonobjectProgress.getString("id");
                                progress_duration=JasonobjectProgress.getString("course_duration");
                                progress_no_of_que=JasonobjectProgress.getString("no_of_que");
                                progress_pass_percent=JasonobjectProgress.getString("pass_percent");
                                progress_test_mandatory=JasonobjectProgress.getString("test_mandatory");
                                progress_startedd=JasonobjectProgress.getInt("started");
                                progress_completed=JasonobjectProgress.getInt("completed");
                                progress_completion_date=JasonobjectProgress.getString("completion_date");
                                //insert in progress courses into sqlite

                                db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status,progress_path, progress_startdate,progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory,progress_startedd,progress_completed,progress_completion_date));





                            }



                        }

                        /**********  end fetch all  completed courses ***********/

                        /******** fetch all courses ***********/



                        db1.deleteAllCoursesTable();
                        if (!jObject.isNull("all_courses")) {
                            JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                            final int progress_length = jsonArrayAllCourses.length();

                            for (int i = 0; i < progress_length; i++) {
                                JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                                all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                                all_name = JasonobjectAll.getString("name");
                                all_desc = JasonobjectAll.getString("description");
                                all_startdate = JasonobjectAll.getString("assign_date");
                                all_course_id=JasonobjectAll.getString("course_id");
                                all_enrollment_key=JasonobjectAll.getInt("enrollment_key");
                                all_status=JasonobjectAll.getInt("status");
                                all_track_content= String.valueOf(JasonobjectAll.getInt("track_course"));
                                user_id=JasonobjectAll.getString("user_id");
                                all_users_to_courseid=JasonobjectAll.getString("id");
                                all_duration=JasonobjectAll.getString("course_duration");
                                all_no_of_que=JasonobjectAll.getString("no_of_que");
                                all_pass_percent=JasonobjectAll.getString("pass_percent");
                                all_test_mandatory=JasonobjectAll.getString("test_mandatory");
                                all_startedd=JasonobjectAll.getInt("started");

                                all_completed=JasonobjectAll.getInt("completed");
                                all_completion_date=JasonobjectAll.getString("completion_date");
                                //insert all courses into sqlite

                                db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status,all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory,all_startedd,all_completed,all_completion_date));





                            }


                        }

                        /**********  end fetch all  completed courses ***********/








                    }
                    catch (Exception e)
                    {

                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    params.put("value1", sharedpreferences.getString("Username",null));
                    return params;
                }

            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);







    /*End of Mesages call*/

        } catch (Exception ex) {
        }
    }
}