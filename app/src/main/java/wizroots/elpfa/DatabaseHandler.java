package wizroots.elpfa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by reflective2 on 25-07-2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Elpfa_database1";
    private static final String TABLE_NAME_USERS = "users";
    private static final String TABLE_NAME_USER_TYPES = "user_types";
    private static final String TABLE_NAME_COURSES = "courses";
    private static final String TABLE_NAME_TEAMS = "teams";
    private static final String TABLE_NAME_COURSE_REPORT = "course_reports";
    private static final String TABLE_NAME_COMPLETED_COURSES = "completed_courses";
    private static final String TABLE_NAME_SCHEDULED_COURSES = "scheduled_courses";
    private static final String TABLE_NAME_IN_PROGRESS_COURSES = "in_progress_courses";
    private static final String TABLE_NAME_NOTIFICATIONS = "notifications";
    private static final String TABLE_NAME_INBOX_MESSAGES = "inbox_messages";
    private static final String TABLE_NAME_OUTBOX_MESSAGES = "outbox_messages";
    private static final String TABLE_NAME_OFFLINE_READ = "offline_read";
    private static final String TABLE_NAME_OFFLINE_DELETE_INBOX = "offline_delete";
    private static final String TABLE_NAME_OFFLINE_DELETE_OUTBOX= "offline_delete_outbox";
    private static final String TABLE_NAME_TEAMCHAT= "team_chat";
    private static final String TABLE_NAME_TEAMCHAT_NEW= "team_chat_new";
    private static final String TABLE_NAME_RECENT_WEBINARS= "recent_webinar_events";
    private static final String TABLE_NAME_UPCOMING_WEBINARS= "upcoming_webinar_events";
    private static final String TABLE_NAME_TEAM_MEMBERS= "team_members";
    private static final String TABLE_NAME_USERS_TO_CONTENTS= "users_to_contents";
    private static final String TABLE_NAME_ENROLL_CONTENTS= "enroll_contents";
    private static final String TABLE_NAME_TO= "to_table";

    private static final String KEY_user_id = "id";
    private static final String KEY_name = "name";
    private  static  final String KEY_password="password";
    private  static  final String KEY_email="email";
    private  static  final String KEY_company_name="company_name";
    private  static  final String KEY_company_address="company_address";
    private  static  final String KEY_user_type="user_type";
    private  static  final String KEY_photo="photo";
    private  static  final String KEY_registration="registration";
    private  static  final String KEY_status="status";
    private  static  final String KEY_mobileno="mobileno";

    private  static  final String KEY_user_types_id="id";
    private  static  final String KEY_user_types_usertype="usertype";
    private  static  final String KEY_user_types_status="status";

    private  static  final String KEY_to_id="id";
    private  static  final String KEY_to_to_id="to_id";
    private  static  final String KEY_to_name="to_name";
    private  static  final String KEY_to_email="to_email";
    private  static  final String KEY_to_type="to_type";


    private  static  final String KEY_all_course_id="course_id";
    private  static  final String KEY_all_course_name="name";
    private  static  final String KEY_all_course_description="description";
    private  static  final String KEY_all_course_duration="course_duration";
    private  static  final String KEY_all_course_enrollment_key="enrollment_key";
    private  static  final String KEY_all_course_status="status";
    private  static  final String KEY_all_course_path="path";
    private  static  final String KEY_all_course_assign_date="assign_date";
    private  static  final String KEY_all_course_track_course="track_course";
    private  static  final String KEY_all_course_test_mandatory="test_mandatory";
    private  static  final String KEY_all_course_no_of_que="no_of_que";
    private  static  final String KEY_all_course_pass_percent="pass_percent";
    private  static  final String KEY_all_course_users_to_course_id="users_to_course_id";
    private  static  final String KEY_all_course_started="started";
    private  static  final String KEY_all_course_completed="completed";
    private  static  final String KEY_all_course_completion_date="completeion_date";

    private  static  final String KEY_team_id="id";
    private  static  final String KEY_team_name="name";
    private  static  final String KEY_team_path="path";
    private  static  final String KEY_team_description="description";

    private  static  final String KEY_assigned_courses="asigned";
    private  static  final String KEY_started_courses="started";
    private  static  final String KEY_not_started_courses="not_started";
    private  static  final String KEY_completed_courses="completed";

    private  static  final String KEY_completed_course_id="course_id";
    private  static  final String KEY_completed_course_name="name";
    private  static  final String KEY_completed_course_description="description";
    private  static  final String KEY_completed_course_duration="course_duration";
    private  static  final String KEY_completed_course_enrollment_key="enrollment_key";
    private  static  final String KEY_completed_course_status="status";
    private  static  final String KEY_completed_course_path="path";
    private  static  final String KEY_completed_course_assign_date="assign_date";
    private  static  final String KEY_completed_course_track_course="track_course";
    private  static  final String KEY_completed_course_test_mandatory="test_mandatory";
    private  static  final String KEY_completed_course_no_of_que="no_of_que";
    private  static  final String KEY_completed_course_pass_percent="pass_percent";
    private  static  final String KEY_completed_course_users_to_course_id="users_to_course_id";
    private  static  final String KEY_completed_course_started="started";
    private  static  final String KEY_completed_course_completed="completed";
    private  static  final String KEY_completed_course_completion_date="completion_date";

     private  static  final String KEY_scheduled_course_id="course_id";
    private  static  final String KEY_scheduled_course_name="name";
    private  static  final String KEY_scheduled_course_description="description";
    private  static  final String KEY_scheduled_course_duration="course_duration";
    private  static  final String KEY_scheduled_course_enrollment_key="enrollment_key";
    private  static  final String KEY_scheduled_course_status="status";
    private  static  final String KEY_scheduled_course_path="path";
    private  static  final String KEY_scheduled_course_assign_date="assign_date";
    private  static  final String KEY_scheduled_course_track_course="track_course";
    private  static  final String KEY_scheduled_course_test_mandatory="test_mandatory";
    private  static  final String KEY_scheduled_course_no_of_que="no_of_que";
    private  static  final String KEY_scheduled_course_pass_percent="pass_percent";
    private  static  final String KEY_scheduled_course_users_to_course_id="users_to_course_id";
    private  static  final String KEY_scheduled_course_started="started";
    private  static  final String KEY_scheduled_course_completed="completed";
    private  static  final String KEY_scheduled_course_completion_date="completion_date";

    private static final String KEY_recent_webinar_id="recent_webinar_id";
    private static final String KEY_recent_webinar_name="recent_webinar_name";
    private static final String KEY_recent_webinar_agenda="recent_webinar_agenda";
    private static final String KEY_recent_webinar_starts_at="recent_webinar_starts_at";
    private static final String KEY_recent_webinar_ends_at="recent_webinar_ends_at";
    private static final String KEY_recent_webinar_duration="recent_webinar_duration";
    private static final String KEY_recent_webinar_room_url="recent_webinar_room_url";

    private static final String KEY_upcoming_webinar_id="upcoming_webinar_id";
    private static final String KEY_upcoming_webinar_name="upcoming_webinar_name";
    private static final String KEY_upcoming_webinar_agenda="upcoming_webinar_agenda";
    private static final String KEY_upcoming_webinar_starts_at="upcoming_webinar_starts_at";
    private static final String KEY_upcoming_webinar_ends_at="upcoming_webinar_ends_at";
    private static final String KEY_upcoming_webinar_duration="upcoming_webinar_duration";
    private static final String KEY_upcoming_webinar_room_url="upcoming_webinar_room_url";

private  static  final String KEY_in_progress_course_id="course_id";
    private  static  final String KEY_in_progress_course_name="name";
    private  static  final String KEY_in_progress_course_description="description";
    private  static  final String KEY_in_progress_course_duration="course_duration";
    private  static  final String KEY_in_progress_course_enrollment_key="enrollment_key";
    private  static  final String KEY_in_progress_course_status="status";
    private  static  final String KEY_in_progress_course_path="path";
    private  static  final String KEY_in_progress_course_assign_date="assign_date";
    private  static  final String KEY_in_progress_course_track_course="track_course";
    private  static  final String KEY_in_progress_course_test_mandatory="test_mandatory";
    private  static  final String KEY_in_progress_course_no_of_que="no_of_que";
    private  static  final String KEY_in_progress_course_pass_percent="pass_percent";
    private  static  final String KEY_in_progress_course_users_to_course_id="users_to_course_id";
    private  static  final String KEY_in_progress_course_started="started";
    private  static  final String KEY_in_progress_course_completed="completed";
    private  static  final String KEY_in_progress_course_completion_date="completion_date";

    private static final String KEY_notification_id="notif_id";
    private static final String KEY_notification_title="notif_title";
    private static final String KEY_notification_notification="notif_notification";
    private static final String KEY_notification_date="notif_date";
    private static final String KEY_notification_status="notif_status";

    private static final String KEY_inbox_message_id="inbox_msg_id";
    private static final String KEY_inbox_message_msg_id="inbox_msg_msg_id";
    private static final String KEY_inbox_message_sender="inbox_msg_sender";
    private static final String KEY_inbox_message_receiver="inbox_msg_receiver";
    private static final String KEY_inbox_message_subject="inbox_msg_subject";
    private static final String KEY_inbox_message_msg="inbox_msg_msg";
    private static final String KEY_inbox_message_photo="inbox_msg_photo";
    private static final String KEY_inbox_message_date="inbox_msg_date";
    private static final String KEY_inbox_message_viewed="inbox_msg_viewed";
    private static final String KEY_inbox_message_important="inbox_msg_important";
    private static final String KEY_inbox_message_attachments="inbox_msg_attachments";

    private static final String KEY_outbox_message_id="outbox_id";
    private static final String KEY_outbox_message_msg_id="outbox_msg_id";
    private static final String KEY_outbox_message_sender="outbox_sender";
    private static final String KEY_outbox_message_receiver="outbox_receiver";
    private static final String KEY_outbox_message_subject="outbox_subject";
    private static final String KEY_outbox_message_message="outbox_message";
    private static final String KEY_outbox_message_photo="outbox_photo";
    private static final String KEY_outbox_message_date="outbox_date";
    private static final String KEY_outbox_message_important="outbox_important";
    private static final String KEY_outbox_message_attachments="outbox_msg_attachments";

    private static final String KEY_offline_read_msgid="offline_read_msgid";

    private static final String KEY_offline_delete_inbox_msgid="offline_delete_inbox_msgid";

    private static final String KEY_offline_delete_outbox_msgtimestamp="offline_delete_outbox_msgtimestamp";

    private static final String KEY_teamchat_id="teamchat_id";
    private static final String KEY_teamchat_chatid="teamchat_chatid";
    private static final String KEY_teamchat_teamid="teamchat_teamid";
    private static final String KEY_teamchat_username="teamchat_username";
    private static final String KEY_teamchat_imagepath="teamchat_imagepath";
    private static final String KEY_teamchat_messages="teamchat_messages";
    private static final String KEY_teamchat_createdat="teamchat_createdat";

    private static final String KEY_teamchat_id_new="teamchat_id_new";
    private static final String KEY_teamchat_chatid_new="teamchat_chatid_new";
    private static final String KEY_teamchat_teamid_new="teamchat_teamid_new";
    private static final String KEY_teamchat_username_new="teamchat_username_new";
    private static final String KEY_teamchat_imagepath_new="teamchat_imagepath_new";
    private static final String KEY_teamchat_messages_new="teamchat_messages_new";
    private static final String KEY_teamchat_createdat_new="teamchat_createdat_new";


    private static final String KEY_team_members_id="team_members_id";
    private static final String KEY_team_members_teamid="team_members_teamid";
    private static final String KEY_team_members_name="team_members_name";
    private static final String KEY_team_members_photo="team_members_photo";

    private static final String KEY_usersToContents_Id="usersToCourses_id";
    private static final String KEY_usersToContents_courseId="usersToCourses_courseId";
    private static final String KEY_usersToContents_usersToContentId="usersToCourses_usersToContentId";
    private static final String KEY_usersToContents_viewed="usersToCourses_viewed";
    private static final String KEY_usersToContents_completed="usersToCourses_completed";
    private static final String KEY_usersToContents_type="usersToCourses_type";
    private static final String KEY_usersToContents_name="usersToCourses_name";
    private static final String KEY_usersToContents_path="usersToCourses_path";

    private  static  final String KEY_enroll_contents_id="enroll_contents_id";
    private  static  final String KEY_enroll_contents_course_id="enroll_contents_course_id";
    private  static  final String KEY_enroll_contents_name="enroll_contents_name";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_NAME_USERS + "(" + KEY_user_id + " TEXT," + KEY_name + " TEXT,"  + KEY_password + " TEXT,"  + KEY_email + " TEXT,"  + KEY_user_type + " TEXT,"  + KEY_company_name + " TEXT," + KEY_company_address + " TEXT,"  +KEY_photo + " TEXT,"  /*+  KEY_registration + " TEXT,"  +  KEY_status + " TEXT,"*/ + KEY_mobileno + " TEXT "+")";
        db.execSQL(CREATE_USERS_TABLE);

        String CREATE_USER_TYPES_TABLE = "CREATE TABLE " + TABLE_NAME_USER_TYPES + "(" + KEY_user_types_id + " TEXT," + KEY_user_types_usertype + " TEXT," + KEY_user_types_status + " TEXT "+")";
        db.execSQL(CREATE_USER_TYPES_TABLE);

        String CREATE_COURSE_REPORTS_TABLE = "CREATE TABLE " + TABLE_NAME_COURSE_REPORT + "(" + KEY_assigned_courses + " TEXT," + KEY_started_courses + " TEXT," + KEY_not_started_courses + " TEXT," + KEY_completed_courses + " TEXT "+")";
        db.execSQL(CREATE_COURSE_REPORTS_TABLE);

        String CREATE_ENROLL_CONTENTS_TABLE = "CREATE TABLE " + TABLE_NAME_ENROLL_CONTENTS + "(" + KEY_enroll_contents_id + " TEXT," + KEY_enroll_contents_course_id + " TEXT," + KEY_enroll_contents_name + " TEXT "+")";
        db.execSQL(CREATE_ENROLL_CONTENTS_TABLE);

        String CREATE_COMPLETED_COURSES_TABLE = "CREATE TABLE " + TABLE_NAME_COMPLETED_COURSES + "(" + KEY_completed_course_id + " TEXT," + KEY_completed_course_users_to_course_id + " TEXT," + KEY_completed_course_name + " TEXT," + KEY_completed_course_description + " TEXT," + KEY_completed_course_duration + " TEXT," + KEY_completed_course_enrollment_key + " TEXT," + KEY_completed_course_status + " TEXT," + KEY_completed_course_path + " TEXT," + KEY_completed_course_assign_date + " TEXT," + KEY_completed_course_track_course + " TEXT," + KEY_completed_course_no_of_que + " TEXT," + KEY_completed_course_pass_percent + " TEXT," + KEY_completed_course_test_mandatory + " TEXT," + KEY_completed_course_started + " TEXT," + KEY_completed_course_completed + " TEXT," + KEY_completed_course_completion_date + " TEXT "+")";
        db.execSQL(CREATE_COMPLETED_COURSES_TABLE);

        String CREATE_SCHEDULED_COURSES_TABLE = "CREATE TABLE " + TABLE_NAME_SCHEDULED_COURSES + "(" + KEY_scheduled_course_id + " TEXT," + KEY_scheduled_course_users_to_course_id + " TEXT," + KEY_scheduled_course_name + " TEXT," + KEY_scheduled_course_description + " TEXT," + KEY_scheduled_course_duration + " TEXT," + KEY_scheduled_course_enrollment_key + " TEXT," + KEY_scheduled_course_status + " TEXT," + KEY_scheduled_course_path + " TEXT," + KEY_scheduled_course_assign_date + " TEXT," + KEY_scheduled_course_track_course + " TEXT," + KEY_scheduled_course_no_of_que + " TEXT," + KEY_scheduled_course_pass_percent + " TEXT," + KEY_scheduled_course_test_mandatory + " TEXT," + KEY_scheduled_course_started + " TEXT," + KEY_scheduled_course_completed + " TEXT," + KEY_scheduled_course_completion_date + " TEXT "+")";
        db.execSQL(CREATE_SCHEDULED_COURSES_TABLE);

         String CREATE_IN_PROGRESS_COURSES_TABLE = "CREATE TABLE " + TABLE_NAME_IN_PROGRESS_COURSES + "(" + KEY_in_progress_course_id + " TEXT," + KEY_in_progress_course_users_to_course_id + " TEXT," + KEY_in_progress_course_name + " TEXT," + KEY_in_progress_course_description + " TEXT," + KEY_in_progress_course_duration + " TEXT," + KEY_in_progress_course_enrollment_key + " TEXT," + KEY_in_progress_course_status + " TEXT," + KEY_in_progress_course_path + " TEXT," + KEY_in_progress_course_assign_date + " TEXT," + KEY_in_progress_course_track_course + " TEXT," + KEY_in_progress_course_no_of_que + " TEXT," + KEY_in_progress_course_pass_percent + " TEXT," + KEY_in_progress_course_test_mandatory + " TEXT," + KEY_in_progress_course_started + " TEXT," + KEY_in_progress_course_completed + " TEXT," + KEY_in_progress_course_completion_date + " TEXT "+")";
        db.execSQL(CREATE_IN_PROGRESS_COURSES_TABLE);

        String CREATE_COURSES_TABLE = "CREATE TABLE " + TABLE_NAME_COURSES + "(" + KEY_all_course_id + " TEXT," + KEY_all_course_users_to_course_id + " TEXT," + KEY_all_course_name + " TEXT," + KEY_all_course_description + " TEXT," + KEY_all_course_duration + " TEXT," + KEY_all_course_enrollment_key + " TEXT," + KEY_all_course_status + " TEXT," + KEY_all_course_path + " TEXT," + KEY_all_course_assign_date + " TEXT," + KEY_all_course_track_course + " TEXT," + KEY_all_course_no_of_que + " TEXT," + KEY_all_course_pass_percent + " TEXT," + KEY_all_course_test_mandatory + " TEXT," + KEY_all_course_started + " TEXT," + KEY_all_course_completed + " TEXT," + KEY_all_course_completion_date + " TEXT "+")";
        db.execSQL(CREATE_COURSES_TABLE);

        String CREATE_RECENT_WEBINARS_TABLE = "CREATE TABLE " + TABLE_NAME_RECENT_WEBINARS + "(" + KEY_recent_webinar_id + " TEXT," +  KEY_recent_webinar_name+ " TEXT," + KEY_recent_webinar_agenda + " TEXT," + KEY_recent_webinar_starts_at + " TEXT," + KEY_recent_webinar_ends_at + " TEXT," + KEY_recent_webinar_duration + " TEXT," + KEY_recent_webinar_room_url + " TEXT "+")";
        db.execSQL(CREATE_RECENT_WEBINARS_TABLE);

        String CREATE_UPCOMING_WEBINARS_TABLE = "CREATE TABLE " + TABLE_NAME_UPCOMING_WEBINARS + "(" + KEY_upcoming_webinar_id + " TEXT," +  KEY_upcoming_webinar_name+ " TEXT," + KEY_upcoming_webinar_agenda + " TEXT," + KEY_upcoming_webinar_starts_at + " TEXT," + KEY_upcoming_webinar_ends_at + " TEXT," + KEY_upcoming_webinar_duration + " TEXT," + KEY_upcoming_webinar_room_url + " TEXT "+")";
        db.execSQL(CREATE_UPCOMING_WEBINARS_TABLE);

        String CREATE_TEAMS_TABLE = "CREATE TABLE " + TABLE_NAME_TEAMS + "(" + KEY_team_id + " TEXT," + KEY_team_name + " TEXT," + KEY_team_path + " TEXT," + KEY_team_description + " TEXT "+")";
        db.execSQL(CREATE_TEAMS_TABLE);

        String CREATE_NOTIFICATIONS_TABLE = "CREATE TABLE " + TABLE_NAME_NOTIFICATIONS + "(" + KEY_notification_id + " TEXT," + KEY_notification_title + " TEXT," + KEY_notification_notification + " TEXT," + KEY_notification_date + " TEXT," + KEY_notification_status + " TEXT "+")";
        db.execSQL(CREATE_NOTIFICATIONS_TABLE);

        String CREATE_INBOX_MESSAGES_TABLE = "CREATE TABLE " + TABLE_NAME_INBOX_MESSAGES + "(" + KEY_inbox_message_id + " TEXT," + KEY_inbox_message_msg_id + " TEXT," + KEY_inbox_message_sender + " TEXT," + KEY_inbox_message_receiver + " TEXT," + KEY_inbox_message_subject + " TEXT," + KEY_inbox_message_msg + " TEXT," + KEY_inbox_message_photo + " TEXT," + KEY_inbox_message_date + " TEXT," + KEY_inbox_message_viewed + " TEXT," + KEY_inbox_message_important + " TEXT," + KEY_inbox_message_attachments + " TEXT "+")";
        db.execSQL(CREATE_INBOX_MESSAGES_TABLE);


 String CREATE_OUTBOX_MESSAGES_TABLE = "CREATE TABLE " + TABLE_NAME_OUTBOX_MESSAGES + "(" + KEY_outbox_message_id + " TEXT," + KEY_outbox_message_msg_id + " TEXT," + KEY_outbox_message_sender + " TEXT," + KEY_outbox_message_receiver + " TEXT," + KEY_outbox_message_subject + " TEXT," + KEY_outbox_message_message + " TEXT," + KEY_outbox_message_photo + " TEXT," + KEY_outbox_message_date + " TEXT," + KEY_outbox_message_important + " TEXT," + KEY_outbox_message_attachments + " TEXT "+")";
        db.execSQL(CREATE_OUTBOX_MESSAGES_TABLE);

         String CREATE_OFFLINE_READ_TABLE = "CREATE TABLE " + TABLE_NAME_OFFLINE_READ + "(" + KEY_offline_read_msgid + " TEXT "+")";
        db.execSQL(CREATE_OFFLINE_READ_TABLE);

  String CREATE_OFFLINE_DELETE_INBOX_TABLE = "CREATE TABLE " + TABLE_NAME_OFFLINE_DELETE_INBOX+ "(" + KEY_offline_delete_inbox_msgid + " TEXT "+")";
        db.execSQL(CREATE_OFFLINE_DELETE_INBOX_TABLE);

        String CREATE_OFFLINE_DELETE_OUTBOX_TABLE = "CREATE TABLE " + TABLE_NAME_OFFLINE_DELETE_OUTBOX+ "(" + KEY_offline_delete_outbox_msgtimestamp + " TEXT "+")";
        db.execSQL(CREATE_OFFLINE_DELETE_OUTBOX_TABLE);

        String CREATE_TEAMCHAT_TABLE = "CREATE TABLE " + TABLE_NAME_TEAMCHAT + "(" + KEY_teamchat_id + " TEXT," +  KEY_teamchat_chatid+ " TEXT," + KEY_teamchat_teamid + " TEXT," + KEY_teamchat_username + " TEXT," + KEY_teamchat_imagepath + " TEXT," + KEY_teamchat_messages + " TEXT," + KEY_teamchat_createdat + " TEXT "+")";
        db.execSQL(CREATE_TEAMCHAT_TABLE);

        String CREATE_TEAMCHAT_NEW_TABLE = "CREATE TABLE " + TABLE_NAME_TEAMCHAT_NEW + "(" + KEY_teamchat_id_new + " TEXT," +  KEY_teamchat_chatid_new+ " TEXT," + KEY_teamchat_teamid_new + " TEXT," + KEY_teamchat_username_new + " TEXT," + KEY_teamchat_imagepath_new + " TEXT," + KEY_teamchat_messages_new + " TEXT," + KEY_teamchat_createdat_new + " TEXT "+")";
        db.execSQL(CREATE_TEAMCHAT_NEW_TABLE);


        String CREATE_TEAM_MEMBERS_TABLE = "CREATE TABLE " + TABLE_NAME_TEAM_MEMBERS + "(" + KEY_team_members_id + " TEXT," + KEY_team_members_teamid + " TEXT," + KEY_team_members_name + " TEXT," +  KEY_team_members_photo + " TEXT "+")";
        db.execSQL(CREATE_TEAM_MEMBERS_TABLE);


        String CREATE_USERS_TO_CONTENTS_TABLE = "CREATE TABLE " + TABLE_NAME_USERS_TO_CONTENTS + "(" + KEY_usersToContents_Id + " TEXT," +  KEY_usersToContents_courseId+ " TEXT," + KEY_usersToContents_usersToContentId + " TEXT," + KEY_usersToContents_viewed + " TEXT," + KEY_usersToContents_completed + " TEXT," + KEY_usersToContents_type + " TEXT," + KEY_usersToContents_name + " TEXT," + KEY_usersToContents_path + " TEXT "+")";
        db.execSQL(CREATE_USERS_TO_CONTENTS_TABLE);

//        String CREATE_TO_TABLE = "CREATE TABLE " + TABLE_NAME_TO + "(" + KEY_to_id + " TEXT," + KEY_to_to_id + " TEXT," + KEY_to_name + " TEXT," + KEY_to_email + " TEXT," + KEY_to_type + " TEXT "+")";
//        db.execSQL(CREATE_TO_TABLE);

        String CREATE_TO_TABLE = "CREATE TABLE " + TABLE_NAME_TO + "(" + KEY_to_to_id + " TEXT," + KEY_to_name + " TEXT," + KEY_to_email + " TEXT," + KEY_to_type + " TEXT "+")";
        db.execSQL(CREATE_TO_TABLE);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USER_TYPES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_COURSE_REPORT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_COMPLETED_COURSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SCHEDULED_COURSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_IN_PROGRESS_COURSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_COURSES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_NOTIFICATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_INBOX_MESSAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TEAMCHAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_RECENT_WEBINARS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_UPCOMING_WEBINARS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TEAMCHAT_NEW);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_OFFLINE_DELETE_INBOX);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_OFFLINE_DELETE_OUTBOX);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TEAM_MEMBERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USERS_TO_CONTENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ENROLL_CONTENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_TO);
        onCreate(db);
    }
    public void addUserTypes(Elpfa elpfa) {



            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_user_types_id, elpfa.getUserTypeId());
            values.put(KEY_user_types_usertype, elpfa.getUserType());
            values.put(KEY_user_types_status, elpfa.getUserTypestatus());

            // Inserting Row
            db.insert(TABLE_NAME_USER_TYPES, null, values);

        db.close(); // Closing database connection



    }

    // Getting All user types details
    public List<Elpfa> getUserTypes() {
        List<Elpfa> user_types_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_USER_TYPES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUserTypeId(cursor.getString(0));
                elpfa.setUserType(cursor.getString(1));
                elpfa.setUserTypeStatus(cursor.getString(2));
                user_types_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return list
        return user_types_list;
    }
    /* end getting all user type details */

    /* insert all user details */
    public void addUserDetails(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_user_id, elpfa.getUserId());
        values.put(KEY_name, elpfa.getUserName());
        values.put(KEY_password, elpfa.getUserPassword());
        values.put(KEY_email, elpfa.getUserEmail());
        values.put(KEY_user_type, elpfa.getUserUsertype());
        values.put(KEY_company_name, elpfa.getUserCompanyName());
        values.put(KEY_company_address, elpfa.getUserCompanyAddress());
        values.put(KEY_photo, elpfa.getUserPhoto());
       /* values.put(KEY_registration, elpfa.getUserRegistration());
        values.put(KEY_status, elpfa.getUserStatus());*/
        values.put(KEY_mobileno, elpfa.getUserMobileNo());
        db.insert(TABLE_NAME_USERS, null, values);
    }
    /* end insert all user details */







    // Getting All user details
    public List<Elpfa> getUserDetails() {
        List<Elpfa> user_details_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUserId(cursor.getString(0));
                elpfa.setUserName(cursor.getString(1));
                elpfa.setUserPassword(cursor.getString(2));
                elpfa.setUserEmail(cursor.getString(3));
                elpfa.setUserUsertype(cursor.getString(4));
                elpfa.setUserCompanyName(cursor.getString(5));
                elpfa.setUserCompanyAddress(cursor.getString(6));
                elpfa.setUserPhoto(cursor.getString(7));
//                elpfa.setUserRegistration(cursor.getString(8));
               /* elpfa.setUserStatus(cursor.getString(9));*/
                elpfa.setUserMobileNo(cursor.getString(8));
                user_details_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return user_details_list;
    }
    /* end getting all user type details */


    // Getting All completed courses details
    public List<Elpfa> getCompletedCourses() {
        List<Elpfa> completed_courses_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_COMPLETED_COURSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setCompletedCourseId(cursor.getString(0));
                elpfa.setCompletedCourseUsersToCourseId(cursor.getString(1));
                elpfa.setCompletedCourseName(cursor.getString(2));
                elpfa.setCompletedCourseDescription(cursor.getString(3));
                elpfa.setCompletedCourseDuration(cursor.getString(4));
                elpfa.setCompletedCourseEnrollmentKey(cursor.getInt(5));
                elpfa.setCompletedCourseStatus(cursor.getInt(6));
                elpfa.setCompletedCoursePath(cursor.getString(7));
                elpfa.setCompletedCourseAssignDate(cursor.getString(8));
                elpfa.setCompletedCourseTrackCourse(cursor.getString(9));
                elpfa.setCompletedCourseNoOfQue(cursor.getString(10));
                elpfa.setCompletedCoursePassPercent(cursor.getString(11));
                elpfa.setCompletedCourseTestMandatory(cursor.getString(12));
                elpfa.setCompletedCourseStarted(cursor.getInt(13));
                elpfa.setCompletedCourseCompleted(cursor.getInt(14));
                elpfa.setCompletedCourseTestMandatory(cursor.getString(15));
                completed_courses_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return completed_courses_list;
    }
    /* end getting all completed courses */

    /* insert all completed courses */
    public void addCompletedCourses(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_completed_course_id, elpfa.getCompletedCourseId());
        values.put(KEY_completed_course_users_to_course_id,elpfa.getCompletedCourseUsersToCourseId());
        values.put(KEY_completed_course_name, elpfa.getCompletedCourseName());
        values.put(KEY_completed_course_description, elpfa.getCompletedCourseDescription());
        values.put(KEY_completed_course_duration, elpfa.getCompletedCourseDuration());
        values.put(KEY_completed_course_enrollment_key, elpfa.getCompletedCourseEnrollmentKey());//company name
        values.put(KEY_completed_course_status, elpfa.getCompletedCourseStatus());//company name
        values.put(KEY_completed_course_path, elpfa.getCompletedCoursePath());
        values.put(KEY_completed_course_assign_date,elpfa.getCompletedCourseAssignDate());
        values.put(KEY_completed_course_track_course, elpfa.getCompletedCourseTrackCourse());
        values.put(KEY_completed_course_no_of_que,elpfa.getCompletedCourseNoOfQue());
        values.put(KEY_completed_course_pass_percent, elpfa.getCompletedCoursePassPercent());
        values.put(KEY_completed_course_test_mandatory,elpfa.getCompletedCourseTestMandatory());
        values.put(KEY_completed_course_started,elpfa.getCompletedCourseStarted());
        values.put(KEY_completed_course_completed,elpfa.getCompletedCourseCompleted());
        values.put(KEY_completed_course_completion_date,elpfa.getCompletedCourseCompletionDate());
        // Inserting Row
        db.insert(TABLE_NAME_COMPLETED_COURSES, null, values);
    }
    /* end insert all completed courses */



    /* insert all course reports */
    public void addCourseReports(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_assigned_courses, elpfa.getAssignedCourses());
        values.put(KEY_started_courses, elpfa.getStartedCourses());
        values.put(KEY_not_started_courses, elpfa.getNotStartedCourses());
        values.put(KEY_completed_courses, elpfa.getCompletedCourses());

        // Inserting Row
        db.insert(TABLE_NAME_COURSE_REPORT, null, values);
    }
    /* end insert all user details */


    // Getting All user details
    public List<Elpfa> getCourseReports() {
        List<Elpfa> course_reports_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_COURSE_REPORT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setAssignedCourses(cursor.getString(0));
                elpfa.setStartedCourses(cursor.getString(1));
                elpfa.setNotStartedCourses(cursor.getString(2));
                elpfa.setCompletedCourses(cursor.getString(3));
                // Adding contact to list
                course_reports_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return course_reports_list;
    }
    /* end getting all course reports details */



/*******scheduled courses ********/
/* insert scheduled courses */
    public void addScheduledCourses(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_scheduled_course_id, elpfa.getScheduledCourseId());
        values.put(KEY_scheduled_course_users_to_course_id, elpfa.getScheduledUsersToCourseId());
        values.put(KEY_scheduled_course_name, elpfa.getScheduledCourseName());
        values.put(KEY_scheduled_course_description, elpfa.getScheduledDescription());
        values.put(KEY_scheduled_course_duration, elpfa.getScheduledDuration());
        values.put(KEY_scheduled_course_enrollment_key, elpfa.getScheduledEnrollmentKey());
        values.put(KEY_scheduled_course_status, elpfa.getScheduledStatus());
        values.put(KEY_scheduled_course_path,elpfa.getScheduledCoursePath());
        values.put(KEY_scheduled_course_assign_date, elpfa.getScheduledAssignDate());
        values.put(KEY_scheduled_course_track_course,elpfa.getScheduledTrackCourse());
        values.put(KEY_scheduled_course_no_of_que,elpfa.getScheduledNumberOfQuestions());
        values.put(KEY_scheduled_course_pass_percent,elpfa.getScheduledPassPercent());
        values.put(KEY_scheduled_course_test_mandatory,elpfa.getScheduledTestMandatory());
        values.put(KEY_scheduled_course_started,elpfa.getScheduledCourseStarted());
        values.put(KEY_scheduled_course_completed,elpfa.getScheduledCourseCompleted());
        values.put(KEY_scheduled_course_completion_date,elpfa.getScheduledCourseCompletionDate());
        // Inserting Row
        db.insert(TABLE_NAME_SCHEDULED_COURSES, null, values);
    }
    /* end insert all scheduled course  */

    // Getting All scheduled courses
    public List<Elpfa> getScheduledCourses() {
        List<Elpfa> scheduled_courses_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_SCHEDULED_COURSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setScheduledCourseId(cursor.getString(0));
                elpfa.setScheduledUsersToCourseId(cursor.getString(1));
                elpfa.setScheduledCourseName(cursor.getString(2));
                elpfa.setScheduledCourseDescription(cursor.getString(3));
                elpfa.setScheduledCourseDuration(cursor.getString(4));
                elpfa.setScheduledEnrollmentKey(cursor.getInt(5));
                elpfa.setScheduledStatus(cursor.getInt(6));
                elpfa.setScheduledCoursePath(cursor.getString(7));
                elpfa.setScheduledAssignDate(cursor.getString(8));
                elpfa.setScheduledTrackCourse(cursor.getString(9));
                elpfa.setScheduledNumberOfQuestions(cursor.getString(10));
                elpfa.setScheduledPassPercent(cursor.getString(11));
                elpfa.setScheduledTestMandatory(cursor.getString(12));
                elpfa.setScheduledCourseStarted(cursor.getInt(13));
                elpfa.setScheduledCourseCompleted(cursor.getInt(14));
                elpfa.setScheduledCourseCompletionDate(cursor.getString(15));
                // Adding contact to list
                scheduled_courses_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return scheduled_courses_list;
    }
    /* end getting all scheduled courses */

    /*******in progress courses ********/
/* insert in progress courses */
    public void addInProgressCourses(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_in_progress_course_id, elpfa.getInProgressCourseId());
        values.put(KEY_in_progress_course_users_to_course_id, elpfa.getInProgressUsersToCourseId());
        values.put(KEY_in_progress_course_name, elpfa.getInProgressCourseName());
        values.put(KEY_in_progress_course_description, elpfa.getInProgressDescription());
        values.put(KEY_in_progress_course_duration, elpfa.getInProgressDuration());
        values.put(KEY_in_progress_course_enrollment_key, elpfa.getInProgressEnrollmentKey());
        values.put(KEY_in_progress_course_status, elpfa.getInProgressStatus());
        values.put(KEY_in_progress_course_path,elpfa.getInProgressCoursePath());
        values.put(KEY_in_progress_course_assign_date,elpfa.getInProgressAssignDate());
        values.put(KEY_in_progress_course_track_course,elpfa.getInProgressTrackCourse());
        values.put(KEY_in_progress_course_no_of_que,elpfa.getInProgressNumberOfQuestions());
        values.put(KEY_in_progress_course_pass_percent,elpfa.getInProgressPassPercent());
        values.put(KEY_in_progress_course_test_mandatory,elpfa.getInProgressTestMandatory());
        values.put(KEY_in_progress_course_started,elpfa.getInProgressCourseStarted());
        values.put(KEY_in_progress_course_completed,elpfa.getInProgressCourseCompleted());
        values.put(KEY_in_progress_course_completion_date,elpfa.getInProgressCourseCompletionDate());
        // Inserting Row
        db.insert(TABLE_NAME_IN_PROGRESS_COURSES, null, values);
    }
    /* end insert all in progress course  */

    // Getting All in progress courses
    public List<Elpfa> getInProgressCourses() {
        List<Elpfa> in_progress_courses_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_IN_PROGRESS_COURSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setInProgressCourseId(cursor.getString(0));
                elpfa.setInProgressUsersToCourseId(cursor.getString(1));
                elpfa.setInProgressCourseName(cursor.getString(2));
                elpfa.setInProgressCourseDescription(cursor.getString(3));
                elpfa.setInProgressCourseDuration(cursor.getString(4));
                elpfa.setInProgressEnrollmentKey(cursor.getInt(5));
                elpfa.setInProgressStatus(cursor.getInt(6));
                elpfa.setInProgressCoursePath(cursor.getString(7));
                elpfa.setInProgressAssignDate(cursor.getString(8));
                elpfa.setInProgressTrackCourse(cursor.getString(9));
                elpfa.setInProgressNumberOfQuestions(cursor.getString(10));
                elpfa.setInProgressPassPercent(cursor.getString(11));
                elpfa.setInProgressTestMandatory(cursor.getString(12));
                elpfa.setInProgressCourseStarted(cursor.getInt(13));
                elpfa.setInProgressCourseCompleted(cursor.getInt(14));
                elpfa.setInProgressCourseCompletionDate(cursor.getString(15));
                // Adding contact to list
                in_progress_courses_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return in_progress_courses_list;
    }
    /* end getting all in progress */


    /*******All courses ********/
/* insert all courses */
    public void addAllCourses(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_all_course_id, elpfa.getAllCourseId());
        values.put(KEY_all_course_users_to_course_id, elpfa.getAllUsersToCourseId());
        values.put(KEY_all_course_name, elpfa.getAllCourseName());
        values.put(KEY_all_course_description, elpfa.getAllDescription());
        values.put(KEY_all_course_duration, elpfa.getAllDuration());
        values.put(KEY_all_course_enrollment_key, elpfa.getAllEnrollmentKey());
        values.put(KEY_all_course_status, elpfa.getAllStatus());
        values.put(KEY_all_course_path,elpfa.getAllCoursePath());
        values.put(KEY_all_course_assign_date,elpfa.getAllAssignDate());
        values.put(KEY_all_course_track_course,elpfa.getAllTrackCourse());
        values.put(KEY_all_course_no_of_que,elpfa.getAllNumberOfQuestions());
        values.put(KEY_all_course_pass_percent,elpfa.getAllPassPercent());
        values.put(KEY_all_course_test_mandatory,elpfa.getAllTestMandatory());
        values.put(KEY_all_course_started,elpfa.getAllCourseStarted());
        values.put(KEY_all_course_completed,elpfa.getAllCourseCompleted());
        values.put(KEY_all_course_completion_date,elpfa.getAllCourseCompletionDate());
        // Inserting Row
        db.insert(TABLE_NAME_COURSES, null, values);
    }
    /* end insert all in progress course  */

    // Getting All in progress courses
    public List<Elpfa> getCourseDetails(int courseid) {
        List<Elpfa> all_courses_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_COURSES + " WHERE "+ KEY_all_course_id + " = " + courseid;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setAllCourseId(cursor.getString(0));
                elpfa.setAllUsersToCourseId(cursor.getString(1));
                elpfa.setAllCourseName(cursor.getString(2));
                elpfa.setAllCourseDescription(cursor.getString(3));
                elpfa.setAllCourseDuration(cursor.getString(4));
                elpfa.setAllEnrollmentKey(cursor.getInt(5));
                elpfa.setAllStatus(cursor.getInt(6));
                elpfa.setAllCoursePath(cursor.getString(7));
                elpfa.setAllAssignDate(cursor.getString(8));
                elpfa.setAllTrackCourse(cursor.getString(9));
                elpfa.setAllNumberOfQuestions(cursor.getString(10));
                elpfa.setAllPassPercent(cursor.getString(11));
                elpfa.setAllTestMandatory(cursor.getString(12));
                elpfa.setAllCourseStarted(cursor.getInt(13));
                elpfa.setAllCourseCompleted(cursor.getInt(14));
                elpfa.setAllCourseCompletionDate(cursor.getString(15));
                // Adding contact to list
                all_courses_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return all_courses_list;
    }
    /* end getting all courses */
    public List<Elpfa> getAllCourses() {
        List<Elpfa> all_courses_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_COURSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setAllCourseId(cursor.getString(0));
                elpfa.setAllUsersToCourseId(cursor.getString(1));
                elpfa.setAllCourseName(cursor.getString(2));
                elpfa.setAllCourseDescription(cursor.getString(3));
                elpfa.setAllCourseDuration(cursor.getString(4));
                elpfa.setAllEnrollmentKey(cursor.getInt(5));
                elpfa.setAllStatus(cursor.getInt(6));
                elpfa.setAllCoursePath(cursor.getString(7));
                elpfa.setAllAssignDate(cursor.getString(8));
                elpfa.setAllTrackCourse(cursor.getString(9));
                elpfa.setAllNumberOfQuestions(cursor.getString(10));
                elpfa.setAllPassPercent(cursor.getString(11));
                elpfa.setAllTestMandatory(cursor.getString(12));
                elpfa.setAllCourseStarted(cursor.getInt(13));
                elpfa.setAllCourseCompleted(cursor.getInt(14));
                elpfa.setAllCourseCompletionDate(cursor.getString(15));
                // Adding contact to list
                all_courses_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return all_courses_list;
    }

    /**** teams *****/
    /*******All courses ********/
/* insert all teaams */
    public void addTeams(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_team_id, elpfa.getTeamId());
        values.put(KEY_team_name, elpfa.getTeamName());
        values.put(KEY_team_path, elpfa.getTeamPath());
        values.put(KEY_team_description, elpfa.getTeamDescription());
        // Inserting Row
        db.insert(TABLE_NAME_TEAMS, null, values);
    }
    /* end insert teams  */

    // Getting All teams
    public List<Elpfa> getTeams() {
        List<Elpfa> teams_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TEAMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTeamId(cursor.getString(0));
                elpfa.setTeamName(cursor.getString(1));
                elpfa.setTeamPath(cursor.getString(2));
                elpfa.setTeamDescription(cursor.getString(3));
                // Adding contact to list
                teams_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return teams_list;
    }
    /* end getting teams */

    // Getting one team members
    public List<Elpfa> getOneTeamMembers(int teamid) {
        List<Elpfa> teams_list = new ArrayList<Elpfa>();
        // Select All Query

        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TEAM_MEMBERS + " WHERE "+ KEY_team_members_teamid + " = " + teamid;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTeamMembersId(cursor.getInt(0));
                elpfa.setTeamMembersTeamId(cursor.getInt(1));
                elpfa.setTeamMembersName(cursor.getString(2));
                elpfa.setTeamMembersPhoto(cursor.getString(3));
                // Adding contact to list
                teams_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return teams_list;
    }
    /* end getting one team members */

    // Getting one inbox message
    public List<Elpfa> getOneOutboxMessage(int message_id) {
        List<Elpfa> message_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_OUTBOX_MESSAGES+ " WHERE "+ KEY_outbox_message_msg_id+ " = " + message_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setOutboxMessageId(cursor.getInt(0));
                elpfa.setOutboxMessageMsgid(cursor.getInt(1));
                elpfa.setOutbox_msg_sender(cursor.getString(2));
                elpfa.setOutbox_msg_receiver(cursor.getString(3));
                elpfa.setOutbox_msg_subject(cursor.getString(4));
                elpfa.setOutbox_msg_msg(cursor.getString(5));
                elpfa.setOutbox_msg_photo(cursor.getString(6));
                elpfa.setOutbox_msg_date(cursor.getString(7));
                elpfa.setOutbox_msg_important(cursor.getInt(8));
                elpfa.setOutbox_msg_attachment(cursor.getString(9));
                // Adding contact to list
                message_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return message_list;
    }

    // Getting one outbox message
    public List<Elpfa> getOneInboxMessage(int message_id) {
        List<Elpfa> message_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_INBOX_MESSAGES+ " WHERE "+ KEY_inbox_message_msg_id+ " = " + message_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setInboxMessageId(cursor.getInt(0));
                elpfa.setInboxMessageMsgid(cursor.getInt(1));
                elpfa.setInbox_msg_sender(cursor.getString(2));
                elpfa.setInbox_msg_receiver(cursor.getString(3));
                elpfa.setInbox_msg_subject(cursor.getString(4));
                elpfa.setInboxMessageMsg(cursor.getString(5));
                elpfa.setInbox__msg_photo(cursor.getString(6));
                elpfa.setInbox_msg_date(cursor.getString(7));
                elpfa.setInboxMessageViewed(cursor.getInt(8));
                elpfa.setInbox_msg_important(cursor.getInt(9));
                elpfa.setInbox_msg_attachment(cursor.getString(10));
                // Adding contact to list
                message_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return message_list;
    }

    // Getting one team details
    public List<Elpfa> getOneTeam(int teamid) {
        List<Elpfa> teams_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TEAMS + " WHERE "+ KEY_team_id + " = " + teamid;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTeamId(cursor.getString(0));
                elpfa.setTeamName(cursor.getString(1));
                elpfa.setTeamPath(cursor.getString(2));
                elpfa.setTeamDescription(cursor.getString(3));
                // Adding contact to list
                teams_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return teams_list;
    }
    /* end getting teams */

/************************************ notification insert and retrieve **************************/
    /* insert all notifications */
    public void addNotifications(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_notification_id, elpfa.getNotifId());
        values.put(KEY_notification_title, elpfa.getNotifTitle());
        values.put(KEY_notification_notification, elpfa.getNotifNotif());
        values.put(KEY_notification_date, elpfa.getNotifDate());
        values.put(KEY_notification_status, elpfa.getNotifStatus());
        // Inserting Row
        db.insert(TABLE_NAME_NOTIFICATIONS, null, values);
    }
    /* end insert teams  */

    // Getting All notifications
    public List<Elpfa> getNotifications() {
        List<Elpfa> notif_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_NOTIFICATIONS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setNotifId(cursor.getString(0));
                elpfa.setNotifTitle(cursor.getString(1));
                elpfa.setNotifNotif(cursor.getString(2));
                elpfa.setNotifDate(cursor.getString(3));
                elpfa.setNotifStatus(cursor.getInt(4));
                // Adding contact to list
                notif_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return notif_list;
    }
    /* end getting notification */

/************************************ notification insert and retrieve end **************************/

    /************************************ inbox messages insert and retrieve **************************/
    /* insert all inbox msg */
    public void addInboxMessages(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_inbox_message_id, elpfa.getInboxMessageId());
        values.put(KEY_inbox_message_msg_id, elpfa.getInboxMessageMsgid());
        values.put(KEY_inbox_message_sender, elpfa.getInbox_msg_sender());
        values.put(KEY_inbox_message_receiver, elpfa.getInbox_msg_receiver());
        values.put(KEY_inbox_message_subject, elpfa.getInbox_msg_subject());
        values.put(KEY_inbox_message_msg, elpfa.getInboxMessageMsg());
        values.put(KEY_inbox_message_photo, elpfa.getInbox_msg_photo());
        values.put(KEY_inbox_message_date, elpfa.getInbox_msg_date());
        values.put(KEY_inbox_message_viewed, elpfa.getInboxMessageViewed());
        values.put(KEY_inbox_message_important, elpfa.getInbox_msg_important());
        values.put(KEY_inbox_message_attachments, elpfa.getInbox_msg_attachment());

        db.insert(TABLE_NAME_INBOX_MESSAGES, null, values);
    }
    /* end insert inbox msg  */

    // Getting All inbox msg
    public List<Elpfa> getInboxMessages() {
        List<Elpfa> notif_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_INBOX_MESSAGES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setInboxMessageId(cursor.getInt(0));
                elpfa.setInboxMessageMsgid(cursor.getInt(1));
                elpfa.setInbox_msg_sender(cursor.getString(2));
                elpfa.setInbox_msg_receiver(cursor.getString(3));
                elpfa.setInbox_msg_subject(cursor.getString(4));
                elpfa.setInboxMessageMsg(cursor.getString(5));
                elpfa.setInbox__msg_photo(cursor.getString(6));
                elpfa.setInbox_msg_date(cursor.getString(7));
                elpfa.setInboxMessageViewed(cursor.getInt(8));
                elpfa.setInbox_msg_important(cursor.getInt(9));
                elpfa.setInbox_msg_attachment(cursor.getString(10));

                // Addin to list
                notif_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return notif_list;
    }
    /* end getting inbox messages */

    /* update inbox msg status */

    public int updateInbox(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_inbox_message_viewed, elpfa.getInboxMessageViewed());

        // updating row
        return db.update(TABLE_NAME_INBOX_MESSAGES, values, KEY_inbox_message_msg_id + " = ?", new String[] { String.valueOf(elpfa.getInboxMessageMsgid()) });
    }
 /* update all course started status */

    public int updateAllCourseStarted(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_all_course_started, elpfa.getAllCourseStarted());

        // updating row
        return db.update(TABLE_NAME_COURSES,values, KEY_all_course_users_to_course_id + " = ?", new String[] { String.valueOf(elpfa.getAllUsersToCourseId()) });
    }
    /* update in progress course started status */
    public int updateInProgressCourseStarted(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_in_progress_course_started, elpfa.getInProgressCourseStarted());

        // updating row
        return db.update(TABLE_NAME_IN_PROGRESS_COURSES,values, KEY_in_progress_course_users_to_course_id + " = ?", new String[] { String.valueOf(elpfa.getInProgressUsersToCourseId()) });
    }

    /* end update inbox msg status */
/* update inbox msg status */

    public int updateContentStatus(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_usersToContents_completed, elpfa.getUsersToContentsCompleted());

        // updating row
        return db.update(TABLE_NAME_USERS_TO_CONTENTS, values, KEY_usersToContents_usersToContentId + " = ?", new String[] { String.valueOf(elpfa.getUsersToContentsUsersToContentId()) });
    }


    /* end update inbox msg status */

 /* update inbox msg important */

    public int updateInboxImportant(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_inbox_message_important, elpfa.getInbox_msg_important());
        // updating row
        return db.update(TABLE_NAME_INBOX_MESSAGES, values, KEY_inbox_message_msg_id + " = ?", new String[] { String.valueOf(elpfa.getInboxMessageMsgid()) });
    }


 /* update outbox msg important */

    public int updateOutboxImportant(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_outbox_message_important, elpfa.getOutbox_msg_important());
        // updating row
        return db.update(TABLE_NAME_OUTBOX_MESSAGES, values, KEY_outbox_message_msg_id + " = ?", new String[] { String.valueOf(elpfa.getOutboxMessageMsgid()) });
    }

/* update inbox msg status */

    public int updateContentViewed(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_usersToContents_viewed, elpfa.getUsersToContentsViewed());

        // updating row
        return db.update(TABLE_NAME_USERS_TO_CONTENTS, values, KEY_usersToContents_usersToContentId + " = ?", new String[] { String.valueOf(elpfa.getUsersToContentsUsersToContentId()) });
    }


    /* end update inbox msg status */

/************************************ inbox insert and retrieve end **************************/


/************************************ outbox insert and retrieve **************************/

    /* insert all outbox msg */
    public void addOutboxMessages(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_outbox_message_id, elpfa.getOutboxMessageId());
        values.put(KEY_outbox_message_msg_id, elpfa.getOutboxMessageMsgid());
        values.put(KEY_outbox_message_sender, elpfa.getOutbox_msg_sender());
        values.put(KEY_outbox_message_receiver, elpfa.getOutbox_msg_receiver());
        values.put(KEY_outbox_message_subject, elpfa.getOutbox_msg_subject());
        values.put(KEY_outbox_message_message, elpfa.getOutbox_msg_msg());
        values.put(KEY_outbox_message_photo, elpfa.getOutbox_msg_photo());
        values.put(KEY_outbox_message_date, elpfa.getOutbox_msg_date());
        values.put(KEY_outbox_message_important, elpfa.getOutbox_msg_important());
        values.put(KEY_outbox_message_attachments, elpfa.getOutbox_msg_attachment());


        db.insert(TABLE_NAME_OUTBOX_MESSAGES, null, values);
    }
    /* end insert outbox msg  */

    // Getting All outbox msg
    public List<Elpfa> getInOutboxMessages() {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_OUTBOX_MESSAGES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setOutboxMessageId(cursor.getInt(0));
                elpfa.setOutboxMessageMsgid(cursor.getInt(1));
                elpfa.setOutbox_msg_sender(cursor.getString(2));
                elpfa.setOutbox_msg_receiver(cursor.getString(3));
                elpfa.setOutbox_msg_subject(cursor.getString(4));
                elpfa.setOutbox_msg_msg(cursor.getString(5));
                elpfa.setOutbox_msg_photo(cursor.getString(6));
                elpfa.setOutbox_msg_date(cursor.getString(7));
                elpfa.setOutbox_msg_important(cursor.getInt(8));
                elpfa.setOutbox_msg_attachment(cursor.getString(9));


                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting outbox messages */

/************************************ outbox insert and retrieve end **************************/

    /************************************ offline read **************************/

    /* insert offline read msg ids */
    public void addOfflineReadMsgIds(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_offline_read_msgid, elpfa.getOfflineReadMsgId());


        db.insert(TABLE_NAME_OFFLINE_READ, null, values);
    }
    /* end insert ffline read msg ids  */

    // Getting All offline read msg ids
    public List<Elpfa> getOfflineReadMsgIds() {
        List<Elpfa> offline_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_OFFLINE_READ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setOfflineReadMsgId(cursor.getInt(0));


                // Addin to list
                offline_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return offline_list;
    }
    /* end getting offline read msg ids */

/************************************ end offline read **************************/

    /************************************ offline delete inbox **************************/

    /* insert offline delete inbox msg ids */
    public void addOfflineDeleteInboxMsgIds(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_offline_delete_inbox_msgid, elpfa.getOfflineDeleteInboxMsgId());


        db.insert(TABLE_NAME_OFFLINE_DELETE_INBOX, null, values);
    }
    /* end insert offline delete inbox msg ids  */

    /* insert offline delete outbox timestamp */
    public void addOfflineDeleteOutboxMsgTimestamp(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_offline_delete_outbox_msgtimestamp, elpfa.getOfflineDeleteOutboxMsgTimestamp());


        db.insert(TABLE_NAME_OFFLINE_DELETE_OUTBOX, null, values);
    }
    /* end insert offline delete inbox msg ids  */

    // Getting All offline delete inbox msg ids
    public List<Elpfa> getOfflineDeleteInboxMsgIds() {
        List<Elpfa> offline_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_OFFLINE_DELETE_INBOX;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setOfflineDeleteInboxMsgId(cursor.getInt(0));


                // Addin to list
                offline_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return offline_list;
    }
    /* end getting offline read msg ids */

/************************************ end offline delete inbox **************************/



/******************offline delete outbox *************/

        /* end insert offline delete inbox msg ids  */

// Getting All offline delete inbox msg ids
public List<Elpfa> getOfflineDeleteOutboxMsgTimestamps() {
    List<Elpfa> offline_list = new ArrayList<>();
    // Select All Query
    String selectQuery = "SELECT  * FROM " + TABLE_NAME_OFFLINE_DELETE_OUTBOX;

    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
            Elpfa elpfa = new Elpfa();
            elpfa.setOutboxDeleteTimestamp(cursor.getString(0));


            // Addin to list
            offline_list.add(elpfa);
        } while (cursor.moveToNext());
    }

    // return  list
    return offline_list;
}
    /* end getting offline read msg ids */


/******************end offline delete outbox *************/



 /* delete inbox offline delete msg  */

public int deleteInboxMsgOffline(Elpfa elpfa) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    // updating row
//        return db.update(TABLE_NAME_INBOX_MESSAGES, values, KEY_inbox_message_msg_id + " = "+ elpfa.getInboxMessageMsgid(),null);
    return db.delete(TABLE_NAME_INBOX_MESSAGES, KEY_inbox_message_msg_id + " = ?", new String[] { String.valueOf(elpfa.getOfflineDeleteInboxMsgId()) });
}


 /* end delete inbox offline delete msg  */


    /************************************ teamchat insert and retrieve **************************/

    /* insert all teamchat */
    public void addTeamChat(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_teamchat_id, elpfa.getTeamChatId());
        values.put(KEY_teamchat_chatid, elpfa.getTeamChatChatId());
        values.put(KEY_teamchat_teamid, elpfa.getTeamChatTeamId());
        values.put(KEY_teamchat_username, elpfa.getTeamChatUsername());
        values.put(KEY_teamchat_imagepath, elpfa.getTeamChatImagepath());
        values.put(KEY_teamchat_messages, elpfa.getTeamChatMessages());
        values.put(KEY_teamchat_createdat, elpfa.getTeamChatCreatedAt());


        db.insert(TABLE_NAME_TEAMCHAT, null, values);
    }
    /* end insert teamchat msg  */


    /*******Enroll contents ********/
/* insert enroll contents */
    public void addEnrollContents(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_enroll_contents_id, elpfa.getEnroll_contents_id());
        values.put(KEY_enroll_contents_course_id, elpfa.getEnroll_contents_course_id());
        values.put(KEY_enroll_contents_name, elpfa.getEnroll_contents_name());
        // Inserting Row
        db.insert(TABLE_NAME_ENROLL_CONTENTS, null, values);
    }
    /* end insert enroll contents  */

    // Getting enroll contents
    public List<Elpfa> getEnrollContents() {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_ENROLL_CONTENTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setEnroll_contents_id(cursor.getString(0));
                elpfa.setEnroll_contents_course_id(cursor.getString(1));
                elpfa.setEnroll_contents_name(cursor.getString(2));

                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }

    /* insert all teamchat */
    public void addTeamChatNew(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_teamchat_id_new, elpfa.getTeamChatIdNew());
        values.put(KEY_teamchat_chatid_new, elpfa.getTeamChatChatIdNew());
        values.put(KEY_teamchat_teamid_new, elpfa.getTeamChatTeamIdNew());
        values.put(KEY_teamchat_username_new, elpfa.getTeamChatUsernameNew());
        values.put(KEY_teamchat_imagepath_new, elpfa.getTeamChatImagepathNew());
        values.put(KEY_teamchat_messages_new, elpfa.getTeamChatMessagesNew());
        values.put(KEY_teamchat_createdat_new, elpfa.getTeamChatCreatedAtNew());


        db.insert(TABLE_NAME_TEAMCHAT_NEW, null, values);
    }


/*delete notification  */
public int deleteNotification(Elpfa elpfa) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    return db.delete(TABLE_NAME_NOTIFICATIONS, KEY_notification_id + " = ?", new String[] { String.valueOf(elpfa.getNotifId()) });
}
    /* end delete notification  */




    /*delete from scheduled courses  */
    public int deleteFromScheduledCourses(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        return db.delete(TABLE_NAME_SCHEDULED_COURSES, KEY_scheduled_course_users_to_course_id + " = ?", new String[] { String.valueOf(elpfa.getScheduledUsersToCourseId()) });
    }
    /* end delete notification  */

    // Getting particular teamchat msg
    public List<Elpfa> getTeamChat(int teamid) {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TEAMCHAT + " WHERE "+ KEY_teamchat_teamid + " = " + teamid;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTeamChatId(cursor.getInt(0));
                elpfa.setTeamChatChatId(cursor.getInt(1));
                elpfa.setTeamChatTeamId(cursor.getInt(2));
                elpfa.setTeamChatUsername(cursor.getString(3));
                elpfa.setTeamChatImagepath(cursor.getString(4));
                elpfa.setTeamChatMessages(cursor.getString(5));
                elpfa.setTeamChatCreatedAt(cursor.getString(6));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting particular teamchat messages */




    // Getting particular teamchat msg after particular time
    public List<Elpfa> getTeamChatAfterTimestamp(int teamid) {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TEAMCHAT_NEW + " WHERE "+ KEY_teamchat_teamid_new + " = " + teamid;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTeamChatIdNew(cursor.getInt(0));
                elpfa.setTeamChatChatIdNew(cursor.getInt(1));
                elpfa.setTeamChatTeamIdNew(cursor.getInt(2));
                elpfa.setTeamChatUsernameNew(cursor.getString(3));
                elpfa.setTeamChatImagepathNew(cursor.getString(4));
                elpfa.setTeamChatMessagesNew(cursor.getString(5));
                elpfa.setTeamChatCreatedAtNew(cursor.getString(6));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }

/************************************ outbox insert and retrieve end **************************/

    /* delete outbox msg using timestamp */
    public int deleteOutboxMsgUsingTimestamp(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        return db.delete(TABLE_NAME_OUTBOX_MESSAGES, KEY_outbox_message_date+ " = ?", new String[] { String.valueOf(elpfa.getOutboxDeleteTimestamp())});

    }


    /************************************ team members insert and retrieve **************************/

    /* insert all team members*/
    public void addTeamMembers(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_team_members_id, elpfa.getTeamMembersId());
        values.put(KEY_team_members_teamid, elpfa.getTeamMembersTeamId());
        values.put(KEY_team_members_name, elpfa.getTeamMembersName());
        values.put(KEY_team_members_photo, elpfa.getTeamMembersPhoto());


        db.insert(TABLE_NAME_TEAM_MEMBERS, null, values);
    }
    /* end insert team members  */



 /* insert all recent webinars */
    public void addRecentWebinars(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_recent_webinar_id, elpfa.getRecentWebinar_id());
        values.put(KEY_recent_webinar_name, elpfa.getRecentWebinar_name());
        values.put(KEY_recent_webinar_agenda, elpfa.getRecentWebinar_agenda());
        values.put(KEY_recent_webinar_starts_at, elpfa.getRecentWebinar_starts_at());
        values.put(KEY_recent_webinar_ends_at, elpfa.getRecentWebinar_ends_at());
        values.put(KEY_recent_webinar_duration, elpfa.getRecentWebinar_duration());
        values.put(KEY_recent_webinar_room_url, elpfa.getRecentWebinar_room_url());


        db.insert(TABLE_NAME_RECENT_WEBINARS, null, values);
    }

    /* insert all webinars */
    public void addUpcomingWebinars(Elpfa elpfa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_upcoming_webinar_id, elpfa.getUpcomingWebinar_id());
        values.put(KEY_upcoming_webinar_name, elpfa.getUpcomingWebinar_name());
        values.put(KEY_upcoming_webinar_agenda, elpfa.getUpcomingWebinar_agenda());
        values.put(KEY_upcoming_webinar_starts_at, elpfa.getUpcomingWebinar_starts_at());
        values.put(KEY_upcoming_webinar_ends_at, elpfa.getUpcomingWebinar_ends_at());
        values.put(KEY_upcoming_webinar_duration, elpfa.getUpcomingWebinar_duration());
        values.put(KEY_upcoming_webinar_room_url, elpfa.getUpcomingWebinar_room_url());


        db.insert(TABLE_NAME_UPCOMING_WEBINARS, null, values);
    }


    // Getting All recent webinars
    public List<Elpfa> getRecentWebinars() {
        List<Elpfa> out_list = new ArrayList<Elpfa>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_RECENT_WEBINARS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setRecentWebinar_id(cursor.getString(0));
                elpfa.setRecentWebinar_name(cursor.getString(1));
                elpfa.setRecentWebinar_agenda(cursor.getString(2));
                elpfa.setRecentWebinar_starts_at(cursor.getString(3));
                elpfa.setRecentWebinar_ends_at(cursor.getString(4));
                elpfa.setRecentWebinar_duration(cursor.getString(5));
                elpfa.setRecentWebinar_room_url(cursor.getString(6));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting all recent webinars */

    // Getting All upcoming webinars
    public List<Elpfa> getUpcomingWebinars() {
        List<Elpfa> out_list = new ArrayList<Elpfa>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_UPCOMING_WEBINARS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUpcomingWebinar_id(cursor.getString(0));
                elpfa.setUpcomingWebinar_name(cursor.getString(1));
                elpfa.setUpcomingWebinar_agenda(cursor.getString(2));
                elpfa.setUpcomingWebinar_starts_at(cursor.getString(3));
                elpfa.setUpcomingWebinar_ends_at(cursor.getString(4));
                elpfa.setUpcomingWebinar_duration(cursor.getString(5));
                elpfa.setUpcomingWebinar_room_url(cursor.getString(6));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting all recent webinars */

/**** users to contents ****/
    /* insert all webinars */
public void addUsersToContents(Elpfa elpfa) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(KEY_usersToContents_Id, elpfa.getUsersToContentsId());
    values.put(KEY_usersToContents_courseId, elpfa.getUsersToContentsCourseId());
    values.put(KEY_usersToContents_usersToContentId, elpfa.getUsersToContentsUsersToContentId());
    values.put(KEY_usersToContents_viewed, elpfa.getUsersToContentsViewed());
    values.put(KEY_usersToContents_completed, elpfa.getUsersToContentsCompleted());
    values.put(KEY_usersToContents_type, elpfa.getUsersToContentsType());
    values.put(KEY_usersToContents_name, elpfa.getUsersToContentsName());
    values.put(KEY_usersToContents_path, elpfa.getUsersToContentsPath());



    db.insert(TABLE_NAME_USERS_TO_CONTENTS, null, values);
}
    /* end insert users to contents  */

    // Getting particular users to content details
    public List<Elpfa> getUsersToContent(int users_to_content) {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_USERS_TO_CONTENTS + " WHERE "+ KEY_usersToContents_usersToContentId + " = " + users_to_content;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUsersToContents_id(cursor.getInt(0));
                elpfa.setUsersToContentsCourseId(cursor.getInt(1));
                elpfa.setUsersToContentsUsersToContentId(cursor.getInt(2));
                elpfa.setUsersToContentsViewed(cursor.getInt(3));
                elpfa.setUsersToContentsCompleted(cursor.getInt(4));
                elpfa.setUsersToContents_type(cursor.getString(5));
                elpfa.setUsersToContents_name(cursor.getString(6));
                elpfa.setUsersToContents_path(cursor.getString(7));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting particular users to content details */
    // Getting particular users to content details
    public List<Elpfa> getCourseContents(int course_id) {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_USERS_TO_CONTENTS + " WHERE "+ KEY_usersToContents_courseId + " = " + course_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUsersToContents_id(cursor.getInt(0));
                elpfa.setUsersToContentsCourseId(cursor.getInt(1));
                elpfa.setUsersToContentsUsersToContentId(cursor.getInt(2));
                elpfa.setUsersToContentsViewed(cursor.getInt(3));
                elpfa.setUsersToContentsCompleted(cursor.getInt(4));
                elpfa.setUsersToContents_type(cursor.getString(5));
                elpfa.setUsersToContents_name(cursor.getString(6));
                elpfa.setUsersToContents_path(cursor.getString(7));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting particular users to content details */

    // Getting particular users to content details
    public List<Elpfa> getEnrollContents(int course_id) {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_ENROLL_CONTENTS + " WHERE "+ KEY_enroll_contents_course_id + " = " + course_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setEnroll_contents_id(cursor.getString(0));
                elpfa.setEnroll_contents_course_id(cursor.getString(1));
                elpfa.setEnroll_contents_name(cursor.getString(2));
                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting particular users to content details */

    // Getting all contents of the user
    public List<Elpfa> getAllUsersToContent() {
        List<Elpfa> out_list = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_USERS_TO_CONTENTS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setUsersToContents_id(cursor.getInt(0));
                elpfa.setUsersToContentsCourseId(cursor.getInt(1));
                elpfa.setUsersToContentsUsersToContentId(cursor.getInt(2));
                elpfa.setUsersToContentsViewed(cursor.getInt(3));
                elpfa.setUsersToContentsCompleted(cursor.getInt(4));
                elpfa.setUsersToContents_type(cursor.getString(5));
                elpfa.setUsersToContents_name(cursor.getString(6));
                elpfa.setUsersToContents_path(cursor.getString(7));



                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting all contents of user */
/**** end users to contents ****/


     /* insert all to */
public void addTo(Elpfa elpfa) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(KEY_to_to_id, elpfa.getTo_to_id());
    values.put(KEY_to_name, elpfa.getTo_name());
    values.put(KEY_to_email, elpfa.getTo_email());
    values.put(KEY_to_type, elpfa.getTo_type());
    db.insert(TABLE_NAME_TO, null, values);
}


    // Getting All to
    public List<Elpfa> getTo() {
        List<Elpfa> out_list = new ArrayList<Elpfa>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME_TO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Elpfa elpfa = new Elpfa();
                elpfa.setTo_to_id(cursor.getString(0));
                elpfa.setTo_name(cursor.getString(1));
                elpfa.setTo_email(cursor.getString(2));
                elpfa.setTo_type(cursor.getString(3));
                // Addin to list
                out_list.add(elpfa);
            } while (cursor.moveToNext());
        }

        // return  list
        return out_list;
    }
    /* end getting all recent webinars */



    /*******delete tables ********/
    //delete user table
    public void deleteUserTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_USERS,null,null);
        db.close();
    }

    //delete user_types table
    public void deleteUserTypesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_USER_TYPES,null,null);
        db.close();
    }

    //delete course reports table
    public void deleteCourseReportsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_COURSE_REPORT,null,null);
        db.close();
    }

    //delete completed courses table
    public void deleteCompletedCoursesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_COMPLETED_COURSES,null,null);
        db.close();
    }

//delete scheduled courses table
    public void deleteScheduledCoursesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_SCHEDULED_COURSES,null,null);
        db.close();
    }

//delete in progress courses table
    public void deleteInProgressCoursesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_IN_PROGRESS_COURSES,null,null);
        db.close();
    }

//delete all courses table
    public void deleteAllCoursesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_COURSES,null,null);
        db.close();
    }




    //delete teams table
    public void deleteTeamsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TEAMS,null,null);
        db.close();
    }


    //delete notifications table
    public void deleteNotificationsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_NOTIFICATIONS,null,null);
        db.close();
    }

    //delete inbox messages table
    public void deleteInboxMessagesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_INBOX_MESSAGES,null,null);
        db.close();
    }

    //delete outbox messages table
    public void deleteOutboxMessagesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_OUTBOX_MESSAGES,null,null);
        db.close();
    }

    //delete offline read messages table
    public void deleteOfflineReadMessagesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_OFFLINE_READ,null,null);
        db.close();
    }

    //delete offline delete inbox messages table
    public void deleteOfflineDeleteInboxMessagesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_OFFLINE_DELETE_INBOX,null,null);
        db.close();
    }

    //delete teamchat table
    public void deleteTeamChatTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TEAMCHAT,null,null);
        db.close();
    }

    //delete teamchat table
    public void deleteTeamChatTableNew() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TEAMCHAT_NEW,null,null);
        db.close();
    }

    //delete offline delete outbox messages table
    public void deleteOfflineDeleteOutboxMessagesTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_OFFLINE_DELETE_OUTBOX,null,null);
        db.close();
    }

    //delete team members table
    public void deleteTeamMembersTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TEAM_MEMBERS,null,null);
        db.close();
    }

    //delete recent webinars table
    public void deleteRecentWebinarsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_RECENT_WEBINARS,null,null);
        db.close();
    }

    //delete upcoming webinars table
    public void deleteUpcomingWebinarsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_UPCOMING_WEBINARS,null,null);
        db.close();
    }

    //delete users to contents table
    public void deleteUsersToContentsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_USERS_TO_CONTENTS,null,null);
        db.close();
    }


    //delete enroll contents table
    public void deleteEnrollContents() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_ENROLL_CONTENTS,null,null);
        db.close();
    }

    //delete to table
    public void deleteToTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_TO,null,null);
        db.close();
    }
}
