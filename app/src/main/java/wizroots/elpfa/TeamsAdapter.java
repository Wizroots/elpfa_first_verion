package wizroots.elpfa;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.MyViewHolder> {

    private List<Team> teamList;
    private List<Team> teamList1;
    Context context;

    private ArrayList<Team> arraylist;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView team_name;
        public TextView team_description;
        public ImageView team_image;
        public LinearLayout card_main;



        public MyViewHolder(View view) {
            super(view);
            team_name = (TextView) view.findViewById(R.id.team_name);
            team_description = (TextView) view.findViewById(R.id.team_description);
            team_image = (ImageView) view.findViewById(R.id.team_image);
            card_main=(LinearLayout) view.findViewById(R.id.main_card);


        }
    }


    public TeamsAdapter(List<Team> teamList) {
        this.teamList = teamList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(teamList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.team_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Team team = teamList.get(position);
        holder.team_name.setText(team.getTeam_name());
        holder.team_description.setText(team.getTeam_description());
        holder.card_main.setTag(team.getTeam_tag());

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        teamList.clear();

        if (charText.length() == 0) {
            teamList.addAll(arraylist);
        } else {
            for (Team wp : arraylist) {
                if (wp.getTeam_name().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    teamList.add(wp);
                }
            }
        }

        notifyDataSetChanged();
    }
    public void setFilter(List<Team> teams) {
        teamList1 = new ArrayList<>();
        teamList1.addAll(teams);
        notifyDataSetChanged();
    }
}