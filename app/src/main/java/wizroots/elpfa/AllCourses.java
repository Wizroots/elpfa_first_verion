package wizroots.elpfa;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllCourses.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllCourses#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllCourses extends Fragment/* implements SearchView.OnQueryTextListener*/{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String searchview_text;
    String searchview_text_submit;
    int new_pos;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    Toolbar toolbar;
    private OnFragmentInteractionListener mListener;
     View rootView;
    public List<Course> courseList = new ArrayList<>();
    public List<Course> courseList_search = new ArrayList<>();
    public List<Course> courseListAll = new ArrayList<>();
    private RecyclerView recyclerView;
    public CourseAdapter courseAdapter;
    DatabaseHandler db1;
    RelativeLayout no_courses;
    SwipeRefreshLayout refresh;
    ArrayList<String> course_id_arr= new ArrayList<>();
    ArrayList<String> course_started_arr= new ArrayList<>();
    ArrayList<String> course_enroll_arr= new ArrayList<>();
    ArrayList<String> course_name_arr= new ArrayList<>();
    ArrayList<Integer> course_status_arr= new ArrayList<>();

    /* server sync variables */
    String user_details_id, user_details_name, user_details_email, user_details_photo, user_details_password, user_details_type,user_details_phone, user_details_status, user_details_company_name, user_details_company_address, user_details_registration;
    String user_type_id, user_type_type, user_type_status;
    String team_id, team_name, team_path, team_description;
    String number_started, number_assigned, number_not_started, number_completed;
    String compl_path, compl_name, compl_desc, compl_startdate, compl_course_id, compl_track_content, compl_users_to_courseid, compl_completion_date;
    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
    String notification, notif_title, notif_date;
    int notif_status;
    String notif_id;
    int scheduled_completed, progress_completed, all_completed, compl_completed;
    String compl_duration, compl_pass_percent, compl_test_mandatory, compl_no_of_que;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;

    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
    int compl_enrollment_key, scheduled_enrollment_key, progress_enrollment_key, all_enrollment_key,all_status,completed_status,scheduled_status,in_progress_status;
    String user_id, userid;

    String recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url;
    String upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url;

    ArrayList<Integer> arr_notifications_status = new ArrayList<>();
    ArrayList<String> arr_notifications_title = new ArrayList<>();
    ArrayList<String> arr_notifications = new ArrayList<>();
    ArrayList<String> arr_notifications_id = new ArrayList<>();
    ArrayList<String> arr_notifications_date = new ArrayList<>();

    JSONObject jObject1;
    JSONObject jObject;

    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    String teamchat_createdat;
    int teamchat_teamid;
    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;
    int teamchat_chatid;
    ArrayList<Integer> teammembers_teamid_arr = new ArrayList<>();
    ArrayList<String> teammembers_name_arr = new ArrayList<>();
    ArrayList<String> teammembers_photo_arr = new ArrayList<>();

    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
    ArrayList<String> teamchat_username_arr = new ArrayList<>();
    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();

    int team_members_teamid;
    String team_members_name;
    String team_members_photo;
    /* end server sync variables */

    String course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress;
    int enroll,started,completed,course_status;

    public AllCourses() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllCourses.
     */
    // TODO: Rename and change types and number of parameters
    public static AllCourses newInstance(String param1, String param2) {
        AllCourses fragment = new AllCourses();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_all_courses, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        refresh=(SwipeRefreshLayout)rootView.findViewById(R.id.swipe_refresh_layout);
        no_courses=(RelativeLayout)rootView.findViewById(R.id.no_courses);



         db1 = new DatabaseHandler(getActivity());
        courseAdapter = new CourseAdapter(courseList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(courseAdapter);
        courseList.clear();
        courseListAll.clear();
        prepareCourseData();
//        courseListAll.clear();
//        courseListAll=courseList;
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                courseAdapter = new CourseAdapter(courseList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(courseAdapter);
                courseList.clear();
                courseListAll.clear();
                prepareCourseData();
                refresh.setRefreshing(false);


            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                new_pos=Integer.parseInt(String.valueOf(view.findViewById(R.id.main_card).getTag()));

                        if (course_status_arr.get(new_pos)==1) {
                            Intent i = new Intent(getActivity(), CourseContents.class);
                            i.putExtra("course_id", course_id_arr.get(new_pos));
                            i.putExtra("course_started",course_started_arr.get(new_pos));
                            i.putExtra("course_enroll",course_enroll_arr.get(new_pos));
                            i.putExtra("course_name",course_name_arr.get(new_pos));
                            startActivity(i);
                        }
                        else
                        {
                            Toast.makeText(getActivity(),"This course is inactive",Toast.LENGTH_SHORT).show();
                        }
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
private void prepareCourseData()
{
    try
    {
        course_id_arr.clear();
        course_enroll_arr.clear();
        course_name_arr.clear();
        course_started_arr.clear();
        course_status_arr.clear();
        courseList.clear();
        int count=0;
        int course_tag;
        //retrieve all completed courses
        List<Elpfa> elpfa_all = db1.getAllCourses();
        for (Elpfa elp : elpfa_all) {
            course_id=elp.getAllCourseId();
            course_id_arr.add(count,course_id);
            course_started_arr.add(count, String.valueOf(elp.getAllCourseStarted()));
            course_enroll_arr.add(count, String.valueOf(elp.getAllEnrollmentKey()));
            course_name_arr.add(count, String.valueOf(elp.getAllCourseName()));
            course_status_arr.add(count,elp.getAllStatus());
            course_tag=count;
            count++;
            course_name = elp.getAllCourseName();
            enroll=elp.getAllEnrollmentKey();
            if (enroll==0)
            {
                course_type="Course Type : Assigned";
            }
            else
            {
                course_type="Course Type : Self Enroll";
            }
            started=elp.getAllCourseStarted();

            completed=elp.getAllCourseCompleted();
            if (started==0)
            {
                course_state="Course State : Not Started";
            }
            else if (completed==1)
            {
                course_state="Course State : Completed";
            }
            else if (started==1)
            {
                course_state="Course State : In Progress";
            }

            course_assign_date = elp.getAllAssignDate();
            course_progress=elp.getAllTrackCourse();
            course_path=elp.getAllCoursePath();
            course_status=elp.getAllStatus();
            Course course = new Course(course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress,course_tag,course_status);
            courseList.add(course);


            courseAdapter.notifyDataSetChanged();

            if (courseList.size()==0)
            {
                no_courses.setVisibility(View.VISIBLE);
            }
        }



    }
    catch (Exception e)
    {

    }

}
public CourseAdapter getCourseAdapter()
{
    return  this.courseAdapter;
}


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_course, menu);

        final MenuItem item = menu.findItem(R.id.menu_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                courseAdapter = new CourseAdapter(courseList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(courseAdapter);

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                final List<Course> filteredModelList = new ArrayList<>();
                filteredModelList.clear();
                for (Course model : courseList) {
                    final String text = model.getCourseName().toLowerCase();
                    if (text.contains(newText)) {
                        filteredModelList.add(model);
                    }
                }
                courseList_search.clear();
                courseList_search=filteredModelList;
                courseAdapter = new CourseAdapter(courseList_search);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(courseAdapter);
                //end filter
                return false;
            }
        });


        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        Toast.makeText(getActivity(),"collapsed",Toast.LENGTH_SHORT).show();
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        Toast.makeText(getActivity(),"expanded",Toast.LENGTH_SHORT).show();
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_search) {
            Toast.makeText(getActivity(), "search clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(courseAdapter);
        courseList.clear();
        courseListAll.clear();
        prepareCourseData();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
