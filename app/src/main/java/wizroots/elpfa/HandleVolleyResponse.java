package wizroots.elpfa;

/**
 * Created by Design on 17-01-2018.
 */

public interface HandleVolleyResponse {
    void onVolleyResponse(boolean isSuccess);
}
