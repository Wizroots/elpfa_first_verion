package wizroots.elpfa;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RecentWebinars.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RecentWebinars#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecentWebinars extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ListView lv1=null;
    ArrayList<String> webinar_ids= new ArrayList<>();
    ArrayList<String> webinar_name= new ArrayList<>();
    ArrayList<String> webinar_agenda= new ArrayList<>();
    ArrayList<String> webinar_starts_at= new ArrayList<>();
    ArrayList<String> webinar_ends_at= new ArrayList<>();
    ArrayList<String> webinar_duration= new ArrayList<>();
    ArrayList<String> webinar_path= new ArrayList<>();
    public static final String MyPREFERENCES = "MyPrefs";
    View rootView;
    RelativeLayout no_webinars;

    private List<Webinar> webinarList = new ArrayList<>();
    private RecyclerView recyclerView;
    public RecentWebinarAdapter webinarAdapter;
    ScrollView root;
    private OnFragmentInteractionListener mListener;
    ArrayList<String> webinar_id_arr= new ArrayList<>();
    String webinar_id_txt,webinar_name_txt,webinar_agenda_txt,webinar_starts_at_txt,webinar_ends_at_txt,webinar_duration_txt,webinar_path_txt;
    public RecentWebinars() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecentWebinars.
     */
    // TODO: Rename and change types and number of parameters
    public static RecentWebinars newInstance(String param1, String param2) {
        RecentWebinars fragment = new RecentWebinars();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_recent_webinars, container, false);
        no_webinars=(RelativeLayout)rootView.findViewById(R.id.no_webinars);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        webinarAdapter = new RecentWebinarAdapter(webinarList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(webinarAdapter);
        webinarList.clear();
        prepareWebinarData();
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void prepareWebinarData()
    {
        try {
            Log.d("webinar_check","1");
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            Log.d("webinar_check","2");
            webinar_id_arr.clear();
            Log.d("webinar_check","3");
            int count = 0;
            //retrieve all completed courses
            List<Elpfa> elpfa_all = db1.getRecentWebinars();
            int i = 0;
            for (Elpfa elp : elpfa_all) {
                Log.d("webinar_check","1");
//                WebinarPojo webPojo = new WebinarPojo();
                webinar_id_txt = elp.getRecentWebinar_id();
                webinar_name_txt = elp.getRecentWebinar_name();
                webinar_agenda_txt = elp.getRecentWebinar_agenda();
                webinar_starts_at_txt = elp.getRecentWebinar_starts_at();
                webinar_ends_at_txt = elp.getRecentWebinar_ends_at();
                webinar_duration_txt = elp.getRecentWebinar_duration();
                webinar_path_txt = elp.getRecentWebinar_room_url();

                //change start date format
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date start = df.parse(webinar_starts_at_txt);
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));


                //change end date format
                DateFormat dfEnd = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
                SimpleDateFormat sdfEnd = new SimpleDateFormat("yyyy-MM-dd");
                Date end = dfEnd.parse(webinar_ends_at_txt);
                sdfEnd.setTimeZone(TimeZone.getTimeZone("GMT"));


//                webPojo.setName(webinar_name_txt);
//                webPojo.setAgenda(webinar_agenda_txt);
//                webPojo.setStartDate(sdf.format(start));
//                webPojo.setEndDate(sdf.format(end));
//                webPojo.setDuration(webinar_duration_txt);
//                webPojo.setPath(webinar_path_txt);

//                webinar_name.add(i, webinar_name_txt);
//                webinar_agenda.add(i, webinar_agenda_txt);
//                webinar_starts_at.add(i, webinar_starts_at_txt);
//                webinar_ends_at.add(i, webinar_ends_at_txt);
//                webinar_duration.add(i, webinar_duration_txt);
//                webinar_path.add(i, webinar_path_txt);
//                results.add(webPojo);

                Webinar webinar = new Webinar(webinar_id_txt, webinar_name_txt, webinar_agenda_txt, webinar_starts_at_txt, webinar_ends_at_txt, webinar_duration_txt, webinar_path_txt,com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_OUT_SLOW_IN_INTERPOLATOR));
                webinarList.add(webinar);
                webinarAdapter.notifyDataSetChanged();
                if (webinarList.size()==0)
                {
                    no_webinars.setVisibility(View.VISIBLE);
                }
            }
        }


        catch (Exception e)
        {

        }

    }
}
