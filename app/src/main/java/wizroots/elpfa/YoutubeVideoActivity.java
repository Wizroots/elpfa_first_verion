package wizroots.elpfa;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import wizroots.elpfa.utils.JsonUtils;

public class YoutubeVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    @BindView(R.id.youtube_view)
    YouTubePlayerView youtubeView;
    @BindView(R.id.activity_youtube_player)
    RelativeLayout activityYoutubePlayer;

    private static final String TAG = YoutubeVideoActivity.class.getSimpleName();
    private String viodeoPath;
    private YouTubePlayer youTubePlayer;
    private int pos = 0;
    private boolean isFullScreen = false;
    private int isUpdated = 0;
    private DatabaseHandler databaseHandler;
    private int usersToContentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_video);


        setFullScreen();

        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        if (null != savedInstanceState){
            if (savedInstanceState.containsKey("seekTo")){
                pos = savedInstanceState.getInt("seekTo");
            }
        }

        viodeoPath = getIntent().getStringExtra("content_url");
        usersToContentId = getIntent().getIntExtra("users_to_content_id", 0);

        if (null == youTubePlayer)
            youtubeView.initialize(getResources().getString(R.string.youtube_api_key), this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
        Log.d(TAG, "player initialization success");
        this.youTubePlayer = youTubePlayer;

        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                isFullScreen = b;
            }
        });

        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                UpdateContentUpdateResult resultCallback = new UpdateContentUpdateResult() {
                    @Override
                    public void notifyResponse(int updated) {
                        isUpdated = updated;
                        if (updated == 1) {
                            int i = databaseHandler.updateContentStatus(new Elpfa("normal_video_completed", usersToContentId, 1));
                        }
                    }
                };
                JsonUtils.updateContentStatus(getBaseContext(), usersToContentId, resultCallback);
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {

            }
        });

        youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
            @Override
            public void onPlaying() {
                pos = youTubePlayer.getCurrentTimeMillis();
            }

            @Override
            public void onPaused() {
                pos = youTubePlayer.getCurrentTimeMillis();
            }

            @Override
            public void onStopped() {

            }

            @Override
            public void onBuffering(boolean b) {

            }

            @Override
            public void onSeekTo(int i) {
                pos = i;
            }
        });
        if (null != viodeoPath){
            youTubePlayer.cueVideo(viodeoPath);
            youTubePlayer.play();
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Log.d(TAG, "error initilizing player");
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (null != youTubePlayer)
            youTubePlayer.seekToMillis(pos);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("seekTo",pos);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pos = savedInstanceState.getInt("seekTo");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setFullScreen();
    }

    private void setFullScreen() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen)
            youTubePlayer.setFullscreen(false);
        else {
            boolean updated;
            if (isUpdated == 1){
                updated = true;
                Intent i = new Intent();
                i.putExtra("isUpdated", updated);
                setResult(RESULT_OK, i);
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
    }
}
