package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Contents {
    private String users_to_content_id;
    private String content_name;
    private int content_completed;
    private String content_type;
    private String content_path;
    private int content_started;
    private  int content_progress;
    private  String course_started;



    public Contents() {
    }

    public int getContent_completed() {
        return content_completed;
    }

    public Contents(String course_started, String users_to_content_id, String content_name, int content_completed, String content_type, String content_path, int content_started, int content_progress) {
        this.course_started=course_started;
        this.users_to_content_id=users_to_content_id;
        this.content_name=content_name;
        this.content_completed=content_completed;
        this.content_type=content_type;
        this.content_path=content_path;
        this.content_started=content_started;
        this.content_progress=content_progress;

    }

    public String getUsersToContentId() {
        return users_to_content_id;
    }

    public void setUsersToContentId(String users_to_content_id) {
        this.users_to_content_id = users_to_content_id;
    }

    public String getContentName() {
        return content_name;
    }

    public void setContentName(String content_name) {
        this.content_name = content_name;
    }

    public int getCourseCompleted() {
        return content_completed;
    }

    public void setContentCompleted(int content_completed) {
        this.content_completed = content_completed;
    }

    public String getContentType() {
        return content_type;
    }

    public void setContentType(String content_type) {
        this.content_type = content_type;
    }

    public String getContentPath() {
        return content_path;
    }

    public void setContentPath(String content_path) {
        this.content_path = content_path;
    }

    //content started
    public int getContentStarted() {
        return content_started;
    }

    public void setContentStarted(int content_started) {
        this.content_started = content_started;
    }

    //content progress
    public int getContentProgress() {
        return content_progress;
    }

    public void setContentProgress(int content_progress) {
        this.content_progress = content_progress;
    }

//course started
    public String getCourse_started() {
        return course_started;
    }

    public void setCourse_started(String course_started) {
        this.course_started = course_started;
    }

}
