package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Course {
    private String course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress;
    int course_tag,course_status;

    public Course() {
    }

    public Course(String course_id,String course_name, String course_type, String course_state,String course_assign_date,String course_path,String course_progress,int course_tag,int course_status) {
        this.course_id=course_id;
        this.course_name=course_name;
        this.course_type=course_type;
        this.course_state=course_state;
        this.course_assign_date=course_assign_date;
        this.course_path=course_path;
        this.course_progress=course_progress;
        this.course_tag=course_tag;
        this.course_status=course_status;
    }

    public String getCourseId() {
        return course_id;
    }

    public void setCourseId(String course_id) {
        this.course_id = course_id;
    }
    public String getCourseName() {
        return course_name;
    }

    public void setCourseName(String course_name) {
        this.course_name = course_name;
    }
    public String getCourseType() {
        return course_type;
    }

    public void setCourseType(String course_type) {
        this.course_id = course_type;
    }
    public String getCourseState() {
        return course_state;
    }

    public void setCourseState(String course_state) {
        this.course_state = course_state;
    }
    public String getCourseAssignDate() {
        return course_assign_date;
    }

    public void setCourseAssignDate(String course_assign_date) {
        this.course_assign_date = course_assign_date;
    }
    public String getCoursePath() {
        return course_path;
    }

    public void setCoursePath(String course_path) {
        this.course_path = course_path;
    }
    public String getCourseProgress() {
        return course_progress;
    }

    public void setCourseProgress(String course_progress) {
        this.course_progress = course_progress;
    }

    public int getCourseTag() {
        return course_tag;
    }

    public void setCourseTag(int course_tag) {
        this.course_tag = course_tag;
    }

    public int getCourseStatus() {
        return course_status;
    }

    public void setCourseStatus(int course_status) {
        this.course_status = course_status;
    }

}
