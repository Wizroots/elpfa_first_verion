package wizroots.elpfa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ScheduledCourses.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ScheduledCourses#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduledCourses extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<String> course_started_arr= new ArrayList<>();
    ArrayList<String> course_enroll_arr= new ArrayList<>();
    ArrayList<String> course_name_arr= new ArrayList<>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String user_id;
    SharedPreferences sharedpreferences;
    public List<Course> courseList_search = new ArrayList<>();
    ArrayList<Integer> course_status_arr= new ArrayList<>();
    public List<Course> courseListAll = new ArrayList<>();
    public static final String MyPREFERENCES = "MyPrefs";
    Toolbar toolbar;
    int new_pos;
    private OnFragmentInteractionListener mListener;
     View rootView;
    RelativeLayout no_courses;
    private List<Course> courseList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CourseAdapter courseAdapter;

    ArrayList<String> course_id_arr= new ArrayList<>();
//    ArrayList<String> course_name_arr= new ArrayList<>();
//    ArrayList<String> course_type_arr= new ArrayList<>();
//    ArrayList<String> course_state_arr= new ArrayList<>();
//    ArrayList<String> course_assign_date_arr= new ArrayList<>();
//    ArrayList<String> course_progress_arr= new ArrayList<>();
//    ArrayList<String> course_path_arr= new ArrayList<>();

    String course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress;
    int enroll,started,completed;
    int course_status;

    public ScheduledCourses() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllCourses.
     */
    // TODO: Rename and change types and number of parameters
    public static ScheduledCourses newInstance(String param1, String param2) {
        ScheduledCourses fragment = new ScheduledCourses();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_scheduled_courses, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        no_courses=(RelativeLayout)rootView.findViewById(R.id.no_courses);

        courseAdapter = new CourseAdapter(courseList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(courseAdapter);
        courseList.clear();
        courseListAll.clear();
        prepareCourseData();
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                new_pos=Integer.parseInt(String.valueOf(view.findViewById(R.id.main_card).getTag()));

                if (course_status_arr.get(new_pos)==1) {
                    Intent i = new Intent(getActivity(), CourseContents.class);
                    i.putExtra("course_id", course_id_arr.get(new_pos));
                    i.putExtra("course_started",course_started_arr.get(new_pos));
                    i.putExtra("course_enroll",course_enroll_arr.get(new_pos));
                    i.putExtra("course_name",course_name_arr.get(new_pos));
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(getActivity(),"This course is inactive",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
private void prepareCourseData()
{
    try
    {
        DatabaseHandler db1 = new DatabaseHandler(getActivity());
        course_id_arr.clear();
        course_enroll_arr.clear();
        course_name_arr.clear();
        course_started_arr.clear();
        course_status_arr.clear();
        int count=0;
        int course_tag;
        //retrieve all completed courses
        List<Elpfa> elpfa_all = db1.getScheduledCourses();
        for (Elpfa elp : elpfa_all) {
            course_id=elp.getScheduledCourseId();
            course_id_arr.add(count,course_id);
            course_started_arr.add(count, String.valueOf(elp.getAllCourseStarted()));
            course_enroll_arr.add(count, String.valueOf(elp.getAllEnrollmentKey()));
            course_name_arr.add(count, String.valueOf(elp.getAllCourseName()));
            course_status_arr.add(count,elp.getScheduledStatus());
            course_tag=count;
            count++;
            course_name = elp.getScheduledCourseName();
            enroll=elp.getScheduledEnrollmentKey();
            if (enroll==0)
            {
                course_type="Course Type : Assigned";
            }
            else
            {
                course_type="Course Type : Self Enroll";
            }
            started=elp.getScheduledCourseStarted();
            completed=elp.getScheduledCourseCompleted();
            if (started==0)
            {
                course_state="Course State : Not Started";
            }
            else if (completed==1)
            {
                course_state="Course State : Completed";
            }
            else if (started==1)
            {
                course_state="Course State : In Progress";
            }

            course_assign_date = elp.getScheduledAssignDate();
            course_progress=elp.getScheduledTrackCourse();
            course_path=elp.getScheduledCoursePath();
            course_status=elp.getScheduledStatus();
            Course course = new Course(course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress,course_tag,course_status);
            courseList.add(course);
            courseAdapter.notifyDataSetChanged();
            if (courseList.size()==0)
            {
                no_courses.setVisibility(View.VISIBLE);
            }
        }

    }
    catch (Exception e)
    {

    }

}
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_course, menu);

        final MenuItem item = menu.findItem(R.id.menu_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                courseAdapter = new CourseAdapter(courseList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(courseAdapter);

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                final List<Course> filteredModelList = new ArrayList<>();
                filteredModelList.clear();
                for (Course model : courseList) {
                    final String text = model.getCourseName().toLowerCase();
                    if (text.contains(newText)) {
                        filteredModelList.add(model);
                    }
                }
                courseList_search.clear();
                courseList_search=filteredModelList;
                courseAdapter = new CourseAdapter(courseList_search);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(courseAdapter);
                //end filter
                return false;
            }
        });


        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        Toast.makeText(getActivity(),"collapsed",Toast.LENGTH_SHORT).show();
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        Toast.makeText(getActivity(),"expanded",Toast.LENGTH_SHORT).show();
                        return true; // Return true to expand action view
                    }
                });
    }
    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(courseAdapter);
        courseList.clear();
        courseListAll.clear();
        prepareCourseData();
    }
}
