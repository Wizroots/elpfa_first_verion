package wizroots.elpfa;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.MyViewHolder> {

    private List<Contents> courseList;
    Context context;
    ContentClickListener contentClickListener;

    public interface ContentClickListener{
        void onContentClicked(int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView content_name;
        public ImageView content_tick_img;
        public com.vipul.hp_hp.timelineview.TimelineView marker;
        public ImageView lesson_type_image;
        LinearLayout layoutMain;

        public MyViewHolder(View view) {
            super(view);
            content_name = (TextView) view.findViewById(R.id.content_name);
            marker = (com.vipul.hp_hp.timelineview.TimelineView) view.findViewById(R.id.marker);
            lesson_type_image = (ImageView) view.findViewById(R.id.lesson_type_image);
            layoutMain = (LinearLayout) view.findViewById(R.id.root);
        }
    }


    public ContentAdapter(Context context, List<Contents> courseList, ContentClickListener contentClickListener) {
        this.context = context;
        this.courseList = courseList;
        this.contentClickListener = contentClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Contents content = courseList.get(position);
        holder.content_name.setText(content.getContentName());
        if (content.getContent_completed() == 1)
        {
            holder.marker.setMarker(ContextCompat.getDrawable(context, R.drawable.circular_bk_blue));
        }
        else
        {
            holder.marker.setMarker(ContextCompat.getDrawable(context, R.drawable.circular_bk_transparent_blue_border));
        }
        if (content.getContentType().equalsIgnoreCase("Text"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.text);
        }
        else if (content.getContentType().equalsIgnoreCase("Video"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.video);
        }
        else  if (content.getContentType().equalsIgnoreCase("Link"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.link);
        }
        else if (content.getContentType().equalsIgnoreCase("Gallery"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.gallery);
        }
        else if (content.getContentType().equalsIgnoreCase("Test"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.test);
        }
        else if (content.getContentType().equalsIgnoreCase("pdf"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.pdf);
        }
        else if (content.getContentType().equalsIgnoreCase("youtube"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.ic_youtube);
        }

        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contentClickListener.onContentClicked(position);
            }
        });
//        Picasso.with(holder.content_tick_img.getContext()).load(R.drawable.tick_course).into(holder.content_tick_img);

    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }
}