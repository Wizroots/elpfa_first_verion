package wizroots.elpfa.utils;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import wizroots.elpfa.AppController;
import wizroots.elpfa.Config;
import wizroots.elpfa.DatabaseHandler;
import wizroots.elpfa.Elpfa;
import wizroots.elpfa.UpdateContentUpdateResult;

/**
 * Created by Design on 10-01-2018.
 */

public class JsonUtils {

    private static final String TAG  = JsonUtils.class.getSimpleName();

    public static void updateContentStatus(Context context, final int contentId, final UpdateContentUpdateResult resultCallback){

        final DatabaseHandler databaseHandler = new DatabaseHandler(context);




        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.update_content_status, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "response : " + response);
                if (response.equalsIgnoreCase("100")){
                    databaseHandler.updateContentStatus(new Elpfa("normal_video_completed", contentId, 1));
                    resultCallback.notifyResponse(1);
                }
                else {
                    resultCallback.notifyResponse(0);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "error : " + error);
                resultCallback.notifyResponse(0);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("value1", String.valueOf(contentId));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
