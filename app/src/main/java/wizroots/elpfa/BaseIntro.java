package wizroots.elpfa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.github.paolorotolo.appintro.AppIntro;

/**
 * Created by avluis on 08/08/2016.
 * Shared methods between classes
 */
public class BaseIntro extends AppIntro {

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    void loadDomainActivity() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();


        /**
         * added to remove DomainSelection page in between intro and login
         * remove this and redirect to Domainelection.java instead of Login.java later if we need to select domain
         */
        editor.putString("domain","sample_domain");
        editor.commit();


//        Intent intent = new Intent(this, DomainSelection.class);
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }
}
