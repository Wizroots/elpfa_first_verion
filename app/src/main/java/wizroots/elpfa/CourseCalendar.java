package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class CourseCalendar {
    private String course_id,course_name,course_assign_date,course_path;

    public CourseCalendar() {
    }

    public CourseCalendar(String course_id, String course_name, String course_assign_date, String course_path) {
        this.course_id=course_id;
        this.course_name=course_name;
        this.course_assign_date=course_assign_date;
        this.course_path=course_path;
    }

    public String getCourseId() {
        return course_id;
    }

    public void setCourseId(String course_id) {
        this.course_id = course_id;
    }
    public String getCourseName() {
        return course_name;
    }

    public void setCourseName(String course_name) {
        this.course_name = course_name;
    }


    public String getCourseAssignDate() {
        return course_assign_date;
    }

    public void setCourseAssignDate(String course_assign_date) {
        this.course_assign_date = course_assign_date;
    }
    public String getCoursePath() {
        return course_path;
    }

    public void setCoursePath(String course_path) {
        this.course_path = course_path;
    }


}
