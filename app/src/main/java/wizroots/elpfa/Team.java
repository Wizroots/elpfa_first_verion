package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Team {
    private String team_id;
    private String team_name;
    private String team_description;
    private String team_image;
    private int team_tag;

    public Team() {
    }

    public Team(String team_id, String team_name, String team_description, String team_image, int team_tag) {
        this.team_id = team_id;
        this.team_name = team_name;
        this.team_description = team_description;
        this.team_image = team_image;
        this.team_tag = team_tag;
        this.team_description = team_description;
    }

    //team id
    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    //team name
    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    //team description
    public String getTeam_description() {
        return team_description;
    }

    public void setTeam_description(String team_description) {
        this.team_description = team_description;
    }

    //team image
    public String getTeam_image() {
        return team_image;
    }

    public void setTeam_image(String team_image) {
        this.team_image = team_image;
    }


    //team tag
    public int getTeam_tag() {
        return team_tag;
    }

    public void setTeam_tag(int team_tag) {
        this.team_tag = team_tag;
    }



}
