package wizroots.elpfa;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CourseContents extends AppCompatActivity implements ContentAdapter.ContentClickListener{
    private Toolbar mToolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    String course_id;
    DatabaseHandler db1;
    String course_name;
    String course_image_str;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;

    String user_id;
    String course_description;
    TextView course_type_txt;
    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int scheduled_completed, progress_completed, all_completed, compl_completed;

    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;
    int compl_enrollment_key,compl_status, scheduled_enrollment_key,scheduled_status, progress_enrollment_key, progress_status,all_enrollment_key,all_status;

    String course_assign_date;
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;

    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
    int height, width;
    String course_no_of_que;
    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;

    String course_duration;
    int course_pass_percent;
    int course_completed;
    String enroll_contents_course_id;
    String enroll_contents_name;
    String course_test_mandatory;
    JSONObject jObject;
    String course_completion_date;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;
    int course_status;
    String course_progress;
    RelativeLayout state_rel;
    ImageView course_close;
    TextView course_name_dialog, course_description_dialog, course_assign_date_dialog, course_type_dialog;
    RelativeLayout state_rel_dialog;
    TextView state_txt_dialog;
    ImageView left_dialog, right_dialog;
    ImageView course_image_dialog;
    ImageView left,right;
    TextView state_txt;
    TextView course_name_txt;
    TextView course_description_txt;
    ImageView course_image;
    TextView course_assign_date_txt;
    String number_started, number_assigned, number_not_started, number_completed;
    String course_enroll_intent;
    String enroll_course_name_intent;
    String course_started_intent;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    Button see_more;
    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();
//    RelativeLayout state_rel_see_more;
//    DonutProgress course_progress_progressbar,course_progress_see_more;
wizroots.elpfa.progressbar.NumberProgressBar numberProgressBar,numberProgressBar_dialog;
    int course_enroll;
    String users_to_course_id;
    String course_type;
    String users_to_content_id;
    String content_completed;
    String content_name;
    String content_type;
    String content_path;
    int count;
    Toolbar toolbar;
    RelativeLayout navigation_layout;
    ImageButton close_btn;
    TextView course_name_see_more;
    TextView course_des;
    TextView course_date_see_more;
//    RelativeLayout state_rel;
    Button test;
    TextView course_state_txt,course_state_see_more;
    String course_state;
    int started,completed;
    String course_type_dialog_txt;
    ArrayList<String> users_to_content_id_arr= new ArrayList<>();
    ArrayList<String> content_completed_arr= new ArrayList<>();
    ArrayList<String> content_type_arr= new ArrayList<>();
    ArrayList<String> content_name_arr= new ArrayList<>();
    ArrayList<String> content_path_arr= new ArrayList<>();
    private List<Contents> contentList = new ArrayList<>();
    Button start_enroll_now;
    private ContentAdapter contentAdapter;
    private RecyclerView recyclerView;
    DrawerLayout mDrawerLayout;
    int navigation_flag=0;

    private static final String TAG = CourseContents.class.getSimpleName();

    private static final int REQUEST_CODE_CONTENT = 10;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_contents);
        toolbar=(Toolbar)findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigation_flag==0) {
                    CourseContents.this.finish();
                }
            }
        });
        course_name_txt=(TextView)findViewById(R.id.course_name);
        course_description_txt=(TextView)findViewById(R.id.course_description);
        course_assign_date_txt=(TextView)findViewById(R.id.course_assign_date);
        course_image=(ImageView)findViewById(R.id.course_image);
        course_type_txt=(TextView)findViewById(R.id.course_type);
        numberProgressBar=(wizroots.elpfa.progressbar.NumberProgressBar)findViewById(R.id.number_progressbar);
        start_enroll_now=(Button)findViewById(R.id.start_enroll_now);
//        course_progress_see_more=(DonutProgress)findViewById(R.id.course_progress_see_more);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        state_rel=(RelativeLayout)findViewById(R.id.state_rel);
        left=(ImageView)findViewById(R.id.left);
        right=(ImageView)findViewById(R.id.right);
        state_txt=(TextView)findViewById(R.id.state_txt);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

//        state_rel_see_more=(RelativeLayout)findViewById(R.id.state_rel_see_more);
//        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
//        navigation_layout=(RelativeLayout)findViewById(R.id.navigation_layout);
//        close_btn=(ImageButton)findViewById(R.id.close_btn);
//        course_name_see_more=(TextView)findViewById(R.id.course_name_see_more);
//        course_state_see_more=(TextView)findViewById(R.id.course_state_see_more);
//        course_date_see_more=(TextView)findViewById(R.id.course_date_see_more);
//        state_rel=(RelativeLayout) findViewById(R.id.state_rel);
//        course_des=(TextView) findViewById(R.id.course_des);
//        course_state_txt=(TextView) findViewById(R.id.course_state);

        course_id=getIntent().getStringExtra("course_id");
        course_enroll_intent=getIntent().getStringExtra("course_enroll");
        course_started_intent=getIntent().getStringExtra("course_started");
        enroll_course_name_intent=getIntent().getStringExtra("course_name");

        test=(Button) findViewById(R.id.test);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (started==0)
                {
                    if (course_enroll==1)
                    {
                        Toast.makeText(getApplicationContext(),"Enroll course",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Start course",Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Intent i=new Intent(getApplicationContext(),TestInstructions.class);
                    i.putExtra("course_id",course_id);
                    startActivity(i);
                }
            }
        });
//        int width = getResources().getDisplayMetrics().widthPixels;
//        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) navigation_layout.getLayoutParams();
//        params.width = width;
//        navigation_layout.setLayoutParams(params);
//        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        see_more=(Button)findViewById(R.id.see_more);
//        see_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                navigation_flag=1;
//                mDrawerLayout.openDrawer(Gravity.RIGHT); //Edit Gravity.START need API 14
//               /* final Dialog dialog = new Dialog(CourseContents.this);
//                dialog.setContentView(R.layout.see_more_dialog);
//                dialog.setCanceledOnTouchOutside(false);
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                lp.copyFrom(dialog.getWindow().getAttributes());
//                lp.width = lp.MATCH_PARENT;
//                lp.height = lp.MATCH_PARENT;
//                dialog.getWindow().setAttributes(lp);
//
//
//
//
//
//
//
//                // there are a lot of settings, for dialog, check them all out!
//                // set up radiobutton
//
//                final TextView description = (TextView) dialog.findViewById(R.id.description);
//
//                dialog.show();*/
//            }
//        });
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDrawerLayout.closeDrawer(Gravity.RIGHT);
//                navigation_flag=0;
//
//            }
//        });
        contentAdapter = new ContentAdapter(this,contentList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(contentAdapter);
        if (course_started_intent.equals("0"))
        {
            if (course_enroll_intent.equals("1"))
            {
                prepareEnrollContents();
            }
            else {
                prepareContents();
            }
        }
        else
        {
            prepareContents();
        }


        collapsingToolbarLayout.setTitle(""+course_name);

        collapsingToolbarLayout.setExpandedTitleColor(Color.argb(8,255, 255, 255));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                if (started==0)
                {
                    if (course_enroll==1)
                    {
                        Toast.makeText(getApplicationContext(),"Enroll course",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Start course",Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));

        course_description_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(CourseContents.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);




                dialog1.setContentView(R.layout.course_details);
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                width = size.x;
                height = size.y;
                int new_height=height-300;
                int new_width=width-100;
                dialog1.getWindow().setLayout(new_width, new_height);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                course_close=(ImageView)dialog1.findViewById(R.id.close);
                course_image_dialog=(ImageView)dialog1.findViewById(R.id.course_image);
                left_dialog=(ImageView)dialog1.findViewById(R.id.left);
                right_dialog=(ImageView)dialog1.findViewById(R.id.right);
                course_name_dialog=(TextView) dialog1.findViewById(R.id.course_name);
                course_description_dialog=(TextView) dialog1.findViewById(R.id.course_description);
                course_assign_date_dialog=(TextView) dialog1.findViewById(R.id.course_assign_date);
                course_type_dialog=(TextView) dialog1.findViewById(R.id.course_type);
                state_txt_dialog=(TextView) dialog1.findViewById(R.id.state_txt);
                state_rel_dialog=(RelativeLayout) dialog1.findViewById(R.id.state_rel);
                numberProgressBar_dialog=(wizroots.elpfa.progressbar.NumberProgressBar)dialog1.findViewById(R.id.number_progressbar);


                course_name_dialog.setText(""+course_name);
                course_description_dialog.setText(""+course_description);
                course_assign_date_dialog.setText("Start date: "+course_assign_date);
                course_type_dialog.setText(""+course_type_dialog_txt);
                Picasso.with(getApplicationContext()).load(course_image_str).into(course_image_dialog);
                if (course_state.equals("Course State : Not Started"))
                {
                    numberProgressBar.setProgressTextVisibility(wizroots.elpfa.progressbar.NumberProgressBar.ProgressTextVisibility.Invisible);

                    if (course_type.equals("Course Type : Assigned"))
                    {
                        state_rel_dialog.setVisibility(View.VISIBLE);
                        state_txt_dialog.setText("Start now");
                        numberProgressBar_dialog.setProgressTextSize(0);
                        left_dialog.setImageResource(R.drawable.circle_holo_white);
                        right_dialog.setImageResource(R.drawable.circle_holo_white);
                        numberProgressBar_dialog.setProgress(0);
                    }
                    else if (course_type.equals("Course Type : Self Enroll"))
                    {

                        state_rel_dialog.setVisibility(View.VISIBLE);
                        state_txt_dialog.setText("Enroll now");

                        left_dialog.setImageResource(R.drawable.circle_holo_white);
                        right_dialog.setImageResource(R.drawable.circle_holo_white);
                        numberProgressBar_dialog.setProgress(0);
                        numberProgressBar_dialog.setProgressTextSize(0);
                    }
                }
                else if (course_state.equals("Course State : Completed"))
                {
                    numberProgressBar.setProgressTextVisibility(wizroots.elpfa.progressbar.NumberProgressBar.ProgressTextVisibility.Invisible);
                    state_rel_dialog.setVisibility(View.VISIBLE);
                    state_txt_dialog.setText("Completed");
                    left_dialog.setImageResource(R.drawable.circle_bk_white);
                    right_dialog.setImageResource(R.drawable.circle_bk_white);
                    numberProgressBar_dialog.setProgress(100);
                }
                else if (course_state.equals("Course State : In Progress"))
                {
                    if (course_progress.equals("0"))
                    {
                        numberProgressBar.setProgressTextVisibility(wizroots.elpfa.progressbar.NumberProgressBar.ProgressTextVisibility.Invisible);
                        numberProgressBar_dialog.setProgress(Integer.parseInt(course_progress));
                        left_dialog.setImageResource(R.drawable.circle_bk_white);
                        right_dialog.setImageResource(R.drawable.circle_holo_white);
                        state_rel_dialog.setVisibility(View.VISIBLE);
                        state_txt_dialog.setText("Started");
                        numberProgressBar_dialog.setProgressTextSize(0);
                    }
                    else if (course_progress.equals("100"))
                    {
                        numberProgressBar_dialog.setProgress(100);
                        numberProgressBar.setProgressTextVisibility(wizroots.elpfa.progressbar.NumberProgressBar.ProgressTextVisibility.Invisible);
                        left_dialog.setImageResource(R.drawable.circle_bk_white);
                        right_dialog.setImageResource(R.drawable.circle_bk_white);
                        state_rel_dialog.setVisibility(View.VISIBLE);
                        state_txt_dialog.setText("Completed");
                    }
                    else
                    {
                        numberProgressBar_dialog.setProgress(Integer.parseInt(course_progress));
                        left_dialog.setImageResource(R.drawable.circle_bk_white);
                        right_dialog.setImageResource(R.drawable.circle_holo_white);
                        state_rel_dialog.setVisibility(View.INVISIBLE);
                    }
                }

                course_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        Toast.makeText(getApplicationContext(),"close",Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(),""+course_description,Toast.LENGTH_SHORT).show();
                        dialog1.cancel();
                    }
                });
////                full = (ImageView) dialog1.findViewById(R.id.list);
//                String path=sharedpreferences.getString("photo","no_img");
//                if (path.equals("no_img"))
//                {
//                    Toast.makeText(getApplicationContext(),"No picture",Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//                    profile_color_text.setVisibility(View.INVISIBLE);
//                    img.setColorFilter(getResources().getColor(R.color.transparent));
//                    Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(full);
//                }

                dialog1.show();
            }
        });
        start_enroll_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check internet connection. if connected, start course
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if (isConnected == true) {
                    ProgressDialog pd=new ProgressDialog(CourseContents.this);
                    pd.setMessage("Please wait");
                    pd.show();
                    if (course_enroll==0)
                    {
                        //if course is not self em=nroll course
                        //update course start status in database
                        try {

                            Config url_details = new Config();
                            String tag_string_req = "req_state";
                            StringRequest strReq = new StringRequest(Request.Method.POST,
                                    url_details.StartAssignedCourse, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response.equals("updated"))
                                    {
                                        db1.updateAllCourseStarted(new Elpfa("start_assigned_all",users_to_course_id));
                                        db1.addInProgressCourses(new Elpfa("in_progress", course_id, users_to_course_id, course_name, course_description, course_duration, course_enroll, course_status,course_image_str, course_assign_date, course_progress, course_no_of_que, course_progress, course_test_mandatory, 1, course_completed, course_completion_date));
                                        db1.deleteFromScheduledCourses(new Elpfa("delete_from_scheduled",users_to_course_id));
                                        course_started_intent= String.valueOf(1);

//                                        db1.updateInProgressCourseStarted(new Elpfa("start_assigned_inprogress",users_to_course_id));
                                        prepareContents();
                                        start_enroll_now.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),"Something went wrong, try again",Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("value1", users_to_course_id);
                                    return params;
                                }

                            };
                            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                        } catch (Exception ex) {
                        }
                        pd.cancel();
                    }
                    else
                    {

                        //if course is self enrolled
                        //if course is not self em=nroll course
                        //update course start status in database
                        try {

                            Config url_details = new Config();
                            String tag_string_req = "req_state";
                            StringRequest strReq = new StringRequest(Request.Method.POST,
                                    url_details.EnrollNowNew, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if (response.equals("Successfully enrolled"))
                                    {
                                        getCoursesAndContentsAfterEnroll();
                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),"Something went wrong, try again",Toast.LENGTH_SHORT).show();
                                    }
//                                    if (response.equals("updated"))
//                                    {
//                                        db1.updateAllCourseStarted(new Elpfa("start_assigned_all",users_to_course_id));
//                                        db1.addInProgressCourses(new Elpfa("in_progress", course_id, users_to_course_id, course_name, course_description, course_duration, course_enroll, course_status,course_image_str, course_assign_date, course_progress, course_no_of_que, course_progress, course_test_mandatory, 1, course_completed, course_completion_date));
//                                        db1.deleteFromScheduledCourses(new Elpfa("delete_from_scheduled",users_to_course_id));
//                                        course_started_intent= String.valueOf(1);
//
////                                        db1.updateInProgressCourseStarted(new Elpfa("start_assigned_inprogress",users_to_course_id));
//                                        prepareContents();
//                                        start_enroll_now.setVisibility(View.GONE);
//                                    }
//                                    else
//                                    {
//                                        Toast.makeText(getApplicationContext(),"Something went wrong, try again",Toast.LENGTH_SHORT).show();
//                                    }

                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("value1", course_id);
                                    params.put("value2", sharedpreferences.getString("User_id",null));
                                    params.put("value3", "1999-01-01");
                                    return params;
                                }

                            };
                            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                        } catch (Exception ex) {
                        }
                        pd.cancel();
                    }
                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(findViewById(R.id.root), "check internet connection and try again", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    final TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextSize(18);
                    snackBarView.setBackgroundColor(Color.rgb(236, 42, 42));
                    tv.setTextColor(Color.rgb(255, 255, 255));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    else
                        tv.setGravity(Gravity.CENTER_HORIZONTAL);
                    snackbar.show();
                }
            }
        });
   }
    public void prepareContents()
    {
        contentList.clear();
        db1=new DatabaseHandler(getApplicationContext());
        course_id=getIntent().getStringExtra("course_id");

        List<Elpfa> elpfa_course = db1.getCourseDetails(Integer.parseInt(course_id));
        for (Elpfa elp : elpfa_course) {
            course_name=elp.getAllCourseName();
            course_description=elp.getAllDescription();
            course_assign_date=elp.getAllAssignDate();
            course_progress=elp.getAllTrackCourse();
            course_status=elp.getAllStatus();
            course_duration= String.valueOf(Integer.parseInt(elp.getAllDuration()));
            course_enroll=elp.getAllEnrollmentKey();
            users_to_course_id=elp.getAllUsersToCourseId();
            if (course_enroll==1)
            {
                start_enroll_now.setText("Enroll Now");
            }
            else  if (course_enroll==0)
            {
                start_enroll_now.setText("Start Now");
            }
            course_image_str=elp.getAllCoursePath();
            if (course_enroll==0)
            {
                course_type="Course Type : Assigned";
                course_type_txt.setText("Course type: Assigned");
                course_type_dialog_txt="Course type : Assigned";
            }
            else
            {
                course_type="Course Type : Self Enroll";
                course_type_txt.setText("Course type: Self enroll");
                course_type_dialog_txt="Course type : Self enroll";
            }
            started=elp.getAllCourseStarted();
            if (started==1)
            {
                start_enroll_now.setVisibility(View.GONE);
            }
            else
            {
                start_enroll_now.setVisibility(View.VISIBLE);
            }
            completed=elp.getAllCourseCompleted();
            if (started==0)
            {
                course_state="Course State : Not Started";
            }
            else if (completed==1)
            {
                course_state="Course State : Completed";
            }
            else if (started==1)
            {
                course_state="Course State : In Progress";
            }
        }
        course_name_txt.setText(""+course_name);
//        course_name_see_more.setText(""+course_name);
        course_description_txt.setText(""+course_description);
//        course_des.setText(""+course_description);
        course_assign_date_txt.setText("Assign date: "+course_assign_date);



        Picasso.with(getApplicationContext()).load(course_image_str).into(course_image);
//        course_date_see_more.setText(""+course_assign_date);
//        course_progress_progressbar.setProgress(Integer.parseInt(course_progress));
//        course_progress_see_more.setProgress(Integer.parseInt(course_progress));

        if (course_enroll==1)
        {
            course_assign_date_txt.setVisibility(View.INVISIBLE);
//            course_date_see_more.setVisibility(View.INVISIBLE);
        }
        if (course_enroll==0)
        {
            course_type="Course Type : Assigned";
        }
        else
        {
            course_type="Course Type : Self Enroll";
        }
        if (course_state.equals("Course State : Not Started"))
        {

            if (course_type.equals("Course Type : Assigned"))
            {
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Start now");
                left.setImageResource(R.drawable.circle_holo_white);
                right.setImageResource(R.drawable.circle_holo_white);
                //numberProgressBar.setProgressTextSize(0);
                numberProgressBar.setProgress(0);
            }
            else if (course_type.equals("Course Type : Self Enroll"))
            {
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Enroll now");
                left.setImageResource(R.drawable.circle_holo_white);
                right.setImageResource(R.drawable.circle_holo_white);
              //  numberProgressBar.setProgressTextSize(0);
               // numberProgressBar_dialog.setProgressTextSize(0);
                numberProgressBar.setProgress(0);
            }
        }
        else if (course_state.equals("Course State : Completed"))
        {
            state_rel.setVisibility(View.VISIBLE);
            state_txt.setText("Completed");
            left.setImageResource(R.drawable.circle_bk_white);
            right.setImageResource(R.drawable.circle_bk_white);
           // numberProgressBar.setProgressTextSize(0);
           // numberProgressBar_dialog.setProgressTextSize(0);
            numberProgressBar.setProgress(100);
        }
        else if (course_state.equals("Course State : In Progress"))
        {
            if (course_progress.equals("0"))
            {
                numberProgressBar.setProgress(Integer.parseInt(course_progress));
                //numberProgressBar.setProgressTextSize(0);
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_holo_white);
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Started");
            }
            else if (course_progress.equals("100"))
            {
                numberProgressBar.setProgress(100);
              //  numberProgressBar.setProgressTextSize(0);
              //  numberProgressBar_dialog.setProgressTextSize(0);
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_bk_white);
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Completed");
            }
            else
            {
                numberProgressBar.setProgress(Integer.parseInt(course_progress));
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_holo_white);
                state_rel.setVisibility(View.INVISIBLE);
            }
        }
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.back_white);
        mToolbar.setTitleTextColor(Color.rgb(255,255,255));
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setContentScrimColor(Color.rgb(25,118,210));
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(255,255,255));
        AppBarLayout appBarLayout=(AppBarLayout)findViewById(R.id.app_bar_layout);

        //get allcourse contents
        users_to_content_id_arr.clear();
        content_completed_arr.clear();
        content_path_arr.clear();
        content_name_arr.clear();
        content_type_arr.clear();

        count=0;
        setCourseContents();
        }

    private void setCourseContents() {
        contentList.clear();
        recyclerView.setAdapter(contentAdapter);
        count = 0;
        List<Elpfa> elpfa_course_contents = db1.getCourseContents(Integer.parseInt(course_id));
        for (Elpfa elp : elpfa_course_contents) {
            users_to_content_id = String.valueOf(elp.getUsersToContentsUsersToContentId());
            content_completed = String.valueOf(elp.getUsersToContentsCompleted());
            content_type = elp.getUsersToContentsType();
            content_name = elp.getUsersToContentsName();
            content_path = elp.getUsersToContentsPath();
            users_to_content_id_arr.add(count, users_to_content_id);
            content_completed_arr.add(count, content_completed);
            content_type_arr.add(count, content_type);
            content_name_arr.add(count, content_name);
            content_path_arr.add(count, content_path);
            //just for dummy values
            int started;
            int progress;
            if (count == 0) {
                started = 1;
            } else {
                started = 1;
            }
            if (count == 0) {
                progress = 0;
            } else if (count == 1) {
                progress = 60;
            } else {
                progress = 100;
            }

            //end dummy

            count++;
            Contents contents = new Contents(course_started_intent, users_to_content_id, content_name, Integer.parseInt(content_completed), content_type, content_path, started, progress);
            contentList.add(contents);
        }

            contentAdapter.notifyDataSetChanged();

        setTestVisibility();
    }

    private void setTestVisibility() {
        boolean isAllcompleted = true;
        for (int i=0;i<content_completed_arr.size();i++){
            if (content_completed_arr.get(i).equalsIgnoreCase("0")){
                isAllcompleted = false;
            }
        }
        if (isAllcompleted)
            test.setVisibility(View.VISIBLE);
        else
            test.setVisibility(View.GONE);

        if (completed==1){
            test.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
    public void prepareEnrollContents()
    {
        contentList.clear();
        db1=new DatabaseHandler(getApplicationContext());
        course_id=getIntent().getStringExtra("course_id");
        List<Elpfa> elpfa_course = db1.getCourseDetails(Integer.parseInt(course_id));
        for (Elpfa elp : elpfa_course) {
            course_name=elp.getAllCourseName();
            course_description=elp.getAllDescription();
            course_assign_date=elp.getAllAssignDate();
            course_progress=elp.getAllTrackCourse();
            course_status=elp.getAllStatus();
            course_duration= String.valueOf(Integer.parseInt(elp.getAllDuration()));
            course_enroll=elp.getAllEnrollmentKey();
            users_to_course_id=elp.getAllUsersToCourseId();
            if (course_enroll==1)
            {
                start_enroll_now.setText("Enroll Now");
            }
            else  if (course_enroll==0)
            {
                start_enroll_now.setText("Start Now");
            }
            course_image_str=elp.getAllCoursePath();
            if (course_enroll==0)
            {
                course_type="Course Type : Assigned";
                course_type_txt.setText("Course type: Assigned");
                course_type_dialog_txt="Course type : Assigned";
            }
            else
            {
                course_type="Course Type : Self Enroll";
                course_type_txt.setText("Course type: Self enroll");
                course_type_dialog_txt="Course type : Self enroll";
            }
            started=elp.getAllCourseStarted();
            if (started==1)
            {
                start_enroll_now.setVisibility(View.GONE);
            }
            else
            {
                start_enroll_now.setVisibility(View.VISIBLE);
            }
            completed=elp.getAllCourseCompleted();
            if (started==0)
            {
                course_state="Course State : Not Started";
            }
            else if (completed==1)
            {
                course_state="Course State : Completed";
            }
            else if (started==1)
            {
                course_state="Course State : In Progress";
            }

        }
        course_name_txt.setText(""+course_name);
        course_description_txt.setText(""+course_description);
        course_assign_date_txt.setText("Assign date: "+course_assign_date);
        Picasso.with(getApplicationContext()).load(course_image_str).into(course_image);
        if (course_enroll==1)
        {
            course_assign_date_txt.setVisibility(View.INVISIBLE);
        }
        if (course_enroll==0)
        {
            course_type="Course Type : Assigned";
        }
        else
        {
            course_type="Course Type : Self Enroll";
        }
        if (course_state.equals("Course State : Not Started"))
        {

            if (course_type.equals("Course Type : Assigned"))
            {
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Start now");
                left.setImageResource(R.drawable.circle_holo_white);
                right.setImageResource(R.drawable.circle_holo_white);
               // numberProgressBar.setProgressTextSize(0);
                numberProgressBar.setProgress(0);
            }
            else if (course_type.equals("Course Type : Self Enroll"))
            {
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Enroll now");
                left.setImageResource(R.drawable.circle_holo_white);
                right.setImageResource(R.drawable.circle_holo_white);
             //   numberProgressBar.setProgressTextSize(0);
                numberProgressBar.setProgress(0);
            }
        }
        else if (course_state.equals("Course State : Completed"))
        {
            state_rel.setVisibility(View.VISIBLE);
            state_txt.setText("Completed");
            left.setImageResource(R.drawable.circle_bk_white);
            right.setImageResource(R.drawable.circle_bk_white);
          //  numberProgressBar.setProgressTextSize(0);
          //  numberProgressBar_dialog.setProgressTextSize(0);
            numberProgressBar.setProgress(100);
        }
        else if (course_state.equals("Course State : In Progress"))
        {
            if (course_progress.equals("0"))
            {
                numberProgressBar.setProgress(Integer.parseInt(course_progress));
             //   numberProgressBar.setProgressTextSize(0);
              //  numberProgressBar_dialog.setProgressTextSize(0);
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_holo_white);
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Started");
            }
            else if (course_progress.equals("100"))
            {
                numberProgressBar.setProgress(100);
          //      numberProgressBar.setProgressTextSize(0);
              //  numberProgressBar_dialog.setProgressTextSize(0);
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_bk_white);
                state_rel.setVisibility(View.VISIBLE);
                state_txt.setText("Completed");
            }
            else
            {
                numberProgressBar.setProgress(Integer.parseInt(course_progress));
                left.setImageResource(R.drawable.circle_bk_white);
                right.setImageResource(R.drawable.circle_holo_white);
                state_rel.setVisibility(View.INVISIBLE);
            }
        }
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.back_white);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setContentScrimColor(Color.rgb(25,118,210));
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(255,255,255));
        users_to_content_id_arr.clear();
        content_completed_arr.clear();
        content_path_arr.clear();
        content_name_arr.clear();

        count=0;
        List<Elpfa> elpfa_course_contents = db1.getEnrollContents(Integer.parseInt(course_id));
            for (Elpfa elp : elpfa_course_contents) {
            enroll_contents_course_id= String.valueOf(elp.getEnroll_contents_course_id());
            enroll_contents_name= String.valueOf(elp.getEnroll_contents_name());
            int started=0;
            int progress=0;
            count++;
            content_type="type";
            content_path="path";
            Contents contents = new Contents(course_started_intent,String.valueOf(count+1),enroll_contents_name,0,content_type,content_path,started,progress);
            contentList.add(contents);
            contentAdapter.notifyDataSetChanged();
        }
    }
    public void getCoursesAndContentsAfterEnroll()
    {
        try {
            Config url_login = new Config();
                    /* using volley in login - combine everything */
            String tag_string_req = "req_state";
            StringRequest strReq = new StringRequest(Request.Method.POST,
                    url_login.GetCoursesAfterEnroll, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                        try {

                            try {

                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                try {
                                    // fetch all user types details
                                    DatabaseHandler db1 = new DatabaseHandler(CourseContents.this);
                                    jObject = new JSONObject(response);

                                    db1.deleteCourseReportsTable();
                                    //fetch number of assigned courses
                                    if (!jObject.isNull("number_assigned_courses")) {

                                        JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                                        JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                                        number_assigned = Jasonobject.getString("count(*)");

                                        editor.putString("assigned", number_assigned);
                                        editor.commit();
                                    }

                                        //fetch number of started courses
                                        if (!jObject.isNull("number_started_courses")) {

                                            JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                                            JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                                            number_started = JasonobjectStarted.getString("count(*)");

                                            editor.putString("started", number_started);
                                            editor.commit();
                                        }
                                        //fetch number of not-started courses
                                        if (!jObject.isNull("number_not_started_courses")) {

                                            JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                                            JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                                            number_not_started = JasonobjectNotStarted.getString("count(*)");

                                            editor.putString("not_started", number_not_started);
                                            editor.commit();

                                        }
                                        //fetch number of completed courses
                                        if (!jObject.isNull("number_completed_courses")) {

                                            JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                                            JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                                            number_completed = JasonobjectCompleted.getString("count(*)");

                                            editor.putString("completed", number_completed);
                                            editor.commit();
                                        }

                                            /* add to course reports table */
                                        db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

                                                                        //fetch all  courses

//                                    editor.putString("json_String_scheduled", response);
                                    editor.putString("json_String_progress", response);
//                                    editor.putString("json_String_completed", response);
//                                    editor.putString("json_String_all", response);
                                    editor.commit();




                                    /******** fetch all in progress courses ***********/


                                    db1.deleteInProgressCoursesTable();
                                    if (!jObject.isNull("progress_courses")) {
                                        JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                                        final int progress_length = jsonArrayInProgressCourses.length();

                                        for (int i = 0; i < progress_length; i++) {
                                            JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                                            progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");

                                            progress_name = JasonobjectProgress.getString("name");
                                            progress_desc = JasonobjectProgress.getString("description");
                                            progress_startdate = JasonobjectProgress.getString("assign_date");
                                            progress_course_id = JasonobjectProgress.getString("course_id");
                                            progress_enrollment_key = JasonobjectProgress.getInt("enrollment_key");
                                            progress_status = JasonobjectProgress.getInt("status");
                                            progress_track_content = String.valueOf(JasonobjectProgress.getInt("track_course"));
                                            user_id = JasonobjectProgress.getString("user_id");
                                            progress_users_to_courseid = JasonobjectProgress.getString("id");
                                            progress_duration = JasonobjectProgress.getString("course_duration");
                                            progress_no_of_que = JasonobjectProgress.getString("no_of_que");
                                            progress_pass_percent = JasonobjectProgress.getString("pass_percent");
                                            progress_test_mandatory = JasonobjectProgress.getString("test_mandatory");
                                            progress_startedd = JasonobjectProgress.getInt("started");
                                            progress_completed = JasonobjectProgress.getInt("completed");
                                            progress_completion_date = JasonobjectProgress.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status,progress_path, progress_startdate, progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory, progress_startedd, progress_completed, progress_completion_date));


                                        }

                                    }

                                    db1.deleteAllCoursesTable();
                                    if (!jObject.isNull("all_courses")) {
                                        JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                                        final int progress_length = jsonArrayAllCourses.length();

                                        for (int i = 0; i < progress_length; i++) {
                                            JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                                            all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                                            all_name = JasonobjectAll.getString("name");
                                            all_desc = JasonobjectAll.getString("description");
                                            all_startdate = JasonobjectAll.getString("assign_date");
                                            all_course_id = JasonobjectAll.getString("course_id");
                                            all_enrollment_key = JasonobjectAll.getInt("enrollment_key");
                                            all_status = JasonobjectAll.getInt("status");
                                            all_track_content = String.valueOf(JasonobjectAll.getInt("track_course"));
                                            user_id = JasonobjectAll.getString("user_id");
                                            all_users_to_courseid = JasonobjectAll.getString("id");
                                            all_duration = JasonobjectAll.getString("course_duration");
                                            all_no_of_que = JasonobjectAll.getString("no_of_que");
                                            all_pass_percent = JasonobjectAll.getString("pass_percent");
                                            all_test_mandatory = JasonobjectAll.getString("test_mandatory");
                                            all_startedd = JasonobjectAll.getInt("started");
                                            all_completed = JasonobjectAll.getInt("completed");
                                            all_completion_date = JasonobjectAll.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status,all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory, all_startedd, all_completed, all_completion_date));


                                        }




                                    }

                                    /**********  end fetch all courses ***********/

                                    db1.deleteUsersToContentsTable();
                                    if (!jObject.isNull("user_contents")) {
                                        contents_course_id_arr.clear();
                                        contents_usersToCourse_id_arr.clear();
                                        contents_viewed_arr.clear();
                                        contents_completed_arr.clear();
                                        contents_type_arr.clear();
                                        contents_name_arr.clear();
                                        contents_path_arr.clear();

                                        JSONArray jsonArrayContents = jObject.getJSONArray("user_contents");
                                        int contents_length = jsonArrayContents.length();
                                        for (int i = 0; i < contents_length; i++) {
                                            JSONObject JasonobjectContents = jsonArrayContents.getJSONObject(i);
                                            contents_course_id = JasonobjectContents.getInt("course_id");
                                            contents_users_to_course_id = JasonobjectContents.getInt("id");
                                            contents_completed = JasonobjectContents.getInt("status");
                                            if (contents_completed == 0) {
                                                contents_viewed = 0;
                                            } else {
                                                contents_viewed = 1;
                                            }

                                            contents_type = JasonobjectContents.getString("item_name");
                                            contents_name = JasonobjectContents.getString("name");
                                            if (contents_type.equals("youtube")) {
                                                contents_path = JasonobjectContents.getString("path");
                                            } else {
                                                contents_path = Config.BASE_CONTENT_URL + JasonobjectContents.getString("path");
                                            }

                                            contents_course_id_arr.add(i, contents_course_id);
                                            contents_usersToCourse_id_arr.add(i, contents_users_to_course_id);
                                            contents_viewed_arr.add(i, contents_viewed);
                                            contents_completed_arr.add(i, contents_completed);
                                            contents_type_arr.add(i, contents_type);
                                            contents_name_arr.add(i, contents_name);
                                            contents_path_arr.add(i, contents_path);


                                            //insert team members into sqlite
                                            db1.addUsersToContents(new Elpfa(contents_course_id, contents_users_to_course_id, contents_viewed, contents_completed, contents_type, contents_name, contents_path));


                                        }


                                    }
                                    //after fetching data from server, update course details page
                                    course_started_intent= String.valueOf(1);
                                    prepareContents();
                                    start_enroll_now.setVisibility(View.GONE);








                                } catch (Exception e) {
                                }
//

                            } catch (Exception e) {

                            }


                        } catch (Exception e) {

                        }
                        //animation to expand login button

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("value1", sharedpreferences.getString("User_id",null));
                    return params;
                }

            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


        } catch (Exception ex) {
        }

    }

    @Override
    public void onContentClicked(int pos) {
        if (course_started_intent.equals("0"))
        {
            if (course_enroll_intent.equals("1"))
            {
                Toast.makeText(this, "Enroll in course to view contents", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(this, "Start course to view contents", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Contents contents = contentList.get(pos);
            Intent i;
            if (contents.getContentType().equalsIgnoreCase("video")){
                Intent iVideo = new Intent(CourseContents.this, ViewVideoActivity.class);
                iVideo.putExtra("content_url", contents.getContentPath());
                iVideo.putExtra("users_to_content_id", Integer.parseInt(contents.getUsersToContentId()));
                startActivityForResult(iVideo, REQUEST_CODE_CONTENT);
            }
            else if (contents.getContentType().equalsIgnoreCase("youtube")){
                Intent iYoutube = new Intent(CourseContents.this, YoutubeVideoActivity.class);
                iYoutube.putExtra("content_url", contents.getContentPath());
                iYoutube.putExtra("users_to_content_id", Integer.parseInt(contents.getUsersToContentId()));
                startActivityForResult(iYoutube, REQUEST_CODE_CONTENT);
            }
            else if (contents.getContentType().equalsIgnoreCase("pdf")){
                Intent pdf = new Intent(CourseContents.this, ViewPdfActivity.class);
                pdf.putExtra("content_url", contents.getContentPath());
                pdf.putExtra("users_to_content_id", Integer.parseInt(contents.getUsersToContentId()));
                startActivity(pdf);
            }
            else {
                Toast.makeText(this, contents.getContentType(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONTENT && resultCode ==RESULT_OK){
            boolean isUpdated = data.getBooleanExtra("isUpdated", false);
            if (isUpdated)
                setCourseContents();
        }
    }
}
