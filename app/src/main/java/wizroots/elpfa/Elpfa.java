package wizroots.elpfa;

import android.util.Log;

/**
 * Created by reflective2 on 25-07-2016.
 */
public class Elpfa {
    String user_id;
    String user_name;
    String password;
    String email;
    String user_userType;
    String company_name;
    String company_address;
    String photo;
    String registration;
    String status;
    String mobileno;

    String user_type;
    String user_type_id;
    String user_type_status;

    String assigned;
    String started;
    String not_started;
    String completed;

    String completed_course_id;
    String completed_users_to_course_id;
    String completed_course_name;
    String completed_course_description;
    String completed_course_duration;
    int completed_course_enrollment_key;
    int completed_course_status;
    String completed_course_path;
    String completed_course_assign_date;
    String completed_track_course;
    String completed_no_of_que;
    String completed_pass_percent;
    String completed_test_mandatory;
    int completed_started;
    int completed_completed;
    String completed_completion_date;

    String scheduled_course_id;
    String scheduled_users_to_course_id;
    String scheduled_course_name;
    String scheduled_course_description;
    String scheduled_course_duration;
    int scheduled_course_enrollment_key;
    int scheduled_course_status;
    String scheduled_course_path;
    String scheduled_course_assign_date;
    String scheduled_track_course;
    String scheduled_no_of_que;
    String scheduled_pass_percent;
    String scheduled_test_mandatory;
    int scheduled_started;
    int scheduled_completed;
    String scheduled_completion_date;

    String in_progress_course_id;
    String in_progress_users_to_course_id;
    String in_progress_course_name;
    String in_progress_course_description;
    String in_progress_course_duration;
    int in_progress_course_enrollment_key;
    int in_progress_course_status;
    String in_progress_course_path;
    String in_progress_course_assign_date;
    String in_progress_track_course;
    String in_progress_no_of_que;
    String in_progress_pass_percent;
    String in_progress_test_mandatory;
    int in_progress_started;
    int in_progress_completed;
    String in_progress_completion_date;

    String all_course_id;
    String all_users_to_course_id;
    String all_course_name;
    String all_course_description;
    String all_course_duration;
    int all_course_enrollment_key;
    int all_course_status;
    String all_course_path;
    String all_course_assign_date;
    String all_track_course;
    String all_no_of_que;
    String all_pass_percent;
    String all_test_mandatory;
    int all_started;
    int all_completed;
    String all_completion_date;

    String to_id;
    String to_to_id;
    String to_name;
    String to_email;
    String to_type;

    String team_id;
    String team_name;
    String team_path;
    String team_description;

    String notification_id;
    String notification_title;
    String notification_notification;
    String notification_date;
    int notification_status;

    int inbox_msg_id;
    int inbox_msg_msgid;
    String inbox_msg_sender;
    String inbox_msg_receiver;
    String inbox_msg_subject;
    String inbox_msg_msg;
    String inbox_msg_photo;
    String inbox_msg_date;
    int inbox_msg_viewed;
    int inbox_msg_important;
    String inbox_msg_attachment;

    int outbox_msg_id;
    int outbox_msg_msgid;
    String outbox_msg_sender;
    String outbox_msg_receiver;
    String outbox_msg_subject;
    String outbox_msg_msg;
    String outbox_msg_photo;
    String outbox_msg_date;
    int outbox_msg_important;
    String outbox_msg_attachment;

    int offline_read_msgid;

    int offline_delete_inbox_msgid;


    String recent_webinar_id;
    String recent_webinar_name;
    String recent_webinar_agenda;
    String recent_webinar_starts_at;
    String recent_webinar_ends_at;
    String recent_webinar_duration;
    String recent_webinar_room_url;


    String upcoming_webinar_id;
    String upcoming_webinar_name;
    String upcoming_webinar_agenda;
    String upcoming_webinar_starts_at;
    String upcoming_webinar_ends_at;
    String upcoming_webinar_duration;
    String upcoming_webinar_room_url;

    String enroll_contents_id;
    String enroll_contents_course_id;
    String enroll_contents_name;

    int teamchat_id;
    int teamchat_chatid;
    int teamchat_teamid;
    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    String teamchat_createdat;

    int teamchat_id_new;
    int teamchat_chatid_new;
    int teamchat_teamid_new;
    String teamchat_username_new;
    String teamchat_imagepath_new;
    String teamchat_messages_new;
    String teamchat_createdat_new;

    int offline_delete_outbox_msgid;

    String outbox_delete_timestamp;

    int team_members_id;
    int team_members_teamid;
    String team_members_name;
    String team_members_photo;



    int usersToContents_id;
    int usersToContents_course_id;
    int usersToContents_users_to_course_id;
    int usersToContents_viewed;
    int usersToContents_completed;
    String usersToContents_type;
    String usersToContents_name;
    String usersToContents_path;

    public Elpfa(){
    }

    //team members constructor
    public Elpfa(int teamid, String name, String photo)
    {
        this.team_members_teamid=teamid;
        this.team_members_name=name;
        this.team_members_photo=photo;
    }

    //offline read and delete msg
    public Elpfa(String str, int offline_id)
    {
        if ("read".equals(str)) {
            this.offline_read_msgid = offline_id;
        }
        else if ("delete_inbox".equals(str))
        {
            this.offline_delete_inbox_msgid=offline_id;
        }

    }

    public Elpfa(String str, String val)
    {
        if (str.equals("start_assigned_all"))
        {
            this.all_users_to_course_id=val;
            this.all_started=1;
        }
        else if (str.equals("start_assigned_inprogress"))
        {
            this.in_progress_users_to_course_id=val;
            this.in_progress_started=1;
        }
       else if ("sent_delete".equals(str))
        {
            this.outbox_delete_timestamp=val;
        }
        else if ("delete_notif".equals(str))
        {
            this.notification_id=String.valueOf(val);
        }
        else  if (str.equals("delete_from_scheduled"))
        {
            this.scheduled_users_to_course_id=val;
        }
    }


   /* // user types constructor
    public Elpfa(String usertype_id, String usertype, String usertype_status)
    {
        this.user_type_id=usertype_id;
        this.user_type=usertype;
        this.user_type_status=usertype_status;
    } */


    public Elpfa(String str, String enroll_contents_course_id, String enroll_contents_name)
    {
        if (str.equals("enroll_contents")) {
            this.enroll_contents_course_id = enroll_contents_course_id;
            this.enroll_contents_name = enroll_contents_name;
        }
        else
        {
            this.user_type_id=str;
            this.user_type=enroll_contents_course_id;
            this.user_type_status=enroll_contents_name;
        }
    }
    public Elpfa(int a, int b)
    {

        this.inbox_msg_msgid=a;
        this.inbox_msg_viewed=b;
    }
    public Elpfa(String str, int a, int b)
    {
        if (str.equals("normal_video_viewed")) {
            this.usersToContents_users_to_course_id= a;
            this.usersToContents_viewed = b;
        }
        else if (str.equals("normal_video_completed"))
        {
            this.usersToContents_users_to_course_id=a;
            this.usersToContents_completed=b;
        }
        else if (str.equals("inbox_message_important"))
        {
            this.inbox_msg_msgid=a;
            this.inbox_msg_important=b;
        }
        else if (str.equals("outbox_message_important"))
        {
            this.outbox_msg_msgid=a;
            this.outbox_msg_important=b;
        }
    }
 public Elpfa(int a)
    {

        this.offline_delete_inbox_msgid=a;
    }

    //course reports and teams constructor
    public Elpfa(String type, String a, String b, String c, String d)
    {

        if ("report".equals(type)) {
            this.assigned = a;
            this.started = b;
            this.not_started = c;
            this.completed = d;
        }
        else if ("teams".equals(type))
        {
            this.team_id=a;
            this.team_name=b;
            this.team_path=c;
            this.team_description=d;
        }
        else  if ("to".equals(type))
        {
            this.to_to_id=a;
            this.to_name=b;
            this.to_email=c;
            this.to_type=d;
        }
    }

//users to content table
    public Elpfa(int course_id, int users_to_course_id, int viewed, int completed, String type, String name, String path)
    {
        usersToContents_course_id=course_id;
        usersToContents_users_to_course_id=users_to_course_id;
        usersToContents_viewed=viewed;
        usersToContents_completed=completed;
        usersToContents_type=type;
        usersToContents_name=name;
        usersToContents_path=path;
    }

//user details constructor

    public Elpfa(String user_id, String user_name, String password, String email, String type, String company_name, String company_address, String photo, /*String registration, String status,*/String monileno){
       this.user_id=user_id;
        this.user_name=user_name;
        this.password=password;
        this.email=email;
        this.user_userType = type;
        this.company_name=company_name;
        this.company_address=company_address;
        this.photo=photo;
//        this.registration=registration;
        this.status=status;
        this.mobileno=monileno;
    }

//notification constructor

    public Elpfa(String str, String a, String b, String c, String d, int e){
        if ("notification".equals(str)) {
            this.notification_id = a;
            this.notification_title = b;
            this.notification_notification = c;
            this.notification_date = d;
            this.notification_status = e;
        }
    }

    //teamchat constructor

    public Elpfa(String str1, String str2, int chatid, int teamid, String username, String imagepath, String messages, String createdat){
            if ("all".equals(str1)) {
                this.teamchat_chatid = chatid;
                this.teamchat_teamid = teamid;
                this.teamchat_username = username;
                this.teamchat_imagepath = imagepath;
                this.teamchat_messages = messages;
                this.teamchat_createdat = createdat;
            }
        else if ("after".equals(str1))
            {
                this.teamchat_chatid_new = chatid;
                this.teamchat_teamid_new = teamid;
                this.teamchat_username_new = username;
                this.teamchat_imagepath_new = imagepath;
                this.teamchat_messages_new = messages;
                this.teamchat_createdat_new = createdat;

            }

    }

    //inbox msg constructor

    public Elpfa(int id,int msg_id, String sender, String receiver, String subject, String msg, String photo, String date, int viewed, int important, String attachments){
        this.inbox_msg_id=id;
        this.inbox_msg_msgid=msg_id;
        this.inbox_msg_sender=sender;
        this.inbox_msg_receiver=receiver;
        this.inbox_msg_subject=subject;
        this.inbox_msg_msg=msg;
        this.inbox_msg_photo=photo;
        this.inbox_msg_date=date;
        this.inbox_msg_viewed=viewed;
        this.inbox_msg_important=important;
        this.inbox_msg_attachment=attachments;
    }


    //outbox msg constructor

    public Elpfa(String str, int id, int msg_id, String sender, String receiver, String subject,String msg, String photo, String date, int important, String attachments){
        if ("outbox".equals(str)) {
            this.outbox_msg_id = id;
            this.outbox_msg_msgid = msg_id;
            this.outbox_msg_sender = sender;
            this.outbox_msg_receiver = receiver;
            this.outbox_msg_subject = subject;
            this.outbox_msg_msg = msg;
            this.outbox_msg_photo = photo;
            this.outbox_msg_date = date;
            this.outbox_msg_important= important;
            this.outbox_msg_attachment = attachments;
        }
    }

    //courses constructor
    public Elpfa(String type, String course_id, String users_to_course_id, String course_name, String course_description, String course_duration, int enrollment_key,int status, String path, String assign_date, String track_course, String no_of_que, String pass_percent, String test_mandatory, int started, int completed, String completion){
        if ("completed".equals(type)) {
            this.completed_course_id = course_id;
            this.completed_users_to_course_id = users_to_course_id;
            this.completed_course_name = course_name;
            this.completed_course_description = course_description;
            this.completed_course_duration = course_duration;
            this.completed_course_enrollment_key = enrollment_key;
            this.completed_course_status = status;
            this.completed_course_path = path;
            this.completed_course_assign_date = assign_date;
            this.completed_track_course = track_course;
            this.completed_no_of_que = no_of_que;
            this.completed_pass_percent = pass_percent;
            this.completed_test_mandatory = test_mandatory;
            this.completed_started=started;
            this.completed_completed=completed;
            this.completed_completion_date=completion;
        }
        else if("scheduled".equals(type))
        {
            this.scheduled_course_id=course_id;
            this.scheduled_users_to_course_id=users_to_course_id;
            this.scheduled_course_name=course_name;
            this.scheduled_course_description=course_description;
            this.scheduled_course_duration=course_duration;
            this.scheduled_course_enrollment_key=enrollment_key;
            this.scheduled_course_status=status;
            this.scheduled_course_path=path;
            this.scheduled_course_assign_date=assign_date;
            this.scheduled_track_course=track_course;
            this.scheduled_no_of_que=no_of_que;
            this.scheduled_pass_percent=pass_percent;
            this.scheduled_test_mandatory=test_mandatory;
            this.scheduled_started=started;
            this.scheduled_completed=completed;
            this.scheduled_completion_date=completion;
        }
        else if("in_progress".equals(type))
        {
            this.in_progress_course_id=course_id;
            this.in_progress_users_to_course_id=users_to_course_id;
            this.in_progress_course_name=course_name;
            this.in_progress_course_description=course_description;
            this.in_progress_course_duration=course_duration;
            this.in_progress_course_enrollment_key=enrollment_key;
            this.in_progress_course_status=status;
            this.in_progress_course_path=path;
            this.in_progress_course_assign_date=assign_date;
            this.in_progress_track_course=track_course;
            this.in_progress_no_of_que=no_of_que;
            this.in_progress_pass_percent=pass_percent;
            this.in_progress_test_mandatory=test_mandatory;
            this.in_progress_started=started;
            this.in_progress_completed=completed;
            this.in_progress_completion_date=completion;
        }
        else if ("all".equals(type))
        {
            this.all_course_id=course_id;
            this.all_users_to_course_id=users_to_course_id;
            this.all_course_name=course_name;
            this.all_course_description=course_description;
            this.all_course_duration=course_duration;
            this.all_course_enrollment_key=enrollment_key;
            this.all_course_status=status;
            this.all_course_path=path;
            this.all_course_assign_date=assign_date;
            this.all_track_course=track_course;
            this.all_no_of_que=no_of_que;
            this.all_pass_percent=pass_percent;
            this.all_test_mandatory=test_mandatory;
            this.all_started=started;
            this.all_completed=completed;
            this.all_completion_date=completion;
        }
    }

    //webinar constructor

    public Elpfa(String webinar_type,String name, String agenda, String starts_at, String ends_at, String duration, String room_url){
if (webinar_type.equals("recent"))
{
    this.recent_webinar_name = name;
    this.recent_webinar_agenda=agenda;
    this.recent_webinar_starts_at=starts_at;
    this.recent_webinar_ends_at=ends_at;
    this.recent_webinar_duration=duration;
    this.recent_webinar_room_url=room_url;
}
        else if (webinar_type.equals("upcoming"))
        {
            this.upcoming_webinar_name = name;
            this.upcoming_webinar_agenda=agenda;
            this.upcoming_webinar_starts_at=starts_at;
            this.upcoming_webinar_ends_at=ends_at;
            this.upcoming_webinar_duration=duration;
            this.upcoming_webinar_room_url=room_url;
        }


    }

/****** methods ********/
    public String getUserType(){

        return this.user_type;
    }
    public void setUserType(String type){

        this.user_type = type;
    }
    public String getUserTypeId(){

        return this.user_type_id;
    }
    public void setUserTypeId(String type){

        this.user_type_id = type;
    }
    public String getUserTypestatus(){

        return this.user_type_status;
    }
    public void setUserTypeStatus(String status){

        this.user_type_status = status;
    }

    // user id
    public String getUserId(){

        return this.user_id;
    }
    public void setUserId(String id){

        this.user_id = id;
    }

    // user name
    public String getUserName(){

        return this.user_name;
    }
    public void setUserName(String name){

        this.user_name = name;
    }

    // mobileno
    public String getUserMobileNo(){

        return this.mobileno;
    }
    public void setUserMobileNo(String mobile){

        this.mobileno = mobile;
    }

    // user password
    public String getUserPassword(){

        return this.password;
    }
    public void setUserPassword(String pass){

        this.password = pass;
    }

    // user photo
    public String getUserPhoto(){

        return this.photo;
    }
    public void setUserPhoto(String photo){

        this.photo = photo;
    }

    // user company name
    public String getUserCompanyName(){

        return this.company_name;
    }
    public void setUserCompanyName(String name){

        this.company_name = name;
    }

    // user company address
    public String getUserCompanyAddress(){

        return this.company_address;
    }
    public void setUserCompanyAddress(String address){

        this.company_address = address;
    }

    // user email
    public String getUserEmail(){

        return this.email;
    }
    public void setUserEmail(String mail){

        this.email = mail;
    }


    // usertype
    public String getUserUsertype(){

        return this.user_userType;
    }
    public void setUserUsertype(String type){

        this.user_userType = type;
    }

    // user registration
    public String getUserRegistration(){

        return this.registration;
    }
    public void setUserRegistration(String reg){

        this.registration = reg;
    }

    // user status
    public String getUserStatus(){

        return this.status;
    }

    public void setUserStatus(String status){

        this.status = status;
    }


    // assigned courses
    public String getAssignedCourses(){

        return this.assigned;
    }
    public void setAssignedCourses(String assigned){

        this.assigned = assigned;
    }

    // started courses
    public String getStartedCourses(){

        return this.started;
    }
    public void setStartedCourses(String started){

        this.started = started;
    }

    // not started courses
    public String getNotStartedCourses(){

        return this.not_started;
    }
    public void setNotStartedCourses(String not_started){

        this.not_started = not_started;
    }

    // completed courses
    public String getCompletedCourses(){

        return this.completed;
    }
    public void setCompletedCourses(String completed){

        this.completed = completed;
    }

    // completed course id
    public String getCompletedCourseId(){

        return this.completed_course_id;
    }
    public void setCompletedCourseId(String completed){

        this.completed_course_id = completed;
    }

    // completed course name
    public String getCompletedCourseName(){

        return this.completed_course_name;
    }
    public void setCompletedCourseName(String completedName){

        this.completed_course_name = completedName;
    }

    // completed course description
    public String getCompletedCourseDescription(){

        return this.completed_course_description;
    }
    public void setCompletedCourseDescription(String completedDes){

        this.completed_course_description = completedDes;
    }

    // completed course duration
    public String getCompletedCourseDuration(){

        return this.completed_course_duration;
    }
    public void setCompletedCourseDuration(String completedDuration){

        this.completed_course_duration = completedDuration;
    }

    // completed course enrollment key
    public int getCompletedCourseEnrollmentKey(){

        return this.completed_course_enrollment_key;
    }
    public void setCompletedCourseEnrollmentKey(int completedKey){

        this.completed_course_enrollment_key = completedKey;
    }
    // completed course status
    public int getCompletedCourseStatus(){

        return this.completed_course_status;
    }
    public void setCompletedCourseStatus(int status){

        this.completed_course_status = status;
    }

    // completed course path
    public String getCompletedCoursePath(){

        return this.completed_course_path;
    }
    public void setCompletedCoursePath(String completedPath){

        this.completed_course_path = completedPath;
    }

    // completed course assign date
    public String getCompletedCourseAssignDate(){

        return this.completed_course_assign_date;
    }
    public void setCompletedCourseAssignDate(String completedAssignDate){

        this.completed_course_assign_date = completedAssignDate;
    }

    // completed course track course
    public String getCompletedCourseTrackCourse(){

        return this.completed_track_course;
    }
    public void setCompletedCourseTrackCourse(String completedTrackCourse){

        this.completed_track_course = completedTrackCourse;
    }

    // completed course no of que
    public String getCompletedCourseNoOfQue(){

        return this.completed_no_of_que;
    }
    public void setCompletedCourseNoOfQue(String completedQue){

        this.completed_no_of_que = completedQue;
    }

    // completed course pass percent
    public String getCompletedCoursePassPercent(){

        return this.completed_pass_percent;
    }
    public void setCompletedCoursePassPercent(String completedPercent){

        this.completed_pass_percent = completedPercent;
    }

    // completed course pass percent
    public String getCompletedCourseTestMandatory(){

        return this.completed_test_mandatory;
    }
    public void setCompletedCourseTestMandatory(String completedMandatory){

        this.completed_test_mandatory = completedMandatory;
    }

    // completed course pass percent
    public String getCompletedCourseUsersToCourseId(){

        return this.completed_users_to_course_id;
    }
    public void setCompletedCourseUsersToCourseId(String completeduserstocourseid){

        this.completed_users_to_course_id = completeduserstocourseid;
    }

    // completed course started
    public int getCompletedCourseStarted(){

        return this.completed_started;
    }
    public void setCompletedCourseStarted(int started){

        this.completed_started = started;
    }

    // completed course completed
    public int getCompletedCourseCompleted(){

        return this.completed_completed;
    }
    public void setCompletedCourseCompleted(int completed){

        this.completed_completed = completed;
    }

    // completed course completion date
    public String getCompletedCourseCompletionDate(){

        return this.completed_completion_date;
    }
    public void setCompletedCourseCompletionDate(String date){

        this.completed_completion_date = date;
    }
/**** svheduled ***/
    // scheduled course id
    public String getScheduledCourseId(){

        return this.scheduled_course_id;
    }
    public void setScheduledCourseId(String id){

        this.scheduled_course_id = id;
    }


 // scheduled course name
    public String getScheduledCourseName(){

        return this.scheduled_course_name;
    }
    public void setScheduledCourseName(String name){

        this.scheduled_course_name = name;
    }


// scheduled course description
    public String getScheduledDescription(){

        return this.scheduled_course_description;
    }
    public void setScheduledCourseDescription(String des){

        this.scheduled_course_description = des;
    }


// scheduled course duration
    public String getScheduledDuration(){

        return this.scheduled_course_duration;
    }
    public void setScheduledCourseDuration(String duration){

        this.scheduled_course_duration = duration;
    }


// scheduled course enrollment key
    public int getScheduledEnrollmentKey(){

        return this.scheduled_course_enrollment_key;
    }
    public void setScheduledEnrollmentKey(int enroll){

        this.scheduled_course_enrollment_key = enroll;
    }
    // scheduled course status
    public int getScheduledStatus(){

        return this.scheduled_course_status;
    }
    public void setScheduledStatus(int status){

        this.scheduled_course_status = status;
    }


// scheduled course path
    public String getScheduledCoursePath(){

        return this.scheduled_course_path;
    }
    public void setScheduledCoursePath(String path){

        this.scheduled_course_path = path;
    }

// scheduled course assign date
    public String getScheduledAssignDate(){

        return this.scheduled_course_assign_date;
    }
    public void setScheduledAssignDate(String date){

        this.scheduled_course_assign_date = date;
    }



// scheduled course track course
    public String getScheduledTrackCourse(){

        return this.scheduled_track_course;
    }
    public void setScheduledTrackCourse(String track){

        this.scheduled_track_course = track;
    }



// scheduled course test mandatory
    public String getScheduledTestMandatory(){

        return this.scheduled_test_mandatory;
    }
    public void setScheduledTestMandatory(String test){

        this.scheduled_test_mandatory = test;
    }

// scheduled course no. of questions
    public String getScheduledNumberOfQuestions(){

        return this.scheduled_no_of_que;
    }
    public void setScheduledNumberOfQuestions(String que){

        this.scheduled_no_of_que = que;
    }



// scheduled course pass percent
    public String getScheduledPassPercent(){

        return this.scheduled_pass_percent;
    }
    public void setScheduledPassPercent(String percent){

        this.scheduled_pass_percent = percent;
    }

// scheduled course users to course id
    public String getScheduledUsersToCourseId(){

        return this.scheduled_users_to_course_id;
    }
    public void setScheduledUsersToCourseId(String id){

        this.scheduled_users_to_course_id = id;
    }

    // scheduled course started
    public int getScheduledCourseStarted(){

        return this.scheduled_started;
    }
    public void setScheduledCourseStarted(int started){

        this.scheduled_started = started;
    }

    // scheduled course completed
    public int getScheduledCourseCompleted(){

        return this.scheduled_completed;
    }
    public void setScheduledCourseCompleted(int completed){

        this.scheduled_completed = completed;
    }

    // scheduled course completion date
    public String getScheduledCourseCompletionDate(){

        return this.scheduled_completion_date;
    }
    public void setScheduledCourseCompletionDate(String date){

        this.scheduled_completion_date = date;
    }

/**** end scheduled courses ***/

    /*** in progress ****/
    // in progress course id
    public String getInProgressCourseId(){

        return this.in_progress_course_id;
    }
    public void setInProgressCourseId(String id){

        this.in_progress_course_id = id;
    }


    // in progress course name
    public String getInProgressCourseName(){

        return this.in_progress_course_name;
    }
    public void setInProgressCourseName(String name){

        this.in_progress_course_name = name;
    }


    // InProgress course description
    public String getInProgressDescription(){

        return this.in_progress_course_description;
    }
    public void setInProgressCourseDescription(String des){

        this.in_progress_course_description = des;
    }


    // InProgress course duration
    public String getInProgressDuration(){

        return this.in_progress_course_duration;
    }
    public void setInProgressCourseDuration(String duration){

        this.in_progress_course_duration = duration;
    }


    // InProgress course enrollment key
    public int getInProgressEnrollmentKey(){

        return this.in_progress_course_enrollment_key;
    }
    public void setInProgressEnrollmentKey(int enroll){

        this.in_progress_course_enrollment_key = enroll;
    }
    // InProgress course status
    public int getInProgressStatus(){

        return this.in_progress_course_status;
    }
    public void setInProgressStatus(int status){

        this.in_progress_course_status = status;
    }

    // InProgress course path
    public String getInProgressCoursePath(){

        return this.in_progress_course_path;
    }
    public void setInProgressCoursePath(String path){

        this.in_progress_course_path = path;
    }

    // InProgress course assign date
    public String getInProgressAssignDate(){

        return this.in_progress_course_assign_date;
    }
    public void setInProgressAssignDate(String date){

        this.in_progress_course_assign_date = date;
    }



    // InProgress course track course
    public String getInProgressTrackCourse(){

        return this.in_progress_track_course;
    }
    public void setInProgressTrackCourse(String track){

        this.in_progress_track_course = track;
    }



    // InProgress course test mandatory
    public String getInProgressTestMandatory(){

        return this.in_progress_test_mandatory;
    }
    public void setInProgressTestMandatory(String test){

        this.in_progress_test_mandatory = test;
    }

    // InProgress course no. of questions
    public String getInProgressNumberOfQuestions(){

        return this.in_progress_no_of_que;
    }
    public void setInProgressNumberOfQuestions(String que){

        this.in_progress_no_of_que = que;
    }



    // InProgress course pass percent
    public String getInProgressPassPercent(){

        return this.in_progress_pass_percent;
    }
    public void setInProgressPassPercent(String percent){

        this.in_progress_pass_percent = percent;
    }

    // InProgress course users to course id
    public String getInProgressUsersToCourseId(){

        return this.in_progress_users_to_course_id;
    }
    public void setInProgressUsersToCourseId(String id){

        this.in_progress_users_to_course_id = id;
    }

    // InProgress course started
    public int getInProgressCourseStarted(){

        return this.in_progress_started;
    }
    public void setInProgressCourseStarted(int started){

        this.in_progress_started = started;
    }

    // InProgress course completed
    public int getInProgressCourseCompleted(){

        return this.in_progress_completed;
    }
    public void setInProgressCourseCompleted(int completed){

        this.in_progress_completed = completed;
    }

    // InProgress course completion date
    public String getInProgressCourseCompletionDate(){

        return this.in_progress_completion_date;
    }
    public void setInProgressCourseCompletionDate(String date){

        this.in_progress_completion_date = date;
    }


/**** end in progress courses ***/


    /*** all courses ***/

    // all course id
    public String getAllCourseId(){

        return this.all_course_id;
    }
    public void setAllCourseId(String id){

        this.all_course_id = id;
    }


    // All course name
    public String getAllCourseName(){

        return this.all_course_name;
    }
    public void setAllCourseName(String name){

        this.all_course_name = name;
    }


    // All course description
    public String getAllDescription(){

        return this.all_course_description;
    }
    public void setAllCourseDescription(String des){

        this.all_course_description = des;
    }


    // All course duration
    public String getAllDuration(){

        return this.all_course_duration;
    }
    public void setAllCourseDuration(String duration){

        this.all_course_duration = duration;
    }


    // All course enrollment key
    public int getAllEnrollmentKey(){

        return this.all_course_enrollment_key;
    }
    public void setAllEnrollmentKey(int enroll){

        this.all_course_enrollment_key = enroll;
    }

    // All course status
    public int getAllStatus(){

        return this.all_course_status;
    }
    public void setAllStatus(int status){

        this.all_course_status = status;
    }
    // All course path
    public String getAllCoursePath(){

        return this.all_course_path;
    }
    public void setAllCoursePath(String path){

        this.all_course_path = path;
    }

    // All course assign date
    public String getAllAssignDate(){

        return this.all_course_assign_date;
    }
    public void setAllAssignDate(String date){

        this.all_course_assign_date = date;
    }



    // All course track course
    public String getAllTrackCourse(){

        return this.all_track_course;
    }
    public void setAllTrackCourse(String track){

        this.all_track_course = track;
    }



    // All course test mandatory
    public String getAllTestMandatory(){

        return this.all_test_mandatory;
    }
    public void setAllTestMandatory(String test){

        this.all_test_mandatory = test;
    }

    // All course no. of questions
    public String getAllNumberOfQuestions(){

        return this.all_no_of_que;
    }
    public void setAllNumberOfQuestions(String que){

        this.all_no_of_que = que;
    }



    // All course pass percent
    public String getAllPassPercent(){

        return this.all_pass_percent;
    }
    public void setAllPassPercent(String percent){

        this.all_pass_percent = percent;
    }

    // All course users to course id
    public String getAllUsersToCourseId(){

        return this.all_users_to_course_id;
    }
    public void setAllUsersToCourseId(String id){

        this.all_users_to_course_id = id;
    }

    // All course started
    public int getAllCourseStarted(){

        return this.all_started;
    }
    public void setAllCourseStarted(int started){

        this.all_started = started;
    }

    // All course completed
    public int getAllCourseCompleted(){

        return this.all_completed;
    }
    public void setAllCourseCompleted(int completed){

        this.all_completed = completed;
    }

    // InProgress course completion date
    public String getAllCourseCompletionDate(){

        return this.all_completion_date;
    }
    public void setAllCourseCompletionDate(String date){

        this.all_completion_date = date;
    }


/**** end scheduled courses ***/
 /**** team ****/

 // team id
 public String getTeamId(){

     return this.team_id;
 }
    public void setTeamId(String test){

        this.team_id = test;
    }

    // team name
    public String getTeamName(){

        return this.team_name;
    }
    public void setTeamName(String que){

        this.team_name = que;
    }



    // team path
    public String getTeamPath(){

        return this.team_path;
    }
    public void setTeamPath(String percent){

        this.team_path = percent;
    }

    // team description
    public String getTeamDescription(){

        return this.team_description;
    }
    public void setTeamDescription(String id){

        this.team_description = id;
    }

    /*** end team ***/

    /**** notifications ****/

    // notif id
    public String getNotifId(){

        return this.notification_id;
    }
    public void setNotifId(String id){

        this.notification_id = id;
    }

    // Notif title
    public String getNotifTitle(){

        return this.notification_title;
    }
    public void setNotifTitle(String title){

        this.notification_title = title;
    }



    // Notif Notif
    public String getNotifNotif(){

        return this.notification_notification;
    }
    public void setNotifNotif(String notif){

        this.notification_notification = notif;
    }

    // Notif date
    public String getNotifDate(){

        return this.notification_date;
    }
    public void setNotifDate(String date){

        this.notification_date = date;
    }

    // Notif status
    public int getNotifStatus(){

        return this.notification_status;
    }
    public void setNotifStatus(int status){

        this.notification_status = status;
    }
    /*** end notification ***/

    /**** Inbox messages ****/

    // inbox msg id
    public int getInboxMessageId(){

        return this.inbox_msg_id;
    }
    public void setInboxMessageId(int id){

        this.inbox_msg_id = id;
    }

    // inbox msg msg_id
    public int getInboxMessageMsgid(){

        return this.inbox_msg_msgid;
    }
    public void setInboxMessageMsgid(int msgid){

        this.inbox_msg_msgid = msgid;
    }


    // inbox msg sender
    public String getInbox_msg_sender(){

        return this.inbox_msg_sender;
    }
    public void setInbox_msg_sender(String sender){

        this.inbox_msg_sender = sender;
    }

    // inbox msg receiver
    public String getInbox_msg_receiver(){

        return this.inbox_msg_receiver;
    }
    public void setInbox_msg_receiver(String receiver){

        this.inbox_msg_receiver = receiver;
    }

    // inbox msg subject
    public String getInbox_msg_subject(){

        return this.inbox_msg_subject;
    }
    public void setInbox_msg_subject(String subject){

        this.inbox_msg_subject = subject;
    }


    // inbox msg msg
    public String getInboxMessageMsg(){

        return this.inbox_msg_msg;
    }
    public void setInboxMessageMsg(String msg){

        this.inbox_msg_msg = msg;
    }
    // Inbox Message photo
    public String getInbox_msg_photo(){

        return this.inbox_msg_photo;
    }
    public void setInbox__msg_photo(String photo){

        this.inbox_msg_photo = photo;
    }
    // Inbox message date
    public String getInbox_msg_date(){

        return this.inbox_msg_date;
    }
    public void setInbox_msg_date(String date){

        this.inbox_msg_date = date;
    }

    // inbox msg viewed
    public int getInboxMessageViewed(){

        return this.inbox_msg_viewed;
    }
    public void setInboxMessageViewed(int viewed){

        this.inbox_msg_viewed = viewed;
    }

    // inbox msg important
    public int getInbox_msg_important(){

        return this.inbox_msg_important;
    }
    public void setInbox_msg_important(int important){

        this.inbox_msg_important = important;
    }

    // inbox msg attachments
    public String getInbox_msg_attachment(){

        return this.inbox_msg_attachment;
    }
    public void setInbox_msg_attachment(String attachments){

        this.inbox_msg_attachment = attachments;
    }



    /*** end inbox messages ***/



    /**** outbox messages ****/

    // outbox  id
    public int getOutboxMessageId(){

        return this.outbox_msg_id;
    }
    public void setOutboxMessageId(int id){

        this.outbox_msg_id = id;
    }

    // OutboxMessage msg msg_id
    public int getOutboxMessageMsgid(){

        return this.outbox_msg_msgid;
    }
    public void setOutboxMessageMsgid(int msgid){

        this.outbox_msg_msgid = msgid;
    }


    // OutboxMessage sender
    public String getOutbox_msg_sender(){

        return this.outbox_msg_sender;
    }
    public void setOutbox_msg_sender(String sender){

        this.outbox_msg_sender = sender;
    }

    // OutboxMessage receiver
    public String getOutbox_msg_receiver(){

        return this.outbox_msg_receiver;
    }
    public void setOutbox_msg_receiver(String receiver){

        this.outbox_msg_receiver = receiver;
    }


    // OutboxMessage subject
    public String getOutbox_msg_subject(){

        return this.outbox_msg_subject;
    }
    public void setOutbox_msg_subject(String subject){

        this.outbox_msg_subject = subject;
    }

    // OutboxMessage message
    public String getOutbox_msg_msg(){

        return this.outbox_msg_msg;
    }
    public void setOutbox_msg_msg(String msg){

        this.outbox_msg_msg = msg;
    }

    // OutboxMessage photo
    public String getOutbox_msg_photo(){

        return this.outbox_msg_photo;
    }
    public void setOutbox_msg_photo(String photo){

        this.outbox_msg_photo = photo;
    }


    // Inbox message date
    public String getOutbox_msg_date(){

        return this.outbox_msg_date;
    }
    public void setOutbox_msg_date(String date){

        this.outbox_msg_date = date;
    }

    // Inbox message important
    public int getOutbox_msg_important(){

        return this.outbox_msg_important;
    }
    public void setOutbox_msg_important(int important){

        this.outbox_msg_important = important;
    }

    // Inbox message attachment
    public String getOutbox_msg_attachment(){

        return this.outbox_msg_attachment;
    }
    public void setOutbox_msg_attachment(String attachment){

        this.outbox_msg_attachment = attachment;
    }

    /*** end inbox messages ***/

    /*** offline read msg ***/
    // offline read msg id
    public int getOfflineReadMsgId(){

        return this.offline_read_msgid;
    }
    public void setOfflineReadMsgId(int id){

        this.offline_read_msgid = id;
    }


    /*** end offline read msg ***/


    /*** offline delete inbox msg ***/
    // offline msg id
    public int getOfflineDeleteInboxMsgId(){

        return this.offline_delete_inbox_msgid;
    }
    public void setOfflineDeleteInboxMsgId(int id){

        this.offline_delete_inbox_msgid = id;
    }


    /*** end offline delete inbox msg ***/

    /*** offline delete outbox msg timestamp***/
    // offline msg timestamp
    public String getOfflineDeleteOutboxMsgTimestamp(){

        return this.outbox_delete_timestamp;
    }
    public void setOfflineDeleteOutboxMsgTimestamp(String timestmp){

        this.outbox_delete_timestamp = timestmp;
    }


    /*** end offline delete inbox msg ***/


    /*** team chat ***/
    // team chat id
    public int getTeamChatId(){

        return this.teamchat_id;
    }
    public void setTeamChatId(int id){

        this.teamchat_id = id;
    }


    // team chat chatid
    public int getTeamChatChatId(){

        return this.teamchat_chatid;
    }
    public void setTeamChatChatId(int chatid){

        this.teamchat_chatid = chatid;
    }

    // team chat teamid
    public int getTeamChatTeamId(){

        return this.teamchat_teamid;
    }
    public void setTeamChatTeamId(int teamid){

        this.teamchat_teamid = teamid;
    }

     // team chat username
    public String getTeamChatUsername(){

        return this.teamchat_username;
    }
    public void setTeamChatUsername(String username){

        this.teamchat_username = username;
    }

     // team chat imagepath
    public String getTeamChatImagepath(){

        return this.teamchat_imagepath;
    }
    public void setTeamChatImagepath(String imagepath){

        this.teamchat_imagepath = imagepath;
    }

    // team chat messages
    public String getTeamChatMessages(){

        return this.teamchat_messages;
    }
    public void setTeamChatMessages(String msg){

        this.teamchat_messages = msg;
    }

    // team chat createdat
    public String getTeamChatCreatedAt(){

        return this.teamchat_createdat;
    }
    public void setTeamChatCreatedAt(String createdat){

        this.teamchat_createdat = createdat;
    }



    /*** end team chat ***/

    /*** team chat after timestamp***/
    // team chat id new
    public int getTeamChatIdNew(){

        return this.teamchat_id_new;
    }
    public void setTeamChatIdNew(int id){

        this.teamchat_id_new = id;
    }


    // team chat chatid new
    public int getTeamChatChatIdNew(){

        return this.teamchat_chatid_new;
    }
    public void setTeamChatChatIdNew(int chatid){

        this.teamchat_chatid_new = chatid;
    }

    // team chat teamid new
    public int getTeamChatTeamIdNew(){

        return this.teamchat_teamid_new;
    }
    public void setTeamChatTeamIdNew(int teamid){

        this.teamchat_teamid_new = teamid;
    }

    // team chat username new
    public String getTeamChatUsernameNew(){

        return this.teamchat_username_new;
    }
    public void setTeamChatUsernameNew(String username){

        this.teamchat_username_new = username;
    }

    // team chat imagepath new
    public String getTeamChatImagepathNew(){

        return this.teamchat_imagepath_new;
    }
    public void setTeamChatImagepathNew(String imagepath){

        this.teamchat_imagepath_new = imagepath;
    }

    // team chat messages new
    public String getTeamChatMessagesNew(){

        return this.teamchat_messages_new;
    }
    public void setTeamChatMessagesNew(String msg){

        this.teamchat_messages_new = msg;
    }

    // team chat createdat new
    public String getTeamChatCreatedAtNew(){

        return this.teamchat_createdat_new;
    }
    public void setTeamChatCreatedAtNew(String createdat){

        this.teamchat_createdat_new = createdat;
    }



    /*** end team chat after timestamp ***/


    /*** offline delete outbox msg ***/
    // offline msg id
    public int getOfflineDeleteOutboxMsgId(){

        return this.offline_delete_outbox_msgid;
    }
    public void setOfflineDeleteOutboxMsgId(int id){

        this.offline_delete_outbox_msgid = id;
    }


    /*** end offline delete inbox msg ***/

    /*** offline delete outbox msg ***/
    // offline msg id
    public String getOutboxDeleteTimestamp(){

        return this.outbox_delete_timestamp;
    }
    public void setOutboxDeleteTimestamp(String timestmp){

        this.outbox_delete_timestamp = timestmp;
    }


    /*** end offline delete inbox msg ***/

    /*** team members ***/

    // team members id
    public int getTeamMembersId(){

        return this.team_members_id;
    }
    public void setTeamMembersId(int id){

        this.team_members_id = id;
    }

    // team members teamid
    public int getTeamMembersTeamId(){

        return this.team_members_teamid;
    }
    public void setTeamMembersTeamId(int teamid){

        this.team_members_teamid = teamid;
    }

    // team members name
    public String getTeamMembersName(){

        return this.team_members_name;
    }
    public void setTeamMembersName(String name){

        this.team_members_name = name;
    }


    // team members photo
    public String getTeamMembersPhoto(){

        return this.team_members_photo;
    }
    public void setTeamMembersPhoto(String photo){

        this.team_members_photo = photo;
    }

    /*** end team members ***/
    /*** recent webinars ***/

    // webinar id
    public String getRecentWebinar_id(){

        return this.recent_webinar_id;
    }
    public void setRecentWebinar_id(String id){

        this.recent_webinar_id = id;
    }

    // webinar name
    public String getRecentWebinar_name(){

        return this.recent_webinar_name;
    }
    public void setRecentWebinar_name(String name){

        this.recent_webinar_name = name;
    }
    // webinar agenda
    public String getRecentWebinar_agenda(){

        return this.recent_webinar_agenda;
    }
    public void setRecentWebinar_agenda(String agenda){

        this.recent_webinar_agenda = agenda;
    }

    // webinar starts at
    public String getRecentWebinar_starts_at(){

        return this.recent_webinar_starts_at;
    }
    public void setRecentWebinar_starts_at(String start){

        this.recent_webinar_starts_at = start;
    }

    // webinar starts at
    public String getRecentWebinar_ends_at(){

        return this.recent_webinar_ends_at;
    }
    public void setRecentWebinar_ends_at(String end){

        this.recent_webinar_ends_at = end;
    }

    // webinar duration
    public String getRecentWebinar_duration(){

        return this.recent_webinar_duration;
    }
    public void setRecentWebinar_duration(String duration){

        this.recent_webinar_duration = duration;
    }

    // webinar room url
    public String getRecentWebinar_room_url(){

        return this.recent_webinar_room_url;
    }
    public void setRecentWebinar_room_url(String url){

        this.recent_webinar_room_url = url;
    }

    /*** end recent webinars ***/
    /*** upcoming webinars ***/

    // webinar id
    public String getUpcomingWebinar_id(){

        return this.upcoming_webinar_id;
    }
    public void setUpcomingWebinar_id(String id){

        this.upcoming_webinar_id = id;
    }

    // webinar name
    public String getUpcomingWebinar_name(){

        return this.upcoming_webinar_name;
    }
    public void setUpcomingWebinar_name(String name){

        this.upcoming_webinar_name = name;
    }
    // webinar agenda
    public String getUpcomingWebinar_agenda(){

        return this.upcoming_webinar_agenda;
    }
    public void setUpcomingWebinar_agenda(String agenda){

        this.upcoming_webinar_agenda = agenda;
    }

    // webinar starts at
    public String getUpcomingWebinar_starts_at(){

        return this.upcoming_webinar_starts_at;
    }
    public void setUpcomingWebinar_starts_at(String start){

        this.upcoming_webinar_starts_at = start;
    }

    // webinar starts at
    public String getUpcomingWebinar_ends_at(){

        return this.upcoming_webinar_ends_at;
    }
    public void setUpcomingWebinar_ends_at(String end){

        this.upcoming_webinar_ends_at = end;
    }

    // webinar duration
    public String getUpcomingWebinar_duration(){

        return this.upcoming_webinar_duration;
    }
    public void setUpcomingWebinar_duration(String duration){

        this.upcoming_webinar_duration = duration;
    }

    // webinar room url
    public String getUpcomingWebinar_room_url(){

        return this.upcoming_webinar_room_url;
    }
    public void setUpcomingWebinar_room_url(String url){

        this.upcoming_webinar_room_url = url;
    }

    /*** end upcoming webinars ***/

    /*** users to contents ****/
    // usersToContents courseid
    public int getUsersToContentsCourseId(){

        return this.usersToContents_course_id;
    }
    public void setUsersToContentsCourseId(int id){

        this.usersToContents_course_id = id;
    }

    // usersToContents userstocontentid
    public int getUsersToContentsUsersToContentId(){

        return this.usersToContents_users_to_course_id;
    }
    public void setUsersToContentsUsersToContentId(int id){

        this.usersToContents_users_to_course_id = id;
    }

    // usersToContents viewed
    public int getUsersToContentsViewed(){

        return this.usersToContents_viewed;
    }
    public void setUsersToContentsViewed(int viewed){

        this.usersToContents_viewed = viewed;
    }

    // usersToContents completed
    public int getUsersToContentsCompleted(){

        return this.usersToContents_completed;
    }
    public void setUsersToContentsCompleted(int completed){

        this.usersToContents_completed = completed;
    }

    // usersToContents type
    public String getUsersToContentsType(){

        return this.usersToContents_type;
    }
    public void setUsersToContents_type(String type){

        this.usersToContents_type = type;
    }

    // usersToContents name
    public String getUsersToContentsName(){

        return this.usersToContents_name;
    }
    public void setUsersToContents_name(String name){

        this.usersToContents_name = name;
    }

    // usersToContents id
    public int getUsersToContentsId(){

        return this.usersToContents_id;
    }
    public void setUsersToContents_id(int id){

        this.usersToContents_id = id;
    }

    // usersToContents path
    public String getUsersToContentsPath(){

        return this.usersToContents_path;
    }
    public void setUsersToContents_path(String path){

        this.usersToContents_path = path;
    }

    /*** end users to contents ****/

    /*** enroll contents ***/
    // enroll id
    public String getEnroll_contents_id(){

        return this.enroll_contents_id;
    }
    public void setEnroll_contents_id(String id){

        this.enroll_contents_id = id;
    }

    // enroll course id
    public String getEnroll_contents_course_id(){

        return this.enroll_contents_course_id;
    }
    public void setEnroll_contents_course_id(String course_id){

        this.enroll_contents_course_id = course_id;
    }

    // enroll content name
    public String getEnroll_contents_name(){

        return this.enroll_contents_name;
    }
    public void setEnroll_contents_name(String name){

        this.enroll_contents_name = name;
    }

    /*** end enroll contents ***/

    /*** To ***/
    // to id
    public String getTo_id(){

        return this.to_id;
    }
    public void setTo_id(String id){

        this.to_id = id;
    }

    // to name
    public String getTo_name(){

        return this.to_name;
    }
    public void setTo_name(String name){

        this.to_name = name;
    }

    // to email
    public String getTo_email(){

        return this.to_email;
    }
    public void setTo_email(String email){

        this.to_email = email;
    }


    // to type
    public String getTo_type(){

        return this.to_type;
    }
    public void setTo_type(String type){

        this.to_type = type;
    }
    // to to id
    public String getTo_to_id(){

        return this.to_to_id;
    }
    public void setTo_to_id(String id){

        this.to_to_id = id;
    }
    /*** End To ***/
}
