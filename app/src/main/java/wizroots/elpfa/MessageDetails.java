package wizroots.elpfa;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static wizroots.elpfa.R.drawable.notification;

public class MessageDetails extends AppCompatActivity {
    String message_id;
    Toolbar toolbar;
    String inbox_outbox;
    String message_subject_txt;
    String message_contact_name_txt;
    int message_important_txt;
    String message_message_txt;
    String message_attachment_txt;
    String message_photo_txt;
    int n;
    TextView message_subject;
    TextView message_contact_name;
    TextView message_message;
    RatingBar message_important;
    TaskStackBuilder stackBuilder;
    NotificationCompat.Builder builder;
    private RecyclerView recyclerView;
    String message_timestamp;
    TextView message_time;
    ImageView download;
    int attachment_count=0;
    File file;
    ImageView contact_image;
    private AttachmentAdapter attachmentAdapter;
    String url;
    Intent resultIntent;
    TextView attachment_name;
    DatabaseHandler db;
    LinearLayout attachment_layout;
    PendingIntent pIntent;
    NotificationManager manager;
    private List<Attachment> attachmentList = new ArrayList<>();
    ImageView attachment_content;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);
        message_id=getIntent().getStringExtra("message_id");
        inbox_outbox=getIntent().getStringExtra("inbox_outbox");
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        message_important=(RatingBar) findViewById(R.id.message_important);
        message_subject=(TextView)findViewById(R.id.message_subject);
        message_contact_name=(TextView)findViewById(R.id.message_contact_name);
        message_message=(TextView)findViewById(R.id.message_message);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        message_important=(RatingBar)findViewById(R.id.message_important);
        message_time=(TextView)findViewById(R.id.message_time);
        attachment_layout=(LinearLayout)findViewById(R.id.attachment_layout);
        attachment_content=(ImageView)findViewById(R.id.attachment_content);
        attachment_name=(TextView)findViewById(R.id.attachment_name);
        download=(ImageView)findViewById(R.id.attachment_download);
        contact_image=(ImageView)findViewById(R.id.contact_image);
        attachmentAdapter = new AttachmentAdapter(attachmentList);
        toolbar.setNavigationIcon(R.drawable.back_white);
        db=new DatabaseHandler(getApplicationContext());
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDetails.this.finish();
            }
        });
        prepareMessage();
        message_subject.setText(""+message_subject_txt);
        message_contact_name.setText(""+message_contact_name_txt);
        message_message.setText(""+message_message_txt);
        message_important.setRating(message_important_txt);

        if (message_photo_txt.equals(""))
        {
            message_photo_txt="http://abc";
            Picasso.with(getApplicationContext()).load(message_photo_txt).placeholder(R.drawable.circle_bk_contact).into(contact_image);
        }
        else
        {
            Picasso.with(getApplicationContext()).load(message_photo_txt).placeholder(R.drawable.circle_bk_contact).into(contact_image);
        }

        url =message_attachment_txt;
        if (url!=null) {
            url = url.substring(url.lastIndexOf("/") + 1, url.length());
        }
        attachment_name.setText(""+url);
        String dateAsText = new SimpleDateFormat("K:mm a")
                .format(new java.sql.Date(Long.parseLong(message_timestamp) * 1000L));
        message_time.setText(""+dateAsText);
//        Log.d("attachment_check","message_attachment_txt : "+message_attachment_txt);
        if (message_attachment_txt.equals("0"))
        {
            attachment_layout.setVisibility(View.INVISIBLE);
        }
        else
        {
//            attachment_layout.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(message_attachment_txt)/*.placeholder(R.drawable.loading)*/.into(attachment_content);

        }

        message_important.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // TODO perform your action here

                    if (inbox_outbox.equals("inbox")) {
                        if (message_important_txt == 0) {
                            message_important.setRating(1);
                            message_important_txt = 1;
                            int res = db.updateInboxImportant(new Elpfa("inbox_message_important", Integer.parseInt(message_id), 1));

                        } else {
                            message_important.setRating(0);
                            message_important_txt = 0;
                            int res = db.updateInboxImportant(new Elpfa("inbox_message_important", Integer.parseInt(message_id), 0));
                        }
                    }
                    else if (inbox_outbox.equals("outbox"))
                    {
                        //if from outbox
                        if (message_important_txt == 0) {
                            message_important.setRating(1);
                            message_important_txt = 1;
                            int res = db.updateOutboxImportant(new Elpfa("outbox_message_important", Integer.parseInt(message_id), 1));

                        } else {
                            message_important.setRating(0);
                            message_important_txt = 0;
                            int res = db.updateOutboxImportant(new Elpfa("outbox_message_important", Integer.parseInt(message_id), 0));
                        }
                    }
                }

                return true;
            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachment_content.buildDrawingCache();
                Bitmap bitmap = attachment_content.getDrawingCache();
                BitmapDrawable drawable = (BitmapDrawable)attachment_content.getDrawable();
                if (drawable!=null) {
                    Bitmap bitmap1 = drawable.getBitmap();
                    downloadAttachment(bitmap1);
                }
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(attachmentAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.msg_details_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                Toast.makeText(getApplicationContext(),"delete",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_read_unread:
                Toast.makeText(getApplicationContext(),"read unread",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void prepareMessage() {
        //retrieve message details
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        int count=0;
        if (inbox_outbox.equals("inbox"))
        {
            List<Elpfa> elpfa_all = db.getOneInboxMessage(Integer.parseInt(message_id));
            for (Elpfa elp : elpfa_all) {
                message_subject_txt=elp.getInbox_msg_subject();
                message_contact_name_txt=elp.getInbox_msg_sender();
                message_photo_txt=elp.getInbox_msg_photo();
                message_message_txt=elp.getInboxMessageMsg();
                message_important_txt=elp.getInbox_msg_important();
                message_timestamp=elp.getInbox_msg_date();
                message_attachment_txt=elp.getInbox_msg_attachment();
                if (!message_attachment_txt.equals("0"))
                {
                    List<String> new_attachment_list = Arrays.asList(message_attachment_txt.split(","));
                    for (int i=0;i<new_attachment_list.size();i++)
                    {
                        byte [] encodeByte= Base64.decode(new_attachment_list.get(i),Base64.DEFAULT);
                        Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                        url = new_attachment_list.get(i).substring(new_attachment_list.get(i).lastIndexOf("/") + 1, new_attachment_list.get(i).length());
                        String path=Config.BASE_CONTENT_URL+new_attachment_list.get(i);
                        path = path.replace(" ", "");
                        Attachment attachment = new Attachment("1",String.valueOf(attachment_count),attachment_count,"image",bitmap,url,"MessageDetails",path);
                        attachmentList.add(attachment);
                        attachmentAdapter.notifyDataSetChanged();
                        count++;
                    }
                }

            }
        }
        else if (inbox_outbox.equals("outbox"))
        {
            List<Elpfa> elpfa_all = db.getOneOutboxMessage(Integer.parseInt(message_id));
            for (Elpfa elp : elpfa_all) {
                message_subject_txt=elp.getOutbox_msg_subject();
                message_contact_name_txt=elp.getOutbox_msg_receiver();
                message_photo_txt=elp.getOutbox_msg_photo();
                message_message_txt=elp.getOutbox_msg_msg();
                message_important_txt=elp.getOutbox_msg_important();
                message_timestamp=elp.getOutbox_msg_date();
                message_attachment_txt=elp.getOutbox_msg_attachment();
                if (!message_attachment_txt.equals("0"))
                {
                    message_attachment_txt=Config.BASE_CONTENT_URL+message_attachment_txt;
                }
            }
        }

    }

    private void downloadAttachment(Bitmap finalBitmap) {

        Toast.makeText(getApplicationContext(),"Downloading...",Toast.LENGTH_SHORT).show();

        String root = Environment.getExternalStorageDirectory().toString();
        final File myDir = new File(root+"/"+getApplicationContext().getFilesDir() + "/elpfa");
        myDir.mkdirs(); //create folders where write files
        Random generator = new Random();
        n = 10000;
        n = generator.nextInt(n);
        file = new File(myDir, url);
        myDir.mkdirs();

        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            showNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void showNotification() {

        // TODO Auto-generated method stub
        //Creating Notification Builder
        builder = new NotificationCompat.Builder(MessageDetails.this);

        //Title for Notification
        builder.setContentTitle("Downloading...");
        //Message in the Notification
//        builder.setContentText("Downloading...");
        //Alert shown when Notification is received
        builder.setTicker("New Message Alert!");
        //Icon to be set on Notification
        builder.setSmallIcon(R.drawable.notification);
        //Creating new Stack Builder
        stackBuilder = TaskStackBuilder.create(MessageDetails.this);
        stackBuilder.addParentStack(CreateMessage.class);
        //Intent which is opened when notification is clicked

        resultIntent=new Intent();
        resultIntent.setAction(Intent.ACTION_VIEW);
        resultIntent.setDataAndType(Uri.fromFile(new File(String.valueOf(file))), "image/*");
        stackBuilder.addNextIntent(resultIntent);
        pIntent =  stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pIntent);
        builder.setAutoCancel(true);
        manager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(n, builder.build());


    }
    private LinearLayoutManager getLinearLayoutManager() {

//        if (mOrientation == Orientation.horizontal) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        return linearLayoutManager;
    }
}
