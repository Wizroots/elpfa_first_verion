package wizroots.elpfa;

/**
 * Created by android1 on 22-04-2016.
 */
public class Config {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

//    public static final String BASE_URL = "http://www.reflectivelearn.net/mlearning/android/";
    public static final String BASE_URL = "http://elpfa.co/android/";
    public static final String BASE_CONTENT_URL = "http://demo.elpfa.co/";

    public  static  final String load_account_info= BASE_URL + "load_account_info.php";
    public  static  final String change_password_url= BASE_URL + "change_password.php";
    public  static  final String load_notification_url= BASE_URL + "load_notification.php";
    public  static  final String clear_notification= BASE_URL + "clear_notification.php";
    public  static  final String clear_all_notifications= BASE_URL + "clear_all_notifications.php";
    public  static  final String clear_single_notification= BASE_URL + "clear_single_notification.php";
    public  static  final String clear_single_notification_new= BASE_URL + "clear_single_notification_new.php";
    public static final String ScheduledCourses= BASE_URL + "ScheduledCourses.php";
    public static final String ScheduledLessons= BASE_URL + "ScheduledLessons.php";
    public static final String update_personal_info_url= BASE_URL + "update_personal_info.php";
    public static final String contents= BASE_URL + "contents.php";
    public static final String update_content_status= BASE_URL + "update_content_status.php";
    public static final String imageContents= BASE_URL + "imageContents.php";
    public static final String CompletedCourses= BASE_URL + "CompletedCourses.php";
    public static final String AllCourses= BASE_URL + "Allcourses.php";
    public static final String sendCourseMsg= BASE_URL + "sendCourseMsg.php";
    public static final String getAllCourseMsg= BASE_URL + "getAllCourseMsg.php";
    public static final String registerGcm= BASE_URL + "register.php?regId=";
    public static final String registerFirebase= BASE_URL + "registerFirebase.php";
    public static final String fetchUpdatedCourseDetails = BASE_URL + "fetchUpdatedCourseDetails.php";
    public static final String courseStarted = BASE_URL + "courseStarted.php";
    public static final String EnrollNow = BASE_URL + "EnrollNow.php";
    public static final String fetchContentsOfCourse = BASE_URL + "FetchContentsOfCourse.php";
    public static final String InProgressCourses = BASE_URL + "InProgressCourses.php";
    public static final String getTrackOfCourse = BASE_URL + "getTrackOfCourse.php";
    public static final String getTrackOfContents = BASE_URL + "getTrackOfContents.php";
    public static final String NumberOfQuestions = BASE_URL + "NumberOfQuestions.php";
    public static final String load_all_without_question = BASE_URL + "load_all_without_question.php";
    public static final String load_question = BASE_URL + "load_question.php";
    public static final String LoadPassPercentage = BASE_URL + "load_pass_percentage.php";
    public static final String update_score = BASE_URL + "update_score.php";
    public static final String loadAllTeams = BASE_URL + "LoadAllTeams.php";
    public static final String SndMessageTeam = BASE_URL + "SndMessageTeam.php";
    public static final String NumberAssignedCourses = BASE_URL + "NumberAssignedCourses.php";
    public static final String NumberStartedCourses = BASE_URL + "NumberStartedCourses.php";
    public static final String NumberNotStartedCourses = BASE_URL + "NumberNotStartedCourses.php";
    public static final String NumberCompletedCourses = BASE_URL + "NumberCompletedCourses.php";
    public static final String ForgotPassword = BASE_URL + "forget_password.php";
    public static final String getTrackOfImprovementContent = BASE_URL + "getTrackofImprovmentContent.php";
    public static final String checkUserStatus = BASE_URL + "checkUserStatus.php";
    public static final String checkLearnerStatus = BASE_URL + "CheckLearnerStatus.php";
    public static final String SendMessageNew = BASE_URL + "SendMessageNew.php";
    public static final String editProfile = BASE_URL + "EditProfile.php";
    public static final String StartAssignedCourse = BASE_URL + "StartAssignedCourse.php";
    public static final String EnrollNowNew = BASE_URL + "EnrollNowNew.php";
    public static final String loadQuestionIds = BASE_URL + "load_question_ids.php";
    public static final String getAllTrackContent = BASE_URL + "getAllTrackContent.php";
    public static final String getAllContents = BASE_URL + "getAllContents.php";
    public static final String CheckTestAlreadyDone = BASE_URL + "check_test_already_done.php";
    public static final String checkTeamExist = BASE_URL + "checkTeamExist.php";
    public static final String checkUsersToTeamExist = BASE_URL + "checkUsersToTeamExist.php";
    public static final String updatePush = BASE_URL + "updatePush.php";
    public static final String ReceiverName = BASE_URL + "message_send_name.php";
    public static final String FetchMessages = BASE_URL + "FetchMessages.php";
    public static final String MessageViewCounter = BASE_URL + "messageViewCounter.php";
    public static final String FetchAllDetails = BASE_URL + "FetchAllDetails.php";
    public static final String GetCoursesAfterEnroll = BASE_URL + "GetCoursesAfterEnroll.php";
    public static final String FetchAllDetailsAfter = BASE_URL + "FetchAllDetailsAfter.php";
    public static final String FetchAllDetailsRefresh = BASE_URL + "FetchAllDetailsRefresh.php";
    public static final String FetchUserDetails = BASE_URL + "FetchUserDetails.php";
    public static final String SendMessage = BASE_URL + "send_message.php";
    public static final String Delete = BASE_URL + "delete_message.php";
    public static final String DeleteSent = BASE_URL + "delete_message_sent.php";
    public static final String DeleteMultiple = BASE_URL + "delete_multiple_message.php";
    public static final String DeleteMultipleSent = BASE_URL + "delete_multiple_message_sent.php";
    public static final String UpdateOfflineReadMessages = BASE_URL + "update_offline_read_messages.php";
    public static final String update_msg_viewed = BASE_URL + "update_msg_viewed.php";
    public static final String CheckForChatMessages = BASE_URL + "CheckForChatMessages.php";
    public static final String upload_user_photo_new = BASE_URL + "upload_user_photo_new.php";
    public static final String numRowsAll = BASE_URL + "numRowsAll.php";

}