package wizroots.elpfa;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;


public class FirstFragment extends Fragment implements ISlideBackgroundColorHolder {
    private RelativeLayout layoutContainer;
    ImageView monitor;
    Animation zoom_in;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        layoutContainer = (RelativeLayout) view.findViewById(R.id.main);
        monitor = (ImageView) view.findViewById(R.id.monitor);
         zoom_in = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
        monitor.startAnimation(zoom_in);
        zoom_in.setFillAfter(true);
        return view;
    }


    @Override
    public int getDefaultBackgroundColor() {
        return Color.parseColor("#009FE3");
    }

    @Override
    public void setBackgroundColor(@ColorInt int backgroundColor) {
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
