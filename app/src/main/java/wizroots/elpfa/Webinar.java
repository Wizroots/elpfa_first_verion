package wizroots.elpfa;

import android.animation.TimeInterpolator;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Webinar {
    private String webinar_id,webinar_name,webinar_agenda,webinar_starts_at,webinar_ends_at,webinar_duration,webinar_path;

    public   TimeInterpolator interpolator;

    public Webinar() {
    }

    public Webinar(String id,String name,String agenda,String starts_at,String ends_at,String duration,String path, TimeInterpolator interpolator) {
        webinar_id=id;
        this.webinar_name=name;
        this.webinar_agenda=agenda;
        this.webinar_starts_at=starts_at;
        this.webinar_ends_at=ends_at;
        this.webinar_duration=duration;
        this.webinar_path=path;
        this.interpolator = interpolator;

    }

    public String getWebunarId() {
        return webinar_id;
    }

    public void setWebinarId(String webinar_id) {
        this.webinar_id = webinar_id;
    }

    public String getWebinarName() {
        return webinar_name;
    }

    public void setWebinarName(String webinar_name) {
        this.webinar_name = webinar_name;
    }
    public String getWebunarAgenda() {
        return webinar_agenda;
    }

    public void setWebinarAgenda(String webinar_agenda) {
        this.webinar_agenda= webinar_agenda;
    }
    public String getWebinarStartsAt() {
        return webinar_starts_at;
    }

    public void setWebinarStartsAt(String webinar_starts_at) {
        this.webinar_starts_at= webinar_starts_at;
    }
    public String getWebinarEndsAt() {
        return webinar_ends_at;
    }

    public void setWebinarEndsAt(String webinar_ends_at) {
        this.webinar_ends_at= webinar_ends_at;
    }
    public String getWebinarDuration() {
        return webinar_duration;
    }

    public void setWebinarDuration(String webinar_duration) {
        this.webinar_duration= webinar_duration;
    }
    public String getWebunarPath() {
        return webinar_path;
    }

    public void setWebinarPath(String webinar_path) {
        this.webinar_path= webinar_path;
    }
}
