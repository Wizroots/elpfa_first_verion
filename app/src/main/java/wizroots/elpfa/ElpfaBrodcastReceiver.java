package wizroots.elpfa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by reflective2 on 18-11-2016.
 */

public class ElpfaBrodcastReceiver extends BroadcastReceiver {
    static int noOfTimes = 0;
    String num;
    JSONObject jObject;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    // Method gets called when Broad Case is issued from MainActivity for every 10 seconds
    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO Auto-generated method stub
        noOfTimes++;
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Config url_login1 = new Config();
                    /* using volley in login - combine everything */
        String tag_string_req1 = "req_state1";
        StringRequest strReq1 = new StringRequest(Request.Method.POST,
                url_login1.numRowsAll, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }


        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("value1", sharedpreferences.getString("User_id",null));
                return params;
            }

        };
        strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);



    }










//        AsyncHttpClient client = new AsyncHttpClient();
//        RequestParams params = new RequestParams();
//        // Checks if new records are inserted in Remote MySQL DB to proceed with Sync operation
//        client.post("http://192.168.2.4:9000/mysqlsqlitesync/getdbrowcount.php",params ,new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(String response) {
//                System.out.println(response);
//                try {
//                    // Create JSON object out of the response sent by getdbrowcount.php
//                    JSONObject obj = new JSONObject(response);
//                    System.out.println(obj.get("count"));
//                    // If the count value is not zero, call MyService to display notification
//                    if(obj.getInt("count") != 0){
//                        final Intent intnt = new Intent(context, MyService.class);
//                        // Set unsynced count in intent data
//                        intnt.putExtra("intntdata", "Unsynced Rows Count "+obj.getInt("count"));
//                        // Call MyService
//                        context.startService(intnt);
//                    }else{
//                        Toast.makeText(context, "Sync not needed", Toast.LENGTH_SHORT).show();
//                    }
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//
//
//            @Override
//            public void onFailure(int statusCode, Throwable error,
//                                  String content) {
//                // TODO Auto-generated method stub
//                if(statusCode == 404){
//                    Toast.makeText(context, "404", Toast.LENGTH_SHORT).show();
//                }else if(statusCode == 500){
//                    Toast.makeText(context, "500", Toast.LENGTH_SHORT).show();
//                }else{
//                    Toast.makeText(context, "Error occured!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
}
