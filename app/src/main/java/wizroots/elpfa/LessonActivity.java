package wizroots.elpfa;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class LessonActivity extends AppCompatActivity {

    String users_to_lesson_id;
    String lesson_name;
    int lesson_completed;
    String lesson_type;
    int lesson_viewed;
    int lesson_tag;

    private Orientation mOrientation;
    public final static String TAG_ORIENTATION = "vertical";
    private RecyclerView recyclerView;
    DatabaseHandler db1;
    String course_id;
    private LessonAdapter lessonAdapter;
    TextView module_description;
    int height, width;

    ImageView clode_dialog;
    TextView module_name_dialog;
    DonutProgress module_progress_dialog;
    TextView module_description_dialog;
    Toolbar toolbar;

    private List<Lessons> lessonList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        db1=new DatabaseHandler(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        module_description=(TextView)findViewById(R.id.module_description);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.back_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LessonActivity.this.finish();
            }
        });
        toolbar.setTitle("Module Name");

        lessonAdapter = new LessonAdapter(lessonList);

        mOrientation = (Orientation) getIntent().getSerializableExtra(TAG_ORIENTATION);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);

//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(lessonAdapter);
        prepareContents();
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));

        module_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(LessonActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.module_details);
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                width = size.x;
                height = size.y;
                int new_height=height-200;
                dialog1.getWindow().setLayout(width, new_height);
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);

                clode_dialog=(ImageView)dialog1.findViewById(R.id.close);
                module_name_dialog=(TextView)dialog1.findViewById(R.id.module_name_dialog);
                module_description_dialog=(TextView)dialog1.findViewById(R.id.module_description_dialog);
                module_progress_dialog=(DonutProgress)dialog1.findViewById(R.id.module_progress_dialog);
                clode_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.cancel();
                    }
                });

                dialog1.show();
            }
        });
    }
    private LinearLayoutManager getLinearLayoutManager() {

        if (mOrientation == Orientation.horizontal) {

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            return linearLayoutManager;
        } else {
//
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            return linearLayoutManager;
        }

    }
//    public void prepareLessons()
//    {
//        lessonList.clear();
//        for(int i = 0;i <4;i++) {
//            LessonsModel model = new LessonsModel();
//            if (i==0||i==1||i==3)
//            {
//                model.setViewed(1);
//            }
//            else {
//                model.setViewed(0);
//
//            }
//            model.setTag(i);
//            model.setLessonName("NAME");
//            lessonList.add(model);
//        }
//
//        mTimeLineAdapter = new TimeLineAdapter(mDataList, mOrientation);
//        mRecyclerView.setAdapter(mTimeLineAdapter);
//    }

    public void prepareContents()
    {

//        db1=new DatabaseHandler(getApplicationContext());
//        course_id=getIntent().getStringExtra("course_id");
//        Log.d("course_check","in course contents : "+course_id);
//
//        List<Elpfa> elpfa_course = db1.getCourseDetails(Integer.parseInt(course_id));
//        for (Elpfa elp : elpfa_course) {
//            course_name=elp.getAllCourseName();
//            course_description=elp.getAllDescription();
//            course_assign_date=elp.getAllAssignDate();
//            course_progress=elp.getAllTrackCourse();
//            course_enroll=elp.getAllEnrollmentKey();
//            course_image_str=elp.getAllCoursePath();
//            if (course_enroll==0)
//            {
//                course_type="Course Type : Assigned";
//                course_type_txt.setText("Course type: Assigned");
//                course_type_dialog_txt="Course type : Assigned";
//            }
//            else
//            {
//                course_type="Course Type : Self Enroll";
//                course_type_txt.setText("Course type: Self enroll");
//                course_type_dialog_txt="Course type : Self enroll";
//            }
//            started=elp.getAllCourseStarted();
//            completed=elp.getAllCourseCompleted();
//            if (started==0)
//            {
//                course_state="Course State : Not Started";
//            }
//            else if (completed==1)
//            {
//                course_state="Course State : Completed";
//            }
//            else if (started==1)
//            {
//                course_state="Course State : In Progress";
//            }
//
//            Log.d("course_check","course name : "+course_name);
//            Log.d("course_check","course description : "+course_description);
//            Log.d("course_check","course assign date : "+course_assign_date);
//            Log.d("course_check","course progress : "+course_progress);
//            Log.d("course_check","type : "+course_enroll);
//        }
//        course_name_txt.setText(""+course_name);
////        course_name_see_more.setText(""+course_name);
//        course_description_txt.setText(""+course_description);
////        course_des.setText(""+course_description);
//        course_assign_date_txt.setText("Assign date: "+course_assign_date);
//
//
//
//        Picasso.with(getApplicationContext()).load(course_image_str).into(course_image);
////        course_date_see_more.setText(""+course_assign_date);
////        course_progress_progressbar.setProgress(Integer.parseInt(course_progress));
////        course_progress_see_more.setProgress(Integer.parseInt(course_progress));
//
//        if (course_enroll==1)
//        {
//            course_assign_date_txt.setVisibility(View.INVISIBLE);
////            course_date_see_more.setVisibility(View.INVISIBLE);
//        }
//        if (course_enroll==0)
//        {
//            course_type="Course Type : Assigned";
//        }
//        else
//        {
//            course_type="Course Type : Self Enroll";
//        }
//        if (course_state.equals("Course State : Not Started"))
//        {
//
//            if (course_type.equals("Course Type : Assigned"))
//            {
//                state_rel.setVisibility(View.VISIBLE);
//                state_txt.setText("Start now");
//                left.setImageResource(R.drawable.circle_holo_blue);
//                right.setImageResource(R.drawable.circle_holo_blue);
//                numberProgressBar.setProgressTextSize(0);
//                numberProgressBar.setProgress(0);
//            }
//            else if (course_type.equals("Course Type : Self Enroll"))
//            {
//                state_rel.setVisibility(View.VISIBLE);
//                state_txt.setText("Enroll now");
//                left.setImageResource(R.drawable.circle_holo_blue);
//                right.setImageResource(R.drawable.circle_holo_blue);
//                numberProgressBar.setProgressTextSize(0);
//                numberProgressBar.setProgress(0);
//            }
//        }
//        else if (course_state.equals("Course State : Completed"))
//        {
//            state_rel.setVisibility(View.VISIBLE);
//            state_txt.setText("Completed");
//            left.setImageResource(R.drawable.circle_bk_blue);
//            right.setImageResource(R.drawable.circle_bk_blue);
//            numberProgressBar.setProgressTextSize(0);
//            numberProgressBar.setProgress(100);
//        }
//        else if (course_state.equals("Course State : In Progress"))
//        {
//            if (course_progress.equals("0"))
//            {
//                numberProgressBar.setProgress(Integer.parseInt(course_progress));
//                numberProgressBar.setProgressTextSize(0);
//                left.setImageResource(R.drawable.circle_bk_blue);
//                right.setImageResource(R.drawable.circle_holo_blue);
//                state_rel.setVisibility(View.VISIBLE);
//                state_txt.setText("Started");
//            }
//            else if (course_progress.equals("100"))
//            {
//                numberProgressBar.setProgress(100);
//                numberProgressBar.setProgressTextSize(0);
//                left.setImageResource(R.drawable.circle_bk_blue);
//                right.setImageResource(R.drawable.circle_bk_blue);
//                state_rel.setVisibility(View.VISIBLE);
//                state_txt.setText("Completed");
//            }
//            else
//            {
//                numberProgressBar.setProgress(Integer.parseInt(course_progress));
//                left.setImageResource(R.drawable.circle_bk_blue);
//                right.setImageResource(R.drawable.circle_holo_blue);
//                state_rel.setVisibility(View.INVISIBLE);
//            }
//        }
//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolbar.setNavigationIcon(R.drawable.back_blue);
//        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
//        collapsingToolbarLayout.setContentScrimColor(Color.rgb(25,118,210));
//        collapsingToolbarLayout.setTitle("");
//
//        //get allcourse contents
//        users_to_content_id_arr.clear();
//        content_completed_arr.clear();
//        content_path_arr.clear();
//        content_name_arr.clear();
//        content_type_arr.clear();
//
//        count=0;
//        List<Elpfa> elpfa_course_contents = db1.getCourseContents(Integer.parseInt(course_id));
//        for (Elpfa elp : elpfa_course_contents) {
//            users_to_content_id= String.valueOf(elp.getUsersToContentsUsersToContentId());
//            content_completed= String.valueOf(elp.getUsersToContentsCompleted());
//            content_type= elp.getUsersToContentsType();
//            content_name= elp.getUsersToContentsName();
//            content_path= elp.getUsersToContentsPath();
//            users_to_content_id_arr.add(count,users_to_content_id);
//            content_completed_arr.add(count,content_completed);
//            content_type_arr.add(count,content_type);
//            content_name_arr.add(count,content_name);
//            content_path_arr.add(count,content_path);
//            count++;
//            Contents contents = new Contents(users_to_content_id,content_name,Integer.parseInt(content_completed),content_type,content_path);
//            contentList.add(contents);
//
//            contentAdapter.notifyDataSetChanged();
//        }
        int completed;
        String type;
        for (int i=0;i<10;i++)
        {
            if (i==0||i==1||i==2)
            {
                completed=1;
            }
            else {
                completed=0;
            }
            if (i==2||i==6||i==9)
            {
                type="Gallery";
            }
            else if (i==0||i==1||i==4)
            {
                type="Video";
            }
            else if (i==8){

                type="Text";
            }
            else if (i==5)
            {
                type="Link";
            }
            else
            {
                type="Test";
            }
            Lessons lessons = new Lessons(String.valueOf(i),"Lesson "+i,completed,type,i,mOrientation);
            lessonList.add(lessons);

            lessonAdapter.notifyDataSetChanged();
        }
    }
}
