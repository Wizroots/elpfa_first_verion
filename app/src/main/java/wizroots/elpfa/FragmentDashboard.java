package wizroots.elpfa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDashboard#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDashboard extends Fragment implements HandleVolleyResponse{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    int count = 0;
    int count1 = 0;
    @BindView(R.id.txt)
    TextView txt;
    Unbinder unbinder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView email;
    View rootview;
    TextView first_letter;
    Toolbar toolbar;
    String profile_image_color;
    AppBarLayout appbar_ayout;
    Animation fadeOut;
    Animation fadeIn;
    TextView number_asigned_txt, number_started_txt, number_not_started_txt, number_completed_txt;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    ActionBarDrawerToggle toggle;
    CollapsingToolbarLayout collapsingToolbarLayout;
    CardView card;
    RelativeLayout profile_color_text;
    private OnFragmentInteractionListener mListener;
    CircleImageView img;
    DatabaseHandler db;
    CollapsingToolbarLayout coll;
    private boolean isAllAvilable = false;
    private JSONObject jObject;
    private SharedPreferences.Editor editor;
    String number_started, number_assigned, number_not_started, number_completed;
    String compl_path, compl_name, compl_desc, compl_startdate, compl_course_id, compl_track_content, compl_users_to_courseid, compl_completion_date;
    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
    int scheduled_completed, progress_completed, all_completed, compl_completed;
    String compl_duration, compl_pass_percent, compl_test_mandatory, compl_no_of_que;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;
    int compl_enrollment_key,compl_status, scheduled_enrollment_key,scheduled_status, progress_enrollment_key, progress_status,all_enrollment_key,all_status;
    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
    String user_id, userid;
    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();
    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;
    String enroll_contents_course_id;
    String enroll_contents_name;
    private JSONObject jObject1;


    public FragmentDashboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDashboard.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDashboard newInstance(String param1, String param2) {
        FragmentDashboard fragment = new FragmentDashboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    //    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_fragment_dashboard, container, false);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        db = new DatabaseHandler(getActivity());
        toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
        profile_color_text = (RelativeLayout) rootview.findViewById(R.id.profile_color_text);
        first_letter = (TextView) rootview.findViewById(R.id.first_letter);
        number_asigned_txt = (TextView) rootview.findViewById(R.id.number_assigned_txt);
        number_started_txt = (TextView) rootview.findViewById(R.id.number_started_txt);
        number_not_started_txt = (TextView) rootview.findViewById(R.id.number_not_started_txt);
        number_completed_txt = (TextView) rootview.findViewById(R.id.number_completed_txt);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        Transition fade = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            fade = new Fade();
            fade.excludeTarget(android.R.id.statusBarBackground, true);
            fade.excludeTarget(android.R.id.navigationBarBackground, true);
            fade.excludeTarget(R.id.root1, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().getWindow().setExitTransition(fade);
                getActivity().getWindow().setEnterTransition(fade);
            }

        }
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        coll = (CollapsingToolbarLayout) rootview.findViewById(R.id.collapsingToolbarLayout);
        email = (TextView) rootview.findViewById(R.id.email);
        appbar_ayout = (AppBarLayout) rootview.findViewById(R.id.app_bar_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        coll.setTitle("Anusree");
        coll.setContentScrimColor(Color.parseColor("#1976D2"));
//        coll.setCollapsedTitleTextColor(Color.parseColor("#1976D2"));
//        coll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.d("scroll","v : "+v);
//                Log.d("scroll","scrollX : "+scrollX);
//                Log.d("scroll","scrollX : "+scrollX);
//            }
//        });

//        coll.setExpandedTitleMarginTop(100);
        img = (CircleImageView) rootview.findViewById(R.id.img);
        fadeOut.setFillAfter(true);
        fadeIn.setFillAfter(true);
        appbar_ayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset < 0) {

                    if (count == 0) {
                        if (count1 == 0) {
                            img.startAnimation(fadeOut);
                            profile_color_text.startAnimation(fadeOut);
                            email.startAnimation(fadeOut);
                            email.setVisibility(View.INVISIBLE);
                            img.setVisibility(View.INVISIBLE);

                            count = 1;
                        }
                        count1 = 1;
//                    email.setVisibility(View.VISIBLE);
//                    img.setVisibility(View.VISIBLE);
                    }
                } else if (verticalOffset == 0) {
                    if (count == 1) {

                        email.setVisibility(View.VISIBLE);
                        img.setVisibility(View.VISIBLE);
                        img.startAnimation(fadeIn);
                        profile_color_text.startAnimation(fadeIn);
                        email.startAnimation(fadeIn);
                        count = 0;
                        count1 = 0;
                    }


                }

            }
        });
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String path = sharedpreferences.getString("photo", "no_img");
        if (path.equals("no_img")) {
            profile_image_color = sharedpreferences.getString("profile_image_color", "grey");
            if (profile_image_color.equals("purple")) {
                img.setColorFilter(getResources().getColor(R.color.purple));
            } else if (profile_image_color.equals("pink")) {
                img.setColorFilter(getResources().getColor(R.color.pink));
            } else if (profile_image_color.equals("lime")) {
                img.setColorFilter(getResources().getColor(R.color.lime));
            } else if (profile_image_color.equals("orange")) {
                img.setColorFilter(getResources().getColor(R.color.orange));
            } else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        } else {
            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getActivity()).load(sharedpreferences.getString("photo", "no_img")).into(img);
        }
        email.setText("" + sharedpreferences.getString("Username", null));
        coll.setTitle("" + sharedpreferences.getString("person_name", null));
        String first_txt = sharedpreferences.getString("person_name", null).substring(0, 1);
        first_letter.setText("" + first_txt.toUpperCase());
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), Profile.class);
//// Pass data object in the bundle and populate details activity.
////                intent.putExtra(DetailsActivity.EXTRA_CONTACT, contact);
//                ActivityOptionsCompat options = ActivityOptionsCompat.
//                        makeSceneTransitionAnimation(getActivity(), (View)img, "profile");
//                startActivity(intent, options.toBundle());


                Intent intent = new Intent(getActivity(), Profile.class);
                Pair<View, String> p1 = Pair.create((View) img, "profile");
                Pair<View, String> p2 = Pair.create((View) first_letter.findViewById(R.id.first_letter), "first_letter");
                Pair<View, String> p3 = Pair.create((View) rootview.findViewById(R.id.root1), "root_transition");
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(getActivity(), p1, p2, p3);
                startActivity(intent, options.toBundle());


            }
        });

        unbinder = ButterKnife.bind(this, rootview);
        displayCourseReports();

        return rootview;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onVolleyResponse(boolean isSuccess) {
        displayCourseReports();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_noitification) {
            Intent i = new Intent(getActivity(), Notification.class);
            startActivity(i);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isNetworkAvailable(getActivity())){
            new LoadCourseCountAsyncTask().execute();
        }
        displayCourseReports();
        Log.d("CheckonMainActivity","onResume");
        ((MainActivity) getActivity()).updateHeaderImage();
        String first_txt = sharedpreferences.getString("person_name", null).substring(0, 1);
        first_letter.setText("" + first_txt.toUpperCase());
        coll.setTitle("" + sharedpreferences.getString("person_name", null));
        String path = sharedpreferences.getString("photo", "no_img");
        if (path.equals("no_img")) {
            profile_image_color = sharedpreferences.getString("profile_image_color", "grey");
            if (profile_image_color.equals("purple")) {
                img.setColorFilter(getResources().getColor(R.color.purple));
            } else if (profile_image_color.equals("pink")) {
                img.setColorFilter(getResources().getColor(R.color.pink));
            } else if (profile_image_color.equals("lime")) {
                img.setColorFilter(getResources().getColor(R.color.lime));
            } else if (profile_image_color.equals("orange")) {
                img.setColorFilter(getResources().getColor(R.color.orange));
            } else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        } else {
            img.setVisibility(View.VISIBLE);
            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getActivity()).load(sharedpreferences.getString("photo", "no_img")).placeholder(R.color.grey).into(img);
        }

    }

    public void displayCourseReports() {
        List<Elpfa> elpfa = db.getCourseReports();
        for (Elpfa elp : elpfa) {
            number_assigned = elp.getAssignedCourses();
            Log.d("checkNumbers","number_assigned : " + number_assigned);
            number_started = elp.getStartedCourses();
            Log.d("checkNumbers","number_started : " + number_started);
            number_not_started = elp.getNotStartedCourses();
            Log.d("checkNumbers","number_not_started : " + number_not_started);
            number_completed = elp.getCompletedCourses();
            Log.d("checkNumbers","number_completed : " + number_completed);
        }
        number_asigned_txt.setText(sharedpreferences.getString("assigned", ""));
        number_started_txt.setText(sharedpreferences.getString("started", ""));
        number_not_started_txt.setText(sharedpreferences.getString("not_started", ""));
        number_completed_txt.setText(sharedpreferences.getString("completed", ""));
    }

    class LoadCourseCountAsyncTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                Config config = new Config();
                String tag_string_req = "req_state";
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        config.FetchAllDetails, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        if (response.equals("Not successfull")) {
                            onVolleyResponse(isAllAvilable);
                        } else {
                            try {

                                sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//

                                try {
                                    // fetch all user types details
                                    DatabaseHandler db1 = new DatabaseHandler(getActivity());
                                    jObject = new JSONObject(response);


                                    // end getting user details
                                    db1.deleteCourseReportsTable();
                                    //fetch number of assigned courses
                                    if (!jObject.isNull("number_assigned_courses")) {

                                        JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                                        JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                                        number_assigned = Jasonobject.getString("count(*)");

                                        editor.putString("assigned", number_assigned);
                                        editor.commit();
                                    }
                                    //fetch number of started courses
                                    if (!jObject.isNull("number_started_courses")) {

                                        JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                                        JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                                        number_started = JasonobjectStarted.getString("count(*)");

                                        editor.putString("started", number_started);
                                        editor.commit();
                                    }
                                    //fetch number of not-started courses
                                    if (!jObject.isNull("number_not_started_courses")) {

                                        JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                                        JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                                        number_not_started = JasonobjectNotStarted.getString("count(*)");

                                        editor.putString("not_started", number_not_started);
                                        editor.commit();

                                    }
                                    //fetch number of completed courses
                                    if (!jObject.isNull("number_completed_courses")) {

                                        JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                                        JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                                        number_completed = JasonobjectCompleted.getString("count(*)");

                                        editor.putString("completed", number_completed);
                                        editor.commit();
                                    }

                                            /* add to course reports table */
                                    db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

                                    //fetch all  courses

                                    editor.putString("json_String_scheduled", response);
                                    editor.putString("json_String_progress", response);
                                    editor.putString("json_String_completed", response);
                                    editor.putString("json_String_all", response);
                                    editor.commit();

                                    //get all completed courses
                                    db1.deleteCompletedCoursesTable();
                                    if (!jObject.isNull("completed_courses")) {
                                        JSONArray jsonArrayCompletedCourses = jObject.getJSONArray("completed_courses");

                                        final int completed_length = jsonArrayCompletedCourses.length();

                                        for (int i = 0; i < completed_length; i++) {
                                            JSONObject JasonobjectCompl = jsonArrayCompletedCourses.getJSONObject(i);
                                            compl_path = Config.BASE_CONTENT_URL + JasonobjectCompl.getString("path");

                                            compl_name = JasonobjectCompl.getString("name");
                                            compl_desc = JasonobjectCompl.getString("description");
                                            compl_startdate = JasonobjectCompl.getString("assign_date");
                                            compl_course_id = JasonobjectCompl.getString("course_id");
                                            compl_enrollment_key = JasonobjectCompl.getInt("enrollment_key");
                                            compl_status = JasonobjectCompl.getInt("status");
                                            compl_track_content = String.valueOf(JasonobjectCompl.getInt("track_course"));
                                            user_id = JasonobjectCompl.getString("user_id");
                                            compl_users_to_courseid = JasonobjectCompl.getString("id");
                                            compl_duration = JasonobjectCompl.getString("course_duration");
                                            compl_no_of_que = JasonobjectCompl.getString("no_of_que");
                                            compl_pass_percent = JasonobjectCompl.getString("pass_percent");
                                            compl_test_mandatory = JasonobjectCompl.getString("test_mandatory");
                                            compl_startedd = JasonobjectCompl.getInt("started");
                                            compl_completed = JasonobjectCompl.getInt("completed");
                                            compl_completion_date = JasonobjectCompl.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addCompletedCourses(new Elpfa("completed", compl_course_id, compl_users_to_courseid, compl_name, compl_desc, compl_duration, compl_enrollment_key, compl_status,compl_path, compl_startdate, compl_track_content, compl_no_of_que, compl_pass_percent, compl_test_mandatory, compl_startedd, compl_completed, compl_completion_date));


                                        }


                                    }

                                    // end fetch all  completed courses


                                    /******** fetch all scheduled courses ***********/


                                    db1.deleteScheduledCoursesTable();
                                    if (!jObject.isNull("scheduled_courses")) {
                                        JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                                        final int scheduled_length = jsonArrayScheduledCourses.length();

                                        for (int i = 0; i < scheduled_length; i++) {
                                            JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                                            scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");

                                            scheduled_name = JasonobjectScheduled.getString("name");
                                            scheduled_desc = JasonobjectScheduled.getString("description");
                                            scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                                            scheduled_course_id = JasonobjectScheduled.getString("course_id");
                                            scheduled_enrollment_key = JasonobjectScheduled.getInt("enrollment_key");
                                            scheduled_status = JasonobjectScheduled.getInt("status");
                                            scheduled_track_content = String.valueOf(JasonobjectScheduled.getInt("track_course"));
                                            user_id = JasonobjectScheduled.getString("user_id");
                                            scheduled_users_to_courseid = JasonobjectScheduled.getString("id");
                                            scheduled_duration = JasonobjectScheduled.getString("course_duration");
                                            scheduled_no_of_que = JasonobjectScheduled.getString("no_of_que");
                                            scheduled_pass_percent = JasonobjectScheduled.getString("pass_percent");
                                            scheduled_test_mandatory = JasonobjectScheduled.getString("test_mandatory");
                                            scheduled_startedd = JasonobjectScheduled.getInt("started");
                                            scheduled_completed = JasonobjectScheduled.getInt("completed");
                                            scheduled_completion_date = JasonobjectScheduled.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addScheduledCourses(new Elpfa("scheduled", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status,scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));


                                        }


                                    }

                                    /**********  end fetch all  completed courses ***********/


                                    // end fetch all in progress courses

                                    /******** fetch all in progress courses ***********/


                                    db1.deleteInProgressCoursesTable();
                                    if (!jObject.isNull("progress_courses")) {
                                        JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                                        final int progress_length = jsonArrayInProgressCourses.length();

                                        for (int i = 0; i < progress_length; i++) {
                                            JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                                            progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");

                                            progress_name = JasonobjectProgress.getString("name");
                                            progress_desc = JasonobjectProgress.getString("description");
                                            progress_startdate = JasonobjectProgress.getString("assign_date");
                                            progress_course_id = JasonobjectProgress.getString("course_id");
                                            progress_enrollment_key = JasonobjectProgress.getInt("enrollment_key");
                                            progress_status = JasonobjectProgress.getInt("status");
                                            progress_track_content = String.valueOf(JasonobjectProgress.getInt("track_course"));
                                            user_id = JasonobjectProgress.getString("user_id");
                                            progress_users_to_courseid = JasonobjectProgress.getString("id");
                                            progress_duration = JasonobjectProgress.getString("course_duration");
                                            progress_no_of_que = JasonobjectProgress.getString("no_of_que");
                                            progress_pass_percent = JasonobjectProgress.getString("pass_percent");
                                            progress_test_mandatory = JasonobjectProgress.getString("test_mandatory");
                                            progress_startedd = JasonobjectProgress.getInt("started");
                                            progress_completed = JasonobjectProgress.getInt("completed");
                                            progress_completion_date = JasonobjectProgress.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status,progress_path, progress_startdate, progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory, progress_startedd, progress_completed, progress_completion_date));


                                        }

                                    }

                                    /**********  end fetch all  in progress courses ***********/



                                    /******** fetch all courses ***********/


                                    db1.deleteAllCoursesTable();
                                    db1.deleteToTable();
                                    if (!jObject.isNull("all_courses")) {
                                        JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                                        final int progress_length = jsonArrayAllCourses.length();

                                        for (int i = 0; i < progress_length; i++) {
                                            JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                                            all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                                            all_name = JasonobjectAll.getString("name");
                                            all_desc = JasonobjectAll.getString("description");
                                            all_startdate = JasonobjectAll.getString("assign_date");
                                            all_course_id = JasonobjectAll.getString("course_id");
                                            all_enrollment_key = JasonobjectAll.getInt("enrollment_key");
                                            all_status = JasonobjectAll.getInt("status");
                                            all_track_content = String.valueOf(JasonobjectAll.getInt("track_course"));
                                            user_id = JasonobjectAll.getString("user_id");
                                            all_users_to_courseid = JasonobjectAll.getString("id");
                                            all_duration = JasonobjectAll.getString("course_duration");
                                            all_no_of_que = JasonobjectAll.getString("no_of_que");
                                            all_pass_percent = JasonobjectAll.getString("pass_percent");
                                            all_test_mandatory = JasonobjectAll.getString("test_mandatory");
                                            all_startedd = JasonobjectAll.getInt("started");
                                            all_completed = JasonobjectAll.getInt("completed");
                                            all_completion_date = JasonobjectAll.getString("completion_date");

                                            //insert completed courses into sqlite

                                            db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status,all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory, all_startedd, all_completed, all_completion_date));
                                            db1.addTo(new Elpfa("to",all_course_id,all_name,"no_email","course"));


                                        }
                                    }

                                    /**********  end fetch all  completed courses ***********/



                                    /**********  end fetch notifications ***********/
//msg inbx
                                    Config url_login1 = new Config();

                    /* using volley in login - combine everything */
                                    String tag_string_req1 = "req_state1";
                                    StringRequest strReq1 = new StringRequest(Request.Method.POST,
                                            url_login1.FetchMessages, new Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String response) {
                                            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            editor.putString("MessageReceiver", response);
                                            editor.commit();
                                            try {

                                                try {

                                                    sharedpreferences =  getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                                    try {

                                                        DatabaseHandler db1 = new DatabaseHandler( getActivity());

                                                        jObject1 = new JSONObject(response);

                                                        //enroll contents
                                                        db1.deleteEnrollContents();
                                                        if (!jObject.isNull("enroll_contents")) {
                                                            JSONArray jsonArrayEnrollContents = jObject.getJSONArray("enroll_contents");
                                                            int enroll_contents_length = jsonArrayEnrollContents.length();
                                                            for (int i = 0; i < enroll_contents_length; i++) {
                                                                JSONObject JasonobjectEnrollContents = jsonArrayEnrollContents.getJSONObject(i);
                                                                {
                                                                    enroll_contents_course_id = JasonobjectEnrollContents.getString("course_id");
                                                                    enroll_contents_name = JasonobjectEnrollContents.getString("name");
                                                                    db1.addEnrollContents(new Elpfa("enroll_contents", enroll_contents_course_id, enroll_contents_name));

                                                                }

                                                            }



                                                            /**********  end fetch team members ***********/

                                                            /******** fetch all contents ***********/
                                                            db1.deleteUsersToContentsTable();
                                                            if (!jObject.isNull("user_contents")) {
                                                                contents_course_id_arr.clear();
                                                                contents_usersToCourse_id_arr.clear();
                                                                contents_viewed_arr.clear();
                                                                contents_completed_arr.clear();
                                                                contents_type_arr.clear();
                                                                contents_name_arr.clear();
                                                                contents_path_arr.clear();

                                                                JSONArray jsonArrayContents = jObject.getJSONArray("user_contents");
                                                                int contents_length = jsonArrayContents.length();
                                                                for (int i = 0; i < contents_length; i++) {
                                                                    JSONObject JasonobjectContents = jsonArrayContents.getJSONObject(i);
                                                                    contents_course_id = JasonobjectContents.getInt("course_id");
                                                                    contents_users_to_course_id = JasonobjectContents.getInt("id");
                                                                    contents_completed = JasonobjectContents.getInt("status");
                                                                    if (contents_completed == 0) {
                                                                        contents_viewed = 0;
                                                                    } else {
                                                                        contents_viewed = 1;
                                                                    }

                                                                    contents_type = JasonobjectContents.getString("item_name");
                                                                    contents_name = JasonobjectContents.getString("name");
                                                                    if (contents_type.equals("youtube")) {
                                                                        contents_path = JasonobjectContents.getString("path");
                                                                    } else {
                                                                        contents_path = Config.BASE_CONTENT_URL + JasonobjectContents.getString("path");
                                                                    }

                                                                    contents_course_id_arr.add(i, contents_course_id);
                                                                    contents_usersToCourse_id_arr.add(i, contents_users_to_course_id);
                                                                    contents_viewed_arr.add(i, contents_viewed);
                                                                    contents_completed_arr.add(i, contents_completed);
                                                                    contents_type_arr.add(i, contents_type);
                                                                    contents_name_arr.add(i, contents_name);
                                                                    contents_path_arr.add(i, contents_path);


                                                                    //insert team members into sqlite
                                                                    db1.addUsersToContents(new Elpfa(contents_course_id, contents_users_to_course_id, contents_viewed, contents_completed, contents_type, contents_name, contents_path));


                                                                }
                                                            }
                                                        }

                                                        isAllAvilable = true;
                                                        onVolleyResponse(isAllAvilable);


                                                    } catch (Exception e) {
                                                        onVolleyResponse(isAllAvilable);
                                                    }
//

                                                } catch (Exception e) {
                                                    onVolleyResponse(isAllAvilable);

                                                }

                                                isAllAvilable = true;
                                                onVolleyResponse(isAllAvilable);
                                            } catch (Exception e) {
                                                onVolleyResponse(isAllAvilable);
                                            }

                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            onVolleyResponse(isAllAvilable);
                                        }
                                    }) {

                                        @Override
                                        protected Map<String, String> getParams() {
                                            // Posting parameters to login url
                                            Map<String, String> params = new HashMap<String, String>();
                                            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                            params.put("value1", sharedpreferences.getString("User_id", null));
                                            return params;
                                        }

                                    };
                                    strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    // Adding request to request queue
                                    AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);

//end msg inbx

//end msg inbx


                                } catch (Exception e) {
                                    onVolleyResponse(isAllAvilable);
                                }
//

                            } catch (Exception e) {

                            }

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("value1", sharedpreferences.getString("Username", null));
                        params.put("value2", sharedpreferences.getString("Password", null));
                        return params;
                    }

                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
            } catch (Exception ex) {
            }
            return null;
        }
    }
}
