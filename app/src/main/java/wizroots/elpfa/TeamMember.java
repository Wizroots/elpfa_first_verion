package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class TeamMember {
    private String member_name;
    private String member_image;
    private String member_id;

    public TeamMember() {
    }

    public TeamMember(String id, String name, String img) {
        this.member_id = id;
        this.member_name = name;
        this.member_image = img;
    }

    //id
    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    //name
    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
    }

    //image
    public String getMember_image() {
        return member_image;
    }

    public void setMember_image(String member_image) {
        this.member_image = member_image;
    }

}
