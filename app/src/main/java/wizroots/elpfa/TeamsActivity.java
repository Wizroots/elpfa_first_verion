package wizroots.elpfa;

import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class TeamsActivity extends AppCompatActivity implements TeamInfoFragment.OnFragmentInteractionListener, TeamChatFragment.OnFragmentInteractionListener {


    private ViewPager mViewPager;
    private String teamName;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);


        teamName = getIntent().getStringExtra("team_name");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(teamName);
        }
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager() {
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        Fragment fragment1 = new TeamInfoFragment();
        Bundle args1 = new Bundle();
        args1.putString("team_id", getIntent().getStringExtra("team_id"));
        args1.putString("team_name", teamName);
        args1.putString("team_description", getIntent().getStringExtra("team_description"));
        args1.putString("team_path", getIntent().getStringExtra("team_path"));
        fragment1.setArguments(args1);

        Fragment fragment2 = new TeamChatFragment();
        Bundle args2 = new Bundle();
        args2.putString("team_id", getIntent().getStringExtra("team_id"));
        args2.putString("team_name", teamName);
        fragment2.setArguments(args2);

        mSectionsPagerAdapter.addFragment(fragment1, "TEAM INFO");
        mSectionsPagerAdapter.addFragment(fragment2, "TEAM CHAT");
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a Fragment.
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            // Show total pages.
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }
}
