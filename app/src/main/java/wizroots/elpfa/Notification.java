package wizroots.elpfa;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.tubb.smrv.SwipeMenuLayout;
import com.tubb.smrv.SwipeMenuRecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Notification extends AppCompatActivity {
    Toolbar toolbar;
    String count;
    private List<NotificationNew> notifications;
    private AppAdapter mAdapter;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private SwipeMenuRecyclerView mRecyclerView;
    DatabaseHandler db1;
    String notif_id_backup;
    NotificationNew notification_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mRecyclerView = (SwipeMenuRecyclerView) findViewById(R.id.listView);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        // interpolator setting
        mRecyclerView.setOpenInterpolator(new BounceInterpolator());
        mRecyclerView.setCloseInterpolator(new BounceInterpolator());

        notifications = getNotifications();
        mAdapter = new AppAdapter(this, notifications);
        mRecyclerView.setAdapter(mAdapter);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.back_white);
        toolbar.setTitle("Notifications");
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        count=sharedpreferences.getString("count",null);
        setSupportActionBar(toolbar);
        Log.d("initial_list_size","size : "+mRecyclerView.getChildCount());
        if (notifications.size()==0)
        {
            findViewById(R.id.no_notifications).setVisibility(View.VISIBLE);
        }
        if (count.equals("0")) {
//            final Snackbar snackbar = Snackbar
//                    .make(findViewById(R.id.activity_notification), "Swipe left to clear notification", Snackbar.LENGTH_INDEFINITE);
//            View snackBarView = snackbar.getView();
//            snackBarView.setBackgroundColor(Color.rgb(164, 198, 57));
//            snackbar.setAction("OK", new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    snackbar.dismiss();
//                }
//            });
//            snackbar.setActionTextColor(Color.rgb(1,22,39));
//            final TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
//            tv.setTextSize(15);
//            snackbar.show();
            Toast.makeText(getApplicationContext(),"Swipe left to delete individual notification",Toast.LENGTH_LONG).show();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("count","1");
            editor.commit();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        db1=new DatabaseHandler(Notification.this);
        List<Elpfa> elpfa = db1.getNotifications();
        for (Elpfa elp : elpfa) {
            Log.d("notif_check","id : "+elp.getNotifId());
            Log.d("notif_check","notif : "+elp.getNotifNotif());
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menu_delete) {
            Log.d("list_size_onclick","recycler view size : "+mRecyclerView.getChildCount());
            if (mRecyclerView.getChildCount()==0)
            {
                Toast.makeText(getApplicationContext(),"You have no notifications",Toast.LENGTH_SHORT).show();
            }
            else
            {
//            Toast.makeText(getApplicationContext(),"clear all",Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(Notification.this);
            builder.setCancelable(false);
            builder.setMessage("Do you want to clear all notifications?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(getApplicationContext(),"YES",Toast.LENGTH_SHORT).show();
                    dialog.cancel();

                    /* clear all notification */
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                    if (isConnected == true) {
                        try {

                            Config url_clear = new Config();
                    /* using volley in login - combine everything */
                            String tag_string_req = "req_state";
                            StringRequest strReq = new StringRequest(Request.Method.POST,
                                    url_clear.clear_all_notifications, new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("clear_all", "response : " + response);
                                    if (response.equals("Deleted sucessfully")) {
                                        Log.d("clear_all", "in if");
                                        db1 = new DatabaseHandler(Notification.this);
                                        Log.d("clear_all", "1");
                                        db1.deleteNotificationsTable();
                                        notifications = getNotifications();
//                                        if (notifications.size() == 0) {
//                                            findViewById(R.id.no_notifications).setVisibility(View.VISIBLE);
//                                        }
                                        mAdapter = new AppAdapter(Notification.this, notifications);
                                        mRecyclerView.setAdapter(mAdapter);


//                                        int s=notifications.size();
//                                        Log.d("array_size","s : "+s);
//                                        for (int i=0;i<s;i++)
//                                        {
//                                            Log.d("array_size","i : "+i);
//                                            notifications.remove(i);
//                                            mAdapter.notifyItemRemoved(i);
//                                            s=notifications.size();
////                                            s=notifications.size()-1;
//                                        }
                                        Log.d("array_size", "after for loop : ");
//                                        notifications.clear();
//                                        mRecyclerView.invalidate();
//                                        Log.d("clear_all","after delete table");
//                                        for (int i=0;i<notifications.size();i++) {
//                                            Log.d("clear_all","in for");
//                                            notifications.remove(i);
//                                            mAdapter.notifyItemRemoved(i);
//                                         notifications.clear();
//                                        notifications = getNotifications();
//                                            mRecyclerView.invalidate();
//                                        }
                                        Log.d("clear_all", "after for loop");


                                    } else {
                                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                    }


                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("notification_check", "error : " + error);
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    params.put("value1", sharedpreferences.getString("Username", null));
                                    return params;
                                }

                            };
                            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                        } catch (Exception ex) {
                            Log.d("exception", "" + ex);
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Check internet connection and try again", Toast.LENGTH_SHORT).show();
                    }

                    /*end clear single notification */






                        /* clear all notifications */


//                    String url = Config.clear_notification;
//                    ExecuteUrl task = new ExecuteUrl();
//                    task.execute(url,username);
//                    Animation animation = new TranslateAnimation(0, 0, (listView.getPositionForView(v) > lastPosition) ? 100 : -100, 0);
//                    animation.setDuration(700);
//                    listView.startAnimation(animation);
//                    finish();
//                    Intent intent=new Intent(getBaseContext(),Notification.class);
//                    intent.putExtra("username", username);
//                    startActivity(intent);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
     class AppAdapter extends RecyclerView.Adapter {

        private static final int VIEW_TYPE_ENABLE = 0;
        private static final int VIEW_TYPE_DISABLE = 1;

        List<NotificationNew> notifications;

        public AppAdapter(Context context, List<NotificationNew> notif){
            this.notifications = notif;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return notifications.size();
        }

        @Override
        public int getItemViewType(int position) {
//            NotificationNew notification = notifications.get(position);
            return  VIEW_TYPE_ENABLE;
//            if(notification.notifId % 2 == 0){
//                return VIEW_TYPE_DISABLE;
//            }else{
//                return VIEW_TYPE_ENABLE;
//            }
        }

        public boolean swipeEnableByViewType(int viewType) {
            if(viewType == VIEW_TYPE_ENABLE) return true;
            else if(viewType == VIEW_TYPE_DISABLE) return false;
            else return true; // default
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.item_notif, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder vh, final int position) {
            final NotificationNew notification = notifications.get(position);
            final MyViewHolder myViewHolder = (MyViewHolder)vh;
            SwipeMenuLayout itemView = (SwipeMenuLayout) myViewHolder.itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(), "Hi " + notification.notif, Toast.LENGTH_SHORT).show();
                    Log.d("notification_check","notification items clicked");
                }
            });
            myViewHolder.btDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("notification_check","v : "+v);
                    Log.d("notification_check","position : "+position);

                    /* clear single notification */
                    ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                    boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                    if (isConnected == true) {
                        try {

                            Config url_clear = new Config();

                            Log.d("edit_profile","6");
                    /* using volley in login - combine everything */
                            String tag_string_req = "req_state";
                            StringRequest strReq = new StringRequest(Request.Method.POST,
                                    url_clear.clear_single_notification_new, new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("notification_check", "response : " + response);
                                    if (response.equals("Deleted sucessfully"))
                                    {
                                        db1=new DatabaseHandler(Notification.this);
                                        Log.d("delete_notif","notif_id_backup : "+notif_id_backup);
                                        int val=db1.deleteNotification(new Elpfa("delete_notif",notif_id_backup));
                                        if (val==1)
                                        {
                                            notifications.remove(vh.getAdapterPosition());
                                            mAdapter.notifyItemRemoved(vh.getAdapterPosition());
                                            Log.d("notif_check","vh.getAdapterPosition() : "+vh.getAdapterPosition());
                                            notifications.clear();
                                            notifications = getNotifications();

                                            Log.d("delete_notif","val : "+val);
//                                            List<Elpfa> elpfa = db1.getNotifications();
//                                            for (Elpfa elp : elpfa) {
//                                                Log.d("notif_check","id * : "+elp.getNotifId());
//                                                Log.d("notif_check","notif *: "+elp.getNotifNotif());
//                                            }
//                                            notifications = getNotifications();
//                                            mAdapter = new AppAdapter(getApplicationContext(), notifications);
//                                            mRecyclerView.setAdapter(mAdapter);
                                            mRecyclerView.invalidate();
                                            Log.d("list_size","recycler view size : "+mRecyclerView.getChildCount());
                                            if (mRecyclerView.getChildCount()==1)
                                            {
                                                findViewById(R.id.no_notifications).setVisibility(View.VISIBLE);
                                            }

                                        }
                                        else
                                        {
                                            Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                                    }



                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("notification_check","error : "+error);
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    params.put("value1", sharedpreferences.getString("Username", null));
                                    if (notifications.size()==position)
                                    {

                                        notification_id = notifications.get(position-1);
                                    }
                                    else
                                    {
                                        notification_id = notifications.get(position);
                                    }
                                    Log.d("notification_check","notification id : "+notification.notifId);
                                    notif_id_backup=notification.notifId;
                                    params.put("value2",  notification.notifId);

                                    return params;
                                }

                            };
                            strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                        } catch (Exception ex) {
                            Log.d("exception", "" + ex);
                        }


                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Check internet connection and try again",Toast.LENGTH_SHORT).show();
                    }

                    /*end clear single notification */




                }
            });
            Log.d("notif_check","notification : "+notification.notif);
            myViewHolder.notif_txt.setText(""+notification.notif);
            boolean swipeEnable = swipeEnableByViewType(getItemViewType(position));
//            myViewHolder.tvSwipeEnable.setText(swipeEnable ? "swipe on" : "swipe off");

            /**
             * optional
             */
            itemView.setSwipeEnable(swipeEnable);
            itemView.setOpenInterpolator(mRecyclerView.getOpenInterpolator());
            itemView.setCloseInterpolator(mRecyclerView.getCloseInterpolator());
        }
    }
    class NotificationNew{
        public String notifId;
        public String notif;
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView notif_txt;
        TextView date;
        View btDelete;
        public MyViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            notif_txt = (TextView) itemView.findViewById(R.id.notif);
            date = (TextView) itemView.findViewById(R.id.date);
            btDelete = itemView.findViewById(R.id.btDelete);
        }
    }
    private List<NotificationNew> getNotifications() {
//        List<NotificationNew> notificationList = new ArrayList<>();
//        for (int i=0; i<5; i++){
//            NotificationNew notification = new NotificationNew();
//            notification.notifId = i+10;
//            notification.notif = "Notification "+(i+1);
//            notificationList.add(notification);
//        }
//        return notificationList;
        List<NotificationNew> notificationList = new ArrayList<>();
        db1=new DatabaseHandler(Notification.this);
        List<Elpfa> elpfa = db1.getNotifications();
        for (Elpfa elp : elpfa) {
            NotificationNew notification = new NotificationNew();
            notification.notifId = elp.getNotifId();
            notification.notif = elp.getNotifNotif();
            notificationList.add(notification);

        }
        Log.d("delete_notif","notificationList.size() : "+notificationList.size());
        return notificationList;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
