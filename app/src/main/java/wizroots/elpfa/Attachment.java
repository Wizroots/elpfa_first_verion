package wizroots.elpfa;

import android.graphics.Bitmap;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Attachment {
    private String message_id;
    private String attachment_id;
    private String attachment_name;
    private int attachment_tag;
    private String attachment_type;
    private Bitmap attachment_bitmap;
    private String attachment_path;
    private String attachment_from;





    public Attachment() {
    }

    public Attachment(String message_id, String attachment_id, int attachment_tag, String attachment_type, Bitmap attachment_bitmap,String attachment_name,String attachment_from, String attachment_path) {
       this.message_id=message_id;
        this.attachment_id=attachment_id;
        this.attachment_tag=attachment_tag;
        this.attachment_type=attachment_type;
        this.attachment_bitmap=attachment_bitmap;
        this.attachment_name=attachment_name;
        this.attachment_from=attachment_from;
        this.attachment_path = attachment_path;
    }

    //message id
    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    //attachment id
    public String getAttachment_id() {
        return attachment_id;
    }

    public void setAttachment_id(String attachment_id) {
        this.attachment_id = attachment_id;
    }

    //attachment name
    public String getAttachment_name() {
        return attachment_name;
    }

    public void setAttachment_name(String attachment_name) {
        this.attachment_name = attachment_name;
    }

    //attachment tag
    public int getAttachment_tag() {
        return attachment_tag;
    }

    public void setAttachment_tag(int attachment_tag) {
        this.attachment_tag = attachment_tag;
    }

    //attachment type
    public String getAttachment_type() {
        return attachment_type;
    }

    public void setAttachment_type(String attachment_tag) {
        this.attachment_type = attachment_type;
    }
    //attachment btmap
    public Bitmap getAttachment_bitmap() {
        return attachment_bitmap;
    }

    public void setAttachment_bitmap(Bitmap attachment_tag) {
        this.attachment_bitmap = attachment_bitmap;
    }
    //attachment from
    public String getAttachment_from() {
        return attachment_from;
    }

    public void setAttachment_from(String attachment_from) {
        this.attachment_from = attachment_from;
    }

    //attachment path
    public String getAttachment_path() {
        return attachment_path;
    }

    public void setAttachment_path(String attachment_path) {
        this.attachment_path = attachment_path;
    }
}
