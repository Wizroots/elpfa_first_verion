package wizroots.elpfa;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.vipul.hp_hp.timelineview.TimelineView;

import java.util.List;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<TimeLineModel> mFeedList;
    private Context mContext;
    int viewed;
    private Orientation mOrientation;

    public TimeLineAdapter(List<TimeLineModel> feedList, Orientation orientation) {
        mFeedList = feedList;
        mOrientation = orientation;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        View view;

//        if(mOrientation == Orientation.horizontal) {
            view = View.inflate(parent.getContext(), R.layout.item_timeline_horizontal, null);
//        }
//        else {
////            view = View.inflate(parent.getContext(), R.layout.item_timeline, null);
//            view = View.inflate(parent.getContext(), R.layout.item_timeline_horizontal, null);
//        }

        return new TimeLineViewHolder(mContext,view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        TimeLineModel timeLineModel = mFeedList.get(position);
        holder.root.setTag(timeLineModel.getTag());
        viewed=timeLineModel.getViewed();
        if (viewed==0)
        holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.circular_bk_transparent));
        else if (viewed==1)
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.circular_bk_accente));




//        holder.name.setText("name：" + timeLineModel.getName() + "    age：" + timeLineModel.getAge());


    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }

}
