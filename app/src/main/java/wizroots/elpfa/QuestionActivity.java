package wizroots.elpfa;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class QuestionActivity extends AppCompatActivity implements OnClickListener {
    JSONObject jObject;
    android.support.design.widget.CollapsingToolbarLayout collapsingToolbarLayout;

    String question="";
    int total_number_of_questions;
    int current_ques_number;
    RadioGroup options;
    int right;
    int wrong;
    String result;
    String username;
    String course_id;
    String user_id;
    private Toolbar mToolbar;
    RadioButton option1,option2,option3,option4;
    SharedPreferences sharedpreferences;
    String restoredUsername;
    Button proceed;
    public static final String MyPREFERENCES = "MyPrefs" ;
    private GestureDetectorCompat gestureDetectorCompat;
    ArrayList<String> option1_array= new ArrayList<>();
    ArrayList<String> option2_array= new ArrayList<>();
    ArrayList<String> option3_array= new ArrayList<>();
    ArrayList<String> option4_array= new ArrayList<>();
    ArrayList<String> answer_array= new ArrayList<>();
    ArrayList<String> question_array= new ArrayList<>();
    ArrayList<String> question_id_array= new ArrayList<>();
    ArrayList<String> selected_answers= new ArrayList<>();
    ArrayList<String> right_answers= new ArrayList<>();
    ArrayList<String> wrong_answers= new ArrayList<>();
    String course_name;
    View decorView;
    int num=1;
    TextView txtNumQuestions;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        decorView = getWindow().getDecorView();

        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.parseColor("#ffffff"));
            getWindow().setNavigationBarColor(Color.parseColor("#ffffff"));

        }
        setContentView(R.layout.activity_question);
        gestureDetectorCompat = new GestureDetectorCompat(this, new My2ndGestureListener());
        options = (RadioGroup)findViewById(R.id.group1);
        final TextView qText = (TextView) findViewById(R.id.question);
        txtNumQuestions = (TextView) findViewById(R.id.textView_question);
        option1 = (RadioButton) findViewById(R.id.answer1);
        option2 = (RadioButton) findViewById(R.id.answer2);
        option3 = (RadioButton) findViewById(R.id.answer3);
        option4 = (RadioButton) findViewById(R.id.answer4);
        proceed=(Button)findViewById(R.id.proceed);
        collapsingToolbarLayout=(android.support.design.widget.CollapsingToolbarLayout)findViewById(R.id.collapsingToolbarLayout);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.back_blue);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(QuestionActivity.this);
                builder.setCancelable(false);
                builder.setMessage("Do you want to exit test?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        restoredUsername = sharedpreferences.getString("Username", null);
                        intent.putExtra("username", restoredUsername);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("flag",4);
                        startActivity(intent);
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        username=getIntent().getStringExtra("username");
        user_id=getIntent().getStringExtra("user_id");
        course_id=getIntent().getStringExtra("course_id");
        total_number_of_questions = getIntent().getIntExtra("total_number_of_questions", 0);
        option1_array=getIntent().getStringArrayListExtra("option1_array");
        option2_array=getIntent().getStringArrayListExtra("option2_array");
        option3_array=getIntent().getStringArrayListExtra("option3_array");
        option4_array=getIntent().getStringArrayListExtra("option4_array");
        answer_array=getIntent().getStringArrayListExtra("answer_array");
        question_id_array=getIntent().getStringArrayListExtra("question_id_array");
        selected_answers=getIntent().getStringArrayListExtra("selected_answers");
        right_answers=getIntent().getStringArrayListExtra("right_answers");
        wrong_answers=getIntent().getStringArrayListExtra("wrong_answers");
        current_ques_number=getIntent().getIntExtra("current_ques_number", 0);

        course_name=getIntent().getStringExtra("course_name");
        current_ques_number++;
        collapsingToolbarLayout.setTitle(""+current_ques_number);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(25,118,210));
        right=getIntent().getIntExtra("right", 0);
        wrong=getIntent().getIntExtra("wrong", 0);

        try {
            String url1 = Config.load_question;
            ExecuteUrl task1 = new ExecuteUrl();
            String ques_id=question_id_array.get(current_ques_number-1);
            task1.execute(url1,ques_id);
            result = task1.get();
            fillDetailsQuestions(result);

        } catch (Exception ex) {
        }

        /* END  */
        Log.d("checkQuestionNumbers", "total_number_of_questions : " + total_number_of_questions);
        Log.d("checkQuestionNumbers", "current_ques_number : " + current_ques_number);
        if(total_number_of_questions != current_ques_number) {
            Log.d("test_log", "question : " + question);
            Log.d("test_log", "option1_array.get(current_ques_number-1) : " + option1_array.get(current_ques_number - 1));
            Log.d("test_log", "option2_array.get(current_ques_number-1) : " + option2_array.get(current_ques_number - 1));
            Log.d("test_log", "option3_array.get(current_ques_number-1) : " + option3_array.get(current_ques_number - 1));
            Log.d("test_log", "option4_array.get(current_ques_number-1) : " + option4_array.get(current_ques_number - 1));
            txtNumQuestions.setText("Question " + current_ques_number);
            qText.setText("\n" + question);
            option1.setText("" + option1_array.get(current_ques_number - 1));
            option2.setText("" + option2_array.get(current_ques_number - 1));
            option3.setText("" + option3_array.get(current_ques_number - 1));
            option4.setText("" + option4_array.get(current_ques_number - 1));
        }

        options.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (num == 1) {
                   /* Animation show_fab_1 = AnimationUtils.loadAnimation(getApplication(), R.anim.btn_show);
                    proceed.startAnimation(show_fab_1);*/
                    proceed.setVisibility(View.VISIBLE);
                    num = 2;
                }
            }
        });
        option1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked==true)
                {

                    option1.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.a_option),null,null,null);
                    option2.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.b_option_white),null,null,null);
                    option3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.c_option_white),null,null,null);
                    option4.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.d_option_white),null,null,null);
                    option1.setTextColor(Color.rgb(25,118,210));
                    option2.setTextColor(Color.WHITE);
                    option3.setTextColor(Color.WHITE);
                    option4.setTextColor(Color.WHITE);

                }
            }
        });
        option2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked==true)
                {
                    option2.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.b_option),null,null,null);
                    option1.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.a_option_white),null,null,null);
                    option3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.c_option_white),null,null,null);
                    option4.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.d_option_white),null,null,null);
                    option2.setTextColor(Color.rgb(25,118,210));
                    option1.setTextColor(Color.WHITE);
                    option3.setTextColor(Color.WHITE);
                    option4.setTextColor(Color.WHITE);          }
            }
        });
        option3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked==true)
                {
                    option3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.c_option),null,null,null);
                    option2.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.b_option_white),null,null,null);
                    option1.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.a_option_white),null,null,null);
                    option4.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.d_option_white),null,null,null);
                    option3.setTextColor(Color.rgb(25,118,210));
                    option2.setTextColor(Color.WHITE);
                    option1.setTextColor(Color.WHITE);
                    option4.setTextColor(Color.WHITE);     }
            }
        });
        option4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked==true)
                {
                    option4.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.d_option),null,null,null);
                    option2.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.b_option_white),null,null,null);
                    option3.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.c_option_white),null,null,null);
                    option1.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.a_option_white),null,null,null);
                    option4.setTextColor(Color.rgb(25,118,210));
                    option2.setTextColor(Color.WHITE);
                    option3.setTextColor(Color.WHITE);
                    option1.setTextColor(Color.WHITE);      }
            }
        });
        proceed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioButtonID = options.getCheckedRadioButtonId();
                View radioButton = options.findViewById(radioButtonID);
                int idx = options.indexOfChild(radioButton);
                if (idx==0) {
                    selected_answers.set(current_ques_number - 1, "A");
                }
                else if (idx==1) {
                    selected_answers.set(current_ques_number - 1, "B");
                } else if (idx==2) {
                    selected_answers.set(current_ques_number - 1, "C");
                } else if (idx==3) {
                    selected_answers.set(current_ques_number - 1, "D");
                }

                if (answer_array.get(current_ques_number - 1).equals(selected_answers.get(current_ques_number-1))) {
                    right++;

                    right_answers.set(current_ques_number - 1, "1");
                    wrong_answers.set(current_ques_number - 1, "0");

                } else {
                    wrong++;
                    right_answers.set(current_ques_number - 1, "0");
                    wrong_answers.set(current_ques_number - 1, "1");

                }

                if (total_number_of_questions == current_ques_number) {
                    Intent i = new Intent(getApplicationContext(), EndTestActivity.class);
                    i.putExtra("right", right);
                    i.putExtra("wrong", wrong);
                    i.putExtra("username", username);
                    i.putExtra("user_id", user_id);
                    i.putExtra("course_name",course_name);
                    i.putExtra("course_id",course_id);
                    i.putExtra("total_number_of_questions", total_number_of_questions);
                    i.putStringArrayListExtra("selected_answers", selected_answers);
                    i.putStringArrayListExtra("right_answers", right_answers);
                    i.putStringArrayListExtra("wrong_answers", wrong_answers);
                    QuestionActivity.this.finish();
                    startActivity(i);
                } else {
                    Log.d("questions_test","in else ");
                    Intent i = new Intent(getApplicationContext(), QuestionActivity.class);
                    i.putExtra("username", username);
                    i.putExtra("user_id", user_id);
                    i.putExtra("course_id", course_id);
                    i.putExtra("course_name",course_name);
                    i.putExtra("total_number_of_questions", total_number_of_questions);
                    i.putExtra("option1_array", option1_array);
                    i.putExtra("option2_array", option2_array);
                    i.putExtra("option3_array", option3_array);
                    i.putExtra("option4_array", option4_array);
                    i.putExtra("answer_array", answer_array);
                    i.putExtra("question_id_array", question_id_array);
                    i.putExtra("current_ques_number", current_ques_number);
                    i.putExtra("wrong", wrong);
                    i.putExtra("right", right);
                    i.putStringArrayListExtra("selected_answers", selected_answers);
                    i.putStringArrayListExtra("right_answers", right_answers);
                    i.putStringArrayListExtra("wrong_answers", wrong_answers);
                    QuestionActivity.this.finish();
                    startActivity(i);
                }

            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetectorCompat.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class My2ndGestureListener extends GestureDetector.SimpleOnGestureListener {
        //handle 'swipe right' action only
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {



            if(event2.getX() > event1.getX()) {
                Toast.makeText(getBaseContext(),
                        "Swipe right - finish()",
                        Toast.LENGTH_SHORT).show();


                overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
            }

            return true;
        }

    }




    @Override
    public void onClick(View v) {
        //validate a checkbox has been selected
        if (!checkAnswer()) return;


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK : return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * Check if a checkbox has been selected, and if it
     * has then check if its correct and update gamescore
     */
    private boolean checkAnswer() {
        RadioGroup options = (RadioGroup)findViewById(R.id.group1);
        Integer selected = options.getCheckedRadioButtonId();
        if ( selected < 0){
            return false;
        } else {

            return true;
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to exit test?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.rgb(1, 22, 39));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.rgb(1, 22, 39));


    }
    public void fillDetailsQuestions(String listOutput) {
        try {
            question_array.clear();
            jObject = new JSONObject(listOutput);
            JSONArray jsonArray = jObject.getJSONArray("code");
            final int length = jsonArray.length();
            for (int i = 0; i <= length; i++) {
                JSONObject Jasonobject = jsonArray.getJSONObject(i);
                question = Jasonobject.getString("question");

            }

        } catch (JSONException e) {

        }
    }
}
