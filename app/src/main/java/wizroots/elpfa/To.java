package wizroots.elpfa;
public class To {
    String to_id;
    String to_name;
    String to_email;
    String to_type;

    public To() {
    }

    public To(String id,String name, String email, String type) {
        this.to_id=id;
        this.to_name=name;
        this.to_email=email;
        this.to_type=type;
    }

    //to id
    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    //to name
    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    //to email
    public String getTo_email() {
        return to_email;
    }

    public void setTo_email(String to_email) {
        this.to_name = to_email;
    }

    //to type
    public String getTo_type() {
        return to_type;
    }

    public void setTo_type(String to_type) {
        this.to_type = to_type;
    }
}
