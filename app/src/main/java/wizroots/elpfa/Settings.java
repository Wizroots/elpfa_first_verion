package wizroots.elpfa;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Settings.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Settings#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Settings extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RelativeLayout alert;

    RelativeLayout switch_rel;
    View rootView;
    Toolbar toolbar;
    Switch switch_push;

    ActionBarDrawerToggle toggle;
    TextView no_days;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private OnFragmentInteractionListener mListener;
    String push;
    String selected_item;
    String stored_course_days;
    public Settings() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Settings.
     */
    // TODO: Rename and change types and number of parameters
    public static Settings newInstance(String param1, String param2) {
        Settings fragment = new Settings();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_settings, container, false);
        ReplaceFont.setDefaultFont(getActivity(), "DEFAULT", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(getActivity(), "MONOSPACE", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(getActivity(), "SERIF", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(getActivity(), "SANS_SERIF", "PT_Sans_Web_Regular.ttf");
        toolbar=(Toolbar)rootView.findViewById(R.id.toolbar);
        toolbar.setTitle("Settings");

       /* ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        alert=(RelativeLayout)rootView.findViewById(R.id.alert1);
        no_days=(TextView)rootView.findViewById(R.id.no_days);
        switch_rel=(RelativeLayout)rootView.findViewById(R.id.switch_rel);
        switch_push=(Switch) rootView.findViewById(R.id.switchbtn);
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        //checking stored value

        push=sharedpreferences.getString("push",null);
        if (push.equals("on"))
        {
            switch_push.setChecked(true);
        }
        else if (push.equals("off"))
        {
            switch_push.setChecked(false);
        }
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        //clicking on the entire layout
        switch_rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switch_push.isChecked())
                {
                    switch_push.setChecked(false);
                    editor.putString("push","off");
                    editor.commit();
                }
                else if (!(switch_push.isChecked()))
                {
                    switch_push.setChecked(true);
                    editor.putString("push","on");
                    editor.commit();
                }
            }
        });
        stored_course_days=sharedpreferences.getString("course_days","one_day");
        if (stored_course_days.equals("one_day"))
        {
            no_days.setText("1 Day Before");
        }
        else if (stored_course_days.equals("two_days"))
        {
            no_days.setText("2 Days Before");
        }
        else if (stored_course_days.equals("three_days"))
        {
            no_days.setText("3 Days Before");
        }
        else
        {
            no_days.setText("1 Dday Before");
        }
        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom_settings);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(false);
                final RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.rgroup);
                final RadioButton r1 = (RadioButton) dialog.findViewById(R.id.rd_1);
                final RadioButton r2 = (RadioButton) dialog.findViewById(R.id.rd_2);
                final RadioButton r3 = (RadioButton) dialog.findViewById(R.id.rd_3);


                String radiobtn_slct = sharedpreferences.getString("course_days", "one_day");

                if (radiobtn_slct.equals("one_day")) {
                    r1.setChecked(true);
                } else if (radiobtn_slct.equals("two_days")) {
                    r2.setChecked(true);
                } else if (radiobtn_slct.equals("three_days")) {
                    r3.setChecked(true);
                } else {
                    r1.setChecked(true);
                }
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        int selectedId = rg.getCheckedRadioButtonId();
                        RadioButton rb = (RadioButton) dialog.findViewById(selectedId);
                        selected_item = rb.getText().toString();
                        if (selected_item.equals("1 Day Before")) {
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("course_days", "one_day");
                            editor.commit();
                            no_days.setText("1 Day Before");
                        } else if (selected_item.equals("2 Days Before")) {
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("course_days", "two_days");
                            editor.commit();
                            no_days.setText("2 Days Before");
                        } else if (selected_item.equals("3 Days Before")) {
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("course_days", "three_days");
                            editor.commit();
                            no_days.setText("3 Days Before");
                        }
                        dialog.cancel();
                    }
                });


                // there are a lot of settings, for dialog, check them all out!
                // set up radiobutton

                final Button b = (Button) dialog.findViewById(R.id.ok_btn);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get selected radio button from radioGroup

                        dialog.cancel();

                    }
                });
                dialog.show();
            }
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_only_backbtn, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }
    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }
        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }
        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }
}
