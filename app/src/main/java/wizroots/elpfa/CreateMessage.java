package wizroots.elpfa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CreateMessage extends AppCompatActivity {
Toolbar toolbar;
    private Uri picUri;
    ImageView attachment_content_img;
    Bundle extras;
    int id=0;
    EditText msg_message;
    String to_id;
    String url_new;
    String to_name;
    CustomAutoCompleteTextView autoComplete;
    String to_email;
    String to_type;
    List<String> to_names_list = new ArrayList<>();
    List<Integer> to_icons_list = new ArrayList<>();
    List<Integer> to_id_list = new ArrayList<>();
    List<String> to_type_list = new ArrayList<>();
    List<String> attachment_names_arr = new ArrayList<>();
    List<String> attachment_arr = new ArrayList<>();
    EditText msg_subject;
    JSONObject jObject1;
    String temp;
    public ArrayList<String> to_list = new ArrayList<>();
    public ArrayList<String> toListNames = new ArrayList<>();
    LinearLayout attachment_layout;
    Bitmap attachment_type; String attachemnt_name;
    Bitmap attachment_bitmap;
    String selected_sender;
    String url;
    String selected_receiver;
    String selected_subject;
    String selected_message;
    String selected_flag;
    int isAttachment=0;

    private RecyclerView recyclerView;
    private AttachmentAdapter attachmentAdapter;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    static final int CHOOSE_ATTACHMENT = 1;

    private List<Attachment> attachmentList = new ArrayList<>();
    int attachment_count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_message);
        attachment_content_img=(ImageView)findViewById(R.id.attachment_content);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        attachment_layout=(LinearLayout)findViewById(R.id.attachment_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        attachmentAdapter = new AttachmentAdapter(attachmentList);
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
        prepareList();

        for(int i=0;i<to_names_list.size();i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("name", to_names_list.get(i));
            hm.put("icon", Integer.toString(to_icons_list.get(i)) );
            hm.put("id", String.valueOf(to_id_list.get(i)));
            hm.put("type", to_type_list.get(i));
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "icon","name"};

        // Ids of views in listview_layout
        int[] to = { R.id.icon,R.id.name};

        // Instantiating an adapter to store each items
        SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.autocomplete_layout, from, to);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        // Getting a reference to CustomAutoCompleteTextView of activity_main.xml layout file
         autoComplete = ( CustomAutoCompleteTextView) findViewById(R.id.autocomplete);
        msg_subject=(EditText)findViewById(R.id.msg_subject);
        msg_message=(EditText)findViewById(R.id.msg_message);


        /** Defining an itemclick event listener for the autocompletetextview */
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                /** Each item in the adapter is a HashMap object.
                 *  So this statement creates the currently clicked hashmap object
                 * */
//                HashMap<String, String> hm = (HashMap<String, String>) arg0.getAdapter().getItem(position);
//
//                /** Getting a reference to the TextView of the layout file activity_main to set Currency */
//                TextView tvCurrency = (TextView) findViewById(R.id.tv_currency) ;
//
//                /** Getting currency from the HashMap and setting it to the textview */
//                tvCurrency.setText("Currency : " + hm.get("cur"));
                HashMap<String, String> hm = (HashMap<String, String>) arg0.getAdapter().getItem(position);
                selected_sender=sharedpreferences.getString("User_id",null);
                selected_receiver=hm.get("id");
                selected_subject=msg_subject.getText().toString();
                selected_message=msg_message.getText().toString();

                if (hm.get("type").toString().equals("user")) {
                    selected_flag ="1";
                }
                else if (hm.get("type").toString().equals("course"))
                {
                    selected_flag ="2";
                }
                else if (hm.get("type").toString().equals("team"))
                {
                    selected_flag ="3";
                }
            }
        };

        /** Setting the itemclick event listener */
        autoComplete.setOnItemClickListener(itemClickListener);

        /** Setting the adapter to the listView */
        autoComplete.setAdapter(adapter);
        toolbar.setNavigationIcon(R.drawable.back_white);


        toolbar.setTitle("Compose");
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateMessage.this.finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(getLinearLayoutManager());
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(attachmentAdapter);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.compose_msg_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_attachment:
//                Toast.makeText(getApplicationContext(),"Attachment clicked",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(Intent.ACTION_OPEN_DOCUMENT);
                i.setType("text/document");
                startActivity(Intent.createChooser(i, "Select attachment"));
                startActivityForResult(i, CHOOSE_ATTACHMENT);

                return true;
            case R.id.action_send:
                if (autoComplete.getText().toString().equals(""))
                {
                    final  AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateMessage.this);
                    builder1.setCancelable(true);
                    builder1.setMessage("Select recipient");
                    builder1.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else if (msg_subject.getText().toString().equals(""))
                {
                    final  AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateMessage.this);
                    builder1.setCancelable(true);
                    builder1.setMessage("There's no text in the message subject.");
                    builder1.setPositiveButton("SEND",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    sendMessage();
                                }
                            });
                    builder1.setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
//                                    Toast.makeText(getApplicationContext(),"send clicked",Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else if (msg_message.getText().toString().equals(""))
                {
                    final  AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateMessage.this);
                    builder1.setCancelable(true);
                    builder1.setMessage("There's no text in the message body. ");
                    builder1.setPositiveButton("SEND",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    sendMessage();
                                }
                            });
                    builder1.setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
//                                    Toast.makeText(getApplicationContext(),"send clicked",Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
                else
                {
                    sendMessage();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == CHOOSE_ATTACHMENT){
                try
                {
//                Toast.makeText(getApplicationContext(),"attached",Toast.LENGTH_SHORT).show();
                if (data!=null) {
                    extras = data.getExtras();
                    picUri = data.getData();
//                    attachment_layout.setVisibility(View.VISIBLE);
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                    attachment_content_img.setImageBitmap(bitmap);
                    attachemnt_name=String.valueOf(picUri);
                    url =attachemnt_name;
                    if (url!=null) {

                        url = url.substring(url.lastIndexOf("/") + 1, url.length());
                        url = url.substring(url.lastIndexOf("%") + 1, url.length());
                         url_new = url.replace("$", "").replace(",", "");
                        url_new=url_new.substring(2);

                    }
                    attachment_bitmap=bitmap;
                    Attachment attachment = new Attachment("1",String.valueOf(attachment_count),attachment_count,"image",bitmap,url_new,"CreateMessage","path");
                    attachmentList.add(attachment);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    String myBase64Image = encodeToBase64(attachment_bitmap, Bitmap.CompressFormat.JPEG, 100);
                    attachment_names_arr.add(attachment_count,url_new);
                    attachment_arr.add(attachment_count,myBase64Image);
                    attachment_count++;
                    attachmentAdapter.notifyDataSetChanged();
                    isAttachment=1;

                }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }




            }
        }
    }


   private void prepareList()
   {
       try
       {
           to_id_list.clear();
           to_icons_list.clear();
           to_names_list.clear();
           to_type_list.clear();
           DatabaseHandler db1 = new DatabaseHandler(getApplicationContext());
           List<Elpfa> elpfa_all = db1.getTo();
           for (Elpfa elp : elpfa_all) {
               to_id = elp.getTo_to_id();
               to_name = elp.getTo_name();
               to_email = elp.getTo_email();

               to_id_list.add(Integer.valueOf(to_id));
               if (to_email.equals("no_email"))
               {
                   to_names_list.add(to_name);
               }
               else {
                   to_names_list.add(to_email);
               }
               to_type = elp.getTo_type();
               to_type_list.add(to_type);
               if (to_type.equals("course"))
               {
                   to_icons_list.add(R.drawable.book);
               }
               else if (to_type.equals("team"))
               {
                   to_icons_list.add(R.drawable.team_icon_contact);
               }
               else if (to_type.equals("user"))
               {
                   to_icons_list.add(R.drawable.account_blk);
               }



           }

       }
       catch (Exception e)
       {

       }

   }
    public void  sendMessage()
    {
        String list_item;
        int item_found=0;
        for(int i=0;i<to_names_list.size();i++)
        {
            list_item=to_names_list.get(i);
            if (list_item.equals(autoComplete.getText().toString()))
            {
                item_found=1;
            }

        }
        if (item_found==1)
        {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (isConnected == true) {
            Toast.makeText(getApplicationContext(),"Sending...",Toast.LENGTH_SHORT).show();
//            final ProgressDialog pd=new ProgressDialog(CreateMessage.this);
//            pd.setMessage("Sending...");
//            pd.setCancelable(false);
//            pd.setCanceledOnTouchOutside(false);
//            pd.show();
                //code to send message
                try {

                    Config url_send_message = new Config();
                    String tag_string_req = "req_state";
                    StringRequest strReq = new StringRequest(Request.Method.POST,
                            url_send_message.SendMessageNew, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("send_attachment", "response : " + response);
                       /* if (response.equals("1:individual")||response.equals("1:course")||response.equals("team"))
                        {

                            //load messages again
                            Config url_login1 = new Config();

                    *//* using volley in login - combine everything *//*
                            String tag_string_req1 = "req_state1";
                            StringRequest strReq1 = new StringRequest(Request.Method.POST,
                                    url_login1.FetchMessages, new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("MessageReceiver", response);
                                    editor.commit();
                                    try {

                                        try {

                                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                            try {

                                                DatabaseHandler db1 = new DatabaseHandler(getBaseContext());

                                                jObject1 = new JSONObject(response);

                                                //insert inbox messages
                                                Log.d("messages_check","1");
                                                db1.deleteInboxMessagesTable();
                                                if (!jObject1.isNull("inbox_team")) {
                                                    JSONArray jsonArrayInbox1 = jObject1.getJSONArray("inbox_team");

                                                    for (int i = 0; i < jsonArrayInbox1.length(); i++) {
                                                        JSONObject jasonObjectInbox = jsonArrayInbox1.getJSONObject(i);
                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                    }
                                                }
                                                Log.d("messages_check","2");
                                                if (!jObject1.isNull("inbox_course")) {
                                                    JSONArray jsonArrayInbox2 = jObject1.getJSONArray("inbox_course");
                                                    for (int i = 0; i < jsonArrayInbox2.length(); i++) {
                                                        JSONObject jasonObjectInbox = jsonArrayInbox2.getJSONObject(i);
                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                    }
                                                }
                                                if (!jObject1.isNull("inbox_user")) {
                                                    JSONArray jsonArrayInbox3 = jObject1.getJSONArray("inbox_user");
                                                    for (int i = 0; i < jsonArrayInbox3.length(); i++) {
                                                        JSONObject jasonObjectInbox = jsonArrayInbox3.getJSONObject(i);
                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"), jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                    }
                                                }
                                                db1.deleteOutboxMessagesTable();
                                                if (!jObject1.isNull("outbox_team")) {
                                                    JSONArray jsonArrayOutbox1 = jObject1.getJSONArray("outbox_team");
                                                    for (int i = 0; i < jsonArrayOutbox1.length(); i++) {
                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox1.getJSONObject(i);
                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"), "http://reflectivelearn.net/mlearning/web/" + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                    }
                                                }

                                                if (!jObject1.isNull("outbox_course")) {
                                                    JSONArray jsonArrayOutbox2 = jObject1.getJSONArray("outbox_course");
                                                    for (int i = 0; i < jsonArrayOutbox2.length(); i++) {
                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox2.getJSONObject(i);
                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("course_id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null),jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"), "http://reflectivelearn.net/mlearning/web/" + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                    }
                                                }
                                                if (!jObject1.isNull("outbox_user")) {
                                                    JSONArray jsonArrayOutbox3 = jObject1.getJSONArray("outbox_user");
                                                    for (int i = 0; i < jsonArrayOutbox3.length(); i++) {
                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox3.getJSONObject(i);
                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"),sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"),  jasonObjectOutbox.getString("title"),jasonObjectOutbox.getString("message"), jasonObjectOutbox.getString("photo"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                    }
                                                }


                                            } catch (Exception e) {
                                                Log.d("exception", "" + e);
                                            }
//

                                        } catch (Exception e) {
                                            Log.d("exception", "" + e);

                                        }


                                    } catch (Exception e) {
                                        Log.d("exception", "" + e);

                                    }

                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    params.put("value1", sharedpreferences.getString("User_id", null));
                                    return params;
                                }

                            };
                            strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);
                            Toast.makeText(getApplicationContext(),"Message has been sent",Toast.LENGTH_SHORT).show();
                            CreateMessage.this.finish();
                            pd.cancel();

                        }
                        else {
                            pd.cancel();
                            Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                        }*/


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            // Posting parameters to login url
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("value1", selected_sender);
                            params.put("value2", selected_receiver);
                            params.put("value3", msg_subject.getText().toString());
                            params.put("value4", msg_message.getText().toString());
                            params.put("value5", selected_flag);

                            if (isAttachment == 0) {
                                params.put("value6", "0");
                                params.put("value7", "0");
                            } else if (isAttachment == 1) {
                                params.put("value6", String.valueOf(attachment_arr));
                                params.put("value7", "1");
                            }

                        if (isAttachment==0)
                        {
                            params.put("value8", "no_attachment");
                        }
                        else if (isAttachment==1)
                        {
                                params.put("value8", String.valueOf(attachment_names_arr));
                        }
                            return params;
                        }

                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */

                    onBackPressed();


                } catch (Exception ex) {
                }
            }
            else {
                Toast.makeText(getApplicationContext(),"check your internet connection and try again",Toast.LENGTH_SHORT).show();
            }
        }
        else {
            final  AlertDialog.Builder builder1 = new AlertDialog.Builder(CreateMessage.this);
            builder1.setCancelable(true);
            builder1.setMessage("Enter valid recipient");
            builder1.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }
    private LinearLayoutManager getLinearLayoutManager() {

//        if (mOrientation == Orientation.horizontal) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        return linearLayoutManager;
    }
}
