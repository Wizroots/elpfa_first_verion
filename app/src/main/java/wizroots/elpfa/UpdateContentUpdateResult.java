package wizroots.elpfa;

/**
 * Created by Design on 10-01-2018.
 */

public interface UpdateContentUpdateResult {
    void notifyResponse(int isUpdated);
}
