package wizroots.elpfa;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestInstructions extends AppCompatActivity {
    Toolbar toolbar;
    TextView test_questions_txt,pass_percentage_txt;
    Button proceed;
    String course_id;
    JSONObject jObject;
    DatabaseHandler db1;
    String test_questions;
    String pass_percentage;

    int current_ques_number;
    int right;
    int wrong;
    String option1;
    String option2;
    String option3;
    String option4;
    String answer;
    String question_id;
    android.support.design.widget.CollapsingToolbarLayout coll;

    ArrayList<String> option1_array= new ArrayList<>();
    ArrayList<String> option2_array= new ArrayList<>();
    ArrayList<String> option3_array= new ArrayList<>();
    ArrayList<String> option4_array= new ArrayList<>();
    ArrayList<String> answer_array= new ArrayList<>();
    ArrayList<String> question_id_array= new ArrayList<>();
    ArrayList<String> selected_answers= new ArrayList<>();
    ArrayList<String> right_answers= new ArrayList<>();
    ArrayList<String> wrong_answers= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_instructions);
        option1_array.clear();
        option2_array.clear();
        option3_array.clear();
        option4_array.clear();
        answer_array.clear();
        question_id_array.clear();
        selected_answers.clear();
        right_answers.clear();
        wrong_answers.clear();
        db1=new DatabaseHandler(getApplicationContext());
        ReplaceFont.setDefaultFont(this, "DEFAULT", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "MONOSPACE", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SERIF", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SANS_SERIF", "PT_Sans_Web_Regular.ttf");
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        test_questions_txt=(TextView)findViewById(R.id.test_questions);
        coll=(android.support.design.widget.CollapsingToolbarLayout)findViewById(R.id.collapsingToolbarLayout);
        pass_percentage_txt=(TextView)findViewById(R.id.pass_percentage);
        course_id=getIntent().getStringExtra("course_id");

        current_ques_number=getIntent().getIntExtra("current_ques_number", 0);

        right=getIntent().getIntExtra("right", 0);
        wrong=getIntent().getIntExtra("wrong", 0);
        current_ques_number++;
        proceed=(Button) findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if (isConnected == true) {
                    //load all question ids
                    try {

                        Config url_question_ids = new Config();

                        String tag_string_req = "req_state";
                        StringRequest strReq = new StringRequest(Request.Method.POST,
                                url_question_ids.loadQuestionIds, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("test_details", "response : " + response);
                                try {
                                    option1_array.clear();
                                    option2_array.clear();
                                    option3_array.clear();
                                    option4_array.clear();
                                    question_id_array.clear();
                                    answer_array.clear();
                                    selected_answers.clear();
                                    right_answers.clear();
                                    wrong_answers.clear();
                                    jObject = new JSONObject(response);
                                    if (!jObject.isNull("questions")) {
                                        JSONArray jsonArray = jObject.getJSONArray("questions");
                                        final int length = jsonArray.length();
                                        for (int i = 0; i <length; i++) {
                                            JSONObject Jasonobject = jsonArray.getJSONObject(i);
                                            option1=Jasonobject.getString("option1");
                                            option2=Jasonobject.getString("option2");
                                            option3=Jasonobject.getString("option3");
                                            option4=Jasonobject.getString("option4");
                                            answer=Jasonobject.getString("answer");
                                            question_id=Jasonobject.getString("id");
                                            option1_array.add(i,option1);
                                            option2_array.add(i,option2);
                                            option3_array.add(i,option3);
                                            option4_array.add(i,option4);
                                            question_id_array.add(i,question_id);
                                            answer_array.add(i,answer);
                                        }
                                        if (Integer.parseInt(test_questions)!=0)
                                        {
                                            selected_answers.clear();
                                            right_answers.clear();
                                            wrong_answers.clear();

                                            for (int i = 0; i <= Integer.parseInt(test_questions) - 1; i++) {
                                                selected_answers.add(i, "null");
                                                right_answers.add(i, "null");
                                                wrong_answers.add(i, "null");
                                            }
                                            Intent i=new Intent(getApplicationContext(),QuestionActivity.class);
                                            i.putExtra("course_id",course_id);
                                            i.putStringArrayListExtra("option1_array", option1_array);
                                            i.putStringArrayListExtra("option2_array", option2_array);
                                            i.putStringArrayListExtra("option3_array", option3_array);
                                            i.putStringArrayListExtra("option4_array", option4_array);
                                            i.putStringArrayListExtra("answer_array", answer_array);
                                            String current_ques_number = "0";
                                            int right = 0;
                                            int wrong = 0;
                                            i.putExtra("current_ques_number", current_ques_number);
                                            i.putExtra("wrong", wrong);
                                            i.putExtra("total_number_of_questions", Integer.parseInt(test_questions));
                                            i.putExtra("right", right);
                                            i.putStringArrayListExtra("selected_answers", selected_answers);
                                            i.putStringArrayListExtra("right_answers", right_answers);
                                            i.putStringArrayListExtra("wrong_answers", wrong_answers);
                                            i.putStringArrayListExtra("question_id_array", question_id_array);
                                            startActivity(i);
//                                            Intent i=new Intent(getApplicationContext(),TestFail.class);
//                                            startActivity(i);
                                        } else
                                        {
                                            Toast.makeText(getApplicationContext(),"Test is not available now",Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),"Test is not available now",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                // Posting parameters to login url
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("value1", course_id);
                                params.put("value2", test_questions);
                                return params;
                            }

                        };
                        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        // Adding request to request queue
                        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Check internet connection and try again",Toast.LENGTH_SHORT).show();
                }




            }
        });

        toolbar.setTitle("Course name");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestInstructions.this.finish();
            }
        });
        toolbar.setNavigationIcon(R.drawable.back_white);


        db1=new DatabaseHandler(getApplicationContext());

        //get pass percentage and number of questions in particular course
        List<Elpfa> elpfa_course = db1.getCourseDetails(Integer.parseInt(course_id));
        for (Elpfa elp : elpfa_course) {
            test_questions=elp.getAllNumberOfQuestions();
            pass_percentage=elp.getAllPassPercent();
        }
        test_questions_txt.setText(""+test_questions);
        pass_percentage_txt.setText(""+pass_percentage);



    }
}
