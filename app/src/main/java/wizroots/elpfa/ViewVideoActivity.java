package wizroots.elpfa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import wizroots.elpfa.utils.JsonUtils;

public class ViewVideoActivity extends AppCompatActivity implements ExoPlayer.EventListener{

    @BindView(R.id.exoplayer)
    SimpleExoPlayerView exoplayerView;

    private static MediaSessionCompat mediaSession;
    private static final String TAG = ViewVideoActivity.class.getSimpleName();
    private PlaybackStateCompat.Builder builder;
    private SimpleExoPlayer exoPlayer;
    private String contentPath;
    private int usersToContentId;
    private DataSource.Factory mediaDataSourceFactory;
    private BandwidthMeter bandwidthMeter;
    private static final int UPDATE_STATUS_LOADER = 10;
    private int isUpdated = 0;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_video);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        contentPath = getIntent().getStringExtra("content_url");
        usersToContentId = getIntent().getIntExtra("users_to_content_id", 0);

        initializeMediaSession();

        initializeExoPlayer(Uri.parse(contentPath));

    }

    private void initializeExoPlayer(Uri uri) {
        if (null == exoPlayer){
               /**
                * initialise player with trackselector and load control
                */
               TrackSelector trackSelector = new DefaultTrackSelector();
               LoadControl loadControl = new DefaultLoadControl();
               exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            exoplayerView.setPlayer(exoPlayer);

               exoPlayer.addListener(this);

              MediaSource mediaSource = new ExtractorMediaSource(uri, new DefaultDataSourceFactory(
                       this, Util.getUserAgent(this,
                       "Elpfa")), new DefaultExtractorsFactory(), null, null);


            exoPlayer.prepare(mediaSource);
           }
    }

    /**
     * method to initialize the media session
     */
    private void initializeMediaSession() {
        mediaSession = new MediaSessionCompat(this, TAG);

        /**
         * enabl callbacks from media buttons and transport callbacks
         */
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        /**
         * do not let media button to start when app is invisible
         */
        mediaSession.setMediaButtonReceiver(null);

        builder = new PlaybackStateCompat.Builder()
                .setActions(PlaybackStateCompat.ACTION_PLAY |
                PlaybackStateCompat.ACTION_PAUSE |
                PlaybackStateCompat.ACTION_PLAY_PAUSE);

        mediaSession.setPlaybackState(builder.build());

        mediaSession.setCallback(new MySessionCallbacks());

        mediaSession.setActive(true);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if ((playbackState == ExoPlayer.STATE_READY) && playWhenReady == true) {
            builder.setState(PlaybackStateCompat.STATE_PLAYING, exoPlayer.getCurrentPosition(), 1f);
        }
        else if ((playbackState == ExoPlayer.STATE_ENDED)){
            UpdateContentUpdateResult resultCallback = new UpdateContentUpdateResult() {
                @Override
                public void notifyResponse(int updated) {
                    isUpdated = updated;
                    if (updated == 1) {
                        int i = databaseHandler.updateContentStatus(new Elpfa("normal_video_completed", usersToContentId, 1));
                    }
                }
            };
            JsonUtils.updateContentStatus(this, usersToContentId, resultCallback);
        }
        else {
            builder.setState(PlaybackStateCompat.STATE_PAUSED, exoPlayer.getCurrentPosition(), 1f);
        }
        mediaSession.setPlaybackState(builder.build());
    }


    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }

    public class MySessionCallbacks extends MediaSessionCompat.Callback{
        @Override
        public void onPause() {
            exoPlayer.setPlayWhenReady(false);
        }

        @Override
        public void onPlay() {
            exoPlayer.setPlayWhenReady(true);
        }
    }
    /**
     * Broadcast Receiver registered to receive the MEDIA_BUTTON intent coming from clients.
     */
    public static class MyMediaReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            MediaButtonReceiver.handleIntent(mediaSession, intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String currentPosition = String.valueOf(exoPlayer.getCurrentPosition());
        String duration = String.valueOf(exoPlayer.getDuration());
    }

    @Override
    public void onBackPressed() {
        boolean updated;
        if (isUpdated == 1){
            updated = true;
            Intent i = new Intent();
            i.putExtra("isUpdated", updated);
            setResult(RESULT_OK, i);
            finish();
        }
        else {
           super.onBackPressed();
        }
    }
}
