package wizroots.elpfa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class ForgotPassword extends AppCompatActivity {
    View decorView;
    String result;
    String email_addrss;
    EditText email;
    String registered_email;
    Button reset;
    Button back;
    RelativeLayout rel1,rel2;
    Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        decorView = getWindow().getDecorView();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        back=(Button)findViewById(R.id.back);
        reset=(Button)findViewById(R.id.reset);
        rel1=(RelativeLayout)findViewById(R.id.rel1);
        rel2=(RelativeLayout)findViewById(R.id.rel2);
        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPassword.this.finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPassword.this.finish();
            }
        });
        email_addrss=getIntent().getStringExtra("email_address");
        email=(EditText)findViewById(R.id.email);
        if (!("".equals(email_addrss)))
        {
            email.setText(email_addrss);
            email.setSelection(email.length());
        }


        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    registered_email=email.getText().toString();
                    String url = Config.ForgotPassword;
                    ExecuteUrl task = new ExecuteUrl();
                    task.execute(url, registered_email);
                    result = task.get();
                    if (result.contains("Not successful"))
                    {
                        final  AlertDialog.Builder builder1 = new AlertDialog.Builder(ForgotPassword.this);
                        builder1.setCancelable(true);
                        builder1.setMessage("Enter registered Email addrss");
                        builder1.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();


                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    else
                    {
                        rel1.setVisibility(View.INVISIBLE);
//                        rel2.setVisibility(View.VISIBLE);
                        findViewById(R.id.slide_layout).setVisibility(View.VISIBLE);
                        back.setBackgroundResource(R.drawable.back_blue);

                        Animation expandIn = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_up);
                        findViewById(R.id.slide_layout).startAnimation(expandIn);
                    }



                } catch (Exception ex) {
                    Log.d("exception", "" + ex);
                }
            }
        });

        ReplaceFont.setDefaultFont(this, "DEFAULT", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "MONOSPACE", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SERIF", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SANS_SERIF", "PT_Sans_Web_Regular.ttf");
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.parseColor("#5a000000"));
            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
        }
    }
}
