package wizroots.elpfa;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class CourseStartAlertService extends Service {
    NotificationCompat.Builder builder;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public CourseStartAlertService() {
        //empty constructor is necessary
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Calendar c = Calendar.getInstance();
        int hours = c.get(Calendar.HOUR);
        int minutes = c.get(Calendar.MINUTE);
        int seconds = c.get(Calendar.SECOND);
        int am_pm=c.get(Calendar.AM_PM);



if ((hours==11)&&(minutes==30)&&(seconds==1)) {
    if (am_pm == 0)
    {
    DatabaseHandler db1 = new DatabaseHandler(this);
    List<Elpfa> elpfa_scheduled = db1.getScheduledCourses();
    int var = 0;
    String assignedDate;
    int no_days;
    String no_days_str;
    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    no_days_str = sharedpreferences.getString("course_days", "one_day");
    if ("one_day".equals(no_days_str)) {
        no_days = 1;
    } else if ("two_days".equals(no_days_str)) {
        no_days = 2;
    } else if ("three_days".equals(no_days_str)) {
        no_days = 3;
    } else {
        no_days = 1;
    }
    String newDate;
    String currentDate;


    try {

        Thread.sleep(1000);
        for (Elpfa elp : elpfa_scheduled) {
            assignedDate = elp.getScheduledAssignDate();

            // convert 0000-00-00 date format
            try {

                final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                //get current date
                Date date = new Date();
                currentDate = format.format(date);

                //get date after specified days
                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(format.parse(currentDate));
                calendar.add(Calendar.DAY_OF_YEAR, no_days);
                newDate = format.format(calendar.getTime());
                var++;
                if (newDate.equals(assignedDate)) {
                    generateNotification(elp.getScheduledCourseName(), assignedDate);
                }

            } catch (Exception e) {

            }
        }
    } catch (Exception e) {

    }
    startService(new Intent(this, CourseStartAlertService.class));
}
}

else
{
    startService(new Intent(this, CourseStartAlertService.class));

}

        return super.onStartCommand(intent, flags, startId);
    }
    public  void generateNotification(String course_name, String date)
    {
        String msgText = "Course "+course_name.toUpperCase()+" will be started on "+date;

        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        Intent intent=new Intent();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
         builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Elpfa Course start alert")
                .setSmallIcon(R.drawable.elpfa_logo)
                .setAutoCancel(true)
                .setColor(Color.rgb(64, 78, 22))
                   .addAction(0, "", contentIntent);
        android.app.Notification notification = new NotificationCompat.BigTextStyle(builder)
                .bigText(msgText).build();
        Random rand=new Random();
        notificationManager.notify(rand.nextInt(100), notification);
        startService(new Intent(this,CourseStartAlertService.class));

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        startService(new Intent(this, CourseStartAlertService.class));
            }
}
