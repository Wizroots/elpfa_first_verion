package wizroots.elpfa;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class TeamMembersAdapter extends RecyclerView.Adapter<TeamMembersAdapter.MyViewHolder> {

    private List<TeamMember> memberList;
    private List<TeamMember> memberList1;
    Context context;

    private ArrayList<TeamMember> arraylist;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView team_member_name;
        public CircleImageView team_member_iamge;
        public TextView first_letter;
        public RelativeLayout team_member_image_rel;


        public MyViewHolder(View view) {
            super(view);
            team_member_name = (TextView) view.findViewById(R.id.team_member_name);
            team_member_iamge = (CircleImageView) view.findViewById(R.id.team_member_image);
            team_member_image_rel = (RelativeLayout) view.findViewById(R.id.team_member_image_rel);
            first_letter= (TextView) view.findViewById(R.id.first_letter);
        }
    }


    public TeamMembersAdapter(List<TeamMember> memberList) {
        this.memberList = memberList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(memberList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.member_list_row, parent, false);
        context = parent.getContext();


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TeamMember member = memberList.get(position);
        String first_txt=member.getMember_name().substring(0,1);
        holder.team_member_name.setText(""+member.getMember_name());

        if (member.getMember_image().equals(""))
        {
            Random r=new Random();
            int a=r.nextInt(10);
            if (a==1) {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.pink));
            }
            else if (a==2)
            {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.orange));
            } else if (a==3)
            {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.lime));
            }  else if (a==4)
            {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.purple));
            } else if (a==5)
            {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.colorAccent));
            } else
            {
                holder.team_member_iamge.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
            }
            holder.team_member_image_rel.setVisibility(View.VISIBLE);
            holder.first_letter.setText(""+first_txt);
        }
        else {
            Picasso.with(holder.team_member_iamge.getContext()).load(member.getMember_image()).placeholder(R.drawable.ic_user).into(holder.team_member_iamge);
        }

//            Picasso.with(holder.course_image.getContext()).load(course.getCoursePath()).into(holder.course_image);


    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        memberList.clear();

        if (charText.length() == 0) {
            memberList.addAll(arraylist);
        } else {
            for (TeamMember wp : arraylist) {
                if (wp.getMember_name().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    memberList.add(wp);
                }
            }
        }

        notifyDataSetChanged();
    }
    public void setFilter(List<TeamMember> members) {
        memberList1 = new ArrayList<>();
        memberList1.addAll(members);
        notifyDataSetChanged();
    }
}