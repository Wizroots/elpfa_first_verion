package wizroots.elpfa;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.ImagePickerSheetView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class EditProfile extends AppCompatActivity {
private Toolbar mToolbar;
    RelativeLayout edit_img_layout;
    private static final int REQUEST_STORAGE = 0;
    protected BottomSheetLayout bottomSheetLayout;
    private Uri cameraImageUri = null;
    TextView name_txt;
    TextView phone_txt;
    TextView first_letter;
    int flag=0;
    //keep track of cropping intent
    final int PIC_CROP = 3;int  n;
     Bundle extras;
    String temp;
    TextView company_name_txt;
    TextView password_txt;
    TextView re_enter_password_txt;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    DatabaseHandler db1;
    String password;
    String name;
    private Uri picUri;
    String profile_image_color;
    String profile_image_color_new="grey";
    String email;
    String phone;
    String company_name;
    String pass;
    RelativeLayout profile_color_text;
    String decrypt_pass;
    private static byte[] sharedvector = {
            0x01, 0x02, 0x03, 0x05, 0x07, 0x0B, 0x0D, 0x11
    };
    de.hdodenhof.circleimageview.CircleImageView img;
    private static final int REQUEST_IMAGE_CAPTURE = REQUEST_STORAGE + 1;
    private static final int REQUEST_LOAD_IMAGE = REQUEST_IMAGE_CAPTURE + 1;
    static final int CAMERA_CAPTURE = 1;
    static final int CAMERA_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        edit_img_layout=(RelativeLayout)findViewById(R.id.edit_img_layout);
        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        bottomSheetLayout.setPeekOnDismiss(true);
        img=(de.hdodenhof.circleimageview.CircleImageView)findViewById(R.id.profile_img);
        profile_color_text=(RelativeLayout) findViewById(R.id.profile_color_text);
        first_letter=(TextView) findViewById(R.id.first_letter);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String first_txt=sharedpreferences.getString("person_name",null).substring(0,1);
        first_letter.setText(""+first_txt.toUpperCase());
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {
            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {
            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(img);
        }


        name_txt=(TextView)findViewById(R.id.name_txt);
        phone_txt=(TextView)findViewById(R.id.phone_txt);
        company_name_txt=(TextView)findViewById(R.id.company_txt);
        password_txt=(TextView)findViewById(R.id.password_txt);
        re_enter_password_txt=(TextView)findViewById(R.id.re_enter_password_txt);
        db1=new DatabaseHandler(getApplicationContext());
        name_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (flag!=0) {
                    Random rand = new Random();
                    int i = rand.nextInt(16);
                    Log.d("random_number", "i : " + i);
                    if (i == 1 || i == 5 || i == 9 || i == 13) {
                        img.setColorFilter(getResources().getColor(R.color.purple));
                        profile_image_color_new = "purple";
                    } else if (i == 2 || i == 6 || i == 10 || i == 14) {
                        img.setColorFilter(getResources().getColor(R.color.pink));
                        profile_image_color_new = "pink";
                    } else if (i == 3 || i == 7 || i == 11 || i == 15) {
                        img.setColorFilter(getResources().getColor(R.color.lime));
                        profile_image_color_new = "lime";
                    } else if (i == 4 || i == 8 || i == 12 || i == 16) {
                        img.setColorFilter(getResources().getColor(R.color.orange));
                        profile_image_color_new = "orange";
                    }
                }
                else
                {
                    flag=1;
                }
            }
        });
        List<Elpfa> elpfa = db1.getUserDetails();
        for (Elpfa elp : elpfa) {
            name = elp.getUserName();
            email=elp.getUserEmail();
            phone=elp.getUserMobileNo();
            company_name=elp.getUserCompanyName();
            Log.d("user_details","phone : "+phone);
            Log.d("user_details","company name : "+company_name);
            name_txt.setText(""+name);
            if(null==phone) {
                phone_txt.setText("");
            }
            else
            {

                phone_txt.setText(""+phone);
            }
            if(null==company_name) {
                company_name_txt.setText("");
            }
            else
            {
                company_name_txt.setText(""+company_name);
            }
            pass=elp.getUserPassword();
           /* Log.d("edit_profile","password from local db : "+pass);
            password_txt.setText(""+pass);
            re_enter_password_txt.setText(""+pass);*/

        }
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        edit_img_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        mToolbar.setNavigationIcon(R.drawable.back_white);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                EditProfile.this.finish();
                    onBackPressed();
            }
        });
    }
    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.profile_menu_tick, menu);
        return true;
    }
    public void pickImage()
    {
        if (checkNeedsPermission()) {
            requestStoragePermission();
        } else {
            showSheetView();
        }
    }

    private boolean checkNeedsPermission() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && ActivityCompat.checkSelfPermission(EditProfile.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }
//    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
        } else {
            // Eh, prompt anyway
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
        }
    }
    private void showSheetView() {
        ImagePickerSheetView sheetView = new ImagePickerSheetView.Builder(this)
                .setMaxItems(30)
                .setShowCameraOption(createCameraIntent() != null)
                .setShowPickerOption(createPickIntent() != null)
                .setImageProvider(new ImagePickerSheetView.ImageProvider() {
                    @Override
                    public void onProvideImage(ImageView imageView, Uri imageUri, int size) {
                        Glide.with(EditProfile.this)
                                .load(imageUri)
                                .centerCrop()
                                .crossFade()
                                .into(imageView);
                    }
                })
                .setOnTileSelectedListener(new ImagePickerSheetView.OnTileSelectedListener() {
                    @Override
                    public void onTileSelected(ImagePickerSheetView.ImagePickerTile selectedTile) {
                        bottomSheetLayout.dismissSheet();
                        if (selectedTile.isCameraTile()) {
                            Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

                            File file=getOutputMediaFile(1);
                            picUri = Uri.fromFile(file); // create
                            i.putExtra(MediaStore.EXTRA_OUTPUT,picUri); // set the image file

                            startActivityForResult(i, CAMERA_CAPTURE);
                        } else if (selectedTile.isPickerTile()) {
                            startActivityForResult(createPickIntent(), REQUEST_LOAD_IMAGE);
                        } else if (selectedTile.isImageTile()) {
                            showSelectedImage(selectedTile.getImageUri());


                        } else {
                            genericError();
                        }
                    }
                })
                .setTitle("Choose an image...")
                .create();

        bottomSheetLayout.showWithSheetView(sheetView);
    }
    @Nullable
    private Intent createCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            return takePictureIntent;
        } else {
            return null;
        }
    }
    @Nullable
    private Intent createPickIntent() {
        Intent picImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (picImageIntent.resolveActivity(getPackageManager()) != null) {

            return picImageIntent;
        }
        else {
            return null;
        }
    }
//    private void dispatchTakePictureIntent() {
//        Log.d("image_select","in dispatchTakePictureIntent");
//        Log.d("image_select","1");
//        Intent takePictureIntent = createCameraIntent();
//        Log.d("image_select","2");
//        // Ensure that there's a camera activity to handle the intent
//        if (takePictureIntent != null) {
//            Log.d("image_select","3");
//            // Create the File where the photo should go
//            try {
//                Log.d("image_select","4");
//                File imageFile = createImageFile();
//                Log.d("image_select","5");
////                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
//                Log.d("image_select","6");
//                startActivityForResult(takePictureIntent, CAMERA_CAPTURE);
//                Log.d("image_select","7");
//                performCrop();
//            } catch (IOException e) {
//                // Error occurred while creating the File
//                genericError("Could not create imageFile for camera");
//                Log.d("image_select","8");
//            }
//        }
//
//    }
//    private File createImageFile() throws IOException {
//        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
//        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//        File imageFile = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        cameraImageUri = Uri.fromFile(imageFile);
//        return imageFile;
//    }
    private void showSelectedImage(Uri selectedImageUri) {
        try {

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImageUri);
            picUri=selectedImageUri;
            performCrop();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void genericError() {
        genericError(null);
    }

    private void genericError(String message) {
        Toast.makeText(this, message == null ? "Something went wrong." : message, Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_action_edit:
                ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if (isConnected == true) {
                    Log.d("edit_profile","1");
                    Log.d("edit_profile","11");
                    if (name_txt.getText().toString().length() < 3) {
                        Log.d("edit_profile","2");
                        Toast.makeText(getApplicationContext(), "Name should have atleast 3 characters", Toast.LENGTH_SHORT).show();
                    }else if (re_enter_password_txt.getText().toString().length() != 0) {
                        Log.d("edit_profile","4");
                        if (!(password_txt.getText().toString().equals(re_enter_password_txt.getText().toString())))
                        {
                            Toast.makeText(getApplicationContext(), "Password mismatch", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            update();
                        }
                    }else if (password_txt.getText().toString().length() != 0) {
                        Log.d("edit_profile","4");
                        if (!(password_txt.getText().toString().equals(re_enter_password_txt.getText().toString())))
                        {
                            Toast.makeText(getApplicationContext(), "Password mismatch", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            update();
                        }
                    }  else if (phone_txt.getText().toString().length() != 0) {
                        if (!phone_txt.getText().toString().equals("0")){
                            Log.d("edit_profile", "111");
                            if (phone_txt.getText().toString().length() != 10) {
                                Log.d("edit_profile", "112");
                                Toast.makeText(getApplicationContext(), "Enter a valid mobile number", Toast.LENGTH_SHORT).show();
                                Log.d("edit_profile", "113");
                            } else {
                                Log.d("edit_profile", "114");
                                update();
                            }
                        }
                        else
                        {
                            update();
                        }
                    }  else {

                        update();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Check internet connection and try again",Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public static String md5(String s) { try {

        MessageDigest digest = MessageDigest
                .getInstance("MD5");
        digest.update(s.getBytes());
        byte messageDigest[] = digest.digest();

        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < messageDigest.length; i++) {
            String h = Integer.toHexString(0xFF & messageDigest[i]);
            while (h.length() < 2)
                h = "0" + h;
            hexString.append(h);
        }
        return hexString.toString();

    } catch (NoSuchAlgorithmException e) {

    }
        return "";

    }
public void update()
{
    Log.d("edit_profile","5");
    //update user details in server
    try {

        Config url_details = new Config();

        Log.d("edit_profile","6");
                    /* using volley in login - combine everything */
        String tag_string_req = "req_state";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url_details.editProfile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null && response.length() > 0) {
                    Log.d("edit_profile", "response : " + response);

                    Log.d("edit_profile","7");
                    if (response.equals("updated"))
                    {

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("profile_image_color",profile_image_color_new);
                        db1.deleteUserTable();
                        String password_new ;
                        if (password_txt.getText().toString().length()==0)
                        {

                            password_new =  pass;
                        }
                         else
                        {
                            password_new = password_txt.getText().toString();
                        }
                        db1.addUserDetails(new Elpfa(sharedpreferences.getString("User_id",null), name_txt.getText().toString(), password_new, sharedpreferences.getString("Username",null), "1", company_name_txt.getText().toString(), company_name_txt.getText().toString(), sharedpreferences.getString("photo","no_img"),phone_txt.getText().toString()));

                        editor.putString("person_name", name_txt.getText().toString());
                        if (password_txt.getText().toString().length()==0)
                             editor.putString("Password", password);
                        editor.putString("company_name", company_name_txt.getText().toString());
                        editor.putString("phone_number", phone_txt.getText().toString());
                        editor.commit();
                        Intent i=new Intent(getApplicationContext(),Profile.class);

                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(EditProfile.this, (View)img, "profile");
                        EditProfile.this.finish();
                        startActivity(i,options.toBundle());                                    }
                }



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("edit_profile", "error : " + error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                Log.d("edit_profile", "email : " + sharedpreferences.getString("Username", null));
                Log.d("edit_profile", "name : " + name_txt.getText().toString());
                Log.d("edit_profile", "phone : " + phone_txt.getText().toString());
                Log.d("edit_profile", "company name : " + company_name_txt.getText().toString());
                params.put("value1", sharedpreferences.getString("Username", null));
                params.put("value2", name_txt.getText().toString());
                params.put("value3", phone_txt.getText().toString());
                params.put("value4", company_name_txt.getText().toString());

                if (password_txt.getText().toString().length()==0)
                {

                    params.put("value5", pass);
                }
                else
                {
                    password = md5(password_txt.getText().toString());
                    Log.d("edit_profile", "password : " + password);
                    params.put("value5", password);
                }

                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


    } catch (Exception ex) {
        Log.d("exception", "" + ex);
    }
}
    private void performCrop(){
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            //user is returning from capturing an image using the camera
            if(requestCode == CAMERA_CAPTURE){
                Toast.makeText(getApplicationContext(),"captured",Toast.LENGTH_SHORT).show();
                performCrop();
            }
            else if(requestCode == CAMERA_REQUEST){
            }

            else if(requestCode == REQUEST_LOAD_IMAGE){
                extras = data.getExtras();
                picUri = data.getData();
                performCrop();
            }

            //user is returning from cropping the image
            else if(requestCode == PIC_CROP){
                extras = data.getExtras();
                try {

                    Config url_login1 = new Config();

                    //* using volley in login - combine everything *//*
                    String tag_string_req1 = "req_state1";
                    StringRequest strReq1 = new StringRequest(Request.Method.POST,
                            url_login1.upload_user_photo_new, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            db1.deleteUserTable();
                            db1.addUserDetails(new Elpfa(sharedpreferences.getString("User_id",null), sharedpreferences.getString("person_name",null), sharedpreferences.getString("Password",null), sharedpreferences.getString("Username",null), "1", sharedpreferences.getString("company_name",null), sharedpreferences.getString("company_name",null), temp,sharedpreferences.getString("phone_number",null)));

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("photo",Config.BASE_CONTENT_URL +n+".jpg");
                            editor.commit();

                            //get the cropped bitmap
                            Bitmap thePic = (Bitmap) extras.get("data");
                            //display the returned cropped image
                            img.setImageBitmap(thePic);
                            String path=sharedpreferences.getString("photo","no_img");
                            if (path.equals("no_img")) {
                                profile_image_color=sharedpreferences.getString("profile_image_color","grey");
                                if (profile_image_color.equals("purple"))
                                {
                                    img.setColorFilter(getResources().getColor(R.color.purple));
                                }
                                else if (profile_image_color.equals("pink"))
                                {
                                    img.setColorFilter(getResources().getColor(R.color.pink));
                                }
                                else if (profile_image_color.equals("lime"))
                                {
                                    img.setColorFilter(getResources().getColor(R.color.lime));
                                }
                                else if (profile_image_color.equals("orange"))
                                {
                                    img.setColorFilter(getResources().getColor(R.color.orange));
                                }
                                else {
                                    img.setColorFilter(getResources().getColor(R.color.grey));
                                }
                                profile_color_text.setVisibility(View.VISIBLE);
                            }
                            else
                            {

                                profile_color_text.setVisibility(View.INVISIBLE);
                                img.setColorFilter(getResources().getColor(R.color.transparent));
                                Picasso.with(getApplicationContext()).invalidate(sharedpreferences.getString("photo", "no_img"));
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            // Posting parameters to login url
                            Map<String, String> params = new HashMap<String, String>();
                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


                            Bitmap thePic = (Bitmap) extras.get("data");
                            //convert bitmap into string
                            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
                            thePic.compress(Bitmap.CompressFormat.PNG,100, baos);
                            byte [] b=baos.toByteArray();
                             temp= Base64.encodeToString(b, Base64.DEFAULT);
                            params.put("value1", temp);
                            params.put("value2", sharedpreferences.getString("Username", null));
                            Random rand = new Random();

                              n = rand.nextInt(100) + 1;
                            params.put("value3", String.valueOf(n));


                            return params;
                        }

                    };
                    strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);









                } catch (Exception e) {
                }

            }
        }
    }
    /** Create a File for saving an image */
    private  File getOutputMediaFile(int type){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyApplication");

        /**Create the storage directory if it does not exist*/
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        /**Create a media file name*/
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == 1){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }
}
