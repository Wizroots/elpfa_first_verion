package wizroots.elpfa;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Android1 on 4/26/2016.
 */
class ExecuteUrl extends AsyncTask<String, Void, String> {
    StringBuilder response;
    String urlss;

    @Override
    protected String doInBackground(String... urls) {


        HttpURLConnection httpURLConnection = null;
        try {
            urlss=urls[0];
            URL url = new URL(urlss);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.connect();
            BufferedWriter out =
                    new BufferedWriter(new OutputStreamWriter(httpURLConnection.getOutputStream()));
            JSONObject jsonObject = new JSONObject();

            for (int i=urls.length-1;i!=0;i--) {
                String temp=urls[i];
                jsonObject.put("value"+i, temp);
            }
            out.write(jsonObject.toString());
            out.close();
            out.close();


            int responseCode = httpURLConnection.getResponseCode();


            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        httpURLConnection.getInputStream()));
                String inputLine;
                response = new StringBuilder();
                inputLine = in.readLine();
                response.append(inputLine);

                in.close();

            }

            String json = response.toString();
            return json;


        }
        catch (Exception e)
        {
        }

        finally {
            try {
                if (httpURLConnection!=null) {
                    httpURLConnection.disconnect();
                }

            }
            catch (NullPointerException e)
            {
                Log.d("exception", "" + e);
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

    }
}

