package wizroots.elpfa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MessageListFragment extends Fragment implements HandleVolleyResponse{
    Menu menu_context;
    private static final String TAG = "messageFragment";
    Unbinder unbinder;
    private RecyclerView mRecyclerView;
    MessageAdapter messageAdapter;
    ExpandableLinearLayout expandableLinearLayout;
    ActionBarDrawerToggle toggle;
    LinearLayout linear1, linear2, linear3;
    private MultiSelector mMultiSelector = new MultiSelector();
    Toolbar toolbar;
    JSONObject jObject1;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    String color;
    FloatingActionButton fab;
    DatabaseHandler db;
    ArrayList<String> message_id_arr = new ArrayList<>();
    TextView title;

    private RelativeLayout layoutProgress;

    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
    ArrayList<String> teamchat_username_arr = new ArrayList<>();
    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();

    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    String teamchat_createdat;
    int teamchat_teamid;
    int teamchat_chatid;

    private boolean isAllAvailable = false;

    private ModalMultiSelectorCallback mDeleteMode = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            super.onCreateActionMode(actionMode, menu);
            getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);
            menu_context = menu;
            menu_context.add(R.string.title_activity_login);

            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            if (menuItem.getItemId() == R.id.action_delete) {
                Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT);

            } else if (menuItem.getItemId() == R.id.action_read_unread) {

            }
            return false;
        }
    };
    private ArrayList<MessagesNew> mMessages = new ArrayList<>();
    private boolean mSubtitleVisible;
    private JSONObject jObject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle("Messagess");
        mSubtitleVisible = false;
        messageAdapter = new MessageAdapter();

    }

    /**
     * Note: since the fragment is retained. the bundle passed in after state is restored is null.
     * THe only way to pass parcelable objects is through the activities onsavedInstanceState and appropiate startup lifecycle
     * However after having second thoughts, since the fragment is retained then all the states and instance variables are
     * retained as well. no need to make the selection states percelable therefore just check for the selectionstate
     * from the multiselector
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        if (mMultiSelector != null) {
            Bundle bundle = savedInstanceState;
            if (bundle != null) {
                mMultiSelector.restoreSelectionStates(bundle.getBundle(TAG));
            }

            if (mMultiSelector.isSelectable()) {
                if (mDeleteMode != null) {
                    mDeleteMode.setClearOnPrepare(false);
                    ((AppCompatActivity) getActivity()).startSupportActionMode(mDeleteMode);
                }

            }
        }

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBundle(TAG, mMultiSelector.saveSelectionStates());
        super.onSaveInstanceState(outState);
    }

    //    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_message_list, parent, false);
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setTitle("");

        if (mSubtitleVisible) {
            getActivity().getActionBar().setSubtitle("subtitle");
        }

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        expandableLinearLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        linear1 = (LinearLayout) v.findViewById(R.id.linear1);
        linear2 = (LinearLayout) v.findViewById(R.id.linear2);
        linear3 = (LinearLayout) v.findViewById(R.id.linear3);
        fab = (FloatingActionButton) v.findViewById(R.id.fab);
        title = (TextView) v.findViewById(R.id.messages);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);


        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(messageAdapter);
        v.findViewById(R.id.linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableLinearLayout.toggle();
            }
        });
        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Inbox messages", Toast.LENGTH_SHORT).show();
                mMessages.clear();
                messageAdapter.notifyDataSetChanged();
                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                loadInboxMessages();
                expandableLinearLayout.toggle();
                title.setText("Inbox");
            }
        });
        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Outbox messages", Toast.LENGTH_SHORT).show();
                mMessages.clear();
                messageAdapter.notifyDataSetChanged();
                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                loadOutboxMessages();
                expandableLinearLayout.toggle();
                title.setText("Outbox");
            }
        });
        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Important messages", Toast.LENGTH_SHORT).show();
                mMessages.clear();
                ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                messageAdapter.notifyDataSetChanged();
                expandableLinearLayout.toggle();
                title.setText("Important");
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), CreateMessage.class);
                startActivity(i);
            }
        });

        layoutProgress = (RelativeLayout) v.findViewById(R.id.layout_progressbar);
        getMessagesFromServer();
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    private void getMessagesFromServer() {
        layoutProgress.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);

        if (Utils.isNetworkAvailable(getActivity()))
            loadMessageFromServer();
        else {
            layoutProgress.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    private void fetchFromServer() {
        /**********  end fetch notifications ***********/
//msg inbx
        Config url_login1 = new Config();

                    /* using volley in login - combine everything */
        String tag_string_req1 = "req_state1";
        StringRequest strReq1 = new StringRequest(Request.Method.POST,
                url_login1.FetchMessages, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("MessageReceiver", response);
                editor.commit();
                try {

                    try {
                        try {

                            DatabaseHandler db1 = new DatabaseHandler(getActivity());

                            jObject1 = new JSONObject(response);

                            //insert inbox messages
                            db1.deleteInboxMessagesTable();
                            if (!jObject1.isNull("inbox_team")) {
                                JSONArray jsonArrayInbox1 = jObject1.getJSONArray("inbox_team");

                                for (int i = 0; i < jsonArrayInbox1.length(); i++) {
                                    JSONObject jasonObjectInbox = jsonArrayInbox1.getJSONObject(i);
                                    db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                }
                            }
                            if (!jObject1.isNull("inbox_course")) {
                                JSONArray jsonArrayInbox2 = jObject1.getJSONArray("inbox_course");
                                for (int i = 0; i < jsonArrayInbox2.length(); i++) {
                                    JSONObject jasonObjectInbox = jsonArrayInbox2.getJSONObject(i);
                                    db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                }
                            }
                            if (!jObject1.isNull("inbox_user")) {
                                JSONArray jsonArrayInbox3 = jObject1.getJSONArray("inbox_user");
                                for (int i = 0; i < jsonArrayInbox3.length(); i++) {
                                    JSONObject jasonObjectInbox = jsonArrayInbox3.getJSONObject(i);
                                    db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"), jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                }
                            }
                            db1.deleteOutboxMessagesTable();
                            if (!jObject1.isNull("outbox_team")) {
                                JSONArray jsonArrayOutbox1 = jObject1.getJSONArray("outbox_team");
                                for (int i = 0; i < jsonArrayOutbox1.length(); i++) {
                                    JSONObject jasonObjectOutbox = jsonArrayOutbox1.getJSONObject(i);
                                    db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                }
                            }

                            if (!jObject1.isNull("outbox_course")) {
                                JSONArray jsonArrayOutbox2 = jObject1.getJSONArray("outbox_course");
                                for (int i = 0; i < jsonArrayOutbox2.length(); i++) {
                                    JSONObject jasonObjectOutbox = jsonArrayOutbox2.getJSONObject(i);
                                    db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("course_id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null),jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                }
                            }
                            if (!jObject1.isNull("outbox_user")) {
                                JSONArray jsonArrayOutbox3 = jObject1.getJSONArray("outbox_user");
                                for (int i = 0; i < jsonArrayOutbox3.length(); i++) {
                                    JSONObject jasonObjectOutbox = jsonArrayOutbox3.getJSONObject(i);
                                    db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"),sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"),  jasonObjectOutbox.getString("title"),jasonObjectOutbox.getString("message"), jasonObjectOutbox.getString("photo"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                }
                            }

                            db1.deleteTeamChatTable();
                            db1.deleteTeamChatTableNew();
                            if (!jObject1.isNull("teamchat")) {
                                teamchat_id_arr.clear();
                                teamchat_teamid_arr.clear();
                                teamchat_username_arr.clear();
                                teamchat_imagepath_arr.clear();
                                teamchat_messages_arr.clear();
                                teamchat_createdat_arr.clear();
                                JSONArray jsonArrayTeamchat = jObject1.getJSONArray("teamchat");
                                int teamchat_length = jsonArrayTeamchat.length();
                                for (int i = 0; i < teamchat_length; i++) {
                                    JSONObject JasonobjectTeamchat = jsonArrayTeamchat.getJSONObject(i);
                                    teamchat_chatid = JasonobjectTeamchat.getInt("id");
                                    teamchat_teamid = JasonobjectTeamchat.getInt("team_id");
                                    teamchat_username = JasonobjectTeamchat.getString("user_name");
                                    teamchat_imagepath = JasonobjectTeamchat.getString("image_path");
                                    teamchat_messages = JasonobjectTeamchat.getString("messages");
                                    teamchat_createdat = JasonobjectTeamchat.getString("created_at");

                                    teamchat_teamid_arr.add(i, teamchat_teamid);
                                    teamchat_username_arr.add(i, teamchat_username);
                                    teamchat_imagepath_arr.add(i, teamchat_imagepath);
                                    teamchat_messages_arr.add(i, teamchat_messages);
                                    teamchat_createdat_arr.add(i, teamchat_createdat);

                                    //insert teamchat into sqlite

                                    db1.addTeamChat(new Elpfa("all", "chat", teamchat_chatid, teamchat_teamid, teamchat_username, teamchat_imagepath, teamchat_messages, teamchat_createdat));

                                }


                            }

                        } catch (Exception e) {
                            Log.d("exception", "" + e);
                        }
//

                    } catch (Exception e) {
                        Log.d("exception", "" + e);

                    }


                } catch (Exception e) {
                    Log.d("exception", "" + e);

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("value1", sharedpreferences.getString("User_id", null));
                return params;
            }

        };
        strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_messages, menu);
        MenuItem showSubtitle = menu.findItem(R.id.menu_item_show_subtitle);
        if (mSubtitleVisible && showSubtitle != null) {
            showSubtitle.setTitle("hide sub title");
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {

//            final MessagesNew message = new MessagesNew();
//            CrimeLab.get(getActivity()).addCrime(crime);
//
//            mRecyclerView.getAdapter().notifyItemInserted(mCrimes.indexOf(crime));
//
//            // NOTE: Left this code in for commentary. I believe this is what you would do
//            // to wait until the new crime is added, then animate the selection of the new crime.
//            // It does not work, though: the listener will be called immediately,
//            // because no animations have been queued yet.
////                mRecyclerView.getItemAnimator().isRunning(
////                        new RecyclerView.ItemAnimator.ItemAnimatorFinishedListener() {
////                    @Override
////                    public void onAnimationsFinished() {
////                        selectCrime(crime);
////                    }
////                });
            return true;
        } else if (item.getItemId() == R.id.menu_item_show_subtitle) {
            Toast.makeText(getActivity(), "show sub title", Toast.LENGTH_SHORT).show();
//                ActionBar actionBar = getActionBar();
//                if (actionBar.getSubtitle() == null) {
//                    actionBar.setSubtitle(R.string.subtitle);
//                    mSubtitleVisible = true;
//                    item.setTitle(R.string.hide_subtitle);
//                } else {
//                    actionBar.setSubtitle(null);
//                    mSubtitleVisible = false;
//                    item.setTitle(R.string.show_subtitle);
//                }
            return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.message_list_item_context, menu);

        menu.setHeaderTitle("0 selected");

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onVolleyResponse(boolean isSuccess) {
        if (isSuccess){
            layoutProgress.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        else {
            layoutProgress.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }


    private class MessageHolder extends SwappingHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView message_contact;
        TextView message_subject;
        TextView message_message;
        TextView message_date;
        RatingBar message_important;
        LinearLayout root;
        ImageView message_attachment;
        LinearLayout linear;
        private MessagesNew mMessage;

        public MessageHolder(View itemView) {
            super(itemView, mMultiSelector);

            message_contact = (TextView) itemView.findViewById(R.id.message_contact);
            message_subject = (TextView) itemView.findViewById(R.id.message_subject);
            message_message = (TextView) itemView.findViewById(R.id.message_message);
            message_date = (TextView) itemView.findViewById(R.id.message_date);
            message_important = (RatingBar) itemView.findViewById(R.id.message_important);
            message_attachment = (ImageView) itemView.findViewById(R.id.attachment);
            root = (LinearLayout) itemView.findViewById(R.id.root);
            linear = (LinearLayout) itemView.findViewById(R.id.linear);
            db = new DatabaseHandler(itemView.getContext());
            message_important.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                }
            });
            itemView.setOnClickListener(this);
            itemView.setLongClickable(true);
            itemView.setOnLongClickListener(this);
        }

        public void bindMessage(final MessagesNew message) {
            mMessage = message;
            message_important.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                }
            });
            root.setTag(message.getMessage_tag());
            linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), MessageDetails.class);
                    i.putExtra("message_id", message.getMessage_id());
                    if (title.getText().toString().equals("Messages")) {
                        i.putExtra("inbox_outbox", "inbox");
                    } else if (title.getText().toString().equals("Inbox")) {
                        i.putExtra("inbox_outbox", "inbox");
                    } else if (title.getText().toString().equals("Outbox")) {
                        i.putExtra("inbox_outbox", "outbox");
                    }
                    v.getContext().startActivity(i);
                }
            });
//        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                mActionMode = v.startActionMode(new Toolbar_ActionMode_Callback(v.getContext()));
//                return false;
//            }
//        });
//            Log.d("context_toolbar","all_idList : "+all_idList);
            message_important.setRating(message.getMessage_important());
            message_important.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (title.getText().toString().equals("Inbox") || (title.getText().toString().equals("Messages"))) {
                            if (message.getMessage_important() == 0) {
                                message_important.setRating(1);
                                message.setMessage_important(1);
                                int res = db.updateInboxImportant(new Elpfa("inbox_message_important", Integer.parseInt(message.getMessage_id()), 1));

                            } else {
                                message_important.setRating(0);
                                message.setMessage_important(0);
                                int res = db.updateInboxImportant(new Elpfa("inbox_message_important", Integer.parseInt(message.getMessage_id()), 0));
                            }
                        } else if (title.getText().toString().equals("Outbox")) {
                            if (message.getMessage_important() == 0) {
                                message_important.setRating(1);
                                message.setMessage_important(1);
                                int res = db.updateOutboxImportant(new Elpfa("outbox_message_important", Integer.parseInt(message.getMessage_id()), 1));
                            } else {
                                message_important.setRating(0);
                                message.setMessage_important(0);
                                int res = db.updateOutboxImportant(new Elpfa("outbox_message_important", Integer.parseInt(message.getMessage_id()), 0));
                            }
                        }
                    }

                    return true;
                }
            });
            message_contact.setText(message.getMessage_sender());
            message_subject.setText(message.getMessage_subject());
            message_message.setText(message.getMessage_message());
            if (message.getMessage_attachment().equals("0")) {
                message_attachment.setVisibility(View.INVISIBLE);
            } else {
                message_attachment.setVisibility(View.VISIBLE);
            }
            String dateAsText = new SimpleDateFormat("dd MMM yyyy")
                    .format(new Date(Long.parseLong(message.getMessage_date()) * 1000L));
            message_date.setText(dateAsText);
        }

        @Override
        public void onClick(View v) {

            if (mMessage == null) {
                return;
            }
            if (!mMultiSelector.tapSelection(this)) {
                selectMessage(mMessage);
            }

        }


        @Override
        public boolean onLongClick(View v) {

            ((AppCompatActivity) getActivity()).startSupportActionMode(mDeleteMode);
            mMultiSelector.setSelected(this, true);
            return true;
        }


    }

    private void selectMessage(MessagesNew mMessage) {
    }


    private class MessageAdapter extends RecyclerView.Adapter<MessageHolder> {
        @Override
        public MessageHolder onCreateViewHolder(ViewGroup parent, int pos) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_message, parent, false);
            return new MessageHolder(view);
        }

        @Override
        public void onBindViewHolder(MessageHolder holder, int pos) {
            MessagesNew message = mMessages.get(pos);
            holder.bindMessage(message);
            holder.message_important.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                }
            });
        }

        @Override
        public int getItemCount() {
            return mMessages.size();
        }
    }

    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }

    public void loadInboxMessages() {
        mMessages.clear();
        try {
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            message_id_arr.clear();
            int count = 0;
            int course_tag;
            //retrieve all completed courses
            List<Elpfa> elpfa_all = db1.getInboxMessages();
            for (Elpfa elp : elpfa_all) {
                if (elp.getInboxMessageViewed() == 0) {
                    color = "#FFFFFF";
                } else {
                    color = "#E0E0E0";
                }
                message_id_arr.add(count, String.valueOf(elp.getInboxMessageMsgid()));
                MessagesNew messages = new MessagesNew(String.valueOf(elp.getInboxMessageMsgid()), elp.getInbox_msg_sender(), elp.getInbox_msg_receiver(), elp.getInbox_msg_subject(), elp.getInboxMessageMsg(), elp.getInbox_msg_photo(), elp.getInbox_msg_date(), count, color, elp.getInboxMessageViewed(), elp.getInbox_msg_important(), elp.getInbox_msg_attachment(), "inbox");
                mMessages.add(messages);
                count++;
                messageAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadOutboxMessages() {
        mMessages.clear();
        try {
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            message_id_arr.clear();
            int count = 0;
            int course_tag;
            //retrieve all completed courses
            List<Elpfa> elpfa_all = db1.getInOutboxMessages();
            for (Elpfa elp : elpfa_all) {
                color = "#E0E0E0";
                message_id_arr.add(count, String.valueOf(elp.getOutboxMessageMsgid()));
                MessagesNew messages = new MessagesNew(String.valueOf(elp.getOutboxMessageMsgid()), elp.getOutbox_msg_sender(), elp.getOutbox_msg_receiver(), elp.getOutbox_msg_subject(), elp.getOutbox_msg_msg(), elp.getOutbox_msg_photo(), elp.getOutbox_msg_date(), count, color, 1, elp.getOutbox_msg_important(), elp.getOutbox_msg_attachment(), "outbox");
                mMessages.add(messages);
                count++;
                messageAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (title.getText().toString().equals("Inbox")) {

            loadInboxMessages();
        } else if (title.getText().toString().equals("Outbox")) {
            loadOutboxMessages();
        } else {
            loadInboxMessages();
        }
    }
    public void loadMessageFromServer() {
        Config url_login = new Config();

                    /* using volley in login - combine everything */
        String tag_string_req = "req_state";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url_login.FetchAllDetails, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response.equals("Not successfull")) {
                    onVolleyResponse(isAllAvailable);

                } else {

                    try {

                        try {
                            try {
                                // fetch all user types details
                                DatabaseHandler db1 = new DatabaseHandler(getActivity());
                                jObject = new JSONObject(response);



                                /**********  end fetch notifications ***********/
//msg inbx
                                Config url_login1 = new Config();

                    /* using volley in login - combine everything */
                                String tag_string_req1 = "req_state1";
                                StringRequest strReq1 = new StringRequest(Request.Method.POST,
                                        url_login1.FetchMessages, new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString("MessageReceiver", response);
                                        editor.commit();
                                        try {

                                            try {

                                                try {

                                                    DatabaseHandler db1 = new DatabaseHandler(getActivity());

                                                    jObject1 = new JSONObject(response);

                                                    //insert inbox messages
                                                    db1.deleteInboxMessagesTable();
                                                    if (!jObject1.isNull("inbox_team")) {
                                                        JSONArray jsonArrayInbox1 = jObject1.getJSONArray("inbox_team");

                                                        for (int i = 0; i < jsonArrayInbox1.length(); i++) {
                                                            JSONObject jasonObjectInbox = jsonArrayInbox1.getJSONObject(i);
                                                            db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                        }
                                                    }
                                                    if (!jObject1.isNull("inbox_course")) {
                                                        JSONArray jsonArrayInbox2 = jObject1.getJSONArray("inbox_course");
                                                        for (int i = 0; i < jsonArrayInbox2.length(); i++) {
                                                            JSONObject jasonObjectInbox = jsonArrayInbox2.getJSONObject(i);
                                                            db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                        }
                                                    }
                                                    if (!jObject1.isNull("inbox_user")) {
                                                        JSONArray jsonArrayInbox3 = jObject1.getJSONArray("inbox_user");
                                                        for (int i = 0; i < jsonArrayInbox3.length(); i++) {
                                                            JSONObject jasonObjectInbox = jsonArrayInbox3.getJSONObject(i);
                                                            db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"), jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                        }
                                                    }
                                                    db1.deleteOutboxMessagesTable();
                                                    if (!jObject1.isNull("outbox_team")) {
                                                        JSONArray jsonArrayOutbox1 = jObject1.getJSONArray("outbox_team");
                                                        for (int i = 0; i < jsonArrayOutbox1.length(); i++) {
                                                            JSONObject jasonObjectOutbox = jsonArrayOutbox1.getJSONObject(i);
                                                            db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                        }
                                                    }

                                                    if (!jObject1.isNull("outbox_course")) {
                                                        JSONArray jsonArrayOutbox2 = jObject1.getJSONArray("outbox_course");
                                                        for (int i = 0; i < jsonArrayOutbox2.length(); i++) {
                                                            JSONObject jasonObjectOutbox = jsonArrayOutbox2.getJSONObject(i);
                                                            db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("course_id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null),jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                        }
                                                    }
                                                    if (!jObject1.isNull("outbox_user")) {
                                                        JSONArray jsonArrayOutbox3 = jObject1.getJSONArray("outbox_user");
                                                        for (int i = 0; i < jsonArrayOutbox3.length(); i++) {
                                                            JSONObject jasonObjectOutbox = jsonArrayOutbox3.getJSONObject(i);
                                                            db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"),sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"),  jasonObjectOutbox.getString("title"),jasonObjectOutbox.getString("message"), jasonObjectOutbox.getString("photo"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                        }
                                                    }
                                                    db1.deleteTeamChatTable();
                                                    db1.deleteTeamChatTableNew();
                                                    if (!jObject.isNull("teamchat")) {
                                                        teamchat_id_arr.clear();
                                                        teamchat_teamid_arr.clear();
                                                        teamchat_username_arr.clear();
                                                        teamchat_imagepath_arr.clear();
                                                        teamchat_messages_arr.clear();
                                                        teamchat_createdat_arr.clear();
                                                        JSONArray jsonArrayTeamchat = jObject.getJSONArray("teamchat");
                                                        int teamchat_length = jsonArrayTeamchat.length();
                                                        for (int i = 0; i < teamchat_length; i++) {
                                                            JSONObject JasonobjectTeamchat = jsonArrayTeamchat.getJSONObject(i);
                                                            teamchat_chatid = JasonobjectTeamchat.getInt("id");
                                                            teamchat_teamid = JasonobjectTeamchat.getInt("team_id");
                                                            teamchat_username = JasonobjectTeamchat.getString("user_name");
                                                            teamchat_imagepath = JasonobjectTeamchat.getString("image_path");
                                                            teamchat_messages = JasonobjectTeamchat.getString("messages");
                                                            teamchat_createdat = JasonobjectTeamchat.getString("created_at");

                                                            teamchat_teamid_arr.add(i, teamchat_teamid);
                                                            teamchat_username_arr.add(i, teamchat_username);
                                                            teamchat_imagepath_arr.add(i, teamchat_imagepath);
                                                            teamchat_messages_arr.add(i, teamchat_messages);
                                                            teamchat_createdat_arr.add(i, teamchat_createdat);

                                                            //insert teamchat into sqlite

                                                            db1.addTeamChat(new Elpfa("all", "chat", teamchat_chatid, teamchat_teamid, teamchat_username, teamchat_imagepath, teamchat_messages, teamchat_createdat));

                                                        }


                                                    }
                                                    isAllAvailable = true;
                                                    onVolleyResponse(isAllAvailable);


                                                } catch (Exception e) {
                                                    Log.d("exception", "" + e);
                                                }
//

                                            } catch (Exception e) {
                                                Log.d("exception", "" + e);

                                            }


                                        } catch (Exception e) {
                                            Log.d("exception", "" + e);

                                        }

                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                    }
                                }) {

                                    @Override
                                    protected Map<String, String> getParams() {
                                        // Posting parameters to login url
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("value1", sharedpreferences.getString("User_id", null));
                                        return params;
                                    }

                                };
                                strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Adding request to request queue
                                AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);

//end msg inbx

//end msg inbx


                            } catch (Exception e) {
                                Log.d("exception", "" + e);
                            }
//

                        } catch (Exception e) {
                            Log.d("exception", "" + e);

                        }


                    } catch (Exception e) {
                        Log.d("exception", "" + e);

                    }
                    //animation to expand login button

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                params.put("value1", sharedpreferences.getString("Username", null));
                params.put("value2", sharedpreferences.getString("Password", null));
                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onPause() {
        super.onPause();

    }
}

