package wizroots.elpfa;

import android.content.Context;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyViewHolder> {

    private List<Attachment> attachementList;
    Context context;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView attachment_icon;
        public TextView attachment_name;
        public ImageView attachment_content;
        public ImageView attachment_delete;

        public MyViewHolder(View view) {
            super(view);
            attachment_content = (ImageView) view.findViewById(R.id.attachment_content);
            attachment_icon = (ImageView) view.findViewById(R.id.attachment_icon);
            attachment_name = (TextView) view.findViewById(R.id.attachment_name);
            attachment_delete = (ImageView) view.findViewById(R.id.attachment_delete);

        }
    }


    public AttachmentAdapter(List<Attachment> attachmentList) {
        this.attachementList = attachmentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attachment_list_row, parent, false);
        context=parent.getContext();


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Attachment attachment = attachementList.get(position);
        if (attachment.getAttachment_from().equals("CreateMessage"))
        {

            holder.attachment_content.setImageBitmap(attachment.getAttachment_bitmap());
        }
        else {
            Picasso.with(context).load(attachment.getAttachment_path()).into(holder.attachment_content);
        }
        holder.attachment_name.setText(attachment.getAttachment_name());


    }

    @Override
    public int getItemCount() {
        return attachementList.size();
    }
}