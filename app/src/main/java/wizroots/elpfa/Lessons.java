package wizroots.elpfa;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class Lessons {
    private String users_to_lesson_id;
    private String lesson_name;
    private int lesson_completed;
    private String lesson_type;
//    private int lesson_viewed;
    private int lesson_tag;
    private Orientation mOrientation;



    public Lessons() {
    }

    public Lessons(String users_to_lesson_id, String lesson_name, int lesson_completed, String lesson_type/*, int lesson_viewed,*/ ,int lesson_tag,Orientation orientation) {
        this.users_to_lesson_id=users_to_lesson_id;
        this.lesson_name=lesson_name;
        this.lesson_completed=lesson_completed;
        this.lesson_type=lesson_type;
//        this.lesson_viewed=lesson_viewed;
        this.lesson_tag=lesson_tag;
        mOrientation = orientation;
    }

    //users to lesson id
    public String getUsers_to_lesson_id() {
        return users_to_lesson_id;
    }

    public void setUsers_to_lesson_id(String users_to_lesson_id) {
        this.users_to_lesson_id = users_to_lesson_id;
    }

    //lesson name
    public String getLesson_name() {
        return lesson_name;
    }

    public void setLesson_name(String lesson_name) {
        this.lesson_name = lesson_name;
    }

    //lesson completed
    public int getLesson_completed() {
        return lesson_completed;
    }

    public void setLesson_completed(int lesson_completed) {
        this.lesson_completed = lesson_completed;
    }

    //lesson type
    public String getLesson_type() {
        return lesson_type;
    }

    public void setLesson_type(String lesson_type) {
        this.lesson_type = lesson_type;
    }
//    //lesson viewed
//    public int getLesson_viewed() {
//        return lesson_viewed;
//    }
//
//    public void setLesson_viewed(int lesson_viewed) {
//        this.lesson_viewed = lesson_viewed;
//    }

    //tesson tag
    public int getLesson_tag() {
        return lesson_tag;
    }

    public void setLesson_tag(int lesson_tag) {
        this.lesson_tag = lesson_tag;
    }




}
