package wizroots.elpfa;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.*;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.joda.time.LocalDate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Calendar.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Calendar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Calendar extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    String to_date1;
    int flag_expand=0;
    com.github.aakira.expandablelayout.ExpandableLinearLayout expandableLayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String enroll_str;
    String current_year;
    ListView lvList;
    String course_assign_date_interval;
    String durationMonth;
    String name, desc, startdate, enddate,  users_to_courseid, localname;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar; ArrayList<String> cs = new ArrayList<>();
    ArrayList<String> csid = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();
    ArrayList<String> paths = new ArrayList<>();
    ArrayList<String> start = new ArrayList<>();
    private String path;
    ArrayList<String> complete = new ArrayList<>();

    ArrayList<String> course_id_arr = new ArrayList<>();
    ArrayList<String> course_name_arr = new ArrayList<>();
    ArrayList<String> course_assign_date_arr = new ArrayList<>();
    ArrayList<String> course_path_arr = new ArrayList<>();

    ArrayList<String> testArr2 = new ArrayList<>();
    ArrayList<String> ids = new ArrayList<>();


    ArrayList<String> strt = new ArrayList<>();
    ArrayList<Integer> course_dates = new ArrayList<>();
    ArrayList<Integer> course_dates_monthchange = new ArrayList<>();
    ArrayList<Integer> course_dates_month = new ArrayList<>();
    ArrayAdapter<String> adapter = null;

    ArrayList<String> NameDay = new ArrayList<>();
    ArrayList<String> startDay = new ArrayList<>();
    ArrayList<String> descriptionDay = new ArrayList<>();
    ArrayList<String> csDay = new ArrayList<>();
    ArrayList<String> csidDay = new ArrayList<>();
    ArrayList<String> idsDay = new ArrayList<>();
    ArrayList<String> completeDay = new ArrayList<>();
    ArrayList<String> strtDay = new ArrayList<>();
    ArrayList<String> testArr2Day = new ArrayList<>();
    ArrayList<String> pathsDay = new ArrayList<>();


    String expandState;
    CompactCalendarView compactCalendarView;
    TextView date;
    TextView day;
    private OnFragmentInteractionListener mListener;
    View rootView;
    String current_day;
    String current_date;
    ArrayList<String> names= new ArrayList<>();
    private List<CourseCalendar> courseList = new ArrayList<>();
    private RecyclerView recyclerView;
    private java.util.Calendar currentCalender = java.util.Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForCurrentMonth = new SimpleDateFormat("MM", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatForYear = new SimpleDateFormat("yyyy", Locale.getDefault());
    private SimpleDateFormat dateFormatForDay = new SimpleDateFormat("dd", Locale.getDefault());
    private CalendarCourseAdapter courseAdapter;
    String course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress;
    int enroll,started,completed;
    int selectedDay;
    public Calendar() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Calendar.
     */
    // TODO: Rename and change types and number of parameters
    public static Calendar newInstance(String param1, String param2) {
        Calendar fragment = new Calendar();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_calendar, container, false);
        toolbar=(Toolbar)rootView.findViewById(R.id.toolbar);
        date=(TextView)toolbar.findViewById(R.id.date);
        day=(TextView)toolbar.findViewById(R.id.day);
        expandState="false";
        expandableLayout=(com.github.aakira.expandablelayout.ExpandableLinearLayout)rootView.findViewById(R.id.expandableLayout);
        expandableLayout.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_OUT_SLOW_IN_INTERPOLATOR));
        compactCalendarView=(CompactCalendarView)rootView.findViewById(R.id.compactcalendar_view);


        date.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        courseAdapter = new CalendarCourseAdapter(courseList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(courseAdapter);
        courseList.clear();
        current_date=dateFormatForCurrentMonth.format(compactCalendarView.getFirstDayOfCurrentMonth());
        current_day=dateFormatForDay.format(compactCalendarView.getFirstDayOfCurrentMonth());
        current_year=dateFormatForYear.format(compactCalendarView.getFirstDayOfCurrentMonth());

        Date now = new Date();
        String nowAsString = new SimpleDateFormat("dd").format(now);
        day.setText(""+nowAsString);

        getMonthCourses(Integer.parseInt(dateFormatForCurrentMonth.format(compactCalendarView.getFirstDayOfCurrentMonth())),Integer.parseInt(dateFormatForYear.format(compactCalendarView.getFirstDayOfCurrentMonth())));
        addEvents(compactCalendarView,course_dates_monthchange,Integer.parseInt(current_date),Integer.parseInt(current_year));
        compactCalendarView.invalidate();
        compactCalendarView.setShouldShowMondayAsFirstDay(false);
//        compactCalendarView.setCurrentSelectedDayIndicatorStyle(R.style.CalendarStyle);
        //set title on calendar scroll
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                String month="";

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat output = new SimpleDateFormat("MM");
//                Date d = null;
                try {
//                    d = sdf.parse(String.valueOf(dateClicked));
                    month = output.format(dateClicked);


                } catch (Exception e) {
                    e.printStackTrace();
                }



                getDayCourses(Integer.parseInt(dateFormatForDay.format(dateClicked)), Integer.parseInt(month),Integer.parseInt(dateFormatForYear.format(dateClicked)));

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                course_dates_monthchange.clear();
                date.setText(dateFormatForMonth.format(firstDayOfNewMonth));
//                day.setText(dateFormatForDay.format(firstDayOfNewMonth));
                current_date=dateFormatForCurrentMonth.format(firstDayOfNewMonth);
                current_day=dateFormatForDay.format(firstDayOfNewMonth);
                current_year=dateFormatForYear.format(firstDayOfNewMonth);

                getMonthCourses(Integer.parseInt(dateFormatForCurrentMonth.format(firstDayOfNewMonth)),Integer.parseInt(dateFormatForYear.format(firstDayOfNewMonth)));

                addEvents(compactCalendarView,course_dates_monthchange,Integer.parseInt(current_date),Integer.parseInt(current_year));
                compactCalendarView.invalidate();
                courseAdapter.setSelectedDay(String.valueOf(current_day));
            }
        });
        expandState="true";
//        expandableLayout.toggle();
        expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(toolbar.findViewById(R.id.arrow), 0f, 180f).start();
                expandState="true";
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(toolbar.findViewById(R.id.arrow), 180f, 0f).start();
                expandState="false";
            }
        });
        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);
        toolbar.findViewById(R.id.linear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableLayout.toggle();
                if (flag_expand==0)
                {
                    flag_expand=1;
                }
                else
                {
                    flag_expand=0;
                }

//                compactCalendarView.hideCalendarWithAnimation();
            }
        });
        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

//        courseAdapter = new CalendarCourseAdapter(courseList);
////        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(courseAdapter);
//        courseList.clear();

//        prepareCourseData();
//        addEvents(compactCalendarView, -1);
//        addEvents(compactCalendarView, java.util.Calendar.DECEMBER);
//        addEvents(compactCalendarView, java.util.Calendar.AUGUST);
//        Log.d("course_check","course id arr : "+course_id_arr);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
              /*  Intent i=new Intent(getActivity(),CourseContents.class);
//                        Log.d("course_check","course_id : "+course_id_arr.get(position));

                i.putExtra("course_id",course_id_arr.get(position));
                startActivity(i);*/
            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));

//        recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.d("recycler","hi");
////              if (flag_expand==1)
////              {
////                  expandableLayout.toggle();
////                  flag_expand=0;
////              }
////                else if (flag_expand==0)
////              {
////                  expandableLayout.toggle();
////                  flag_expand=1;
////              }
//
//            }
//        });
        rootView.findViewById(R.id.nested).setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (flag_expand==1)
              {
//
                  expandableLayout.toggle();
                  flag_expand=0;
              }
                else if (flag_expand==0)
              {
                  expandableLayout.toggle();
                  flag_expand=1;
              }
            }
        });
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }
        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }
        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }
    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }
    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.ACCELERATE_INTERPOLATOR));
        return animator;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cal, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//        if (id == R.id) {
//
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
    //    private void prepareCourseData()
//    {
//        try
//        {
//            DatabaseHandler db1 = new DatabaseHandler(getActivity());
//            course_id_arr.clear();
//            int count=0;
//            //retrieve all completed courses
//            List<Elpfa> elpfa_all = db1.getScheduledCourses();
//            for (Elpfa elp : elpfa_all) {
//                course_id=elp.getScheduledCourseId();
//                course_id_arr.add(count,course_id);
//                count++;
//                course_name = elp.getScheduledCourseName();
//                enroll=elp.getScheduledEnrollmentKey();
//                if (enroll==0)
//                {
//                    course_type="Course Type : Assigned";
//                }
//                else
//                {
//                    course_type="Course Type : Self Enroll";
//                }
//                started=elp.getScheduledCourseStarted();
//                completed=elp.getScheduledCourseCompleted();
//                if (started==0)
//                {
//                    course_state="Course State : Not Started";
//                }
//                else if (completed==1)
//                {
//                    course_state="Course State : Completed";
//                }
//                else if (started==1)
//                {
//                    course_state="Course State : In Progress";
//                }
//
//                course_assign_date = elp.getScheduledAssignDate();
//                course_progress=elp.getScheduledTrackCourse();
//                course_path=elp.getScheduledCoursePath();
//                Course course = new Course(course_id,course_name,course_type,course_state,course_assign_date,course_path,course_progress);
//                courseList.add(course);
//
//                courseAdapter.notifyDataSetChanged();
//            }
//
//        }
//        catch (Exception e)
//        {
//
//        }
//
//    }
//    private void addEvents(CompactCalendarView compactCalendarView, int month) {
//        currentCalender.setTime(new Date());
//        currentCalender.set(java.util.Calendar.DAY_OF_MONTH, 1);
//        Date firstDayOfMonth = currentCalender.getTime();
//        for (int i = 0; i < 6; i++) {
//            currentCalender.setTime(firstDayOfMonth);
//            if (month > -1) {
//                currentCalender.set(java.util.Calendar.MONTH, month);
//            }
//            currentCalender.add(java.util.Calendar.DATE, i);
//            setToMidnight(currentCalender);
//            long timeInMillis = currentCalender.getTimeInMillis();
//
//            List<Event> events = getEvents(timeInMillis, i);
//
//            compactCalendarView.addEvents(events);
//        }
//    }
    private void addEvents(String dates, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(java.util.Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < 6; i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(java.util.Calendar.MONTH, month);
            }
            currentCalender.add(java.util.Calendar.DATE, i);
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, i);

            compactCalendarView.addEvents(events);
        }
    }
    private void setToMidnight(java.util.Calendar calendar) {
        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);
        calendar.set(java.util.Calendar.MINUTE, 0);
        calendar.set(java.util.Calendar.SECOND, 0);
        calendar.set(java.util.Calendar.MILLISECOND, 0);
    }
    private List<Event> getEvents(long timeInMillis, int day) {
//        if (day < 2) {
        return Arrays.asList(new Event(Color.rgb(61, 179, 158), timeInMillis, "Event at " + new Date(timeInMillis)));
//        }
//        else if ( day > 2 && day <= 4) {
//            return Arrays.asList(
//                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
//                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
//        } else {
//            return Arrays.asList(
//                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis) ),
//                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
//                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
//        }
    }
    public void getMonthCourses(final int selected_month,int selected_year)
    {
        ArrayList<CourseCalendar> results = new ArrayList<CourseCalendar>();
        results.clear();
        courseList.clear();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//
        try {
            course_name_arr.clear();
            course_id_arr.clear();
            course_assign_date_arr.clear();
            course_path_arr.clear();
            adapter=null;
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            List<Elpfa> elpfa_all = db1.getAllCourses();
            int j=0;
            int k=0;
            for (Elpfa elp : elpfa_all) {

                enroll_str=String.valueOf(elp.getAllEnrollmentKey());

                //check if self enrolled course
                if ("0".equals(enroll_str))
                {


                    startdate = elp.getAllAssignDate();
                    Date startDate= (Date)formatter.parse(startdate); // your date
                    java.util.Calendar cal = java.util.Calendar.getInstance();
                    cal.setTime(startDate);
                    int month_start = cal.get(java.util.Calendar.MONTH)+1;
                    int year_start=cal.get(java.util.Calendar.YEAR);

                    DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Calendar c = java.util.Calendar.getInstance();
                    c.setTime(dateFormat.parse(elp.getAllAssignDate()));
                    c.add(java.util.Calendar.DAY_OF_YEAR, Integer.parseInt(elp.getAllDuration()));
                    String start_date= elp.getAllAssignDate();
                    String to_date = dateFormat.format(c.getTime());
                    to_date1=to_date;


                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    Date date = dateFormat.parse(to_date);
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add( java.util.Calendar.DATE, -1);
                    to_date = dateFormat.format(calendar.getTime());

                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

                    String inputDateStr1=start_date;
                    Date date1 = inputFormat.parse(inputDateStr1);
                    String outputDateStr1 = outputFormat.format(date1);

                    String inputDateStr2=to_date;
                    Date date2 = inputFormat.parse(inputDateStr2);
                    String outputDateStr2 = outputFormat.format(date2);
                    if(outputDateStr1.equals(outputDateStr2))
                    {
                        course_assign_date_interval=outputDateStr1;
                    }
                    else
                    {
                        course_assign_date_interval=outputDateStr1+" - "+outputDateStr2;
                    }

                    Date toDate= (Date)formatter.parse(to_date); // your date
                    java.util.Calendar calTo = java.util.Calendar.getInstance();
                    calTo.setTime(toDate);
                    int month_to = calTo.get(java.util.Calendar.MONTH)+1;
                    int year_to=calTo.get(java.util.Calendar.YEAR);
                    if (((month_start<=selected_month)&&(selected_month<=month_to))&&((year_start<=selected_year)&&(selected_year<=year_to)))

                    {
/*********************************************/
                        //get all dates within duration

                        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        //get date after duration
                        java.util.Calendar calndr = java.util.Calendar.getInstance();

                        calndr.setTime(dateFormat.parse(start_date));
                        while (calndr.getTime().before(format.parse(to_date1))) {
                            int day1=calndr.get(java.util.Calendar.DAY_OF_MONTH);
                            int month1=calndr.get(java.util.Calendar.MONTH)+1;
                            if (((month1)==selected_month)&&(!course_dates_monthchange.contains(day1))){

                                course_dates_monthchange.add(k,day1);
                                k++;

                            }
                            calndr.add(java.util.Calendar.DATE, 1);


                        }
/********************************************/


                        //get details from local db
                        course_name=elp.getAllCourseName();
//                        desc=elp.getAllDescription();
                        course_assign_date = elp.getAllAssignDate();
//                        durationMonth=elp.getAllDuration();
                        course_id = elp.getAllCourseId();
                        course_id_arr.add(j,course_id);
                        enddate = elp.getAllCourseCompletionDate();
//                        track_content = Integer.parseInt(elp.getAllTrackCourse());
//                        users_to_courseid=elp.getAllUsersToCourseId();
//                        test=elp.getAllTestMandatory();
//                        user_id = sharedpreferences.getString("User_id", null);
//                        started = String.valueOf(elp.getAllCourseStarted());
//                        completed = String.valueOf(elp.getAllCourseCompleted());

                        course_path=elp.getAllCoursePath();


                        users_to_courseid = elp.getAllUsersToCourseId();
                        CourseCalendar course_cal=new CourseCalendar(course_id,course_name,course_assign_date_interval,course_path);
                        courseList.add(course_cal);
                        courseAdapter.setSelectedDay("");
                        courseAdapter.notifyDataSetChanged();

//                        lvList.setAdapter(new MyCustomCourseList1(getActivity(), results));
//                        setListViewHeightBasedOnChildren(lvList);
//                        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//
//
//
//                                pd.setMessage("Loading...");
//                                pd.show();
//                                new_pos=Integer.parseInt(String.valueOf(v.findViewById(R.id.rel1).getTag()));
//                                Intent intent = new Intent(getActivity(), CourseListEvents.class);
//                                intent.putExtra("user_id", user_id);
//                                intent.putExtra("course_desc", description.get(new_pos));
//                                intent.putExtra("course_name", Name.get(new_pos));
//                                intent.putExtra("test", testArr2.get(new_pos));
//                                intent.putExtra("course_id", ids.get(new_pos));
//                                intent.putExtra("localname", localname);
//
//                                intent.putExtra("fromCalendar", 1);
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("assigndate", strt.get(new_pos));
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("startCourse", Integer.parseInt(start.get(new_pos)));
//                                intent.putExtra("month_flagss", selected_month);
//                                startActivity(intent);
//
//
//                            }
//
//                        });

                        j++;

                    }


                }
                if (courseList.size()==0)
                {
                    recyclerView.setVisibility(View.INVISIBLE);
                    rootView.findViewById(R.id.no_courses).setVisibility(View.VISIBLE);
                }
                else
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.no_courses).setVisibility(View.INVISIBLE);
                }
            }
//
//
//
//
//
//            initializeEvents(course_dates_monthchange);
//
//
        } catch (ParseException e1) {
        }
    }
    public void getDayCourses(int selected_day,int selected_month,int selected_year)
    {
        String date_selected=null;
        ArrayList<CourseCalendar> results = new ArrayList<CourseCalendar>();
        results.clear();
        courseList.clear();
//        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String date_str=selected_year+"-"+selected_month+"-"+selected_day;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date_dates = (Date) formatter.parse(date_str);
            SimpleDateFormat parseFormat =
                    new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            Date date = parseFormat.parse(String.valueOf(date_dates));
            String date_date = formatter.format(date);

            date_selected = date_date;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        try {
            course_name_arr.clear();
            course_id_arr.clear();
            course_assign_date_arr.clear();
            course_path_arr.clear();
            adapter = null;
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            List<Elpfa> elpfa_all = db1.getAllCourses();
            int j = 0;
            int k = 0;
            for (Elpfa elp : elpfa_all) {

                enroll_str = String.valueOf(elp.getAllEnrollmentKey());

                //check if self enrolled course
                if ("0".equals(enroll_str)) {
                    startdate = elp.getAllAssignDate();
                    Date startDate= (Date)formatter.parse(startdate); // your date
                    java.util.Calendar cal = java.util.Calendar.getInstance();
                    cal.setTime(startDate);
                    int month_start = cal.get(java.util.Calendar.MONTH)+1;
                    int year_start=cal.get(java.util.Calendar.YEAR);

                    DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Calendar c = java.util.Calendar.getInstance();
                    c.setTime(dateFormat.parse(elp.getAllAssignDate()));
                    c.add(java.util.Calendar.DAY_OF_YEAR, Integer.parseInt(elp.getAllDuration()));
                    String start_date= elp.getAllAssignDate();
                    String to_date = dateFormat.format(c.getTime());
                    to_date1=to_date;


                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    Date date = dateFormat.parse(to_date);
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add( java.util.Calendar.DATE, -1);
                    to_date = dateFormat.format(calendar.getTime());

                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

                    String inputDateStr1=start_date;
                    Date date1 = inputFormat.parse(inputDateStr1);
                    String outputDateStr1 = outputFormat.format(date1);

                    String inputDateStr2=to_date;
                    Date date2 = inputFormat.parse(inputDateStr2);
                    String outputDateStr2 = outputFormat.format(date2);
                    if(outputDateStr1.equals(outputDateStr2))
                    {
                        course_assign_date_interval=outputDateStr1;
                    }
                    else
                    {
                        course_assign_date_interval=outputDateStr1+" - "+outputDateStr2;
                    }

                    Date toDate= (Date)formatter.parse(to_date); // your date
                    java.util.Calendar calTo = java.util.Calendar.getInstance();
                    calTo.setTime(toDate);
                    int month_to = calTo.get(java.util.Calendar.MONTH)+1;
                    int year_to=calTo.get(java.util.Calendar.YEAR);
                    if (((month_start<=selected_month)&&(selected_month<=month_to))&&((year_start<=selected_year)&&(selected_year<=year_to)))

                    {
/*********************************************/
                        //get all dates within duration

                        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        //get date after duration
                        java.util.Calendar calndr = java.util.Calendar.getInstance();

                        calndr.setTime(dateFormat.parse(start_date));
                        while (calndr.getTime().before(format.parse(to_date1))) {
                            int day1=calndr.get(java.util.Calendar.DAY_OF_MONTH);
                            int month1=calndr.get(java.util.Calendar.MONTH)+1;
                            if (day1==selected_day)
                            {
                                //get details from local db
                                course_name=elp.getAllCourseName();
//                        desc=elp.getAllDescription();
                                course_assign_date = elp.getAllAssignDate();
//                        durationMonth=elp.getAllDuration();
                                course_id = elp.getAllCourseId();
                                /****anu****/
                            course_id_arr.add(k,course_id);
                                k++;
                                enddate = elp.getAllCourseCompletionDate();
//                        track_content = Integer.parseInt(elp.getAllTrackCourse());
//                        users_to_courseid=elp.getAllUsersToCourseId();
//                        test=elp.getAllTestMandatory();
//                        user_id = sharedpreferences.getString("User_id", null);
//                        started = String.valueOf(elp.getAllCourseStarted());
//                        completed = String.valueOf(elp.getAllCourseCompleted());

                                course_path=elp.getAllCoursePath();


                                users_to_courseid = elp.getAllUsersToCourseId();
                                CourseCalendar course_cal=new CourseCalendar(course_id,course_name,course_assign_date_interval,course_path);
                                courseList.add(course_cal);


                            }
                            calndr.add(java.util.Calendar.DATE, 1);
                            courseAdapter.setSelectedDay(String.valueOf(selected_day));
                            courseAdapter.notifyDataSetChanged();

                        }
/********************************************/



//                        lvList.setAdapter(new MyCustomCourseList1(getActivity(), results));
//                        setListViewHeightBasedOnChildren(lvList);
//                        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//
//
//
//                                pd.setMessage("Loading...");
//                                pd.show();
//                                new_pos=Integer.parseInt(String.valueOf(v.findViewById(R.id.rel1).getTag()));
//                                Intent intent = new Intent(getActivity(), CourseListEvents.class);
//                                intent.putExtra("user_id", user_id);
//                                intent.putExtra("course_desc", description.get(new_pos));
//                                intent.putExtra("course_name", Name.get(new_pos));
//                                intent.putExtra("test", testArr2.get(new_pos));
//                                intent.putExtra("course_id", ids.get(new_pos));
//                                intent.putExtra("localname", localname);
//
//                                intent.putExtra("fromCalendar", 1);
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("assigndate", strt.get(new_pos));
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("startCourse", Integer.parseInt(start.get(new_pos)));
//                                intent.putExtra("month_flagss", selected_month);
//                                startActivity(intent);
//
//
//                            }
//
//                        });

                        j++;

                    }



                }
                if (courseList.size()==0)
                {
                    recyclerView.setVisibility(View.INVISIBLE);
                    rootView.findViewById(R.id.no_courses).setVisibility(View.VISIBLE);
                }
                else
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    rootView.findViewById(R.id.no_courses).setVisibility(View.INVISIBLE);
                }
            }
//
//
//
//
//
//            initializeEvents(course_dates_monthchange);
//
//
        }catch (Exception e)
        {
            e.printStackTrace();
        }

//
   /*     try {
            course_name_arr.clear();
            course_id_arr.clear();
            course_assign_date_arr.clear();
            course_path_arr.clear();
            adapter=null;
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            List<Elpfa> elpfa_all = db1.getAllCourses();
            int j=0;
            int k=0;
            for (Elpfa elp : elpfa_all) {

                enroll_str=String.valueOf(elp.getAllEnrollmentKey());

                //check if self enrolled course
                if ("0".equals(enroll_str))
                {


                    startdate = elp.getAllAssignDate();
                    Date startDate= (Date)formatter.parse(startdate); // your date
                    java.util.Calendar cal = java.util.Calendar.getInstance();
                    cal.setTime(startDate);
                    int month_start = cal.get(java.util.Calendar.MONTH)+1;
                    int year_start=cal.get(java.util.Calendar.YEAR);

                    DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Calendar c = java.util.Calendar.getInstance();
                    c.setTime(dateFormat.parse(elp.getAllAssignDate()));
                    c.add(java.util.Calendar.DAY_OF_YEAR, Integer.parseInt(elp.getAllDuration()));
                    String start_date= elp.getAllAssignDate();
                    String to_date = dateFormat.format(c.getTime());
                    to_date1=to_date;


                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    Date date = dateFormat.parse(to_date);
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add( java.util.Calendar.DATE, -1);
                    to_date = dateFormat.format(calendar.getTime());



                    Log.d("calendar_check","course name : "+elp.getAllCourseName());
                    Log.d("calendar_check","start_date : "+start_date);
                    Log.d("calendar_check","to_date : "+to_date);

                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

                    String inputDateStr1=start_date;
                    Date date1 = inputFormat.parse(inputDateStr1);
                    String outputDateStr1 = outputFormat.format(date1);

                    String inputDateStr2=to_date;
                    Date date2 = inputFormat.parse(inputDateStr2);
                    String outputDateStr2 = outputFormat.format(date2);
                    if(outputDateStr1.equals(outputDateStr2))
                    {
                        course_assign_date_interval=outputDateStr1;
                    }
                    else
                    {
                        course_assign_date_interval=outputDateStr1+" - "+outputDateStr2;
                    }

                    Date toDate= (Date)formatter.parse(to_date); // your date
                    java.util.Calendar calTo = java.util.Calendar.getInstance();
                    calTo.setTime(toDate);
                    int month_to = calTo.get(java.util.Calendar.MONTH)+1;
                    int year_to=calTo.get(java.util.Calendar.YEAR);

                    if (((month_start<=selected_month)&&(selected_month<=month_to))&&((year_start<=selected_year)&&(selected_year<=year_to)))

                    {

                        String between_datestr;
                        Log.d("calendar_check","in if , course_name : "+elp.getAllCourseName());
*//*********************************************//*
                        //get all dates within duration

                        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        //get date after duration
                        java.util.Calendar calndr = java.util.Calendar.getInstance();

                        calndr.setTime(dateFormat.parse(start_date));
                        while (calndr.getTime().before(format.parse(to_date1))) {
                            between_datestr = calndr.get(java.util.Calendar.YEAR) + "-" + (calndr.get(java.util.Calendar.MONTH) + 1) + "-" + calndr.get(java.util.Calendar.DAY_OF_MONTH);
                            Date date_dates = (Date) formatter.parse(between_datestr); // your date

                            SimpleDateFormat parseFormat =
                                    new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                            Date date11 = parseFormat.parse(String.valueOf(date_dates));
                            String btwn_date = formatter.format(date11);

                            if ((date_selected!=null)&&(date_selected.equals(btwn_date)))
                            {

                                //get details from local db
                                course_name=elp.getAllCourseName();
//                        desc=elp.getAllDescription();
                                course_assign_date = elp.getAllAssignDate();
//                        durationMonth=elp.getAllDuration();
                                course_id = elp.getAllCourseId();
                                enddate = elp.getAllCourseCompletionDate();
//                        track_content = Integer.parseInt(elp.getAllTrackCourse());
//                        users_to_courseid=elp.getAllUsersToCourseId();
//                        test=elp.getAllTestMandatory();
//                        user_id = sharedpreferences.getString("User_id", null);
//                        started = String.valueOf(elp.getAllCourseStarted());
//                        completed = String.valueOf(elp.getAllCourseCompleted());

                                course_path="http://reflectivelearn.net/mlearning/elpfa/"+elp.getAllCoursePath();


                                users_to_courseid = elp.getAllUsersToCourseId();
                                CourseCalendar course_cal=new CourseCalendar(course_id,course_name,course_assign_date_interval,course_path);
                                courseList.add(course_cal);

                                courseAdapter.notifyDataSetChanged();

                            }
                           *//* Log.d("calendar_check","in while , course_name : "+elp.getAllCourseName());
                            int day1=calndr.get(java.util.Calendar.DAY_OF_MONTH);
                            int month1=calndr.get(java.util.Calendar.MONTH)+1;
                            Log.d("calendar_check","day1 : "+day1);
                            Log.d("calendar_check","month1 : "+month1);
                            if (((month1)==selected_month)&&(!course_dates_monthchange.contains(day1))){
                                Log.d("calendar_check1","in  iffff , course name : "+elp.getAllCourseName());

                                course_dates_monthchange.add(k,day1);
                                k++;

                            }
                            Log.d("calendar_check","course_dates_monthchange : "+course_dates_monthchange);
                            calndr.add(java.util.Calendar.DATE, 1);*//*




                        }
*//********************************************//*



//                        lvList.setAdapter(new MyCustomCourseList1(getActivity(), results));
//                        setListViewHeightBasedOnChildren(lvList);
//                        lvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//
//
//
//                                pd.setMessage("Loading...");
//                                pd.show();
//                                new_pos=Integer.parseInt(String.valueOf(v.findViewById(R.id.rel1).getTag()));
//                                Intent intent = new Intent(getActivity(), CourseListEvents.class);
//                                intent.putExtra("user_id", user_id);
//                                intent.putExtra("course_desc", description.get(new_pos));
//                                intent.putExtra("course_name", Name.get(new_pos));
//                                intent.putExtra("test", testArr2.get(new_pos));
//                                intent.putExtra("course_id", ids.get(new_pos));
//                                intent.putExtra("localname", localname);
//
//                                intent.putExtra("fromCalendar", 1);
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("assigndate", strt.get(new_pos));
//                                intent.putExtra("users_to_courseid", csid.get(new_pos));
//                                intent.putExtra("startCourse", Integer.parseInt(start.get(new_pos)));
//                                intent.putExtra("month_flagss", selected_month);
//                                startActivity(intent);
//
//
//                            }
//
//                        });

                        j++;

                    }


                }
            }
//
//
//
//
//
//            initializeEvents(course_dates_monthchange);
//
//
        } catch (ParseException e1) {
            Log.d("exception : ",""+e1);
        }*/
    }
    private void addEvents(CompactCalendarView compactCalendarView,ArrayList<Integer> dates, int month,int year) {
        compactCalendarView.removeAllEvents();

        LocalDate localDate = new LocalDate(year, month, 1);
        Date date = localDate.toDate();

        currentCalender.setTime(date);
        currentCalender.set(java.util.Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();

        if ((month==Integer.parseInt(current_date))&&(year==Integer.parseInt(current_year)))
        {
            for (int i=0;i<course_dates_monthchange.size();i++)
            {
                currentCalender.setTime(firstDayOfMonth);
//            if (month > -1) {
//                currentCalender.set(java.util.Calendar.MONTH, month);
//            }
                currentCalender.add(java.util.Calendar.DATE, course_dates_monthchange.get(i)-1);
                setToMidnight(currentCalender);
                long timeInMillis = currentCalender.getTimeInMillis();

                List<Event> events = getEvents(timeInMillis, course_dates_monthchange.get(i));

                compactCalendarView.addEvents(events);

            }


        }




    }
}
