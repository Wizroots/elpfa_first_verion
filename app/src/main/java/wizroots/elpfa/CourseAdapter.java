package wizroots.elpfa;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import wizroots.elpfa.progressbar.NumberProgressBar;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyViewHolder> {

    private List<Course> courseList;
    private List<Course> courseList1;
    Context context;

    private ArrayList<Course> arraylist;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView course_name,course_type,course_state,course_assign_date;
        public DonutProgressCourseList course_progress;
        public RelativeLayout state_rel,overlay;
        RelativeLayout state1;
        public ImageView course_image;
        private RelativeLayout card_main;
        TextView state1_txt;
        ImageView left,right;
        wizroots.elpfa.progressbar.NumberProgressBar number_progressbar;


        public MyViewHolder(View view) {
            super(view);
            course_name = (TextView) view.findViewById(R.id.course_name_txt);
            course_type = (TextView) view.findViewById(R.id.course_type_txt);
            course_state = (TextView) view.findViewById(R.id.course_state_txt);
            course_assign_date = (TextView) view.findViewById(R.id.course_assign_date_txt);
            course_progress = (DonutProgressCourseList) view.findViewById(R.id.course_progress);
            course_image = (ImageView) view.findViewById(R.id.course_image);
            left = (ImageView) view.findViewById(R.id.left);
            right = (ImageView) view.findViewById(R.id.right);
            state1_txt=(TextView)view.findViewById(R.id.state1_txt);
            state_rel=(RelativeLayout)view.findViewById(R.id.state_rel);
            overlay=(RelativeLayout)view.findViewById(R.id.overlay);
            state1=(RelativeLayout)view.findViewById(R.id.state1);
            card_main=(RelativeLayout)view.findViewById(R.id.main_card);
            number_progressbar=(wizroots.elpfa.progressbar.NumberProgressBar)view.findViewById(R.id.numberbar1);
        }
    }


    public CourseAdapter(List<Course> courseList) {
        this.courseList = courseList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(courseList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.course_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Course course = courseList.get(position);
        holder.course_name.setText(course.getCourseName());
        holder.course_type.setText(course.getCourseType());
        holder.course_progress.setProgress(Integer.parseInt(course.getCourseProgress()));
        holder.course_assign_date.setText("Start Date: "+course.getCourseAssignDate());
        if (course.getCourseType().equals("Course Type : Self Enroll"))
        {
            holder.course_assign_date.setVisibility(View.INVISIBLE);
        }
        if (course.getCourseState().equals("Course State : Completed"))
        {
            holder.number_progressbar.setProgress(100);
            holder.number_progressbar.setProgressTextVisibility(NumberProgressBar.ProgressTextVisibility.Invisible);
            holder.number_progressbar.setSuffix(null);
            holder.left.setImageResource(R.drawable.circle_bk_blue);
            holder.right.setImageResource(R.drawable.circle_bk_blue);
            holder.state1.setVisibility(View.VISIBLE);
            holder.state1_txt.setText("Completed");

        }
        else if (course.getCourseState().equals("Course State : Not Started"))
        {
            if (course.getCourseType().equals("Course Type : Assigned"))
            {
                holder.number_progressbar.setProgress(0);
                holder.number_progressbar.setProgressTextVisibility(NumberProgressBar.ProgressTextVisibility.Invisible);
                holder.number_progressbar.setSuffix(null);
                holder.left.setImageResource(R.drawable.circle_holo_blue);
                holder.right.setImageResource(R.drawable.circle_holo_blue);
                holder.state1.setVisibility(View.VISIBLE);
                holder.state1_txt.setText("Start Now");
            }
            else if (course.getCourseType().equals("Course Type : Self Enroll"))
            {
                Log.d("check_completed_courses","in else if self enroll");
                holder.number_progressbar.setProgress(0);
                holder.number_progressbar.setProgressTextVisibility(NumberProgressBar.ProgressTextVisibility.Invisible);
                holder.number_progressbar.setSuffix(null);
                holder.left.setImageResource(R.drawable.circle_holo_blue);
                holder.right.setImageResource(R.drawable.circle_holo_blue);
                holder.state1.setVisibility(View.VISIBLE);
                holder.state1_txt.setText("Enroll Now");
            }
        }
        else if (course.getCourseState().equals("Course State : In Progress"))
        {
            if (course.getCourseProgress().equals("0"))
            {
                holder.number_progressbar.setSuffix(null);
                holder.number_progressbar.setProgress(Integer.parseInt(course.getCourseProgress()));
                holder.number_progressbar.setProgressTextVisibility(NumberProgressBar.ProgressTextVisibility.Invisible);
                holder.left.setImageResource(R.drawable.circle_bk_blue);
                holder.right.setImageResource(R.drawable.circle_holo_blue);
                holder.state1.setVisibility(View.VISIBLE);
                holder.state1_txt.setText("Started");
            }
            else if (course.getCourseProgress().equals("100"))
            {
                holder.number_progressbar.setProgress(100);
                holder.number_progressbar.setSuffix(null);
                holder.number_progressbar.setProgressTextVisibility(NumberProgressBar.ProgressTextVisibility.Invisible);
                holder.number_progressbar.setProgressTextSize(0);
                holder.left.setImageResource(R.drawable.circle_bk_blue);
                holder.right.setImageResource(R.drawable.circle_bk_blue);
                holder.state1.setVisibility(View.VISIBLE);
                holder.state1_txt.setText("Completed");
            }
            else
            {
                holder.number_progressbar.setProgress(Integer.parseInt(course.getCourseProgress()));
                holder.left.setImageResource(R.drawable.circle_bk_blue);
                holder.right.setImageResource(R.drawable.circle_holo_blue);
                holder.state_rel.setVisibility(View.INVISIBLE);
            }
        }




        if (course.getCourseStatus()==0)
        {
            holder.card_main.setBackgroundColor(Color.rgb(238,238,238));
            holder.state1.setBackgroundColor(Color.rgb(238,238,238));
            Picasso.with(holder.course_image.getContext()).load(course.getCoursePath()).placeholder(R.drawable.course_inactive_img).into(holder.course_image);
            holder.course_name.setTextColor(Color.rgb(153,153,153));
            holder.course_type.setTextColor(Color.rgb(153,153,153));
            holder.course_assign_date.setTextColor(Color.rgb(153,153,153));
            holder.state_rel.setBackgroundColor(Color.rgb(238,238,238));
            holder.state1_txt.setBackgroundColor(Color.rgb(238,238,238));
            holder.state1_txt.setTextColor(Color.rgb(153,153,153));
        }
        else
        {
            Picasso.with(holder.course_image.getContext()).load(course.getCoursePath()).into(holder.course_image);
        }

        holder.card_main.setTag(course.getCourseTag());

//        if (course.getCourseProgress().equals("0"))
//        {
//            holder.state_rel.setVisibility(View.VISIBLE);
//            holder.number_progressbar.setProgress(150);
//            holder.number_progressbar.setProgressTextSize(0);
//        }
//        else if (course.getCourseProgress().equals("100"))
//        {
//            holder.state_rel.setVisibility(View.VISIBLE);
//            holder.number_progressbar.setProgress(100);
//            holder.number_progressbar.setProgressTextSize(0);
//            holder.right.setImageResource(R.drawable.circle_bk_blue);
//        }
//        else
//        {
//            holder.state_rel.setVisibility(View.INVISIBLE);
//            holder.number_progressbar.setProgress(Integer.parseInt(course.getCourseProgress()));
//        }


    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        courseList.clear();

        if (charText.length() == 0) {
            courseList.addAll(arraylist);
        } else {
            for (Course wp : arraylist) {
                if (wp.getCourseName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    courseList.add(wp);
                }
            }
        }

        notifyDataSetChanged();
    }
    public void setFilter(List<Course> courses) {
        courseList1 = new ArrayList<>();
        courseList1.addAll(courses);
        notifyDataSetChanged();
    }
}