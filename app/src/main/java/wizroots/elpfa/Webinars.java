package wizroots.elpfa;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Webinars#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Webinars extends Fragment implements HandleVolleyResponse{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.layout_progressbar)
    RelativeLayout layoutProgressbar;
    Unbinder unbinder;

    private TabLayout mTabLayout;
    View rootview;
    Toolbar toolbar;
    private FragmentTabHost mTabHost;
    ActionBarDrawerToggle toggle;
    CourseAdapter courseAdapter;
    private ViewPager mViewPager;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private SharedPreferences.Editor editor;

    private JSONObject jObject;
    String enroll_contents_course_id;

    String recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url;
    String upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url;
    private JSONObject jObject1;

    private boolean isAllAvilable = false;

    public Webinars() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Webinars.
     */
    // TODO: Rename and change types and number of parameters
    public static Webinars newInstance(String param1, String param2) {
        Webinars fragment = new Webinars();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_webinars, container, false);

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        mTabLayout = (TabLayout) rootview.findViewById(R.id.tabs);
        mViewPager = (ViewPager) rootview.findViewById(R.id.viewpager);
        toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
        toolbar.setTitle("Webinars");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);

        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        mTabLayout.setupWithViewPager(mViewPager);

        unbinder = ButterKnife.bind(this, rootview);
        if (Utils.isNetworkAvailable(getActivity()))
            getWebinarsFromServer();
        else {
            layoutProgressbar.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            setupViewPager(mViewPager);
        }
        return rootview;
    }

    private void getWebinarsFromServer() {

        layoutProgressbar.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.GONE);

        fetchFromServer();
    }

    private void fetchFromServer() {
        try {
            Config config = new Config();
            String tag_string_req1 = "req_state1";
            StringRequest strReq1 = new StringRequest(Request.Method.POST,
                    config.FetchAllDetails, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        try {

                            try {

                                DatabaseHandler db1 = new DatabaseHandler(getActivity());

                                jObject = new JSONObject(response);


                                /**********  end fetch teamchat ***********/

                                db1.deleteRecentWebinarsTable();
                                if (!jObject.isNull("recent_webinars")) {
                                    JSONArray jsonArrayRecentWebinars = jObject.getJSONArray("recent_webinars");
                                    int recent_webinars_length = jsonArrayRecentWebinars.length();
                                    for (int i = 0; i < recent_webinars_length; i++) {
                                        JSONObject JasonobjectRecentWebinars = jsonArrayRecentWebinars.getJSONObject(i);
                                        recent_webinar_name = JasonobjectRecentWebinars.getString("name");
                                        recent_webinar_agenda = JasonobjectRecentWebinars.getString("agenda");
                                        recent_webinar_starts_at = JasonobjectRecentWebinars.getString("starts_at");
                                        recent_webinar_ends_at = JasonobjectRecentWebinars.getString("ends_at");
                                        recent_webinar_duration = JasonobjectRecentWebinars.getString("duration");
                                        recent_webinar_room_url = JasonobjectRecentWebinars.getString("room_url");
                                        //insert recent webinars into sqlite

                                        db1.addRecentWebinars(new Elpfa("recent",recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url));


                                    }

                                }
                                db1.deleteUpcomingWebinarsTable();
                                if (!jObject.isNull("upcoming_webinars")) {
                                    JSONArray jsonArrayUpcomingWebinars = jObject.getJSONArray("upcoming_webinars");
                                    int upcoming_webinars_length = jsonArrayUpcomingWebinars.length();
                                    for (int i = 0; i < upcoming_webinars_length; i++) {
                                        JSONObject JasonobjectUpcomingWebinars = jsonArrayUpcomingWebinars.getJSONObject(i);
                                        upcoming_webinar_name = JasonobjectUpcomingWebinars.getString("name");
                                        upcoming_webinar_agenda = JasonobjectUpcomingWebinars.getString("agenda");
                                        upcoming_webinar_starts_at = JasonobjectUpcomingWebinars.getString("starts_at");
                                        upcoming_webinar_ends_at = JasonobjectUpcomingWebinars.getString("ends_at");
                                        upcoming_webinar_duration = JasonobjectUpcomingWebinars.getString("duration");
                                        upcoming_webinar_room_url = JasonobjectUpcomingWebinars.getString("room_url");
                                        //insert upcoming webinars into sqlite

                                        db1.addUpcomingWebinars(new Elpfa("upcoming",upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url));
                                    }
                                }
                                isAllAvilable = true;
                                onVolleyResponse(isAllAvilable);

                            } catch (Exception e) {
                                onVolleyResponse(isAllAvilable);
                            }

                        } catch (Exception e) {
                            onVolleyResponse(isAllAvilable);

                        }


                    } catch (Exception e) {
                        onVolleyResponse(isAllAvilable);

                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    onVolleyResponse(isAllAvilable);
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("value1", sharedpreferences.getString("Username", null));
                    params.put("value2", sharedpreferences.getString("Password", null));
                    return params;
                }

            };
            strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);
        }
        catch (Exception e){
            onVolleyResponse(isAllAvilable);
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onVolleyResponse(boolean isSuccess) {
        if (isSuccess){
            layoutProgressbar.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            setupViewPager(mViewPager);
        }
        else {
            layoutProgressbar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager fm = getChildFragmentManager();
        ViewPagerAdapter adapter = new ViewPagerAdapter(fm);
        adapter.addFragment(new UpcomingWebinars(), "Upcoming");
        adapter.addFragment(new RecentWebinars(), "Recent");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragmentList = new ArrayList<>();
        private List<String> fragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
