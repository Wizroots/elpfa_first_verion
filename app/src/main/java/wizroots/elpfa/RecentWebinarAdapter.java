package wizroots.elpfa;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class RecentWebinarAdapter extends RecyclerView.Adapter<RecentWebinarAdapter.MyViewHolder> {

    private List<Webinar> webinarList;
    private List<Webinar> webinarList1;
    Context mContext;

    private SparseBooleanArray expandState = new SparseBooleanArray();

    private ArrayList<Webinar> arraylist;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView webinar_name,webinar_starts_day,webinar_starts_date,webinar_ends_day,webinar_ends_date,webinar_duration,webinar_agenda;
        Button watch_video;
        public TextView textView;
        public RelativeLayout buttonLayout;
        public ExpandableLinearLayout expandableLayout;
        public MyViewHolder(View view) {
            super(view);
            webinar_name = (TextView) view.findViewById(R.id.webinar_name);
            webinar_starts_day = (TextView) view.findViewById(R.id.starts_day);
            webinar_starts_date = (TextView) view.findViewById(R.id.starts_date);
            webinar_ends_day = (TextView) view.findViewById(R.id.ends_day);
            webinar_ends_date = (TextView) view.findViewById(R.id.ends_date);
            webinar_duration = (TextView) view.findViewById(R.id.duration_hour);
            watch_video = (Button) view.findViewById(R.id.watch_video);
            buttonLayout = (RelativeLayout) view.findViewById(R.id.button);
            webinar_agenda = (TextView) view.findViewById(R.id.webinar_agenda);
            expandableLayout = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout);
            mContext=view.getContext();


        }
    }


    public RecentWebinarAdapter(List<Webinar> webinarList) {
        this.webinarList = webinarList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(webinarList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_webinar_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Webinar webinar = webinarList.get(position);
        holder.webinar_name.setText(webinar.getWebinarName());
        //change start date format
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {


            String strDate = webinar.getWebinarStartsAt();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat.parse(strDate);
                SimpleDateFormat sdfmonth = new SimpleDateFormat("dd/MM/yyyy");
                String monthday = sdfmonth.format(convertedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //change end date format
        DateFormat dfEnd = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        SimpleDateFormat sdfEnd = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date end = dfEnd.parse(webinar.getWebinarEndsAt());
            sdfEnd.setTimeZone(TimeZone.getTimeZone("GMT"));
            holder.webinar_ends_date.setText(""+sdfEnd.format(end));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.webinar_duration.setText(webinar.getWebinarDuration());
        holder.watch_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(mContext instanceof MainActivity){
                ((MainActivity)mContext).watch_video(webinar.getWebunarPath());
//                }

            }
        });
        final Webinar item = webinarList.get(position);
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator(item.interpolator);
        holder.expandableLayout.setExpanded(expandState.get(position));
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.webinar_agenda.setText(webinar.getWebunarAgenda());
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        });
    }





    @Override
    public int getItemCount() {
        return webinarList.size();
    }
  /*  public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        webinarList.clear();

        if (charText.length() == 0) {
            webinarList.addAll(arraylist);
        } else {
            for (Webinar wp : arraylist) {
                if (wp.getWebinarName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    courseList.add(wp);
                }
            }
        }

        notifyDataSetChanged();
    }*/
   /* public void setFilter(List<Course> courses) {
        courseList1 = new ArrayList<>();
        courseList1.addAll(courses);
        notifyDataSetChanged();
    }*/
  public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
      ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
      animator.setDuration(300);
      animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_INTERPOLATOR));
      return animator;
  }
    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }


}