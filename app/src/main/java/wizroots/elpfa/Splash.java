package wizroots.elpfa;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.*;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

//import com.amazonaws.mobileconnectors.amazonmobileanalytics.*;
public class Splash extends AppCompatActivity {

    SharedPreferences sharedpreferences;
    public  final String MyPREFERENCES = "MyPrefs" ;
//    //Define a static variable within your activity to hold a reference to the Mobile Analytics client.
//    private static MobileAnalyticsManager analytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final Handler handler = new Handler();
//        //In the activity's onCreate() method, initialize the Mobile Analytics client after the initialization of CognitoCachingCredentialsProvider.
//        try {
//            analytics = MobileAnalyticsManager.getOrCreateInstance(
//                    this.getApplicationContext(),
//                    "a99e9f08417f4703b5790f6b39ea2787", //Amazon Mobile Analytics App ID
//                    "COGNITO_IDENTITY_POOL"); //Amazon Cognito Identity Pool ID
//                    Log.d("amazon_web_services","analytics : "+analytics);
//
//        } catch(InitializationException ex) {
//            Log.d("amazon_web_services", "Failed to initialize Amazon Mobile Analytics"+ex);
//        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 1500ms
                if(sharedpreferences.getString("domain",null)!=null)
                {
                    if (sharedpreferences.getString("Username", null) != null) {
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra("username", sharedpreferences.getString("Username", null));
                    intent.putExtra("user_id", sharedpreferences.getString("User_id", null));
                    intent.putExtra("local_name", sharedpreferences.getString("Local_name", null));
                    startActivity(intent);
                    Splash.this.finish();

                    } else {
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        Splash.this.finish();
                        startActivity(i);
                    }
                }
                else
                {


                    Intent i=new Intent(getApplicationContext(),Introduction.class);
                    startActivity(i);
                    Splash.this.finish();
                }



            }
        }, 1500);
    }
    /**
     * Invoked when the Activity loses user focus
     */
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if(analytics != null) {
//            Log.d("amazon_web_services","in onPause");
//            analytics.getSessionClient().pauseSession();
//            //Attempt to send any events that have been recorded to the Mobile Analytics service.
//            analytics.getEventClient().submitEvents();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if(analytics != null)  {
//            Log.d("amazon_web_services","in onResume");
//            analytics.getSessionClient().resumeSession();
//        }
//    }
}
