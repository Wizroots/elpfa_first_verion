package wizroots.elpfa.service;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import wizroots.elpfa.AppController;
import wizroots.elpfa.DatabaseHandler;
import wizroots.elpfa.Elpfa;
import wizroots.elpfa.R;
import wizroots.elpfa.utils.NotificationUtils;
import wizroots.elpfa.Config;

/**
 * Created by reflective2 on 04-10-2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    Context context;
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
    NotificationCompat.Builder builder;
    private NotificationManager mNotificationManager;
    SharedPreferences.Editor editor;
    int scheduled_completed, progress_completed, all_completed, compl_completed;
    private NotificationUtils notificationUtils;
    String number_started, number_assigned, number_not_started, number_completed;
    SharedPreferences sharedpreferences;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;
    public static final String MyPREFERENCES = "MyPrefs";
    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
    int compl_enrollment_key,compl_status, scheduled_enrollment_key,scheduled_status, progress_enrollment_key, progress_status,all_enrollment_key,all_status;
    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
    JSONObject jObject;
    String push_status;

    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;

    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
         editor = sharedpreferences.edit();

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleNotification(String message) {
        sendNotification(message+"handleNotification");
    }

    private void handleDataMessage(JSONObject json) {

        try {
            JSONObject courses = json.getJSONObject("data");

            String title = courses.getString("title");
            String message = courses.getString("message");
            boolean isBackground = courses.getBoolean("is_background");
            String imageUrl = courses.getString("image");
            final String course_id = courses.getString("course_id");
            final String users_to_course_id = courses.getString("user_to_course_id");
            imageUrl=Config.BASE_CONTENT_URL +imageUrl;
            String timestamp = courses.getString("timestamp");
            JSONObject payload = courses.getJSONObject("payload");

            //fetching course details from server
      /*      try {

                Config url_register = new Config();

                    *//* using volley in login - combine everything *//*
                String tag_string_req = "req_state";
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url_register.fetchUpdatedCourseDetails, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("fetch_updated_course", "response : " + response);
//                        if (!jObject.isNull("all_courses")) {
//                            JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");
//
//                            final int progress_length = jsonArrayAllCourses.length();
//
//                            for (int i = 0; i < progress_length; i++) {
//                                JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
//                                all_path = "http://reflectivelearn.net/mlearning/elpfa/" + JasonobjectAll.getString("path");
//                            }
//                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("register_firebase","error");
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("value1", sharedpreferences.getString("User_id",null));
                        params.put("value2", course_id);
                        return params;
                    }

                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    *//* end *//*









            } catch (Exception ex) {
                Log.d("exception", "" + ex);
            }*/

//            Log.d("syncing_values","payload : "+payload);
//            Log.d("syncing_values","timestamp : "+timestamp);
//            Log.d("syncing_values","isBackground : "+isBackground);




//            Log.d("firebase_notification", "title: " + title);
//            Log.d("firebase_notification", "message: " + message);
//            Log.d("firebase_notification","isBackground: " + isBackground);
//            Log.d("firebase_notification", "payload: " + payload.toString());
//            Log.d("firebase_notification", "imageUrl: " + imageUrl);
//            Log.d("firebase_notification", "timestamp: " + timestamp);


//            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//                Log.d("firebase_check", "10");
//                // app is in foreground, broadcast the push message
////                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
////                pushNotification.putExtra("message", message);
////                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
////
////                // play notification sound
////                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
////
////                notificationUtils.playNotificationSound();
//                sendNotification(message+" 10");
//
//
//
//            } else {
//                Log.d("firebase_check", "11");
////                // app is in background, show the notification in notification tray
////                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
////                resultIntent.putExtra("message", message);
////
////                // check for image attachment
////                if (TextUtils.isEmpty(imageUrl)) {
////                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
////                } else {
////                    // image is present, show notification with image
////                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
////                }
//                sendNotification(message+"    hai");
//            }

            push_status = sharedpreferences.getString("push",null);
            if (push_status.equals("on")|| push_status.equals("off")){
                try {

                    Config url_fetch = new Config();

                    /* using volley in login - combine everything */
                    String tag_string_req = "req_state";
                    StringRequest strReq = new StringRequest(Request.Method.POST,
                            url_fetch.fetchUpdatedCourseDetails, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {

                                try {

                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//

                                    try {
                                        // fetch all user types details
                                        DatabaseHandler db1 = new DatabaseHandler(getApplicationContext());
                                        jObject = new JSONObject(response);


                                        db1.deleteCourseReportsTable();
                                        //fetch number of assigned courses
                                        if (!jObject.isNull("number_assigned_courses")) {

                                            JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                                            JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                                            number_assigned = Jasonobject.getString("count(*)");

                                            editor.putString("assigned", number_assigned);
                                            editor.commit();


//
                                            //fetch number of started courses
                                            if (!jObject.isNull("number_started_courses")) {

                                                JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                                                JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                                                number_started = JasonobjectStarted.getString("count(*)");

                                                editor.putString("started", number_started);
                                                editor.commit();
                                            }
                                            //fetch number of not-started courses
                                            if (!jObject.isNull("number_not_started_courses")) {

                                                JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                                                JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                                                number_not_started = JasonobjectNotStarted.getString("count(*)");

                                                editor.putString("not_started", number_not_started);
                                                editor.commit();

                                            }
                                            //fetch number of completed courses
                                            if (!jObject.isNull("number_completed_courses")) {

                                                JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                                                JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                                                number_completed = JasonobjectCompleted.getString("count(*)");

                                                editor.putString("completed", number_completed);
                                                editor.commit();
                                            }

                                            /* add to course reports table */
                                            db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

                                        }
                                        //fetch all  courses

                                        editor.putString("json_String_scheduled", response);
                                        editor.putString("json_String_progress", response);
                                        editor.putString("json_String_completed", response);
                                        editor.putString("json_String_all", response);
                                        editor.commit();


                                        /******** fetch all scheduled courses ***********/

                                        if (!jObject.isNull("scheduled_courses")) {
                                            JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                                            final int scheduled_length = jsonArrayScheduledCourses.length();

                                            for (int i = 0; i < scheduled_length; i++) {
                                                JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                                                scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");

                                                scheduled_name = JasonobjectScheduled.getString("name");
                                                scheduled_desc = JasonobjectScheduled.getString("description");
                                                scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                                                scheduled_course_id = JasonobjectScheduled.getString("course_id");
                                                scheduled_enrollment_key = JasonobjectScheduled.getInt("enrollment_key");
                                                scheduled_status = JasonobjectScheduled.getInt("status");
                                                scheduled_track_content = String.valueOf(JasonobjectScheduled.getInt("track_course"));
                                                scheduled_users_to_courseid = JasonobjectScheduled.getString("id");
                                                scheduled_duration = JasonobjectScheduled.getString("course_duration");
                                                scheduled_no_of_que = JasonobjectScheduled.getString("no_of_que");
                                                scheduled_pass_percent = JasonobjectScheduled.getString("pass_percent");
                                                scheduled_test_mandatory = JasonobjectScheduled.getString("test_mandatory");
                                                scheduled_startedd = JasonobjectScheduled.getInt("started");
                                                scheduled_completed = JasonobjectScheduled.getInt("completed");
                                                scheduled_completion_date = JasonobjectScheduled.getString("completion_date");

                                                //insert completed courses into sqlite
                                                db1.addAllCourses(new Elpfa("all", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status, scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));
                                                db1.addScheduledCourses(new Elpfa("scheduled", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status, scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));


                                            }


                                        }


                                        /**********  end fetch all   courses ***********/
                                        //fetch contents
                                        if (!jObject.isNull("user_contents")) {
                                            contents_course_id_arr.clear();
                                            contents_usersToCourse_id_arr.clear();
                                            contents_viewed_arr.clear();
                                            contents_completed_arr.clear();
                                            contents_type_arr.clear();
                                            contents_name_arr.clear();
                                            contents_path_arr.clear();

                                            JSONArray jsonArrayContents = jObject.getJSONArray("user_contents");
                                            int contents_length = jsonArrayContents.length();
                                            for (int i = 0; i < contents_length; i++) {
                                                JSONObject JasonobjectContents = jsonArrayContents.getJSONObject(i);
                                                contents_course_id = JasonobjectContents.getInt("course_id");
                                                contents_users_to_course_id = JasonobjectContents.getInt("id");
                                                contents_completed = JasonobjectContents.getInt("status");
                                                if (contents_completed == 0) {
                                                    contents_viewed = 0;
                                                } else {
                                                    contents_viewed = 1;
                                                }

                                                contents_type = JasonobjectContents.getString("item_name");
                                                contents_name = JasonobjectContents.getString("name");
                                                if (contents_type.equals("youtube")) {
                                                    contents_path = JasonobjectContents.getString("path");
                                                } else {
                                                    contents_path = Config.BASE_CONTENT_URL + JasonobjectContents.getString("path");
                                                }

                                                contents_course_id_arr.add(i, contents_course_id);
                                                contents_usersToCourse_id_arr.add(i, contents_users_to_course_id);
                                                contents_viewed_arr.add(i, contents_viewed);
                                                contents_completed_arr.add(i, contents_completed);
                                                contents_type_arr.add(i, contents_type);
                                                contents_name_arr.add(i, contents_name);
                                                contents_path_arr.add(i, contents_path);


                                                //insert team members into sqlite
                                                db1.addUsersToContents(new Elpfa(contents_course_id, contents_users_to_course_id, contents_viewed, contents_completed, contents_type, contents_name, contents_path));


                                            }


                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("value1", sharedpreferences.getString("User_id", null));
                            params.put("value2", course_id);
                            return params;
                        }

                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                } catch (Exception ex) {
                    Log.d("exception", "" + ex);
                }

                if (push_status.equals("on")) {

                    sendNotification(message);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
      private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        builder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Notification from ELPFA")
//                        .setContentText("New team " + courseName + " is now assigned to you")
                        .setContentText(msg)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                        .setAutoCancel(true);
        builder.setLights(Color.RED, 3000, 3000);
        Random rand=new Random();
        mNotificationManager.notify(rand.nextInt(100), builder.build());
    }

}
