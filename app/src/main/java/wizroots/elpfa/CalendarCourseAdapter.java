package wizroots.elpfa;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class CalendarCourseAdapter extends RecyclerView.Adapter<CalendarCourseAdapter.MyViewHolder> {

    private List<CourseCalendar> courseList;
    private List<CourseCalendar> courseList1;
    Context context;
    String selectedDay;

    private ArrayList<CourseCalendar> arraylist;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView course_name,course_assign_date,course_date, txtDay;
        public ImageView course_image;

        public MyViewHolder(View view) {
            super(view);
            course_name = (TextView) view.findViewById(R.id.course_name_txt);
            course_assign_date = (TextView) view.findViewById(R.id.course_assign_date_txt);
            course_image = (ImageView) view.findViewById(R.id.course_image);
            txtDay = (TextView) view.findViewById(R.id.txt_day);
        }
    }


    public CalendarCourseAdapter(List<CourseCalendar> courseList) {
        this.courseList = courseList;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(courseList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cal_course_list_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CourseCalendar course = courseList.get(position);
        holder.course_name.setText(course.getCourseName());
        holder.course_assign_date.setText(course.getCourseAssignDate());
        String path_inactive="htt://abc";
        Picasso.with(holder.course_image.getContext())
                .load(course.getCoursePath())
                .placeholder(R.drawable.no_preview)
                .into(holder.course_image);
        holder.txtDay.setText(selectedDay);
    }

    @Override
    public int getItemCount() {
        return courseList.size();
    }
//    public void filter(String charText) {
//        charText = charText.toLowerCase(Locale.getDefault());
//        courseList.clear();
//
//        if (charText.length() == 0) {
//            courseList.addAll(arraylist);
//        } else {
//            for (CourseCalendar wp : arraylist) {
//                if (wp.getCourseName().toLowerCase(Locale.getDefault())
//                        .contains(charText)) {
//                    courseList.add(wp);
//                }
//            }
//        }
//
//        notifyDataSetChanged();
//    }
//    public void setFilter(List<Course> courses) {
//        courseList1 = new ArrayList<>();
//        courseList1.addAll(courses);
//        notifyDataSetChanged();
//    }

    public void setSelectedDay(String day){
        this.selectedDay = day;
    }
}