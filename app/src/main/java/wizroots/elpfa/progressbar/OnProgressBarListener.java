package wizroots.elpfa.progressbar;

/**
 * Created by Design on 28-12-2017.
 */

public interface OnProgressBarListener {

    void onProgressChange(int current, int max);
}