package wizroots.elpfa;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UpcomingWebinars.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UpcomingWebinars#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpcomingWebinars extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RelativeLayout no_webinars;
    private List<Webinar> webinarList = new ArrayList<>();
    private RecyclerView recyclerView;
    public UpcomingWebinarAdapter webinarAdapter;
    ScrollView root;
    View rootView;
    ArrayList<String> webinar_id_arr= new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    String webinar_id_txt,webinar_name_txt,webinar_agenda_txt,webinar_starts_at_txt,webinar_ends_at_txt,webinar_duration_txt,webinar_path_txt;


    public UpcomingWebinars() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpcomingWebinars.
     */
    // TODO: Rename and change types and number of parameters
    public static UpcomingWebinars newInstance(String param1, String param2) {
        UpcomingWebinars fragment = new UpcomingWebinars();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_upcoming_webinars, container, false);
        no_webinars=(RelativeLayout)rootView.findViewById(R.id.no_webinars);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        webinarAdapter = new UpcomingWebinarAdapter(webinarList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(webinarAdapter);
        webinarList.clear();
        prepareWebinarData();

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void prepareWebinarData()
    {
        try {
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            webinar_id_arr.clear();
            int count = 0;
            //retrieve all completed courses
            List<Elpfa> elpfa_all = db1.getUpcomingWebinars();
            int i = 0;
            for (Elpfa elp : elpfa_all) {
                webinar_id_txt = elp.getUpcomingWebinar_id();
                webinar_name_txt = elp.getUpcomingWebinar_name();
                webinar_agenda_txt = elp.getUpcomingWebinar_agenda();
                webinar_starts_at_txt = elp.getUpcomingWebinar_starts_at();
                webinar_ends_at_txt = elp.getUpcomingWebinar_ends_at();
                webinar_duration_txt = elp.getUpcomingWebinar_duration();
                webinar_path_txt = elp.getUpcomingWebinar_room_url();

                //change start date format
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date start = df.parse(webinar_starts_at_txt);
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));


                //change end date format
                DateFormat dfEnd = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
                SimpleDateFormat sdfEnd = new SimpleDateFormat("yyyy-MM-dd");
                Date end = dfEnd.parse(webinar_ends_at_txt);
                sdfEnd.setTimeZone(TimeZone.getTimeZone("GMT"));

                Webinar webinar = new Webinar(webinar_id_txt, webinar_name_txt, webinar_agenda_txt, webinar_starts_at_txt, webinar_ends_at_txt, webinar_duration_txt, webinar_path_txt,  com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_OUT_SLOW_IN_INTERPOLATOR));
                webinarList.add(webinar);
                webinarAdapter.notifyDataSetChanged();

            }
            if (webinarList.size()==0)
            {
                no_webinars.setVisibility(View.VISIBLE);
            }
        }


        catch (Exception e)
        {

        }

    }
}
