package wizroots.elpfa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Teams#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Teams extends Fragment implements HandleVolleyResponse{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.layout_progressbar)
    RelativeLayout layoutProgressbar;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View rootview;
    ActionBarDrawerToggle toggle;
    RecyclerView recycler_view;
    DatabaseHandler db;    
    String team_id, team_name, team_path, team_description;
    Toolbar toolbar;

    public TeamsAdapter teamAdapter;

    public List<Team> teamList = new ArrayList<>();
    public List<Team> teamListAll = new ArrayList<>();
    Fragment fragment;
    ArrayList<String> team_id_arr = new ArrayList<>();
    ArrayList<String> team_name_arr = new ArrayList<>();
    ArrayList<String> team_description_arr = new ArrayList<>();
    ArrayList<String> team_path_arr = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    int new_pos;
    
    private boolean isAllAvailable = false;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private SharedPreferences.Editor editor;
    private JSONObject jObject;
    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
    ArrayList<String> teamchat_username_arr = new ArrayList<>();
    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();
    ArrayList<Integer> teammembers_teamid_arr = new ArrayList<>();
    ArrayList<String> teammembers_name_arr = new ArrayList<>();
    ArrayList<String> teammembers_photo_arr = new ArrayList<>();
    int team_members_teamid;
    String team_members_name;
    String team_members_photo;
    int teamchat_teamid;
    int teamchat_chatid;
    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    String teamchat_createdat;
    private boolean isAllAvilable = false;
    private String team_image;

    public Teams() {
        // Required empty public constructor
    }
    public static Teams newInstance(String param1, String param2) {
        Teams fragment = new Teams();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_teams, container, false);
        
        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        
        toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.back_white);
        toolbar.setTitle("Teams");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        drawer.setDrawerListener(toggle);
        recycler_view = (RecyclerView) rootview.findViewById(R.id.recycler_view);
        teamAdapter = new TeamsAdapter(teamList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(teamAdapter);
        teamList.clear();
        teamListAll.clear();
        db = new DatabaseHandler(getActivity());
        recycler_view.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recycler_view, new RecyclerClick_Listener() {
            @Override
            public void onClick(View view, int position) {
                Intent i = new Intent(getActivity(), TeamsActivity.class);
                i.putExtra("team_id", team_id_arr.get(position));
                i.putExtra("team_name", team_name_arr.get(position));
                i.putExtra("team_description", team_description_arr.get(position));
                i.putExtra("team_path", team_path_arr.get(position));
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {
                //Select item on long click

            }
        }));
        unbinder = ButterKnife.bind(this, rootview);
        if (Utils.isNetworkAvailable(getActivity()))
            getTeamsFromServer();
        else {
            layoutProgressbar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            prepareTeamsData();
        }
        return rootview;
    }

    private void getTeamsFromServer() {

        layoutProgressbar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        fetchFromServer();
    }

    private void fetchFromServer() {
        try {
            Config config = new Config();
            String tag_string_req1 = "req_state1";
            StringRequest strReq1 = new StringRequest(Request.Method.POST,
                    config.FetchAllDetails, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("login_check","response : "+response);
                    if (response.equals("Not successfull")) {
                        onVolleyResponse(isAllAvailable);
                    } else {

                        try {

                            try {
                                try {
                                    // fetch all user types details
                                    DatabaseHandler db1 = new DatabaseHandler(getActivity());
                                    jObject = new JSONObject(response);
                                    db1.deleteTeamsTable();
                                    if (!jObject.isNull("teams")) {
                                        JSONArray jsonArrayTeams = jObject.getJSONArray("teams");
                                        int team_length = jsonArrayTeams.length();
                                        for (int i = 0; i < team_length; i++) {
                                            JSONObject JasonobjectTeams = jsonArrayTeams.getJSONObject(i);
                                            team_id = JasonobjectTeams.getString("id");
                                            team_name = JasonobjectTeams.getString("name");

                                            team_path = Config.BASE_CONTENT_URL + JasonobjectTeams.getString("path");

                                            team_description = JasonobjectTeams.getString("description");

                                            //insert teams into sqlite

                                            db1.addTeams(new Elpfa("teams", team_id, team_name, team_path, team_description));
                                            db1.addTo(new Elpfa("to",team_id,team_name,"no_email","team"));

                                        }

                                    }


                                    db1.deleteTeamChatTable();
                                    db1.deleteTeamChatTableNew();
                                    if (!jObject.isNull("teamchat")) {
                                        teamchat_id_arr.clear();
                                        teamchat_teamid_arr.clear();
                                        teamchat_username_arr.clear();
                                        teamchat_imagepath_arr.clear();
                                        teamchat_messages_arr.clear();
                                        teamchat_createdat_arr.clear();
                                        JSONArray jsonArrayTeamchat = jObject.getJSONArray("teamchat");
                                        int teamchat_length = jsonArrayTeamchat.length();
                                        for (int i = 0; i < teamchat_length; i++) {
                                            JSONObject JasonobjectTeamchat = jsonArrayTeamchat.getJSONObject(i);
                                            teamchat_chatid = JasonobjectTeamchat.getInt("id");
                                            teamchat_teamid = JasonobjectTeamchat.getInt("team_id");
                                            teamchat_username = JasonobjectTeamchat.getString("user_name");
                                            teamchat_imagepath = JasonobjectTeamchat.getString("image_path");
                                            teamchat_messages = JasonobjectTeamchat.getString("messages");
                                            teamchat_createdat = JasonobjectTeamchat.getString("created_at");

                                            teamchat_teamid_arr.add(i, teamchat_teamid);
                                            teamchat_username_arr.add(i, teamchat_username);
                                            teamchat_imagepath_arr.add(i, teamchat_imagepath);
                                            teamchat_messages_arr.add(i, teamchat_messages);
                                            teamchat_createdat_arr.add(i, teamchat_createdat);

                                            //insert teamchat into sqlite

                                            db1.addTeamChat(new Elpfa("all", "chat", teamchat_chatid, teamchat_teamid, teamchat_username, teamchat_imagepath, teamchat_messages, teamchat_createdat));

                                        }


                                    }

                                    /******** fetch all team members ***********/
                                    db1.deleteTeamMembersTable();
                                    if (!jObject.isNull("team_members")) {
                                        teammembers_teamid_arr.clear();
                                        teammembers_name_arr.clear();
                                        teammembers_photo_arr.clear();

                                        JSONArray jsonArrayTeamMembers = jObject.getJSONArray("team_members");
                                        int teammembers_length = jsonArrayTeamMembers.length();
                                        for (int i = 0; i < teammembers_length; i++) {
                                            JSONObject JasonobjectTeamMembers = jsonArrayTeamMembers.getJSONObject(i);
                                            team_members_teamid = JasonobjectTeamMembers.getInt("team_id");
                                            team_members_name = JasonobjectTeamMembers.getString("name");
                                            team_members_photo = JasonobjectTeamMembers.getString("photo");

                                            teammembers_teamid_arr.add(i, team_members_teamid);
                                            teammembers_name_arr.add(i, team_members_name);
                                            teammembers_photo_arr.add(i, team_members_photo);


                                            //insert team members into sqlite
                                            db1.addTeamMembers(new Elpfa(team_members_teamid, team_members_name, team_members_photo));


                                        }
    

                                    }
                                    isAllAvailable = true;
                                    onVolleyResponse(isAllAvailable);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    onVolleyResponse(isAllAvilable);
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("value1", sharedpreferences.getString("Username", null));
                    params.put("value2", sharedpreferences.getString("Password", null));
                    return params;
                }

            };
            strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);
        }
        catch (Exception e){
            onVolleyResponse(isAllAvilable);
            e.printStackTrace();
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onVolleyResponse(boolean isSuccess) {
        if (isSuccess){
            layoutProgressbar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            prepareTeamsData();
        }
        else {
            layoutProgressbar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }

    public void prepareTeamsData() {
        try {
            team_id_arr.clear();
            team_name_arr.clear();
            team_description_arr.clear();
            team_path_arr.clear();
//                course_enroll_arr.clear();
//                course_name_arr.clear();
//                course_started_arr.clear();
//                course_status_arr.clear();
            teamList.clear();
            int count = 0;
            int team_tag;
            //retrieve all completed courses
            List<Elpfa> elpfa_teams = db.getTeams();
            for (Elpfa elp : elpfa_teams) {
                team_id = elp.getTeamId();
                team_name = elp.getTeamName();
                team_description = elp.getTeamDescription();
                team_image = elp.getTeamPath();
                team_id_arr.add(count, team_id);
                team_name_arr.add(count, team_name);
                team_description_arr.add(count, team_description);
                team_path_arr.add(count, team_image);
                team_tag = count;
                count++;
                team_description = elp.getTeamDescription();
                team_image = elp.getTeamPath();
                Team team = new Team(team_id, team_name, team_description, team_image, team_tag);
                teamList.add(team);
                teamAdapter.notifyDataSetChanged();
            }


        } catch (Exception e) {

        }


    }
}
