package wizroots.elpfa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class DomainSelection extends AppCompatActivity {
View decorView;
    Button next;
    EditText domain_name;
    String domain_name_str;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

//        decorView = getWindow().getDecorView();
//
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().setStatusBarColor(Color.parseColor("#50000000"));
//            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
//        }
     /*   requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/


        setContentView(R.layout.activity_domain_selection);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.rgb(	61, 179, 158));
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        next=(Button)findViewById(R.id.next);
        domain_name=(EditText)findViewById(R.id.domain_name);
        domain_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                domain_name.setHint("  ");

            }
        });

        /*domain_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }
        });*/
        domain_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)
                {
                    next.setEnabled(true);
                    next.setClickable(true);
                    next.setBackgroundColor(Color.WHITE);
                    next.setTextColor(Color.rgb(0,150,136));
                }
                else
                {
                    next.setBackgroundColor(Color.TRANSPARENT);
                    next.setTextColor(Color.parseColor("#3DB39E"));
                    next.setEnabled(false);
                    next.setClickable(false);
                }
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                domain_name_str=domain_name.getText().toString();
                editor.putString("domain",domain_name_str);
                editor.commit();
                Intent i=new Intent(getApplicationContext(), Login.class);
                DomainSelection.this.finish();
                startActivity(i);
            }
        });
        ReplaceFont.setDefaultFont(this, "DEFAULT", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "MONOSPACE", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SERIF", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SANS_SERIF", "PT_Sans_Web_Regular.ttf");

    }
}
