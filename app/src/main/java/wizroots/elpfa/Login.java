package wizroots.elpfa;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wizroots.elpfa.utils.NotificationUtils.clearNotifications;


public class Login extends AppCompatActivity {
    View decorView;
    EditText etUsername;
    EditText etPassword;
    int flag1 = 1;
    String restoredUsername, restoredPassword;
    String user_id, userid;
    String localname;
    String getEnroll_contents_course_id;
    String getEnroll_contents_name;
    String local_name;
    String username, password;
    JSONObject jObject;
    JSONObject jObject1;
    String user_details_id, user_details_name, user_details_email, user_details_photo, user_details_password, user_details_type,user_details_phone, user_details_status, user_details_company_name, user_details_company_address, user_details_registration;
    String user_type_id, user_type_type, user_type_status;
    String team_id, team_name, team_path, team_description;
    String to_id, to_email, to_name;
    String number_started, number_assigned, number_not_started, number_completed;
    String compl_path, compl_name, compl_desc, compl_startdate, compl_course_id, compl_track_content, compl_users_to_courseid, compl_completion_date;
    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
    String notification, notif_title, notif_date;
    String notif_id;
    int scheduled_completed, progress_completed, all_completed, compl_completed;
    String compl_duration, compl_pass_percent, compl_test_mandatory, compl_no_of_que;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;

    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
    int counter_user, counter_team, counter_course, counter_msg;
    int notif_status;

    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    String teamchat_createdat;

    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;


    int teamchat_chatid;

    int team_members_teamid;
    String team_members_name;
    String team_members_photo;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    int teamchat_teamid;
    Button login_btn;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    String recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url;
    String upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url;
    String enroll_contents_id;
    String enroll_contents_course_id;
    String enroll_contents_name;
    String gmt;
    String regid;
    int compl_enrollment_key,compl_status, scheduled_enrollment_key,scheduled_status, progress_enrollment_key, progress_status,all_enrollment_key,all_status;
    int count=0;

    ArrayList<Integer> arr_notifications_status = new ArrayList<>();
    ArrayList<String> arr_notifications_title = new ArrayList<>();
    ArrayList<String> arr_notifications = new ArrayList<>();
    ArrayList<String> arr_notifications_id = new ArrayList<>();
    ArrayList<String> arr_notifications_date = new ArrayList<>();

    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
    ArrayList<String> teamchat_username_arr = new ArrayList<>();
    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();

    ArrayList<Integer> teammembers_teamid_arr = new ArrayList<>();
    ArrayList<String> teammembers_name_arr = new ArrayList<>();
    ArrayList<String> teammembers_photo_arr = new ArrayList<>();

    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();

    RelativeLayout progress_layout;
    TextView forgot_password;
    pl.droidsonroids.gif.GifImageView gif_img;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.parseColor("#5a000000"));
            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
        }
        setContentView(R.layout.activity_login);
        ReplaceFont.setDefaultFont(this, "DEFAULT", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "MONOSPACE", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SERIF", "PT_Sans_Web_Regular.ttf");
        ReplaceFont.setDefaultFont(this, "SANS_SERIF", "PT_Sans_Web_Regular.ttf");
        etUsername = (EditText) findViewById(R.id.id_email);
        etPassword = (EditText) findViewById(R.id.id_password);
        progress_layout = (RelativeLayout) findViewById(R.id.progress_layout);
        login_btn = (Button) findViewById(R.id.id_login);
        login_btn.setVisibility(View.VISIBLE);
//        gif_img=(pl.droidsonroids.gif.GifImageView)findViewById(R.id.gif_img);
//        Matrix matrix = new Matrix();
//        gif_img.setScaleType(ImageView.ScaleType.MATRIX);   //required
//        matrix.postRotate((float) 45, pivotX, pivotY);
//        gif_img.setImageMatrix(matrix);
        Intent mIntent = getIntent();
        flag1 = mIntent.getIntExtra("flag", 1);
        if (flag1 == 2) {
            etUsername.setText("");
            etPassword.setText("");
        }

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        restoredUsername = sharedpreferences.getString("Username", null);
        restoredPassword = sharedpreferences.getString("Password", null);
        userid = sharedpreferences.getString("User_id", null);
        localname = sharedpreferences.getString("Local_name", null);
        forgot_password=(TextView)findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getBaseContext(),ForgotPassword.class);
                i.putExtra("email_address",etUsername.getText().toString());
                startActivity(i);
            }
        });
        if (restoredUsername != null) {
            etUsername.setText(restoredUsername);
            user_id = userid;
            local_name = localname;

        }
        if (restoredPassword != null) {
            etPassword.setText(restoredPassword);
            user_id = userid;
            local_name = localname;


            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.putExtra("username", restoredUsername);
            intent.putExtra("user_id", userid);
            intent.putExtra("local_name", localname);
            startActivity(intent);
            Login.this.finish();

        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    //login
    public void login(View view) {

    /*InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(etUsername.getWindowToken(),
            InputMethodManager.RESULT_UNCHANGED_SHOWN);
    imm.hideSoftInputFromWindow(etPassword.getWindowToken(),
            InputMethodManager.RESULT_UNCHANGED_SHOWN);*/

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected == true) {

            if (validate()) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();
                password = md5(password);

                try {
                    login_btn.setVisibility(View.GONE);
                    progress_layout.setVisibility(View.VISIBLE);
                    Config url_login = new Config();

                    /* using volley in login - combine everything */
                    String tag_string_req = "req_state";
                    StringRequest strReq = new StringRequest(Request.Method.POST,
                            url_login.FetchAllDetails, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.d("login_check","response : "+response);
                            if (response.equals("Not successfull")) {
                                //check if user is deactivated

                                try {

                                    Config url_status = new Config();

                    /* using volley in login - combine everything */
                                    String tag_string_req = "req_state";
                                    StringRequest strReq = new StringRequest(Request.Method.POST,
                                            url_status.checkLearnerStatus, new Response.Listener<String>() {

                                        @Override
                                        public void onResponse(String response) {
                                            if (response.equals("0"))
                                            {
                                                final  AlertDialog.Builder builder1 = new AlertDialog.Builder(Login.this);
                                                builder1.setCancelable(true);
                                                builder1.setMessage("Account has been deactivated");
                                                builder1.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();


                                                            }
                                                        });
                                                AlertDialog alert11 = builder1.create();
                                                alert11.show();
                                               }
                                            else
                                            {
                                                final  AlertDialog.Builder builder1 = new AlertDialog.Builder(Login.this);
                                                builder1.setCancelable(true);
                                                builder1.setMessage("Invalid email or password");
                                                builder1.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();

                                                            }
                                                        });
                                                AlertDialog alert11 = builder1.create();
                                                alert11.show();


                                            }
                                            login_btn.setVisibility(View.VISIBLE);
                                            progress_layout.setVisibility(View.GONE);

                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                        }
                                    }) {

                                        @Override
                                        protected Map<String, String> getParams() {
                                            // Posting parameters to login url
                                            Map<String, String> params = new HashMap<String, String>();
//                        params.put("value1", "anusree@wizroots.com");
                                            params.put("value1", username);
                                            return params;
                                        }

                                    };
                                    strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    // Adding request to request queue
                                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */









                                } catch (Exception ex) {
                                    Log.d("exception", "" + ex);
                                }



                            } else {

                                try {

                                    try {

                                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//

                                        try {
                                            // fetch all user types details
                                            DatabaseHandler db1 = new DatabaseHandler(Login.this);
                                            jObject = new JSONObject(response);
                                            db1.deleteUserTypesTable();
                                            if (!jObject.isNull("user_types")) {
                                                JSONArray jsonArray = jObject.getJSONArray("user_types");
                                                final int length = jsonArray.length();
                                                for (int i = 0; i < length; i++) {
                                                    JSONObject Jasonobject = jsonArray.getJSONObject(i);
                                                    user_type_id = Jasonobject.getString("id");
                                                    user_type_type = Jasonobject.getString("usertype");
                                                    user_type_status = Jasonobject.getString("status");

                                                    db1.addUserTypes(new Elpfa(user_type_id, user_type_type, user_type_status));
//


                                                }
                                            }
                                            db1.deleteUserTable();
                                            //fetch all user details
                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            if (!jObject.isNull("user_details")) {
                                                JSONArray jsonArrayUserDetails = jObject.getJSONArray("user_details");
                                                final int length_jsonArrayUserDetails = jsonArrayUserDetails.length();
                                                for (int i = 0; i < length_jsonArrayUserDetails; i++) {
                                                    JSONObject Jasonobject = jsonArrayUserDetails.getJSONObject(i);
                                                    user_details_id = Jasonobject.getString("id");
                                                    user_details_name = Jasonobject.getString("name");
                                                    user_details_email = Jasonobject.getString("email");
                                                    user_details_password = Jasonobject.getString("password");
                                                    user_details_type = Jasonobject.getString("user_type");
                                                    user_details_phone=Jasonobject.getString("mobileno");
                                                    //reading all user details

                                                    List<Elpfa> elpfa = db1.getUserTypes();

                                                    for (Elpfa elp : elpfa) {
                                                        if (elp.getUserTypeId().equals(user_details_type)) {
                                                            user_details_type = elp.getUserType();
                                                        }


                                                    }
                                                    user_details_photo = Jasonobject.getString("photo");
                                                    if (user_details_photo.equals(""))
                                                    {
                                                        user_details_photo="no_img";
                                                    }
                                                    user_details_status = Jasonobject.getString("status");
                                                    user_details_company_name = Jasonobject.getString("company_name");
                                                    user_details_company_address = Jasonobject.getString("company_address");
                                                    user_details_registration = Jasonobject.getString("registration");
                                                    db1.addUserDetails(new Elpfa(user_details_id, user_details_name, user_details_password, user_details_email, user_details_type, user_details_company_name, user_details_company_address, user_details_photo,/* user_details_registration, user_details_status,*/user_details_phone));

                                                    editor.putString("User_id", Jasonobject.getString("id"));
                                                    userid = Jasonobject.getString("id");
                                                    editor.putString("Username", Jasonobject.getString("email"));
                                                    editor.putString("person_name", Jasonobject.getString("name"));
                                                    if (Jasonobject.getString("photo").equals(""))
                                                    {
                                                        editor.putString("photo", "no_img");
                                                    }
                                                    else
                                                    {
                                                        editor.putString("photo", Jasonobject.getString("photo"));
                                                    }
                                                    editor.putString("Local_name", Jasonobject.getString("name"));
                                                    editor.putString("Password", Jasonobject.getString("password"));
                                                    editor.putString("user_type", Jasonobject.getString("user_type"));
                                                    editor.putInt("status", Integer.parseInt(Jasonobject.getString("status")));
                                                    editor.putString("push", "on");
                                                    editor.putString("count", "0");
                                                    editor.putInt("offline_read", 0);
                                                    editor.putString("from_course", "no");
                                                    editor.putInt("offline_delete_inbox", 0);
                                                    editor.putInt("offline_delete_outbox", 0);
                                                    editor.putString("profile_image_color", "grey");
                                                    editor.putString("course_days", "one_day");
                                                    editor.putString("company_name", Jasonobject.getString("company_name"));
                                                    editor.putString("phone_number", Jasonobject.getString("mobileno"));
                                                    editor.commit();

                                                }


                                            }

                                            // end getting user details
                                            db1.deleteCourseReportsTable();
                                            //fetch number of assigned courses
                                            if (!jObject.isNull("number_assigned_courses")) {

                                                JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                                                JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                                                number_assigned = Jasonobject.getString("count(*)");

                                                editor.putString("assigned", number_assigned);
                                                editor.commit();
                                            }



//
                                                Log.d("login_check","1");
                                                //fetch number of started courses
                                                if (!jObject.isNull("number_started_courses")) {

                                                    JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                                                    JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                                                    number_started = JasonobjectStarted.getString("count(*)");

                                                    editor.putString("started", number_started);
                                                    editor.commit();
                                                }
                                                Log.d("login_check","2");
                                                //fetch number of not-started courses
                                                if (!jObject.isNull("number_not_started_courses")) {

                                                    JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                                                    JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                                                    number_not_started = JasonobjectNotStarted.getString("count(*)");

                                                    editor.putString("not_started", number_not_started);
                                                    editor.commit();

                                                }
                                                Log.d("login_check","3");
                                                //fetch number of completed courses
                                                if (!jObject.isNull("number_completed_courses")) {

                                                    JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                                                    JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                                                    number_completed = JasonobjectCompleted.getString("count(*)");

                                                    editor.putString("completed", number_completed);
                                                    editor.commit();
                                                }

                                            /* add to course reports table */
                                                db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

                                            //fetch all  courses

                                            editor.putString("json_String_scheduled", response);
                                            editor.putString("json_String_progress", response);
                                            editor.putString("json_String_completed", response);
                                            editor.putString("json_String_all", response);
                                            editor.commit();

                                            //get all completed courses
                                            db1.deleteCompletedCoursesTable();
                                            if (!jObject.isNull("completed_courses")) {
                                                JSONArray jsonArrayCompletedCourses = jObject.getJSONArray("completed_courses");

                                                final int completed_length = jsonArrayCompletedCourses.length();

                                                for (int i = 0; i < completed_length; i++) {
                                                    JSONObject JasonobjectCompl = jsonArrayCompletedCourses.getJSONObject(i);
                                                    compl_path = Config.BASE_CONTENT_URL + JasonobjectCompl.getString("path");

                                                    compl_name = JasonobjectCompl.getString("name");
                                                    compl_desc = JasonobjectCompl.getString("description");
                                                    compl_startdate = JasonobjectCompl.getString("assign_date");
                                                    compl_course_id = JasonobjectCompl.getString("course_id");
                                                    compl_enrollment_key = JasonobjectCompl.getInt("enrollment_key");
                                                    compl_status = JasonobjectCompl.getInt("status");
                                                    compl_track_content = String.valueOf(JasonobjectCompl.getInt("track_course"));
                                                    user_id = JasonobjectCompl.getString("user_id");
                                                    compl_users_to_courseid = JasonobjectCompl.getString("id");
                                                    compl_duration = JasonobjectCompl.getString("course_duration");
                                                    compl_no_of_que = JasonobjectCompl.getString("no_of_que");
                                                    compl_pass_percent = JasonobjectCompl.getString("pass_percent");
                                                    compl_test_mandatory = JasonobjectCompl.getString("test_mandatory");
                                                    compl_startedd = JasonobjectCompl.getInt("started");
                                                    compl_completed = JasonobjectCompl.getInt("completed");
                                                    compl_completion_date = JasonobjectCompl.getString("completion_date");

                                                    //insert completed courses into sqlite

                                                    db1.addCompletedCourses(new Elpfa("completed", compl_course_id, compl_users_to_courseid, compl_name, compl_desc, compl_duration, compl_enrollment_key, compl_status,compl_path, compl_startdate, compl_track_content, compl_no_of_que, compl_pass_percent, compl_test_mandatory, compl_startedd, compl_completed, compl_completion_date));


                                                }


                                            }

                                            // end fetch all  completed courses


                                            /******** fetch all scheduled courses ***********/


                                            db1.deleteScheduledCoursesTable();
                                            if (!jObject.isNull("scheduled_courses")) {
                                                JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                                                final int scheduled_length = jsonArrayScheduledCourses.length();

                                                for (int i = 0; i < scheduled_length; i++) {
                                                    JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                                                    scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");

                                                    scheduled_name = JasonobjectScheduled.getString("name");
                                                    scheduled_desc = JasonobjectScheduled.getString("description");
                                                    scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                                                    scheduled_course_id = JasonobjectScheduled.getString("course_id");
                                                    scheduled_enrollment_key = JasonobjectScheduled.getInt("enrollment_key");
                                                    scheduled_status = JasonobjectScheduled.getInt("status");
                                                    scheduled_track_content = String.valueOf(JasonobjectScheduled.getInt("track_course"));
                                                    user_id = JasonobjectScheduled.getString("user_id");
                                                    scheduled_users_to_courseid = JasonobjectScheduled.getString("id");
                                                    scheduled_duration = JasonobjectScheduled.getString("course_duration");
                                                    scheduled_no_of_que = JasonobjectScheduled.getString("no_of_que");
                                                    scheduled_pass_percent = JasonobjectScheduled.getString("pass_percent");
                                                    scheduled_test_mandatory = JasonobjectScheduled.getString("test_mandatory");
                                                    scheduled_startedd = JasonobjectScheduled.getInt("started");
                                                    scheduled_completed = JasonobjectScheduled.getInt("completed");
                                                    scheduled_completion_date = JasonobjectScheduled.getString("completion_date");

                                                    //insert completed courses into sqlite

                                                    db1.addScheduledCourses(new Elpfa("scheduled", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status,scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));


                                                }


                                            }

                                            db1.deleteInProgressCoursesTable();
                                            if (!jObject.isNull("progress_courses")) {
                                                JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                                                final int progress_length = jsonArrayInProgressCourses.length();

                                                for (int i = 0; i < progress_length; i++) {
                                                    JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                                                    progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");

                                                    progress_name = JasonobjectProgress.getString("name");
                                                    progress_desc = JasonobjectProgress.getString("description");
                                                    progress_startdate = JasonobjectProgress.getString("assign_date");
                                                    progress_course_id = JasonobjectProgress.getString("course_id");
                                                    progress_enrollment_key = JasonobjectProgress.getInt("enrollment_key");
                                                    progress_status = JasonobjectProgress.getInt("status");
                                                    progress_track_content = String.valueOf(JasonobjectProgress.getInt("track_course"));
                                                    user_id = JasonobjectProgress.getString("user_id");
                                                    progress_users_to_courseid = JasonobjectProgress.getString("id");
                                                    progress_duration = JasonobjectProgress.getString("course_duration");
                                                    progress_no_of_que = JasonobjectProgress.getString("no_of_que");
                                                    progress_pass_percent = JasonobjectProgress.getString("pass_percent");
                                                    progress_test_mandatory = JasonobjectProgress.getString("test_mandatory");
                                                    progress_startedd = JasonobjectProgress.getInt("started");
                                                    progress_completed = JasonobjectProgress.getInt("completed");
                                                    progress_completion_date = JasonobjectProgress.getString("completion_date");

                                                    //insert completed courses into sqlite

                                                    db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status,progress_path, progress_startdate, progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory, progress_startedd, progress_completed, progress_completion_date));


                                                }

                                            }

                                            db1.deleteAllCoursesTable();
                                            db1.deleteToTable();
                                            if (!jObject.isNull("all_courses")) {
                                                JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                                                final int progress_length = jsonArrayAllCourses.length();

                                                for (int i = 0; i < progress_length; i++) {
                                                    JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                                                    all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                                                    all_name = JasonobjectAll.getString("name");
                                                    all_desc = JasonobjectAll.getString("description");
                                                    all_startdate = JasonobjectAll.getString("assign_date");
                                                    all_course_id = JasonobjectAll.getString("course_id");
                                                    all_enrollment_key = JasonobjectAll.getInt("enrollment_key");
                                                    all_status = JasonobjectAll.getInt("status");
                                                    all_track_content = String.valueOf(JasonobjectAll.getInt("track_course"));
                                                    user_id = JasonobjectAll.getString("user_id");
                                                    all_users_to_courseid = JasonobjectAll.getString("id");
                                                    all_duration = JasonobjectAll.getString("course_duration");
                                                    all_no_of_que = JasonobjectAll.getString("no_of_que");
                                                    all_pass_percent = JasonobjectAll.getString("pass_percent");
                                                    all_test_mandatory = JasonobjectAll.getString("test_mandatory");
                                                    all_startedd = JasonobjectAll.getInt("started");
                                                    all_completed = JasonobjectAll.getInt("completed");
                                                    all_completion_date = JasonobjectAll.getString("completion_date");

                                                    //insert completed courses into sqlite

                                                    db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status,all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory, all_startedd, all_completed, all_completion_date));
                                                    db1.addTo(new Elpfa("to",all_course_id,all_name,"no_email","course"));


                                                }


                                            }


                                            db1.deleteTeamsTable();
                                            if (!jObject.isNull("teams")) {
                                                JSONArray jsonArrayTeams = jObject.getJSONArray("teams");
                                                int team_length = jsonArrayTeams.length();
                                                for (int i = 0; i < team_length; i++) {
                                                    JSONObject JasonobjectTeams = jsonArrayTeams.getJSONObject(i);
                                                    team_id = JasonobjectTeams.getString("id");
                                                    team_name = JasonobjectTeams.getString("name");

                                                    team_path = Config.BASE_CONTENT_URL + JasonobjectTeams.getString("path");

                                                    team_description = JasonobjectTeams.getString("description");

                                                    //insert teams into sqlite

                                                    db1.addTeams(new Elpfa("teams", team_id, team_name, team_path, team_description));
                                                    db1.addTo(new Elpfa("to",team_id,team_name,"no_email","team"));

                                                }

                                            }
                                            //retreve all user emails for msg contact
                                            if (!jObject.isNull("user_to")) {
                                                JSONArray jsonArrayTo = jObject.getJSONArray("user_to");
                                                int to_length = jsonArrayTo.length();
                                                for (int i = 0; i < to_length; i++) {
                                                    JSONObject JasonobjectTo = jsonArrayTo.getJSONObject(i);
                                                    to_id = JasonobjectTo.getString("id");
                                                    to_email = JasonobjectTo.getString("email");
                                                    to_name = JasonobjectTo.getString("email");
                                                    db1.addTo(new Elpfa("to",to_id,to_name,to_email,"user"));

                                                }

                                            }

                                            db1.deleteOfflineDeleteInboxMessagesTable();
                                            db1.deleteOfflineDeleteOutboxMessagesTable();
                                            db1.deleteNotificationsTable();
                                            if (!jObject.isNull("notifications")) {
                                                arr_notifications.clear();
                                                arr_notifications_date.clear();
                                                arr_notifications_id.clear();
                                                JSONArray jsonArrayNotifications = jObject.getJSONArray("notifications");
                                                int notif_length = jsonArrayNotifications.length();
                                                for (int i = 0; i < notif_length; i++) {
                                                    JSONObject JasonobjectNotifications = jsonArrayNotifications.getJSONObject(i);
                                                    notification = JasonobjectNotifications.getString("notification");
                                                    notif_id = JasonobjectNotifications.getString("id");
                                                    notif_date = JasonobjectNotifications.getString("date");
                                                    notif_title = JasonobjectNotifications.getString("title");
                                                    notif_status = JasonobjectNotifications.getInt("status");
                                                    arr_notifications_id.add(i, notif_id);
                                                    arr_notifications_title.add(i, notif_title);
                                                    arr_notifications.add(i, notification);
                                                    arr_notifications_date.add(i, notif_date);
                                                    arr_notifications_status.add(i, notif_status);
                                                    //insert teams into sqlite

                                                    db1.addNotifications(new Elpfa("notification", notif_id, notif_title, notification, notif_date, notif_status));


                                                }
                                            }


                                            /**********  end fetch notifications ***********/
//msg inbx
                                            Config url_login1 = new Config();

                    /* using volley in login - combine everything */
                                            String tag_string_req1 = "req_state1";
                                            StringRequest strReq1 = new StringRequest(Request.Method.POST,
                                                    url_login1.FetchMessages, new Response.Listener<String>() {

                                                @Override
                                                public void onResponse(String response) {
                                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                                    editor.putString("MessageReceiver", response);
                                                    editor.commit();
                                                    try {

                                                        try {

                                                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                                            try {

                                                                DatabaseHandler db1 = new DatabaseHandler(getBaseContext());

                                                                jObject1 = new JSONObject(response);

                                                                //insert inbox messages
                                                                db1.deleteInboxMessagesTable();
                                                                if (!jObject1.isNull("inbox_team")) {
                                                                    JSONArray jsonArrayInbox1 = jObject1.getJSONArray("inbox_team");

                                                                    for (int i = 0; i < jsonArrayInbox1.length(); i++) {
                                                                        JSONObject jasonObjectInbox = jsonArrayInbox1.getJSONObject(i);
                                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                                    }
                                                                }
                                                                if (!jObject1.isNull("inbox_course")) {
                                                                    JSONArray jsonArrayInbox2 = jObject1.getJSONArray("inbox_course");
                                                                    for (int i = 0; i < jsonArrayInbox2.length(); i++) {
                                                                        JSONObject jasonObjectInbox = jsonArrayInbox2.getJSONObject(i);
                                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"),jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                                    }
                                                                }
                                                                if (!jObject1.isNull("inbox_user")) {
                                                                    JSONArray jsonArrayInbox3 = jObject1.getJSONArray("inbox_user");
                                                                    for (int i = 0; i < jsonArrayInbox3.length(); i++) {
                                                                        JSONObject jasonObjectInbox = jsonArrayInbox3.getJSONObject(i);
                                                                        db1.addInboxMessages(new Elpfa(jasonObjectInbox.getInt("id"), jasonObjectInbox.getInt("msg_id"), jasonObjectInbox.getString("user_name"), sharedpreferences.getString("Username",null), jasonObjectInbox.getString("title"), jasonObjectInbox.getString("message"), jasonObjectInbox.getString("photo"), jasonObjectInbox.getString("timestamp"), jasonObjectInbox.getInt("viewed"),0,jasonObjectInbox.getString("attachments")));

                                                                    }
                                                                }
                                                                db1.deleteOutboxMessagesTable();
                                                                if (!jObject1.isNull("outbox_team")) {
                                                                    JSONArray jsonArrayOutbox1 = jObject1.getJSONArray("outbox_team");
                                                                    for (int i = 0; i < jsonArrayOutbox1.length(); i++) {
                                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox1.getJSONObject(i);
                                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                                    }
                                                                }

                                                                if (!jObject1.isNull("outbox_course")) {
                                                                    JSONArray jsonArrayOutbox2 = jObject1.getJSONArray("outbox_course");
                                                                    for (int i = 0; i < jsonArrayOutbox2.length(); i++) {
                                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox2.getJSONObject(i);
                                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("course_id"), jasonObjectOutbox.getInt("msg_id"), sharedpreferences.getString("Username",null),jasonObjectOutbox.getString("name"), jasonObjectOutbox.getString("title"), jasonObjectOutbox.getString("message"),  Config.BASE_CONTENT_URL + jasonObjectOutbox.getString("path"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                                    }
                                                                }
                                                                if (!jObject1.isNull("outbox_user")) {
                                                                    JSONArray jsonArrayOutbox3 = jObject1.getJSONArray("outbox_user");
                                                                    for (int i = 0; i < jsonArrayOutbox3.length(); i++) {
                                                                        JSONObject jasonObjectOutbox = jsonArrayOutbox3.getJSONObject(i);
                                                                        db1.addOutboxMessages(new Elpfa("outbox", jasonObjectOutbox.getInt("id"), jasonObjectOutbox.getInt("msg_id"),sharedpreferences.getString("Username",null), jasonObjectOutbox.getString("name"),  jasonObjectOutbox.getString("title"),jasonObjectOutbox.getString("message"), jasonObjectOutbox.getString("photo"), jasonObjectOutbox.getString("timestamp"),0,jasonObjectOutbox.getString("attachments")));

                                                                    }
                                                                }

                                                            ExecuteUrl messageTask=new ExecuteUrl();
                                                            messageTask.execute(Config.MessageViewCounter, sharedpreferences.getString("User_id",null));
                                                            String messageResult=messageTask.get();
                                                            JSONObject jsonObject = new JSONObject(messageResult);
                                                            if (!jsonObject.isNull("inbox_team_count")) {
                                                                JSONArray jsonArray = jsonObject.getJSONArray("inbox_team_count");
                                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                                    JSONObject jasonObject = jsonArray.getJSONObject(i);
                                                                    counter_team=jasonObject.getInt("count(*)");

                                                                }

                                                            }

                                                            if (!jsonObject.isNull("inbox_course_count")) {
                                                                JSONArray jsonArray = jsonObject.getJSONArray("inbox_course_count");
                                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                                    JSONObject jasonObject = jsonArray.getJSONObject(i);
                                                                    counter_course=jasonObject.getInt("count(*)");

                                                                }
                                                            }


                                                            if (!jsonObject.isNull("inbox_user_count")) {
                                                                JSONArray jsonArray = jsonObject.getJSONArray("inbox_user_count");
                                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                                    JSONObject jasonObject = jsonArray.getJSONObject(i);
                                                                    counter_user=jasonObject.getInt("count(*)");

                                                                }

                                                            }
                                                            counter_msg=counter_team+counter_course+counter_user;
                                                            editor.putInt("counter_msg", counter_msg);
                                                            editor.commit();
                                                            // end couter msg*/


                                                                /******** fetch all teamchat ***********/


                                                                db1.deleteTeamChatTable();
                                                                db1.deleteTeamChatTableNew();
                                                                if (!jObject.isNull("teamchat")) {
                                                                    teamchat_id_arr.clear();
                                                                    teamchat_teamid_arr.clear();
                                                                    teamchat_username_arr.clear();
                                                                    teamchat_imagepath_arr.clear();
                                                                    teamchat_messages_arr.clear();
                                                                    teamchat_createdat_arr.clear();
                                                                    JSONArray jsonArrayTeamchat = jObject.getJSONArray("teamchat");
                                                                    int teamchat_length = jsonArrayTeamchat.length();
                                                                    for (int i = 0; i < teamchat_length; i++) {
                                                                        JSONObject JasonobjectTeamchat = jsonArrayTeamchat.getJSONObject(i);
                                                                        teamchat_chatid = JasonobjectTeamchat.getInt("id");
                                                                        teamchat_teamid = JasonobjectTeamchat.getInt("team_id");
                                                                        teamchat_username = JasonobjectTeamchat.getString("user_name");
                                                                        teamchat_imagepath = JasonobjectTeamchat.getString("image_path");
                                                                        teamchat_messages = JasonobjectTeamchat.getString("messages");
                                                                        teamchat_createdat = JasonobjectTeamchat.getString("created_at");

                                                                        teamchat_teamid_arr.add(i, teamchat_teamid);
                                                                        teamchat_username_arr.add(i, teamchat_username);
                                                                        teamchat_imagepath_arr.add(i, teamchat_imagepath);
                                                                        teamchat_messages_arr.add(i, teamchat_messages);
                                                                        teamchat_createdat_arr.add(i, teamchat_createdat);

                                                                        //insert teamchat into sqlite

                                                                        db1.addTeamChat(new Elpfa("all", "chat", teamchat_chatid, teamchat_teamid, teamchat_username, teamchat_imagepath, teamchat_messages, teamchat_createdat));

                                                                    }


                                                                }


                                                                /**********  end fetch teamchat ***********/

                                                                db1.deleteRecentWebinarsTable();
                                                                if (!jObject.isNull("recent_webinars")) {
                                                                    JSONArray jsonArrayRecentWebinars = jObject.getJSONArray("recent_webinars");
                                                                    Log.d("recent_webinars","3");
                                                                    int recent_webinars_length = jsonArrayRecentWebinars.length();
                                                                    for (int i = 0; i < recent_webinars_length; i++) {
                                                                        JSONObject JasonobjectRecentWebinars = jsonArrayRecentWebinars.getJSONObject(i);
                                                                        recent_webinar_name = JasonobjectRecentWebinars.getString("name");
                                                                        recent_webinar_agenda = JasonobjectRecentWebinars.getString("agenda");
                                                                        recent_webinar_starts_at = JasonobjectRecentWebinars.getString("starts_at");
                                                                        recent_webinar_ends_at = JasonobjectRecentWebinars.getString("ends_at");
                                                                        recent_webinar_duration = JasonobjectRecentWebinars.getString("duration");
                                                                        recent_webinar_room_url = JasonobjectRecentWebinars.getString("room_url");
                                                                        //insert recent webinars into sqlite

                                                                        db1.addRecentWebinars(new Elpfa("recent",recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url));


                                                                    }

                                                                }
                                                                db1.deleteUpcomingWebinarsTable();
                                                                if (!jObject.isNull("upcoming_webinars")) {
                                                                    JSONArray jsonArrayUpcomingWebinars = jObject.getJSONArray("upcoming_webinars");
                                                                    int upcoming_webinars_length = jsonArrayUpcomingWebinars.length();
                                                                    for (int i = 0; i < upcoming_webinars_length; i++) {
                                                                        JSONObject JasonobjectUpcomingWebinars = jsonArrayUpcomingWebinars.getJSONObject(i);
                                                                        upcoming_webinar_name = JasonobjectUpcomingWebinars.getString("name");
                                                                        upcoming_webinar_agenda = JasonobjectUpcomingWebinars.getString("agenda");
                                                                        upcoming_webinar_starts_at = JasonobjectUpcomingWebinars.getString("starts_at");
                                                                        upcoming_webinar_ends_at = JasonobjectUpcomingWebinars.getString("ends_at");
                                                                        upcoming_webinar_duration = JasonobjectUpcomingWebinars.getString("duration");
                                                                        upcoming_webinar_room_url = JasonobjectUpcomingWebinars.getString("room_url");

                                                                        db1.addUpcomingWebinars(new Elpfa("upcoming",upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url));


                                                                    }

                                                                }

                                                             //enroll contents
                                                                db1.deleteEnrollContents();
                                                                if (!jObject.isNull("enroll_contents")) {
                                                                    JSONArray jsonArrayEnrollContents = jObject.getJSONArray("enroll_contents");
                                                                    int enroll_contents_length = jsonArrayEnrollContents.length();
                                                                    for (int i = 0; i < enroll_contents_length; i++) {
                                                                        JSONObject JasonobjectEnrollContents = jsonArrayEnrollContents.getJSONObject(i);
                                                                        {
                                                                            enroll_contents_course_id = JasonobjectEnrollContents.getString("course_id");
                                                                            enroll_contents_name = JasonobjectEnrollContents.getString("name");
                                                                            db1.addEnrollContents(new Elpfa("enroll_contents", enroll_contents_course_id, enroll_contents_name));

                                                                        }

                                                                    }
                                                                }
                                                                db1.deleteTeamMembersTable();
                                                                if (!jObject.isNull("team_members")) {
                                                                    teammembers_teamid_arr.clear();
                                                                    teammembers_name_arr.clear();
                                                                    teammembers_photo_arr.clear();

                                                                    JSONArray jsonArrayTeamMembers = jObject.getJSONArray("team_members");
                                                                    int teammembers_length = jsonArrayTeamMembers.length();
                                                                    for (int i = 0; i < teammembers_length; i++) {
                                                                        JSONObject JasonobjectTeamMembers = jsonArrayTeamMembers.getJSONObject(i);
                                                                        team_members_teamid = JasonobjectTeamMembers.getInt("team_id");
                                                                        team_members_name = JasonobjectTeamMembers.getString("name");
                                                                        team_members_photo = JasonobjectTeamMembers.getString("photo");

                                                                        teammembers_teamid_arr.add(i, team_members_teamid);
                                                                        teammembers_name_arr.add(i, team_members_name);
                                                                        teammembers_photo_arr.add(i, team_members_photo);


                                                                        //insert team members into sqlite
                                                                        db1.addTeamMembers(new Elpfa(team_members_teamid, team_members_name, team_members_photo));


                                                                    }


                                                                }

                                                                Log.d("login_check","10");

                                                                /**********  end fetch team members ***********/

                                                                /******** fetch all contents ***********/
                                                                db1.deleteUsersToContentsTable();
                                                                if (!jObject.isNull("user_contents")) {
                                                                    contents_course_id_arr.clear();
                                                                    contents_usersToCourse_id_arr.clear();
                                                                    contents_viewed_arr.clear();
                                                                    contents_completed_arr.clear();
                                                                    contents_type_arr.clear();
                                                                    contents_name_arr.clear();
                                                                    contents_path_arr.clear();

                                                                    JSONArray jsonArrayContents = jObject.getJSONArray("user_contents");
                                                                    int contents_length = jsonArrayContents.length();
                                                                    for (int i = 0; i < contents_length; i++) {
                                                                        JSONObject JasonobjectContents = jsonArrayContents.getJSONObject(i);
                                                                        contents_course_id = JasonobjectContents.getInt("course_id");
                                                                        contents_users_to_course_id = JasonobjectContents.getInt("id");
                                                                        contents_completed = JasonobjectContents.getInt("status");
                                                                        if (contents_completed == 0) {
                                                                            contents_viewed = 0;
                                                                        } else {
                                                                            contents_viewed = 1;
                                                                        }

                                                                        contents_type = JasonobjectContents.getString("item_name");
                                                                        contents_name = JasonobjectContents.getString("name");
                                                                        if (contents_type.equals("youtube")) {
                                                                            contents_path = JasonobjectContents.getString("path");
                                                                        } else {
                                                                            contents_path = Config.BASE_CONTENT_URL + JasonobjectContents.getString("path");
                                                                        }

                                                                        contents_course_id_arr.add(i, contents_course_id);
                                                                        contents_usersToCourse_id_arr.add(i, contents_users_to_course_id);
                                                                        contents_viewed_arr.add(i, contents_viewed);
                                                                        contents_completed_arr.add(i, contents_completed);
                                                                        contents_type_arr.add(i, contents_type);
                                                                        contents_name_arr.add(i, contents_name);
                                                                        contents_path_arr.add(i, contents_path);


                                                                        //insert team members into sqlite
                                                                        db1.addUsersToContents(new Elpfa(contents_course_id, contents_users_to_course_id, contents_viewed, contents_completed, contents_type, contents_name, contents_path));


                                                                    }


                                                                }
                                                                expandAnimation();

                                                            } catch (Exception e) {
                                                                Log.d("exception", "" + e);
                                                            }
//

                                                        } catch (Exception e) {
                                                            Log.d("exception", "" + e);

                                                        }


                                                    } catch (Exception e) {
                                                        Log.d("exception", "" + e);

                                                    }

                                                }
                                            }, new Response.ErrorListener() {

                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                }
                                            }) {

                                                @Override
                                                protected Map<String, String> getParams() {
                                                    // Posting parameters to login url
                                                    Map<String, String> params = new HashMap<String, String>();
                                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                                    params.put("value1", sharedpreferences.getString("User_id", null));
                                                    return params;
                                                }

                                            };
                                            strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                            // Adding request to request queue
                                            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);

//end msg inbx

//end msg inbx


                                        } catch (Exception e) {
                                            Log.d("exception", "" + e);
                                        }
//

                                    } catch (Exception e) {
                                        Log.d("exception", "" + e);

                                    }


                                } catch (Exception e) {
                                    Log.d("exception", "" + e);

                                }
                                //animation to expand login button

                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
//                            params.put("value1", "anusree@wizroots.com");
//                            params.put("value2", "d89badcaafa31c7ba0f3100defd499e0");
                            params.put("value1", username);
                            params.put("value2", password);
                            return params;
                        }

                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */


                } catch (Exception ex) {
                    Log.d("exception", "" + ex);
                }


            }
        } else {
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.rel1), "No internet connection", Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();
            final TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextSize(18);
            snackBarView.setBackgroundColor(Color.rgb(236, 42, 42));
            tv.setTextColor(Color.rgb(255, 255, 255));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            else
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
            snackbar.show();
        }

    }

    //validate user
    private boolean validate() {
        if (etUsername.getText().toString().trim().equals("")) {
            login_btn.setVisibility(View.GONE);
            progress_layout.setVisibility(View.VISIBLE);
            final  AlertDialog.Builder builder1 = new AlertDialog.Builder(Login.this);
            builder1.setCancelable(true);
            builder1.setMessage("Enter email address");
            builder1.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();


                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
            login_btn.setVisibility(View.VISIBLE);
            progress_layout.setVisibility(View.GONE);
            return false;
        } else if (etPassword.getText().toString().trim().equals("")) {
            login_btn.setVisibility(View.GONE);
            progress_layout.setVisibility(View.VISIBLE);
            final  AlertDialog.Builder builder1 = new AlertDialog.Builder(Login.this);
            builder1.setCancelable(true);
            builder1.setMessage("Enter password");
            builder1.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();


                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
            login_btn.setVisibility(View.VISIBLE);
            progress_layout.setVisibility(View.GONE);
            return false;
        } else
            return true;

    }

    //md5 encryption
    public static String md5(String s) {
        try {

            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            Log.d("exception", "" + e);
        }
        return "";

    }











    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }

    @Override
    public void onStop() {
        super.onStop();
    }




    public void expandAnimation() {
        RelativeLayout slide_layout=(RelativeLayout)findViewById(R.id.slide_layout);
        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slide_layout.startAnimation(slide_up);
        slide_layout.setVisibility(View.VISIBLE);
        slide_up.setFillAfter(true);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // TODO: Your application init goes here.
                registerUsingFirebase();
               /* if (restoredUsername != null) {
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra("username", username);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("local_name", local_name);
                    startActivity(intent);

                    Login.this.finish();

                } else {
                   *//* Log.d("register_check", "in else ");
                    Intent intentService = new Intent(Login.this, CourseStartAlertService.class);
                    Login.this.startService(intentService);
                    gmt = getIntent().getStringExtra("gmt");
                    Log.d("register_check", "gmt : " + gmt);*//*
                    registerUsingFirebase();

                }*/

            }
        }, 1500);





    }

public void registerUsingFirebase()
{

    // register GCM registration complete receiver
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
            new IntentFilter(Config.REGISTRATION_COMPLETE));

    // register new push message receiver
    // by doing this, the activity will be notified each time a new message arrives
    LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
            new IntentFilter(Config.PUSH_NOTIFICATION));



    // clear the notification area when the app is opened
    clearNotifications(getApplicationContext());

    mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // checking for type intent filter
            if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                // gcm successfully registered
                // now subscribe to `global` topic to receive app wide notifications
                FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                displayFirebaseRegId();

            } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                // new push notification is received

                String message = intent.getStringExtra("message");

                Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

//                txtMessage.setText(message);
            }
        }
    };
    displayFirebaseRegId();

        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.putExtra("Username", sharedpreferences.getString("Username",null));
        intent.putExtra("User_id", sharedpreferences.getString("User_id",null));

        startActivity(intent);

        Login.this.finish();





}

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        final String regId = pref.getString("regId", null);


        if (!TextUtils.isEmpty(regId)) {
            try {

                Config url_register = new Config();

                    /* using volley in login - combine everything */
                String tag_string_req = "req_state";
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url_register.registerFirebase, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("register_firebase","response of register : "+response);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("register_firebase","error");
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("value1", "anusree@wizroots.com");
                        params.put("value1", username);
                        params.put("value2", regId);
                        return params;
                    }

                };
                strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


                    /* end */









            } catch (Exception ex) {
                Log.d("exception", "" + ex);
            }
        }
        else {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (count==0)
        {
            Toast.makeText(getApplicationContext(),"Press back again to exit",Toast.LENGTH_SHORT).show();
            count++;
        }
        else {
            System.exit(0);
        }
    }
}