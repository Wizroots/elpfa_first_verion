package wizroots.elpfa;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeamInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.number_members)
    TextView numberMembers;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String team_id;
    String team_name;
    View rootview;
    String team_description;
    String team_path;
    TextView team_description_txt;
    String member_name;
    String member_image;
    String member_id;
    private RecyclerView recyclerView;
    public TeamMembersAdapter membersAdapter;

    public List<TeamMember> memberList = new ArrayList<>();
    public List<TeamMember> memberListAll = new ArrayList<>();
    ArrayList<String> member_id_arr = new ArrayList<>();
    ArrayList<String> member_name_arr = new ArrayList<>();
    ArrayList<String> member_image_arr = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    DatabaseHandler db;

    public TeamInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TeamInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TeamInfoFragment newInstance(String param1, String param2) {
        TeamInfoFragment fragment = new TeamInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            team_id = getArguments().getString("team_id");
            team_name = getArguments().getString("team_name");
            team_description = getArguments().getString("team_description");
            team_path = getArguments().getString("team_path");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_team_info, container, false);
        team_description_txt = (TextView) rootview.findViewById(R.id.team_description);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        membersAdapter = new TeamMembersAdapter(memberList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        db = new DatabaseHandler(getActivity());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(membersAdapter);

        memberList.clear();
        memberListAll.clear();
        prepareMembers();
        team_description_txt.setText("" + team_description);
        unbinder = ButterKnife.bind(this, rootview);
        numberMembers.setText(getActivity().getResources().getQuantityString(R.plurals.members, memberList.size(), memberList.size()));
        return rootview;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void

    prepareMembers() {
        try {
            member_id_arr.clear();
            member_image_arr.clear();
            member_name_arr.clear();
//            course_enroll_arr.clear();
//            course_name_arr.clear();
//            course_started_arr.clear();
//            course_status_arr.clear();
//            courseList.clear();
            int count = 0;
            int member_tag;
            //retrieve all completed courses
            List<Elpfa> elpfa_all = db.getOneTeamMembers(Integer.parseInt(team_id));
            for (Elpfa elp : elpfa_all) {
                member_id = String.valueOf(elp.getTeamMembersId());
                member_id_arr.add(count, member_id);
                member_image = String.valueOf(elp.getTeamMembersPhoto());
                member_image_arr.add(count, member_image);
                member_name = String.valueOf(elp.getTeamMembersName());
                member_name_arr.add(count, member_name);
//                member_tag=count;
                count++;

                TeamMember member = new TeamMember(member_id, member_name, member_image);
                memberList.add(member);
                membersAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {

        }
    }
}
