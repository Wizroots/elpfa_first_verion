package wizroots.elpfa;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Comparator;

/**
 * Created by reflective2 on 10-11-2016.
 */

public class MessagesNew {
    private String message_id;
    private String message_sender;
    private String message_receiver;
    private String message_subject;
    private String message_message;
    private String message_photo;
    private String message_date;
    private int message_tag;
    private String message_color;
    private int message_viewed;
    private int message_important;
    private String message_attachment;
    private String message_inbox_outbox;


    public MessagesNew(String message_id, String message_sender, String message_receiver, String message_subject, String message_message, String message_photo,String message_date, int message_tag, String message_color, int message_viewed,int message_important, String message_attachment, String inbox_outbox)
    {
        this.message_id = message_id;
        this.message_sender = message_sender;
        this.message_receiver = message_receiver;
        this.message_subject = message_subject;
        this.message_message = message_message;
        this.message_photo = message_photo;
        this.message_date = message_date;
        this.message_tag = message_tag;
        this.message_color = message_color;
        this.message_viewed = message_viewed;
        this.message_important = message_important;
        this.message_attachment = message_attachment;
        this.message_inbox_outbox = inbox_outbox;


    }

    //message id
    public String getMessage_id(){
        return message_id;
    }
    public void setMessage_id(String message_id)
    {
        this.message_id=message_id;
    }

    //message sender
    public String getMessage_sender(){
        return message_sender;
    }
    public void setMessage_sender(String message_sender)
    {
        this.message_sender=message_sender;
    }

    //message receiver
    public String getMessage_receiver(){
        return message_receiver;
    }
    public void setMessage_receiver(String message_receiver)
    {
        this.message_receiver=message_receiver;
    }

    //message subject
    public String getMessage_subject(){
        return message_subject;
    }
    public void setMessage_subject(String message_subject)
    {
        this.message_subject=message_subject;
    }

    //message message
    public String getMessage_message(){
        return message_message;
    }
    public void setMessage_message(String message_message)
    {
        this.message_message=message_message;
    }

    //message message
    public String getMessage_date(){
//        String dateAsText = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//                .format(new Date(Long.parseLong(message_date) * 1000L));
        return message_date;
    }
    public void setMessage_date(String message_date)
    {
        this.message_date=message_date;
    }

    //message important
    public int getMessage_important(){
        return message_important;
    }
    public void setMessage_important(int message_important)
    {
        this.message_important=message_important;
    }

    //message tag
    public int getMessage_tag(){
        return message_tag;
    }
    public void setMessage_tag(int message_tag)
    {
        this.message_tag=message_tag;
    }

    //message color
    public String getMessage_color(String message_color){
        return message_color;
    }
    public void setMessage_color(String message_color)
    {
        this.message_color=message_color;
    }
    public static Comparator<MessagesNew> comparator =new Comparator<MessagesNew>()
    {

        @Override
        public int compare(MessagesNew lhs, MessagesNew rhs) {
            return rhs.getMessage_date().compareTo(lhs.getMessage_date());
        }
    };

    //message attachment
    public String getMessage_attachment(){
        return message_attachment;
    }
    public void setMessage_attachment(String message_attachment)
    {
        this.message_attachment=message_attachment;
    }

    //message inbox_outbox
    public String getMessage_inbox_outbox(){
        return message_inbox_outbox;
    }
    public void setMessage_inbox_outbox(String message_inbox_outbox)
    {
        this.message_inbox_outbox=message_inbox_outbox;
    }
}


