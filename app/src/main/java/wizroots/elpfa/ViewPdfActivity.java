package wizroots.elpfa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import wizroots.elpfa.utils.JsonUtils;

public class ViewPdfActivity extends AppCompatActivity  implements ExoPlayer.EventListener{

    @BindView(R.id.webview_pdf)
    WebView webviewPdf;

    private String pdfPath;
    private int usersToContentId;
    private int isUpdated = 0;
    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);
        ButterKnife.bind(this);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);


        pdfPath = getIntent().getStringExtra("content_url");
        usersToContentId = getIntent().getIntExtra("users_to_content_id", 0);
        String url="http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfPath;
        webviewPdf.getSettings().setJavaScriptEnabled(true);
        webviewPdf.getSettings().setAllowFileAccess(true);
        webviewPdf.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
                UpdateContentUpdateResult resultCallback = new UpdateContentUpdateResult() {
                    @Override
                    public void notifyResponse(int updated) {
                        isUpdated = updated;
                        if (updated == 1) {
                            int i = databaseHandler.updateContentStatus(new Elpfa("normal_video_completed", usersToContentId, 1));
                        }
                    }
                };
                JsonUtils.updateContentStatus(ViewPdfActivity.this, usersToContentId, resultCallback);
            }
        });
        webviewPdf.loadUrl(url);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {

    }

    @Override
    public void onBackPressed() {
        boolean updated;
        if (isUpdated == 1){
            updated = true;
            Intent i = new Intent();
            i.putExtra("isUpdated", updated);
            setResult(RESULT_OK, i);
            finish();
        }
        else {
            super.onBackPressed();
        }
    }
}
