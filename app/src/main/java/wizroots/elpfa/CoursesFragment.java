package wizroots.elpfa;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CoursesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CoursesFragment extends Fragment implements HandleVolleyResponse{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    RelativeLayout layoutProgressbar;
    Unbinder unbinder;

    private TabLayout mTabLayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SearchView searchView;
    private OnFragmentInteractionListener mListener;
    View rootview;
    Toolbar toolbar;
    private FragmentTabHost mTabHost;
    ActionBarDrawerToggle toggle;
    CourseAdapter courseAdapter;
    private ViewPager mViewPager;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    private static boolean isCoursesAvailable;
    SharedPreferences.Editor editor;


    String number_started, number_assigned, number_not_started, number_completed;
    String compl_path, compl_name, compl_desc, compl_startdate, compl_course_id, compl_track_content, compl_users_to_courseid, compl_completion_date;
    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;
    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
    int scheduled_completed, progress_completed, all_completed, compl_completed;
    String compl_duration, compl_pass_percent, compl_test_mandatory, compl_no_of_que;
    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;
    int compl_enrollment_key,compl_status, scheduled_enrollment_key,scheduled_status, progress_enrollment_key, progress_status,all_enrollment_key,all_status;
    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
    String user_id, userid;
    private JSONObject jObject;
    private JSONObject jObject1;
    int teamchat_teamid;
    String teamchat_username;
    String teamchat_imagepath;
    String teamchat_messages;
    int teamchat_chatid;
    String teamchat_createdat;
    String recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url;
    String upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url;
    String enroll_contents_course_id;
    String enroll_contents_name;
    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
    ArrayList<String> contents_type_arr = new ArrayList<>();
    ArrayList<String> contents_name_arr = new ArrayList<>();
    ArrayList<String> contents_path_arr = new ArrayList<>();

    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
    ArrayList<String> teamchat_username_arr = new ArrayList<>();
    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();

    ArrayList<Integer> teammembers_teamid_arr = new ArrayList<>();
    ArrayList<String> teammembers_name_arr = new ArrayList<>();
    ArrayList<String> teammembers_photo_arr = new ArrayList<>();

    int contents_course_id;
    int contents_users_to_course_id;
    int contents_viewed;
    int contents_completed;
    String contents_type;
    String contents_name;
    String contents_path;

    int team_members_teamid;
    String team_members_name;
    String team_members_photo;
    private boolean isAllAvilable = false;

    public CoursesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CoursesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CoursesFragment newInstance(String param1, String param2) {
        CoursesFragment fragment = new CoursesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_courses, container, false);
        mTabLayout = (TabLayout) rootview.findViewById(R.id.tabs);
        mViewPager = (ViewPager) rootview.findViewById(R.id.viewpager);
        toolbar = (Toolbar) rootview.findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        isCoursesAvailable = false;

       /* ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        mTabLayout.setupWithViewPager(mViewPager);
        DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        final DrawerLayout drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);

        toggle = new SmoothActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.hamburger);
        drawer.setDrawerListener(toggle);

        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setFillViewport(true);

        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

       // setupViewPager(mViewPager);


        unbinder = ButterKnife.bind(this, rootview);
        layoutProgressbar = (RelativeLayout) rootview.findViewById(R.id.layout_progressbar);
        mViewPager = (ViewPager) rootview.findViewById(R.id.viewpager);
        if (Utils.isNetworkAvailable(getActivity()))
            getCoursesFromServer();
        else {

            layoutProgressbar.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            setupViewPager(mViewPager);
        }
        return rootview;
    }

    private void getCoursesFromServer() {


        layoutProgressbar.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.GONE);

        fetchFromServer();

    }

    private void fetchFromServer() {   try {
        Config config = new Config();

                    /* using volley in login - combine everything */
        String tag_string_req = "req_state";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                config.FetchAllDetails, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login_check","response : "+response);
                if (response.equals("Not successfull")) {
                    onVolleyResponse(isAllAvilable);
                    Toast.makeText(getActivity(), "Somrthing went wrong", Toast.LENGTH_SHORT).show();
                } else {
                    try {

                        sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//

                        try {
                            // fetch all user types details
                            DatabaseHandler db1 = new DatabaseHandler(getActivity());
                            jObject = new JSONObject(response);


                            // end getting user details
                            db1.deleteCourseReportsTable();
                            //fetch number of assigned courses
                            if (!jObject.isNull("number_assigned_courses")) {

                                JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                                JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                                number_assigned = Jasonobject.getString("count(*)");

                                editor.putString("assigned", number_assigned);
                                editor.commit();
                            }
                            //fetch number of started courses
                            if (!jObject.isNull("number_started_courses")) {

                                JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                                JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                                number_started = JasonobjectStarted.getString("count(*)");

                                editor.putString("started", number_started);
                                editor.commit();
                            }
                            //fetch number of not-started courses
                            if (!jObject.isNull("number_not_started_courses")) {

                                JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                                JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                                number_not_started = JasonobjectNotStarted.getString("count(*)");

                                editor.putString("not_started", number_not_started);
                                editor.commit();

                            }
                            //fetch number of completed courses
                            if (!jObject.isNull("number_completed_courses")) {

                                JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                                JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                                number_completed = JasonobjectCompleted.getString("count(*)");

                                editor.putString("completed", number_completed);
                                editor.commit();
                            }

                                            /* add to course reports table */
                            db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

                            //fetch all  courses

                            editor.putString("json_String_scheduled", response);
                            editor.putString("json_String_progress", response);
                            editor.putString("json_String_completed", response);
                            editor.putString("json_String_all", response);
                            editor.commit();

                            //get all completed courses
                            db1.deleteCompletedCoursesTable();
                            if (!jObject.isNull("completed_courses")) {
                                JSONArray jsonArrayCompletedCourses = jObject.getJSONArray("completed_courses");

                                final int completed_length = jsonArrayCompletedCourses.length();

                                for (int i = 0; i < completed_length; i++) {
                                    JSONObject JasonobjectCompl = jsonArrayCompletedCourses.getJSONObject(i);
                                    compl_path = Config.BASE_CONTENT_URL + JasonobjectCompl.getString("path");

                                    compl_name = JasonobjectCompl.getString("name");
                                    compl_desc = JasonobjectCompl.getString("description");
                                    compl_startdate = JasonobjectCompl.getString("assign_date");
                                    compl_course_id = JasonobjectCompl.getString("course_id");
                                    compl_enrollment_key = JasonobjectCompl.getInt("enrollment_key");
                                    compl_status = JasonobjectCompl.getInt("status");
                                    compl_track_content = String.valueOf(JasonobjectCompl.getInt("track_course"));
                                    user_id = JasonobjectCompl.getString("user_id");
                                    compl_users_to_courseid = JasonobjectCompl.getString("id");
                                    compl_duration = JasonobjectCompl.getString("course_duration");
                                    compl_no_of_que = JasonobjectCompl.getString("no_of_que");
                                    compl_pass_percent = JasonobjectCompl.getString("pass_percent");
                                    compl_test_mandatory = JasonobjectCompl.getString("test_mandatory");
                                    compl_startedd = JasonobjectCompl.getInt("started");
                                    compl_completed = JasonobjectCompl.getInt("completed");
                                    compl_completion_date = JasonobjectCompl.getString("completion_date");

                                    //insert completed courses into sqlite

                                    db1.addCompletedCourses(new Elpfa("completed", compl_course_id, compl_users_to_courseid, compl_name, compl_desc, compl_duration, compl_enrollment_key, compl_status,compl_path, compl_startdate, compl_track_content, compl_no_of_que, compl_pass_percent, compl_test_mandatory, compl_startedd, compl_completed, compl_completion_date));


                                }


                            }

                            // end fetch all  completed courses


                            /******** fetch all scheduled courses ***********/


                            db1.deleteScheduledCoursesTable();
                            if (!jObject.isNull("scheduled_courses")) {
                                JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                                final int scheduled_length = jsonArrayScheduledCourses.length();

                                for (int i = 0; i < scheduled_length; i++) {
                                    JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                                    scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");

                                    scheduled_name = JasonobjectScheduled.getString("name");
                                    scheduled_desc = JasonobjectScheduled.getString("description");
                                    scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                                    scheduled_course_id = JasonobjectScheduled.getString("course_id");
                                    scheduled_enrollment_key = JasonobjectScheduled.getInt("enrollment_key");
                                    scheduled_status = JasonobjectScheduled.getInt("status");
                                    scheduled_track_content = String.valueOf(JasonobjectScheduled.getInt("track_course"));
                                    user_id = JasonobjectScheduled.getString("user_id");
                                    scheduled_users_to_courseid = JasonobjectScheduled.getString("id");
                                    scheduled_duration = JasonobjectScheduled.getString("course_duration");
                                    scheduled_no_of_que = JasonobjectScheduled.getString("no_of_que");
                                    scheduled_pass_percent = JasonobjectScheduled.getString("pass_percent");
                                    scheduled_test_mandatory = JasonobjectScheduled.getString("test_mandatory");
                                    scheduled_startedd = JasonobjectScheduled.getInt("started");
                                    scheduled_completed = JasonobjectScheduled.getInt("completed");
                                    scheduled_completion_date = JasonobjectScheduled.getString("completion_date");

                                    //insert completed courses into sqlite

                                    db1.addScheduledCourses(new Elpfa("scheduled", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status,scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));


                                }


                            }

                            /**********  end fetch all  completed courses ***********/


                            // end fetch all in progress courses

                            /******** fetch all in progress courses ***********/


                            db1.deleteInProgressCoursesTable();
                            if (!jObject.isNull("progress_courses")) {
                                JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                                final int progress_length = jsonArrayInProgressCourses.length();

                                for (int i = 0; i < progress_length; i++) {
                                    JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                                    progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");

                                    progress_name = JasonobjectProgress.getString("name");
                                    progress_desc = JasonobjectProgress.getString("description");
                                    progress_startdate = JasonobjectProgress.getString("assign_date");
                                    progress_course_id = JasonobjectProgress.getString("course_id");
                                    progress_enrollment_key = JasonobjectProgress.getInt("enrollment_key");
                                    progress_status = JasonobjectProgress.getInt("status");
                                    progress_track_content = String.valueOf(JasonobjectProgress.getInt("track_course"));
                                    user_id = JasonobjectProgress.getString("user_id");
                                    progress_users_to_courseid = JasonobjectProgress.getString("id");
                                    progress_duration = JasonobjectProgress.getString("course_duration");
                                    progress_no_of_que = JasonobjectProgress.getString("no_of_que");
                                    progress_pass_percent = JasonobjectProgress.getString("pass_percent");
                                    progress_test_mandatory = JasonobjectProgress.getString("test_mandatory");
                                    progress_startedd = JasonobjectProgress.getInt("started");
                                    progress_completed = JasonobjectProgress.getInt("completed");
                                    progress_completion_date = JasonobjectProgress.getString("completion_date");

                                    //insert completed courses into sqlite

                                    db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status,progress_path, progress_startdate, progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory, progress_startedd, progress_completed, progress_completion_date));


                                }

                            }

                            /**********  end fetch all  in progress courses ***********/



                            /******** fetch all courses ***********/


                            db1.deleteAllCoursesTable();
                            db1.deleteToTable();
                            if (!jObject.isNull("all_courses")) {
                                JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                                final int progress_length = jsonArrayAllCourses.length();

                                for (int i = 0; i < progress_length; i++) {
                                    JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                                    all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                                    all_name = JasonobjectAll.getString("name");
                                    all_desc = JasonobjectAll.getString("description");
                                    all_startdate = JasonobjectAll.getString("assign_date");
                                    all_course_id = JasonobjectAll.getString("course_id");
                                    all_enrollment_key = JasonobjectAll.getInt("enrollment_key");
                                    all_status = JasonobjectAll.getInt("status");
                                    all_track_content = String.valueOf(JasonobjectAll.getInt("track_course"));
                                    user_id = JasonobjectAll.getString("user_id");
                                    all_users_to_courseid = JasonobjectAll.getString("id");
                                    all_duration = JasonobjectAll.getString("course_duration");
                                    all_no_of_que = JasonobjectAll.getString("no_of_que");
                                    all_pass_percent = JasonobjectAll.getString("pass_percent");
                                    all_test_mandatory = JasonobjectAll.getString("test_mandatory");
                                    all_startedd = JasonobjectAll.getInt("started");
                                    all_completed = JasonobjectAll.getInt("completed");
                                    all_completion_date = JasonobjectAll.getString("completion_date");

                                    //insert completed courses into sqlite

                                    db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status,all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory, all_startedd, all_completed, all_completion_date));
                                    db1.addTo(new Elpfa("to",all_course_id,all_name,"no_email","course"));


                                }
                            }

                            /**********  end fetch all  completed courses ***********/



                            /**********  end fetch notifications ***********/
//msg inbx
                            Config url_login1 = new Config();

                    /* using volley in login - combine everything */
                            String tag_string_req1 = "req_state1";
                            StringRequest strReq1 = new StringRequest(Request.Method.POST,
                                    url_login1.FetchMessages, new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("MessageReceiver", response);
                                    editor.commit();
                                    try {

                                        try {

                                            sharedpreferences =  getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                            try {

                                                DatabaseHandler db1 = new DatabaseHandler( getActivity());

                                                jObject1 = new JSONObject(response);

                                                //enroll contents
                                                db1.deleteEnrollContents();
                                                if (!jObject.isNull("enroll_contents")) {
                                                    JSONArray jsonArrayEnrollContents = jObject.getJSONArray("enroll_contents");
                                                    int enroll_contents_length = jsonArrayEnrollContents.length();
                                                    for (int i = 0; i < enroll_contents_length; i++) {
                                                        JSONObject JasonobjectEnrollContents = jsonArrayEnrollContents.getJSONObject(i);
                                                        {
                                                            enroll_contents_course_id = JasonobjectEnrollContents.getString("course_id");
                                                            enroll_contents_name = JasonobjectEnrollContents.getString("name");
                                                            db1.addEnrollContents(new Elpfa("enroll_contents", enroll_contents_course_id, enroll_contents_name));

                                                        }

                                                    }



                                                    /**********  end fetch team members ***********/

                                                    /******** fetch all contents ***********/
                                                    db1.deleteUsersToContentsTable();
                                                    if (!jObject.isNull("user_contents")) {
                                                        contents_course_id_arr.clear();
                                                        contents_usersToCourse_id_arr.clear();
                                                        contents_viewed_arr.clear();
                                                        contents_completed_arr.clear();
                                                        contents_type_arr.clear();
                                                        contents_name_arr.clear();
                                                        contents_path_arr.clear();

                                                        JSONArray jsonArrayContents = jObject.getJSONArray("user_contents");
                                                        int contents_length = jsonArrayContents.length();
                                                        for (int i = 0; i < contents_length; i++) {
                                                            JSONObject JasonobjectContents = jsonArrayContents.getJSONObject(i);
                                                            contents_course_id = JasonobjectContents.getInt("course_id");
                                                            contents_users_to_course_id = JasonobjectContents.getInt("id");
                                                            contents_completed = JasonobjectContents.getInt("status");
                                                            if (contents_completed == 0) {
                                                                contents_viewed = 0;
                                                            } else {
                                                                contents_viewed = 1;
                                                            }

                                                            contents_type = JasonobjectContents.getString("item_name");
                                                            contents_name = JasonobjectContents.getString("name");
                                                            if (contents_type.equals("youtube")) {
                                                                contents_path = JasonobjectContents.getString("path");
                                                            } else {
                                                                contents_path = Config.BASE_CONTENT_URL + JasonobjectContents.getString("path");
                                                            }

                                                            contents_course_id_arr.add(i, contents_course_id);
                                                            contents_usersToCourse_id_arr.add(i, contents_users_to_course_id);
                                                            contents_viewed_arr.add(i, contents_viewed);
                                                            contents_completed_arr.add(i, contents_completed);
                                                            contents_type_arr.add(i, contents_type);
                                                            contents_name_arr.add(i, contents_name);
                                                            contents_path_arr.add(i, contents_path);


                                                            //insert team members into sqlite
                                                            db1.addUsersToContents(new Elpfa(contents_course_id, contents_users_to_course_id, contents_viewed, contents_completed, contents_type, contents_name, contents_path));


                                                        }
                                                    }
                                                }


                                            } catch (Exception e) {
                                                onVolleyResponse(isAllAvilable);
                                            }
//

                                        } catch (Exception e) {
                                            onVolleyResponse(isAllAvilable);

                                        }

                                        isAllAvilable = true;
                                        onVolleyResponse(isAllAvilable);
                                    } catch (Exception e) {
                                        onVolleyResponse(isAllAvilable);
                                    }

                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    onVolleyResponse(isAllAvilable);
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    // Posting parameters to login url
                                    Map<String, String> params = new HashMap<String, String>();
                                    sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    params.put("value1", sharedpreferences.getString("User_id", null));
                                    return params;
                                }

                            };
                            strReq1.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req1);

//end msg inbx

//end msg inbx


                        } catch (Exception e) {
                            onVolleyResponse(isAllAvilable);
                        }
//

                    } catch (Exception e) {

                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("value1", sharedpreferences.getString("Username", null));
                params.put("value2", sharedpreferences.getString("Password", null));
                return params;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    } catch (Exception ex) {
    }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onVolleyResponse(boolean isSuccess) {
        if (isSuccess){
            layoutProgressbar.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            setupViewPager(mViewPager);
        }
        else {
            layoutProgressbar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager fm = getChildFragmentManager();
        ViewPagerAdapter adapter = new ViewPagerAdapter(fm);
        adapter.addFragment(new AllCourses(), "All");
        adapter.addFragment(new CompletedCourses(), "Completed");
        adapter.addFragment(new InProgressCourses(), "InProgress");
        adapter.addFragment(new ScheduledCourses(), "Scheduled");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragmentList = new ArrayList<>();
        private List<String> fragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_course, menu);
//        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
//         searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
//        super.onCreateOptionsMenu(menu, inflater);
//    }
    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
//            ((AppCompatActivity)getActivity()).invalidateOptionsMenu();
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void parseResponse(String response) throws JSONException {
        JSONObject jObject = null;
        try {
            jObject = new JSONObject(response);
            DatabaseHandler db1 = new DatabaseHandler(getActivity());
            db1.deleteCourseReportsTable();
            //fetch number of assigned courses
            if (!jObject.isNull("number_assigned_courses")) {

                JSONArray jsonArrayAssigned = jObject.getJSONArray("number_assigned_courses");
                JSONObject Jasonobject = jsonArrayAssigned.getJSONObject(0);
                number_assigned = Jasonobject.getString("count(*)");

                editor.putString("assigned", number_assigned);
                editor.commit();
            }


//
            //fetch number of started courses
            if (!jObject.isNull("number_started_courses")) {

                JSONArray jsonArrayStarted = jObject.getJSONArray("number_started_courses");
                JSONObject JasonobjectStarted = jsonArrayStarted.getJSONObject(0);
                number_started = JasonobjectStarted.getString("count(*)");

                editor.putString("started", number_started);
                editor.commit();
            }
            //fetch number of not-started courses
            if (!jObject.isNull("number_not_started_courses")) {

                JSONArray jsonArrayNotStarted = jObject.getJSONArray("number_not_started_courses");

                JSONObject JasonobjectNotStarted = jsonArrayNotStarted.getJSONObject(0);
                number_not_started = JasonobjectNotStarted.getString("count(*)");

                editor.putString("not_started", number_not_started);
                editor.commit();

            }
            //fetch number of completed courses
            if (!jObject.isNull("number_completed_courses")) {

                JSONArray jsonArrayCompleted = jObject.getJSONArray("number_completed_courses");
                JSONObject JasonobjectCompleted = jsonArrayCompleted.getJSONObject(0);
                number_completed = JasonobjectCompleted.getString("count(*)");

                editor.putString("completed", number_completed);
                editor.commit();
            }

                                            /* add to course reports table */
            db1.addCourseReports(new Elpfa("report", number_assigned, number_started, number_not_started, number_completed));

            //fetch all  courses

            editor.putString("json_String_scheduled", response);
            editor.putString("json_String_progress", response);
            editor.putString("json_String_completed", response);
            editor.putString("json_String_all", response);
            editor.commit();

            //get all completed courses
            db1.deleteCompletedCoursesTable();
            if (!jObject.isNull("completed_courses")) {
                JSONArray jsonArrayCompletedCourses = jObject.getJSONArray("completed_courses");

                final int completed_length = jsonArrayCompletedCourses.length();

                for (int i = 0; i < completed_length; i++) {
                    JSONObject JasonobjectCompl = jsonArrayCompletedCourses.getJSONObject(i);
                    compl_path = Config.BASE_CONTENT_URL + JasonobjectCompl.getString("path");

                    compl_name = JasonobjectCompl.getString("name");
                    compl_desc = JasonobjectCompl.getString("description");
                    compl_startdate = JasonobjectCompl.getString("assign_date");
                    compl_course_id = JasonobjectCompl.getString("course_id");
                    compl_enrollment_key = JasonobjectCompl.getInt("enrollment_key");
                    compl_status = JasonobjectCompl.getInt("status");
                    compl_track_content = String.valueOf(JasonobjectCompl.getInt("track_course"));
                    user_id = JasonobjectCompl.getString("user_id");
                    compl_users_to_courseid = JasonobjectCompl.getString("id");
                    compl_duration = JasonobjectCompl.getString("course_duration");
                    compl_no_of_que = JasonobjectCompl.getString("no_of_que");
                    compl_pass_percent = JasonobjectCompl.getString("pass_percent");
                    compl_test_mandatory = JasonobjectCompl.getString("test_mandatory");
                    compl_startedd = JasonobjectCompl.getInt("started");
                    compl_completed = JasonobjectCompl.getInt("completed");
                    compl_completion_date = JasonobjectCompl.getString("completion_date");

                    //insert completed courses into sqlite

                    db1.addCompletedCourses(new Elpfa("completed", compl_course_id, compl_users_to_courseid, compl_name, compl_desc, compl_duration, compl_enrollment_key, compl_status, compl_path, compl_startdate, compl_track_content, compl_no_of_que, compl_pass_percent, compl_test_mandatory, compl_startedd, compl_completed, compl_completion_date));


                }


            }


            db1.deleteScheduledCoursesTable();
            if (!jObject.isNull("scheduled_courses")) {
                JSONArray jsonArrayScheduledCourses = jObject.getJSONArray("scheduled_courses");

                final int scheduled_length = jsonArrayScheduledCourses.length();

                for (int i = 0; i < scheduled_length; i++) {
                    JSONObject JasonobjectScheduled = jsonArrayScheduledCourses.getJSONObject(i);
                    scheduled_path = Config.BASE_CONTENT_URL + JasonobjectScheduled.getString("path");

                    scheduled_name = JasonobjectScheduled.getString("name");
                    scheduled_desc = JasonobjectScheduled.getString("description");
                    scheduled_startdate = JasonobjectScheduled.getString("assign_date");
                    scheduled_course_id = JasonobjectScheduled.getString("course_id");
                    scheduled_enrollment_key = JasonobjectScheduled.getInt("enrollment_key");
                    scheduled_status = JasonobjectScheduled.getInt("status");
                    scheduled_track_content = String.valueOf(JasonobjectScheduled.getInt("track_course"));
                    user_id = JasonobjectScheduled.getString("user_id");
                    scheduled_users_to_courseid = JasonobjectScheduled.getString("id");
                    scheduled_duration = JasonobjectScheduled.getString("course_duration");
                    scheduled_no_of_que = JasonobjectScheduled.getString("no_of_que");
                    scheduled_pass_percent = JasonobjectScheduled.getString("pass_percent");
                    scheduled_test_mandatory = JasonobjectScheduled.getString("test_mandatory");
                    scheduled_startedd = JasonobjectScheduled.getInt("started");
                    scheduled_completed = JasonobjectScheduled.getInt("completed");
                    scheduled_completion_date = JasonobjectScheduled.getString("completion_date");

                    //insert completed courses into sqlite

                    db1.addScheduledCourses(new Elpfa("scheduled", scheduled_course_id, scheduled_users_to_courseid, scheduled_name, scheduled_desc, scheduled_duration, scheduled_enrollment_key, scheduled_status, scheduled_path, scheduled_startdate, scheduled_track_content, scheduled_no_of_que, scheduled_pass_percent, scheduled_test_mandatory, scheduled_startedd, scheduled_completed, scheduled_completion_date));


                }


            }


            db1.deleteInProgressCoursesTable();
            if (!jObject.isNull("progress_courses")) {
                JSONArray jsonArrayInProgressCourses = jObject.getJSONArray("progress_courses");

                final int progress_length = jsonArrayInProgressCourses.length();

                for (int i = 0; i < progress_length; i++) {
                    JSONObject JasonobjectProgress = jsonArrayInProgressCourses.getJSONObject(i);
                    progress_path = Config.BASE_CONTENT_URL + JasonobjectProgress.getString("path");

                    progress_name = JasonobjectProgress.getString("name");
                    progress_desc = JasonobjectProgress.getString("description");
                    progress_startdate = JasonobjectProgress.getString("assign_date");
                    progress_course_id = JasonobjectProgress.getString("course_id");
                    progress_enrollment_key = JasonobjectProgress.getInt("enrollment_key");
                    progress_status = JasonobjectProgress.getInt("status");
                    progress_track_content = String.valueOf(JasonobjectProgress.getInt("track_course"));
                    user_id = JasonobjectProgress.getString("user_id");
                    progress_users_to_courseid = JasonobjectProgress.getString("id");
                    progress_duration = JasonobjectProgress.getString("course_duration");
                    progress_no_of_que = JasonobjectProgress.getString("no_of_que");
                    progress_pass_percent = JasonobjectProgress.getString("pass_percent");
                    progress_test_mandatory = JasonobjectProgress.getString("test_mandatory");
                    progress_startedd = JasonobjectProgress.getInt("started");
                    progress_completed = JasonobjectProgress.getInt("completed");
                    progress_completion_date = JasonobjectProgress.getString("completion_date");

                    //insert completed courses into sqlite

                    db1.addInProgressCourses(new Elpfa("in_progress", progress_course_id, progress_users_to_courseid, progress_name, progress_desc, progress_duration, progress_enrollment_key, progress_status, progress_path, progress_startdate, progress_track_content, progress_no_of_que, progress_pass_percent, progress_test_mandatory, progress_startedd, progress_completed, progress_completion_date));


                }

            }

            /**********  end fetch all  in progress courses ***********/


            /******** fetch all courses ***********/


            db1.deleteAllCoursesTable();
            db1.deleteToTable();
            if (!jObject.isNull("all_courses")) {
                JSONArray jsonArrayAllCourses = jObject.getJSONArray("all_courses");

                final int progress_length = jsonArrayAllCourses.length();

                for (int i = 0; i < progress_length; i++) {
                    JSONObject JasonobjectAll = jsonArrayAllCourses.getJSONObject(i);
                    all_path = Config.BASE_CONTENT_URL + JasonobjectAll.getString("path");
                    all_name = JasonobjectAll.getString("name");
                    all_desc = JasonobjectAll.getString("description");
                    all_startdate = JasonobjectAll.getString("assign_date");
                    all_course_id = JasonobjectAll.getString("course_id");
                    all_enrollment_key = JasonobjectAll.getInt("enrollment_key");
                    all_status = JasonobjectAll.getInt("status");
                    all_track_content = String.valueOf(JasonobjectAll.getInt("track_course"));
                    user_id = JasonobjectAll.getString("user_id");
                    all_users_to_courseid = JasonobjectAll.getString("id");
                    all_duration = JasonobjectAll.getString("course_duration");
                    all_no_of_que = JasonobjectAll.getString("no_of_que");
                    all_pass_percent = JasonobjectAll.getString("pass_percent");
                    all_test_mandatory = JasonobjectAll.getString("test_mandatory");
                    all_startedd = JasonobjectAll.getInt("started");
                    all_completed = JasonobjectAll.getInt("completed");
                    all_completion_date = JasonobjectAll.getString("completion_date");

                    //insert completed courses into sqlite

                    db1.addAllCourses(new Elpfa("all", all_course_id, all_users_to_courseid, all_name, all_desc, all_duration, all_enrollment_key, all_status, all_path, all_startdate, all_track_content, all_no_of_que, all_pass_percent, all_test_mandatory, all_startedd, all_completed, all_completion_date));
                    db1.addTo(new Elpfa("to", all_course_id, all_name, "no_email", "course"));


                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
