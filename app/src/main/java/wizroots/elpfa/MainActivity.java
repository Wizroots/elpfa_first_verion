package wizroots.elpfa;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,FragmentDashboard.OnFragmentInteractionListener,TeamChatFragment.OnFragmentInteractionListener,TeamInfoFragment.OnFragmentInteractionListener,Teams.OnFragmentInteractionListener,CoursesFragment.OnFragmentInteractionListener,AllCourses.OnFragmentInteractionListener,CompletedCourses.OnFragmentInteractionListener,Settings.OnFragmentInteractionListener,InProgressCourses.OnFragmentInteractionListener,ScheduledCourses.OnFragmentInteractionListener,Webinars.OnFragmentInteractionListener,UpcomingWebinars.OnFragmentInteractionListener,RecentWebinars.OnFragmentInteractionListener,Calendar.OnFragmentInteractionListener{
    android.support.v4.app.Fragment fragment;
    SharedPreferences sharedpreferences;
    public  final String MyPREFERENCES = "MyPrefs" ;
    private FirebaseAnalytics mFirebaseAnalytics;
    RelativeLayout header;
    int counter=0;
    DrawerLayout drawer;
//    BroadcastReceiver broadcastReceiver;
    NavigationView navigationView;
    SmoothActionBarDrawerToggle toggle;
    ImageView header_img;
    TextView name,email;
    String profile_image_color;
    TextView first_letter;
    RelativeLayout profile_color_text;
    int nav_dash=0;
    JSONObject jObject;
int id;


//    /* server sync variables */
//    String user_details_id, user_details_name, user_details_email, user_details_photo, user_details_password, user_details_type,user_details_phone, user_details_status, user_details_company_name, user_details_company_address, user_details_registration;
//    String user_type_id, user_type_type, user_type_status;
//    String team_id, team_name, team_path, team_description;
//    String number_started, number_assigned, number_not_started, number_completed;
//    String compl_path, compl_name, compl_desc, compl_startdate, compl_course_id, compl_track_content, compl_users_to_courseid, compl_completion_date;
//    String scheduled_path, scheduled_name, scheduled_desc, scheduled_startdate, scheduled_course_id, scheduled_track_content, scheduled_users_to_courseid, scheduled_completion_date;
//    String progress_path, progress_name, progress_desc, progress_startdate, progress_course_id, progress_track_content, progress_users_to_courseid, progress_completion_date;
//    String all_path, all_name, all_desc, all_startdate, all_course_id, all_track_content, all_users_to_courseid, all_completion_date;
//    String notification, notif_title, notif_date;
//    int notif_status;
//    String notif_id;
//    int scheduled_completed, progress_completed, all_completed, compl_completed;
//    String compl_duration, compl_pass_percent, compl_test_mandatory, compl_no_of_que;
//    int compl_startedd, scheduled_startedd, progress_startedd, all_startedd;
//
//    String scheduled_duration, scheduled_pass_percent, scheduled_test_mandatory, scheduled_no_of_que;
//    String progress_duration, progress_pass_percent, progress_test_mandatory, progress_no_of_que;
//    String all_duration, all_pass_percent, all_test_mandatory, all_no_of_que;
//    int compl_enrollment_key, scheduled_enrollment_key, progress_enrollment_key, all_enrollment_key;
//    String user_id, userid;
//
//    String recent_webinar_name, recent_webinar_agenda, recent_webinar_starts_at, recent_webinar_ends_at, recent_webinar_duration, recent_webinar_room_url;
//    String upcoming_webinar_name, upcoming_webinar_agenda, upcoming_webinar_starts_at, upcoming_webinar_ends_at, upcoming_webinar_duration, upcoming_webinar_room_url;
//
//    ArrayList<Integer> arr_notifications_status = new ArrayList<>();
//    ArrayList<String> arr_notifications_title = new ArrayList<>();
//    ArrayList<String> arr_notifications = new ArrayList<>();
//    ArrayList<String> arr_notifications_id = new ArrayList<>();
//    ArrayList<String> arr_notifications_date = new ArrayList<>();
//
//    JSONObject jObject1;
//
//    String teamchat_username;
//    String teamchat_imagepath;
//    String teamchat_messages;
//    String teamchat_createdat;
//    int teamchat_teamid;
//    int contents_course_id;
//    int contents_users_to_course_id;
//    int contents_viewed;
//    int contents_completed;
//    String contents_type;
//    String contents_name;
//    String contents_path;
//    int teamchat_chatid;
//    ArrayList<Integer> teammembers_teamid_arr = new ArrayList<>();
//    ArrayList<String> teammembers_name_arr = new ArrayList<>();
//    ArrayList<String> teammembers_photo_arr = new ArrayList<>();
//
//    ArrayList<Integer> contents_course_id_arr = new ArrayList<>();
//    ArrayList<Integer> contents_usersToCourse_id_arr = new ArrayList<>();
//    ArrayList<Integer> contents_viewed_arr = new ArrayList<>();
//    ArrayList<Integer> contents_completed_arr = new ArrayList<>();
//    ArrayList<String> contents_type_arr = new ArrayList<>();
//    ArrayList<String> contents_name_arr = new ArrayList<>();
//    ArrayList<String> contents_path_arr = new ArrayList<>();
//    ArrayList<Integer> teamchat_id_arr = new ArrayList<>();
//    ArrayList<Integer> teamchat_teamid_arr = new ArrayList<>();
//    ArrayList<String> teamchat_username_arr = new ArrayList<>();
//    ArrayList<String> teamchat_imagepath_arr = new ArrayList<>();
//    ArrayList<String> teamchat_messages_arr = new ArrayList<>();
//    ArrayList<String> teamchat_createdat_arr = new ArrayList<>();
//
//    int team_members_teamid;
//    String team_members_name;
//    String team_members_photo;
//    /* end server sync variables */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        header=(RelativeLayout)findViewById(R.id.header);
//        header.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(),"clicked",Toast.LENGTH_SHORT).show();
//            }
//        });
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
// Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

     /*   DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new SmoothActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        final View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        header_img = (ImageView)headerLayout.findViewById(R.id.header_img);
        profile_color_text = (RelativeLayout) headerLayout.findViewById(R.id.profile_color_text);
        name = (TextView) headerLayout.findViewById(R.id.name);
        email = (TextView) headerLayout.findViewById(R.id.email);
        first_letter = (TextView) headerLayout.findViewById(R.id.first_letter);
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {
            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                header_img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {
            profile_color_text.setVisibility(View.INVISIBLE);
            header_img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(header_img);
        }
        name.setText(""+sharedpreferences.getString("person_name",null));
        email.setText(""+sharedpreferences.getString("Username",null));
        header_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(),"img clicked",Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(getApplicationContext(), Profile.class);
//// Pass data object in the bundle and populate details activity.
//                ActivityOptionsCompat options = ActivityOptionsCompat.
//                        makeSceneTransitionAnimation(MainActivity.this, (View)header_img, "profile");
//                startActivity(intent, options.toBundle());


                Intent intent = new Intent(getApplicationContext(), Profile.class);
                Pair<View, String> p1 = Pair.create((View)header_img, "profile");
                Pair<View, String> p2 = Pair.create((View)first_letter.findViewById(R.id.first_letter), "first_letter");
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(MainActivity.this, p1, p2);
                startActivity(intent, options.toBundle());



//                Intent intent = new Intent(getApplicationContext(), Profile.class);
//                Pair<View, String> p1 = Pair.create((View)header_img, "profile");
//                Pair<View, String> p2 = Pair.create((View)findViewById(R.id.root1), "root_transition");
//                ActivityOptionsCompat options = ActivityOptionsCompat.
//                        makeSceneTransitionAnimation(MainActivity.this, p1, p2);
//                startActivity(intent, options.toBundle());
            }
        });
//        broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                Log.d("check_syncing","in broadcast receiver create");
//
//
////                loadData1();
////                prepareDate();
//            }
//        };
        navigationView.setNavigationItemSelectedListener(this);
        loadDashboard();
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (nav_dash==0)
            {
                if (counter == 0) {
                    Toast.makeText(getApplicationContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
                    counter++;
                } else {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    if (android.os.Build.VERSION.SDK_INT >= 21) {
                        finishAndRemoveTask();
                    } else {
                        System.exit(0);
                        finish();
                    }
                }
            }
            else
            {
                navigationView.getMenu().getItem(0).setChecked(true);
                loadDashboard();
                nav_dash=0;
            }

        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
       id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (id == R.id.nav_dashboard) {
//            getSupportActionBar().show();
                    // Handle the camera action
                    loadDashboard();
                } else if (id == R.id.nav_courses) {
                    nav_dash=1;

//            toggle.runWhenIdle(new Runnable() {
//                @Override
//                public void run() {
                    fragment = new CoursesFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();
//                    getSupportActionBar().setTitle("Courses");

//                }
//            });


                } else if (id == R.id.nav_webinars) {
                    nav_dash=1;
                    fragment = new Webinars();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();

                }
                else if (id == R.id.nav_teams) {
                    nav_dash=1;
                    fragment = new Teams();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();

                }

                else if (id == R.id.nav_messages) {
                    nav_dash=1;
                   fragment = new MessageListFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();

                }
                else if (id == R.id.nav_calendar) {
                    nav_dash=1;
                    fragment = new Calendar();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();
                } else if (id == R.id.nav_settings) {
                    nav_dash=1;
                    fragment = new Settings();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_main, fragment);
                    fragmentTransaction.commit();
//            getSupportActionBar().setTitle("Settings");

                }else if (id == R.id.nav_logout) {
                    nav_dash=1;
                    logout();

                }
                else
                {

                    loadDashboard();
                }

            }
        }, 280);


        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    public void loadDashboard()
    {
        navigationView.getMenu().getItem(0).setChecked(true);
        nav_dash=0;
        fragment = new FragmentDashboard();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, fragment);
        fragmentTransaction.commit();
    }
    public void logout()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you really want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.remove("Username");
                editor.remove("Password");
                editor.remove("User_id");
                editor.remove("Local_name");
                editor.putString("push", "off");
                editor.commit();
                final ProgressDialog pd = new ProgressDialog(MainActivity.this);
                pd.setMessage("Please wait...");
                pd.show();

                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Intent i1 = new Intent(getBaseContext(), Login.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        i1.putExtra("flag", 2);
                        startActivity(i1);
                        finish();
                        pd.cancel();

                    }
                }, 1000);


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.rgb(1, 22, 39));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.rgb(1, 22, 39));

    }
    private class SmoothActionBarDrawerToggle extends ActionBarDrawerToggle {

        private Runnable runnable;

        public SmoothActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            invalidateOptionsMenu();
        }
        @Override
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
            invalidateOptionsMenu();
        }
        @Override
        public void onDrawerStateChanged(int newState) {
            super.onDrawerStateChanged(newState);
            if (runnable != null && newState == DrawerLayout.STATE_IDLE) {
                runnable.run();
                runnable = null;
            }
        }

        public void runWhenIdle(Runnable runnable) {
            this.runnable = runnable;
        }
    }
    public  void OpenPath(String path)
    {
        Uri uri = Uri.parse(path);

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        Toast.makeText(getApplicationContext(),""+path,Toast.LENGTH_SHORT).show();
    }
    public  void watch_video(String path)
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("No recorded video available");
        builder1.setCancelable(true);
        builder1.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void updateHeaderImage()
    {
        name.setText(""+sharedpreferences.getString("person_name",null));
        email.setText(""+sharedpreferences.getString("Username",null));
        String first_txt=sharedpreferences.getString("person_name",null).substring(0,1);
        first_letter.setText(""+first_txt.toUpperCase());
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {
            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                header_img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                header_img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {
            profile_color_text.setVisibility(View.INVISIBLE);
            header_img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(header_img);
        }

    }
    public void lock_navigation()
    {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }
    public void open_navigation()
    {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }


}
