package wizroots.elpfa;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by reflective2 on 07-10-2016.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.MyViewHolder> {

    private List<Lessons> lessonList;
    Context context;
    int completed;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView lesson_name;
        public ImageView lesson_type_image;
        public com.vipul.hp_hp.timelineview.TimelineView marker;

        public MyViewHolder(View view) {
            super(view);
            lesson_name = (TextView) view.findViewById(R.id.lesson_name);
            lesson_type_image = (ImageView) view.findViewById(R.id.lesson_type_image);
            marker = (com.vipul.hp_hp.timelineview.TimelineView) view.findViewById(R.id.marker);

        }
    }


    public LessonAdapter(List<Lessons> lessonList) {
        this.lessonList = lessonList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lesson_list_row, parent, false);
        context=parent.getContext();


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Lessons content = lessonList.get(position);
        holder.lesson_name.setText(content.getLesson_name());
        if (content.getLesson_type().equals("Text"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.text);
        }
        else if (content.getLesson_type().equals("Video"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.video);
        }
        else  if (content.getLesson_type().equals("Link"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.link);
        }
        else if (content.getLesson_type().equals("Gallery"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.gallery);
        }
        else if (content.getLesson_type().equals("Test"))
        {
            holder.lesson_type_image.setImageResource(R.drawable.test);
        }

        completed=content.getLesson_completed();
        if (completed==0)
            holder.marker.setMarker(ContextCompat.getDrawable(context, R.drawable.circular_bk_transparent_blue_border));
        else if (completed==1)
            holder.marker.setMarker(ContextCompat.getDrawable(context, R.drawable.circular_bk_blue));
//        holder.lesson_type_image.setText(content.getLesson_name());
//        Picasso.with(holder.content_tick_img.getContext()).load(R.drawable.tick_course).into(holder.content_tick_img);

    }

    @Override
    public int getItemCount() {
        return lessonList.size();
    }
}