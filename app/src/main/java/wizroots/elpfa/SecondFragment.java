package wizroots.elpfa;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class SecondFragment extends FirstFragment {
    ImageView brain;
    ImageView img1;
    private RelativeLayout layoutContainer;
    Animation zoom_in;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_second, container, false);

        layoutContainer = (RelativeLayout) view.findViewById(R.id.main);
        brain = (ImageView) view.findViewById(R.id.brain);
         zoom_in = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_in);
        zoom_in.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                zoom_in.setAnimationListener(this);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img1 = (ImageView)view.findViewById(R.id.img1);
        zoom_in.setFillAfter(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    brain.startAnimation(zoom_in);
            }
        }, 2000);
        zoom_in.setFillAfter(true);
        return view;
    }

    @Override
    public void setBackgroundColor(@ColorInt int backgroundColor) {
        if (layoutContainer != null) {
            layoutContainer.setBackgroundColor(backgroundColor);
            Log.d("background_color","1");
        }
    }
    @Override
    public int getDefaultBackgroundColor() {
        return Color.parseColor("#9B59B6");
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
