package wizroots.elpfa;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Profile extends AppCompatActivity {
    private Toolbar mToolbar;
    View decorView;
    de.hdodenhof.circleimageview.CircleImageView img;
    SharedPreferences sharedpreferences;
    public  final String MyPREFERENCES = "MyPrefs" ;
    TextView name_txt,email_txt,phone_txt,company_txt;
    DatabaseHandler db1;
    String name;
    ImageView full;
    Animation layout_anim;
    String profile_image_color;
    TextView first_letter;
    String email;
    String phone;
    RelativeLayout profile_color_text;
    String company_name;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* decorView = getWindow().getDecorView();
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);                        ;
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(Color.parseColor("#5a000000"));
            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
        }*/
        setContentView(R.layout.activity_profile);
        layout_anim= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.out_animation);

        layout_anim.setFillAfter(true);
        Transition fade = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            fade = new Fade();fade.excludeTarget(android.R.id.statusBarBackground, true);
            fade.excludeTarget(android.R.id.navigationBarBackground, true);
            fade.excludeTarget(R.id.root1, true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setExitTransition(fade);
                getWindow().setEnterTransition(fade);
            }

        }

        findViewById(R.id.animate).startAnimation(layout_anim);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        first_letter = (TextView) findViewById(R.id.first_letter);
        profile_color_text=(RelativeLayout) findViewById(R.id.profile_color_text);
        img=(de.hdodenhof.circleimageview.CircleImageView)findViewById(R.id.profile_img);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String first_txt=sharedpreferences.getString("person_name",null).substring(0,1);
        first_letter.setText(""+first_txt);
        findViewById(R.id.profile_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(Profile.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.custom_fullimage_dialog);
                full = (ImageView) dialog1.findViewById(R.id.list);
                String path=sharedpreferences.getString("photo","no_img");
                if (path.equals("no_img"))
                {
                    Toast.makeText(getApplicationContext(),"No picture",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    profile_color_text.setVisibility(View.INVISIBLE);
                    img.setColorFilter(getResources().getColor(R.color.transparent));
                    Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(full);
                }

                dialog1.show();
            }
        });
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {
            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {
            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(img);
        }
        name_txt=(TextView)findViewById(R.id.name_txt);
        email_txt=(TextView)findViewById(R.id.email_txt);
        phone_txt=(TextView)findViewById(R.id.phone_txt);
        company_txt=(TextView)findViewById(R.id.company_txt);
        db1=new DatabaseHandler(getApplicationContext());
       setDetails();
        mToolbar.setNavigationIcon(R.drawable.back_white);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             onBackPressed();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_edit:
                // edit item was selected
                Intent i=new Intent(getApplicationContext(),EditProfile.class);

//                ActivityOptionsCompat options = ActivityOptionsCompat.
//                        makeSceneTransitionAnimation(Profile.this, (View)img, "profile");
//                startActivity(i,options.toBundle());
                startActivity(i);
                return true;
            case R.id.nav_logout:
                // logout item was selected
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void logout()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you really want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.remove("Username");
                editor.remove("Password");
                editor.remove("User_id");
                editor.remove("Local_name");
                editor.putString("push", "off");
                editor.commit();
                final ProgressDialog pd = new ProgressDialog(Profile.this);
                pd.setMessage("Please wait...");
                pd.show();

                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Intent i1 = new Intent(getBaseContext(), Login.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        i1.putExtra("flag", 2);
                        startActivity(i1);
                        finish();
                        pd.cancel();

                    }
                }, 1000);


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.rgb(1, 22, 39));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.rgb(1, 22, 39));

    }
    public void setDetails()
    {
        List<Elpfa> elpfa = db1.getUserDetails();
        for (Elpfa elp : elpfa) {
            name = elp.getUserName();
            email=elp.getUserEmail();
            phone=elp.getUserMobileNo();
            company_name=elp.getUserCompanyName();
            Log.d("user_details","phone : "+phone);
            Log.d("user_details","company name : "+company_name);
            Log.d("user_details","mobile number : "+phone);
            name_txt.setText(""+name);
            email_txt.setText(""+email);
            if(null==phone) {
                phone_txt.setText("");
            }
            else
            {
                phone_txt.setText(""+phone);
            }
            if(null==company_name) {
                company_txt.setText("");
            }
            else
            {
                company_txt.setText(""+company_name);
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        String first_txt=sharedpreferences.getString("person_name",null).substring(0,1);
        first_letter.setText(""+first_txt.toUpperCase());
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {

            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {

            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(img);
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        String path=sharedpreferences.getString("photo","no_img");
        if (path.equals("no_img"))
        {
            profile_image_color=sharedpreferences.getString("profile_image_color","grey");
            if (profile_image_color.equals("purple"))
            {
                img.setColorFilter(getResources().getColor(R.color.purple));
            }
            else if (profile_image_color.equals("pink"))
            {
                img.setColorFilter(getResources().getColor(R.color.pink));
            }
            else if (profile_image_color.equals("lime"))
            {
                img.setColorFilter(getResources().getColor(R.color.lime));
            }
            else if (profile_image_color.equals("orange"))
            {
                img.setColorFilter(getResources().getColor(R.color.orange));
            }
            else {
                img.setColorFilter(getResources().getColor(R.color.grey));
            }
            profile_color_text.setVisibility(View.VISIBLE);
        }
        else
        {
            profile_color_text.setVisibility(View.INVISIBLE);
            img.setColorFilter(getResources().getColor(R.color.transparent));
            Picasso.with(getApplicationContext()).load(sharedpreferences.getString("photo","no_img")).into(img);
        }
    }

}
