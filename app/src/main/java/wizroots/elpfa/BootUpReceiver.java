package wizroots.elpfa;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import wizroots.elpfa.service.MyFirebaseInstanceIDService;

/**
 * Created by reflective2 on 11-11-2016.
 */

public class BootUpReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pi = PendingIntent.getService(context, 0, new Intent(context, MyFirebaseInstanceIDService.class), PendingIntent.FLAG_UPDATE_CURRENT);
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 2, 2, pi);
        }
}
